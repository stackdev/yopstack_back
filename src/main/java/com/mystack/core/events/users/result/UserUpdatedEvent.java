package com.mystack.core.events.users.result;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.UpdatedEvent;

public class UserUpdatedEvent extends UpdatedEvent {

    private UserDetails details;

    public UserUpdatedEvent(UserDetails details) {
        this.details = details;
    }

    public UserDetails getDetails() {
        return this.details;
    }

    static public UserUpdatedEvent notUpdated(UserDetails user) {
        UserUpdatedEvent event = new UserUpdatedEvent(user);
        event.setNotUpdated();
        return event;
    }

    static public UserUpdatedEvent notFound(UserDetails user) {
        UserUpdatedEvent event = notUpdated(user);
        event.setNotFound();
        return event;
    }
}
