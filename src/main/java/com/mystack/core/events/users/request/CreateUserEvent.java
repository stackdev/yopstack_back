package com.mystack.core.events.users.request;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.CreateEvent;

public class CreateUserEvent extends CreateEvent {
    private UserDetails details;

    public CreateUserEvent(UserDetails details) {
        this.details = details;
    }

    public UserDetails getDetails() {
        return this.details;
    }

    public String toString() {
        return this.details.toString();
    }
}
