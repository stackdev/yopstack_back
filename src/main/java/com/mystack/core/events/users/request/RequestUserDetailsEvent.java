package com.mystack.core.events.users.request;

import java.util.UUID;

import com.mystack.core.events.ReadEvent;

public class RequestUserDetailsEvent extends ReadEvent {
    private UUID   id;
    private String searched_name;

    public RequestUserDetailsEvent(UUID id) {
        this.id = id;
    }

    // moved to requestUserEvent
    public RequestUserDetailsEvent(String name) {
        this.searched_name = name;
    }

    public String getSearchedName() {
        return this.searched_name;
    }

    public UUID getId() {
        return this.id;
    }
}
