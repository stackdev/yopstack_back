package com.mystack.core.events.users.result;

import java.util.UUID;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.DeletedEvent;

public class UserDeletedEvent extends DeletedEvent {

    private UserDetails details;

    public UserDeletedEvent(UserDetails details) {
        this.details = details;
    }

    public UserDetails getDetails() {
        return this.details;
    }

    public static UserDeletedEvent notFound(UUID id) {
        return notFound(new UserDetails(id));
    }

    public static UserDeletedEvent notFound(UserDetails details) {
        UserDeletedEvent event = new UserDeletedEvent(details);
        event.entityFound = false;
        event.deletionCompleted = false;
        return event;
    }

    public static UserDeletedEvent notAuthorized(UserDetails details) {
        UserDeletedEvent event = new UserDeletedEvent(details);
        event.entityFound = true;
        event.deletionCompleted = false;
        return event;
    }
}
