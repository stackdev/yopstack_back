package com.mystack.core.events.users.request;

import java.util.List;
import java.util.UUID;

import com.mystack.core.events.ReadEvent;

public class RequestAllUsersEvent extends ReadEvent {

    private List<UUID> requested_user;
    private String     searched_name;
    private boolean    exact_match = true;

    //TODO: remove this constructor
    public RequestAllUsersEvent() {
    }

    public RequestAllUsersEvent(List<UUID> request_user) {
        this.requested_user = request_user;
    }

    /**
     * Used when user are requested by name
     * 
     * @param name
     */
    @Deprecated
    // moved to requestUserEvent
    public RequestAllUsersEvent(String name) {
        this.searched_name = name;
    }

    public RequestAllUsersEvent(String name, boolean exact_match) {
        this.searched_name = name;
        this.exact_match = exact_match;
    }

    public String getSearchedName() {
        return this.searched_name;
    }

    public boolean getExactmatch() {
        return this.exact_match;
    }

}
