package com.mystack.core.events.users.request;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.UpdateEvent;

public class UpdateUserEvent extends UpdateEvent {

    private UserDetails user;

    public UpdateUserEvent(UserDetails user, String authenticated_user) {
        this.user = user;
        this.authenticated_user = authenticated_user;
    }

    public UserDetails getUserDetails() {
        return user;
    }
}
