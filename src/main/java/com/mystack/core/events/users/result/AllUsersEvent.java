package com.mystack.core.events.users.result;

import java.util.ArrayList;
import java.util.List;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.ReadEvent;

public class AllUsersEvent extends ReadEvent {
    private List<UserDetails> listDetails;

    public AllUsersEvent(List<UserDetails> list) {
        this.listDetails = list;
    }

    public List<UserDetails> getAllUserDetails() {
        return this.listDetails;
    }

    public static AllUsersEvent NotFound() {
        AllUsersEvent event = new AllUsersEvent(new ArrayList<UserDetails>());
        event.entityFound = false;
        return event;
    }

}
