package com.mystack.core.events.users.result;

import java.util.List;
import java.util.UUID;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.ReadEvent;

public class UserDetailsEvent extends ReadEvent {
    private UserDetails details;

    public UserDetailsEvent(UUID id, String name, List<UUID> friend_list, List<UUID> stack_list) {
        this.details = new UserDetails(id, name, friend_list, stack_list);
    }

    public UserDetailsEvent(UserDetails details) {
        this.details = details;
    }

    public UserDetails getDetails() {
        return this.details;
    }

    public static UserDetailsEvent notFound(UUID id) {

        UserDetailsEvent event = new UserDetailsEvent(id, null, null, null);
        event.entityFound = false;
        return event;
    }
}
