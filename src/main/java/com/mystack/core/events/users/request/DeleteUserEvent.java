package com.mystack.core.events.users.request;

import java.util.UUID;

import com.mystack.core.events.DeleteEvent;

public class DeleteUserEvent extends DeleteEvent {

    private UUID id;

    public DeleteUserEvent(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

}
