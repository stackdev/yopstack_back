package com.mystack.core.events.users.result;

import java.util.UUID;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.CreatedEvent;

/**
 * This class is named User created event, but it is also used when the User fails to be created
 * 
 * @author dzd
 */
public class UserCreatedEvent extends CreatedEvent {
  private UserDetails details;
  private UUID newId;

  public UserCreatedEvent(UUID newId, UserDetails details) {
	this.details = details;
	this.newId = newId;
  }

  public UserDetails getDetails() {
	return this.details;
  }

  public UUID getId() {
	return this.newId;
  }

  /**
   * Static method to be called when creation failed
   * 
   * @param details
   * @return
   */
  public static UserCreatedEvent notAuthorized(UserDetails details) {
	UserCreatedEvent event = new UserCreatedEvent(null, details);
	event.setCreated(false);
	return event;
  }

  public static UserCreatedEvent creationFailed(UserDetails details) {
	UserCreatedEvent event = new UserCreatedEvent(null, details);
	event.setCreated(false);
	return event;
  }
}
