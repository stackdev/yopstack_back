package com.mystack.core.events.preferences.result;

import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.ReadEvent;

public class PreferenceReadEvent extends ReadEvent {
    private PreferenceDetails details;

    public PreferenceReadEvent(PreferenceDetails details) {
        this.details = details;
    }

    public PreferenceDetails getDetails() {
        return details;
    }

    public void setDetails(PreferenceDetails details) {
        this.details = details;
    }

    public static PreferenceReadEvent NotFound() {
        PreferenceReadEvent event = new PreferenceReadEvent(new PreferenceDetails());
        event.entityFound = false;
        return event;
    }

    public static PreferenceReadEvent notAuthorized(PreferenceDetails details) {
        PreferenceReadEvent event = new PreferenceReadEvent(details);
        event.setEntityFound(false);
        return event;
    }

}
