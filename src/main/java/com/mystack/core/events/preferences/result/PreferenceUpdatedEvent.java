package com.mystack.core.events.preferences.result;

import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.UpdatedEvent;

public class PreferenceUpdatedEvent extends UpdatedEvent {
    private PreferenceDetails details;

    public PreferenceUpdatedEvent(PreferenceDetails details) {
        this.details = details;
    }

    public PreferenceDetails getDetails() {
        return details;
    }

    public void setDetails(PreferenceDetails details) {
        this.details = details;
    }

    static public PreferenceUpdatedEvent notUpdated(PreferenceDetails details) {
        PreferenceUpdatedEvent event = new PreferenceUpdatedEvent(details);
        event.setNotUpdated();
        return event;
    }

    static public PreferenceUpdatedEvent notFound(PreferenceDetails details) {
        PreferenceUpdatedEvent event = notUpdated(details);
        event.setNotFound();
        return event;
    }

}
