package com.mystack.core.events.preferences.request;

import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.CreateEvent;

public class CreatePreferenceEvent extends CreateEvent {
    private PreferenceDetails details;

    public CreatePreferenceEvent(PreferenceDetails details, String authenticated_user) {
        this.details = details;

        this.authenticated_user = authenticated_user;
    }

    public PreferenceDetails getDetails() {
        return details;
    }

    public void setDetails(PreferenceDetails details) {
        this.details = details;
    }

}
