package com.mystack.core.events.preferences.request;

import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.ReadEvent;

public class ReadPreferenceEvent extends ReadEvent {
    private PreferenceDetails details;

    public ReadPreferenceEvent(PreferenceDetails details, String authenticated_user) {
        this.details = details;
        this.authenticated_user = authenticated_user;
    }

    public PreferenceDetails getDetails() {
        return details;
    }

    public void setDetails(PreferenceDetails details) {
        this.details = details;
    }

}
