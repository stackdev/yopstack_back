package com.mystack.core.events.preferences.result;

import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.DeletedEvent;

public class PreferenceDeletedEvent extends DeletedEvent {
    private PreferenceDetails details;

    public PreferenceDeletedEvent(PreferenceDetails details) {
        this.details = details;
    }

    public PreferenceDetails getDetails() {
        return details;
    }

    public void setDetails(PreferenceDetails details) {
        this.details = details;
    }

    public static PreferenceDeletedEvent notFound(PreferenceDetails details) {
        PreferenceDeletedEvent event = new PreferenceDeletedEvent(details);
        event.entityFound = false;
        event.deletionCompleted = false;
        return event;
    }

    public static PreferenceDeletedEvent notAuthorized(PreferenceDetails details) {
        PreferenceDeletedEvent event = new PreferenceDeletedEvent(details);
        event.entityFound = true;
        event.deletionCompleted = false;
        return event;
    }

}
