package com.mystack.core.events.preferences.result;

import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.CreatedEvent;

public class PreferenceCreatedEvent extends CreatedEvent {
    private PreferenceDetails details;

    public PreferenceCreatedEvent(PreferenceDetails details) {
        this.details = details;
    }

    public PreferenceDetails getDetails() {
        return details;
    }

    public void setDetails(PreferenceDetails details) {
        this.details = details;
    }

    public static PreferenceCreatedEvent notAuthorized(PreferenceDetails details) {
        PreferenceCreatedEvent event = new PreferenceCreatedEvent(details);
        event.setFailedNotAuthorized();
        return event;
    }

    public static PreferenceCreatedEvent creationFailed(PreferenceDetails details) {
        PreferenceCreatedEvent event = new PreferenceCreatedEvent(details);
        event.setCreated(false);
        return event;
    }

}
