package com.mystack.core.events.stacks.request;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.UpdateEvent;

public class UpdateStackEvent extends UpdateEvent {

    private StackDetails stack;

    @Deprecated
    public UpdateStackEvent(StackDetails stack) {
        this.stack = stack;
    }

    public UpdateStackEvent(StackDetails stack, String authenticated_user) {
        this.stack = stack;
        this.authenticated_user = authenticated_user;
    }

    public StackDetails getStackDetails() {
        return stack;
    }
}
