package com.mystack.core.events.stacks.result;

import java.util.UUID;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.ReadEvent;

public class StackDetailsEvent extends ReadEvent {
    private StackDetails details;

    public StackDetailsEvent(UUID stackid) {
        this.details = new StackDetails(stackid);
    }

    public StackDetailsEvent(StackDetails details) {
        this.details = details;
    }

    public StackDetails getDetails() {
        return this.details;
    }

    public static StackDetailsEvent notFound(UUID stackid) {

        StackDetailsEvent event = new StackDetailsEvent(stackid);
        event.entityFound = false;
        return event;
    }
}
