package com.mystack.core.events.stacks.result;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.ReadEvent;

/**
 * AllStackEvent
 * is used by both Stack service (core and persistence)
 * 
 * @author dzd
 * 
 */
public class AllStacksEvent extends ReadEvent {

    private boolean                       hasUserContent = false;

    private List<StackDetails>            stackDetailsList;
    private Map<UUID, List<StackDetails>> userStackDetailsList;

    public AllStacksEvent(List<StackDetails> stackDetailsList) {
        this.stackDetailsList = stackDetailsList;
        hasUserContent = false;
    }

    public AllStacksEvent(Map<UUID, List<StackDetails>> user_stack_list) {
        this.userStackDetailsList = user_stack_list;
        hasUserContent = true;
    }

    public List<StackDetails> getAllStackDetails() {
        return this.stackDetailsList;
    }

    public Map<UUID, List<StackDetails>> getUserStackDetailsList() {
        return this.userStackDetailsList;
    }

    public boolean hasUserContent() {
        return hasUserContent;
    }

    @Override
    public String toString() {
        return "AllStacksEvent [hasUserContent=" + hasUserContent + ", stackDetailsList=" + stackDetailsList + ", userStackDetailsList=" + userStackDetailsList + "]";
    }

}
