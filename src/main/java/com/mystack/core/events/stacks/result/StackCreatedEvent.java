package com.mystack.core.events.stacks.result;

import java.util.UUID;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.CreatedEvent;

/**
 * This class is named Stack created event, but it is also used when the Stack fails to be created
 * 
 * @author dzd
 * 
 */
public class StackCreatedEvent extends CreatedEvent {
    private StackDetails details;
    private UUID         newId;

    public StackCreatedEvent(UUID newId, StackDetails details) {
        this.details = details;
        this.newId = newId;
    }

    public StackDetails getDetails() {
        return this.details;
    }

    public UUID getId() {
        return this.newId;
    }

    /**
     * Static method to be called when creation failed
     * 
     * @param details
     * @return
     */
    public static StackCreatedEvent notAuthorized(StackDetails details) {
        StackCreatedEvent event = new StackCreatedEvent(null, details);
        event.setCreated(false);
        return event;
    }

    public static StackCreatedEvent creationFailed(StackDetails details) {
        StackCreatedEvent event = new StackCreatedEvent(null, details);
        event.setCreated(false);
        return event;
    }
}
