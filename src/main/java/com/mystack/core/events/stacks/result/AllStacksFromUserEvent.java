package com.mystack.core.events.stacks.result;

import java.util.List;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.ReadEvent;

/**
 * AllStackEvent
 * is used by both Stack service (core and persistence)
 * 
 * @author dzd
 * 
 */
public class AllStacksFromUserEvent extends ReadEvent {

    private List<StackDetails> stackDetailsList;

    public AllStacksFromUserEvent(List<StackDetails> stackDetailsList) {
        super();
        this.stackDetailsList = stackDetailsList;
    }

    public List<StackDetails> getStackDetailsList() {
        return stackDetailsList;
    }

    public static AllStacksFromUserEvent NotFound(UserDetails details) {
        AllStacksFromUserEvent event = new AllStacksFromUserEvent(null);
        event.entityFound = false;
        return event;
    }
}
