package com.mystack.core.events.stacks.request;

import java.util.List;
import java.util.UUID;

import com.mystack.core.events.ReadEvent;

/**
 * 
 * @author dzd
 * 
 */
public class RequestAllStacksByIdEvent extends ReadEvent {

    private List<UUID> stackid_list;
    private boolean    only_public;

    @Deprecated
    public RequestAllStacksByIdEvent(List<UUID> stackid_list) {
        super();
        this.stackid_list = stackid_list;
    }

    public RequestAllStacksByIdEvent(List<UUID> stackid_list, String authenticated_user) {
        super();
        this.stackid_list = stackid_list;
        this.authenticated_user = authenticated_user;
    }

    public List<UUID> getStackid_list() {
        return stackid_list;
    }

    public boolean isOnly_public() {
        return only_public;
    }

    public void setOnly_public(boolean only_public) {
        this.only_public = only_public;
    }

    public void setStackid_list(List<UUID> stackid_list) {
        this.stackid_list = stackid_list;
    }

}
