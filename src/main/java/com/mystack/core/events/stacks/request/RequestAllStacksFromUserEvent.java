package com.mystack.core.events.stacks.request;

import java.util.UUID;

import com.mystack.core.events.ReadEvent;

/**
 * Request the all stack from one User
 * Used to call user service
 * 
 * @author dzd
 * 
 */
public class RequestAllStacksFromUserEvent extends ReadEvent {

    private UUID requested_user;

    @Deprecated
    public RequestAllStacksFromUserEvent(UUID requested_user) {
        super();
        this.requested_user = requested_user;
    }

    public RequestAllStacksFromUserEvent(UUID requested_user, String authenticated_user) {
        super();
        this.requested_user = requested_user;
        this.authenticated_user = authenticated_user;
    }

    public UUID getRequested_user() {
        return requested_user;
    }

}
