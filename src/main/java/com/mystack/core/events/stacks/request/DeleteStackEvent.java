package com.mystack.core.events.stacks.request;

import java.util.UUID;

import com.mystack.core.events.DeleteEvent;

public class DeleteStackEvent extends DeleteEvent {
    private UUID   requestingUser;
    private UUID   stackId;
    private String authenticated_user;

    @Deprecated
    public DeleteStackEvent(UUID stackId, UUID requestingUser) {
        this.stackId = stackId;
        this.requestingUser = requestingUser;
    }

    public DeleteStackEvent(UUID stackId, UUID requestingUser, String authenticated_user) {
        this.authenticated_user = authenticated_user;
        this.stackId = stackId;
        this.requestingUser = requestingUser;
    }

    public UUID getId() {
        return stackId;
    }

    public void setRequestingUser(UUID user) {
        this.requestingUser = user;
    }

    public UUID getRequestingUser() {
        return this.requestingUser;
    }

    public String getAuthenticated_user() {
        return authenticated_user;
    }

}
