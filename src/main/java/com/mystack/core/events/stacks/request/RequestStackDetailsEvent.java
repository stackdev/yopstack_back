package com.mystack.core.events.stacks.request;

import java.util.UUID;

import com.mystack.core.events.ReadEvent;

public class RequestStackDetailsEvent extends ReadEvent {
    private UUID id;

    @Deprecated
    public RequestStackDetailsEvent(UUID id) {
        this.id = id;
    }

    public RequestStackDetailsEvent(UUID id, String authenticated_user) {
        this.id = id;
        this.authenticated_user = authenticated_user;
    }

    public UUID getId() {
        return this.id;
    }
}
