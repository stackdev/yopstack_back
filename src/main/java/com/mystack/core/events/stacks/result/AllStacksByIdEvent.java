package com.mystack.core.events.stacks.result;

import java.util.List;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.ReadEvent;

/**
 * AllStackEvent
 * is used by both Stack service (core and persistence)
 * 
 * @author dzd
 * 
 */
//TODO: check if this is not only code duplication with AllStacksFromUserEvent class
public class AllStacksByIdEvent extends ReadEvent {

    private List<StackDetails> stackDetailsList;

    public AllStacksByIdEvent(List<StackDetails> stackDetailsList) {
        super();
        this.stackDetailsList = stackDetailsList;
    }

    public List<StackDetails> getStackDetailsList() {
        return stackDetailsList;
    }

    public static AllStacksByIdEvent NotFound(UserDetails details) {
        AllStacksByIdEvent event = new AllStacksByIdEvent(null);
        event.entityFound = false;
        return event;
    }
}
