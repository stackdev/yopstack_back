package com.mystack.core.events.stacks.request;

import java.util.UUID;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.CreatedEvent;

public class CreateStackEvent extends CreatedEvent {
    private StackDetails details;
    private UUID         requesting_user;
    private String       authenticated_user;

    public CreateStackEvent(StackDetails details, UUID requesting_user, String authenticated_user) {
        init(details, requesting_user, authenticated_user);
    }

    public CreateStackEvent(StackDetails details, UUID requesting_user) {
        init(details, requesting_user, "");
    }

    public void init(StackDetails details, UUID requesting_user, String authenticated_user) {
        this.details = details;
        this.requesting_user = requesting_user;
        this.authenticated_user = authenticated_user;
    }

    public StackDetails getDetails() {
        return this.details;
    }

    public UUID getRequestingUser() {
        return requesting_user;
    }

    public String getAuthenticated_user() {
        return authenticated_user;
    }

}
