package com.mystack.core.events.stacks.result;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.UpdatedEvent;

public class StackUpdatedEvent extends UpdatedEvent {

    private StackDetails details;

    public StackUpdatedEvent(StackDetails details) {
        this.details = details;
    }

    public StackDetails getDetails() {
        return this.details;
    }

    static public StackUpdatedEvent notUpdated(StackDetails stack) {
        StackUpdatedEvent event = new StackUpdatedEvent(stack);
        event.setNotUpdated();
        return event;
    }

    static public StackUpdatedEvent notFound(StackDetails stack) {
        StackUpdatedEvent event = notUpdated(stack);
        event.setNotFound();
        return event;
    }
}
