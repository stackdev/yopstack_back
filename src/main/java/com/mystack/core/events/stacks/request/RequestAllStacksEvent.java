package com.mystack.core.events.stacks.request;

import java.util.List;
import java.util.UUID;

import com.mystack.core.events.ReadEvent;

/**
 * Generic event to request a list of Stack to the core stack service layer and also to the stack persistence service
 * - for stack service layer: a user id list is provided
 * - for stack persistence service layer: a stack id list is provided
 * 
 * @author dzd
 * 
 */
public class RequestAllStacksEvent extends ReadEvent {

    private boolean    useridProvided  = false;
    private boolean    stackidProvided = false;
    private List<UUID> userid_list;
    private List<UUID> stackid_list;
    private boolean    only_public     = true;

    //TODO: is this still relevant ? empty constructor ?
    public RequestAllStacksEvent() {
    }

    @Deprecated
    public RequestAllStacksEvent(List<UUID> uuidlist, boolean isStackId, boolean isUserId) {
        if (isStackId) {
            this.stackid_list = uuidlist;
            stackidProvided = true;
        } else if (isUserId) {
            this.userid_list = uuidlist;
            useridProvided = true;
        }
    }

    /**
     * 
     * @param uuidlist
     *            used for both stackid list and userid list
     * @param isStackId
     *            indicates that we are dealing with stack id
     * @param isUserId
     *            indicates that we are dealing with user id
     */
    public RequestAllStacksEvent(List<UUID> uuidlist, boolean isStackId, boolean isUserId, String authenticated_user) {
        this.authenticated_user = authenticated_user;
        if (isStackId) {
            this.stackid_list = uuidlist;
            stackidProvided = true;
        } else if (isUserId) {
            this.userid_list = uuidlist;
            useridProvided = true;
        }
    }

    public boolean isUseridProvided() {
        return useridProvided;
    }

    public boolean isListidProvided() {
        return stackidProvided;
    }

    public List<UUID> getUserid_list() {
        return userid_list;
    }

    /**
     * Set the list of user id requested
     * Set also a boolean to inform that this list was provided
     * 
     * @param userid_list
     */
    public void setUserid_list(List<UUID> userid_list) {
        this.userid_list = userid_list;
        useridProvided = true;
    }

    public List<UUID> getStackid_list() {
        return stackid_list;
    }

    /**
     * Set the list of requested stack
     * Set also a boolean to show that the stack id were provided
     * 
     * @param stackid_list
     */
    public void setStackid_list(List<UUID> stackid_list) {
        this.stackid_list = stackid_list;
        stackidProvided = true;
    }

    public boolean isStackidProvided() {
        return stackidProvided;
    }

    public void setStackidProvided(boolean stackidProvided) {
        this.stackidProvided = stackidProvided;
    }

    public boolean isOnly_public() {
        return only_public;
    }

    public void setOnly_public(boolean only_public) {
        this.only_public = only_public;
    }

    public void setUseridProvided(boolean useridProvided) {
        this.useridProvided = useridProvided;
    }

    @Override
    public String toString() {
        return "RequestAllStacksEvent [useridProvided=" + useridProvided + ", stackidProvided=" + stackidProvided + ", userid_list=" + userid_list + ", stackid_list=" + stackid_list
                + ", only_public=" + only_public + "]";
    }
}
