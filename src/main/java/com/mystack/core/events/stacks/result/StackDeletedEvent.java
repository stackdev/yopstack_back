package com.mystack.core.events.stacks.result;

import java.util.UUID;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.DeletedEvent;

public class StackDeletedEvent extends DeletedEvent {

    private StackDetails details;

    public StackDeletedEvent(StackDetails details) {
        this.details = details;
    }

    public StackDetails getDetails() {
        return this.details;
    }

    public UUID getId() {
        return details.getId();
    }

    public static StackDeletedEvent deletionFailed(StackDetails details) {
        StackDeletedEvent event = new StackDeletedEvent(details);
        event.entityFound = true;
        event.deletionCompleted = false;
        return event;
    }

    public static StackDeletedEvent notFound(StackDetails details) {
        StackDeletedEvent event = new StackDeletedEvent(details);
        event.entityFound = false;
        event.deletionCompleted = false;
        return event;
    }

    public static StackDeletedEvent notAuthorized(StackDetails details) {
        StackDeletedEvent event = new StackDeletedEvent(details);
        event.entityFound = true;
        event.deletionCompleted = false;
        return event;
    }
}
