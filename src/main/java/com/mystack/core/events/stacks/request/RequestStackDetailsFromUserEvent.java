package com.mystack.core.events.stacks.request;

import java.util.UUID;

import com.mystack.core.events.ReadEvent;

public class RequestStackDetailsFromUserEvent extends ReadEvent {

    private UUID stack_id;
    private UUID user_id;

    @Deprecated
    public RequestStackDetailsFromUserEvent(UUID user_id, UUID stack_id) {
        this.user_id = user_id;
        this.stack_id = stack_id;
    }

    public RequestStackDetailsFromUserEvent(UUID user_id, UUID stack_id, String authenticated_user) {
        this.user_id = user_id;
        this.stack_id = stack_id;
        this.authenticated_user = authenticated_user;
    }

    public UUID getStackId() {
        return this.stack_id;
    }

    public UUID getUserId() {
        return this.user_id;
    }
}
