package com.mystack.core.events;

public class CreatedEvent {
    public static int CODE_OK                             = 0;
    public static int CODE_CREATION_FAILED_ALREADY_EXISTS = 1;
    public static int CODE_CREATION_FAILED_INTERNAL_ERROR = 2;
    public static int CODE_CREATION_FAILED_NOT_AUTHORIZED = 3;

    private boolean   created                             = true;
    private int       creation_code                       = CODE_OK;

    public void setFailedAlreadyExists() {
        creation_code = CODE_CREATION_FAILED_ALREADY_EXISTS;
        created = false;
    }

    public void setFailedInternalError() {
        creation_code = CODE_CREATION_FAILED_INTERNAL_ERROR;
        created = false;
    }

    public void setFailedNotAuthorized() {
        creation_code = CODE_CREATION_FAILED_NOT_AUTHORIZED;
        created = false;
    }

    public int getCreationCode() {
        return creation_code;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public boolean isCreated() {
        return this.created;
    }

}
