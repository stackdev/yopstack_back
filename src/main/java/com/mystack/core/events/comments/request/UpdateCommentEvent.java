package com.mystack.core.events.comments.request;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.UpdateEvent;

public class UpdateCommentEvent extends UpdateEvent {

    private CommentDetails check;

    public UpdateCommentEvent(CommentDetails check, String authenticated_user) {
        this.check = check;
        this.authenticated_user = authenticated_user;
    }

    public CommentDetails getCommentDetails() {
        return check;
    }
}
