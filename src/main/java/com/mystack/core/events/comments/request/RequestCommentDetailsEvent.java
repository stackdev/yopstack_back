package com.mystack.core.events.comments.request;

import java.util.UUID;

import com.mystack.core.events.ReadEvent;

public class RequestCommentDetailsEvent extends ReadEvent {
    private UUID userid;
    private UUID stackid;
    private UUID checkid;
    private UUID commentid;

    public RequestCommentDetailsEvent(UUID userid, UUID stackid, UUID checkid, UUID commentid) {
        this.userid = userid;
        this.stackid = stackid;
        this.checkid = checkid;
        this.commentid = commentid;
    }

    public UUID getCommentid() {
        return this.commentid;
    }
}
