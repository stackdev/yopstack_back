package com.mystack.core.events.comments.result;

import java.util.UUID;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.DeletedEvent;

public class CommentDeletedEvent extends DeletedEvent {

    private CommentDetails details;

    public CommentDeletedEvent(CommentDetails details) {
        this.details = details;
    }

    public CommentDetails getDetails() {
        return this.details;
    }

    public static CommentDeletedEvent notFound(UUID id) {
        return notFound(new CommentDetails(id, null, null, null, null, null, null, null));
    }

    public static CommentDeletedEvent notFound(CommentDetails details) {
        CommentDeletedEvent event = new CommentDeletedEvent(details);
        event.entityFound = false;
        event.deletionCompleted = false;
        return event;
    }

    public static CommentDeletedEvent notAuthorized(CommentDetails details) {
        CommentDeletedEvent event = new CommentDeletedEvent(details);
        event.entityFound = true;
        event.deletionCompleted = false;
        return event;
    }
}
