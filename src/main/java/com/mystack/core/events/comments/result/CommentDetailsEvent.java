package com.mystack.core.events.comments.result;

import java.util.Date;
import java.util.UUID;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.ReadEvent;

public class CommentDetailsEvent extends ReadEvent {
    private CommentDetails details;

    public CommentDetailsEvent(UUID commentid, UUID stack_userid, UUID stackid, UUID checkid, UUID comment_userid, String text, Date date, Date update_date) {
        this.details = new CommentDetails(commentid, stack_userid, stackid, checkid, comment_userid, text, date, update_date);
    }

    public CommentDetailsEvent(CommentDetails details) {
        this.details = details;
    }

    public CommentDetails getDetails() {
        return this.details;
    }

    public static CommentDetailsEvent notFound(UUID id) {

        CommentDetailsEvent event = new CommentDetailsEvent(id, null, null, null, null, null, null, null);
        event.entityFound = false;
        return event;
    }
}
