package com.mystack.core.events.comments.result;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.UpdatedEvent;

public class CommentUpdatedEvent extends UpdatedEvent {

    private CommentDetails details;

    public CommentUpdatedEvent(CommentDetails details) {
        this.details = details;
    }

    public CommentDetails getDetails() {
        return this.details;
    }

    static public CommentUpdatedEvent notUpdated(CommentDetails comment) {
        CommentUpdatedEvent event = new CommentUpdatedEvent(comment);
        event.setNotUpdated();
        return event;
    }

    static public CommentUpdatedEvent notFound(CommentDetails comment) {
        CommentUpdatedEvent event = notUpdated(comment);
        event.setNotFound();
        return event;
    }
}
