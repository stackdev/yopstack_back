package com.mystack.core.events.comments.request;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.config.FunctionalConfiguration;
import com.mystack.core.events.ReadEvent;

public class RequestAllCommentsEvent extends ReadEvent {
    private static Logger LOG   = LoggerFactory.getLogger(RequestAllCommentsEvent.class);

    private UUID          stackid;
    private UUID          userid;
    private List<UUID>    checkids;

    private Date          before_date;
    private Date          after_date;
    private int           count = FunctionalConfiguration.COMMENT_MAX_GET_COUNT;

    public RequestAllCommentsEvent(UUID stackid, UUID userid, UUID checkid, String before_date, int count) {
        init(stackid, userid, Arrays.asList(checkid), before_date, "", count);
    }

    public RequestAllCommentsEvent(UUID stackid, UUID userid, UUID checkid, String before_date, String after_date) {
        init(stackid, userid, Arrays.asList(checkid), before_date, after_date, 0);
    }
    
    public RequestAllCommentsEvent(UUID stackid, UUID userid, List<UUID> checkids, String before_date, int count) {
        init(stackid, userid, checkids, before_date, "", count);
    }

    private void init(UUID stack_id, UUID userid, List<UUID> checkids, String before_date, String after_date, int count) {

        LOG.info(this.toString());
        this.stackid = stack_id;
        this.userid = userid;
        this.checkids = checkids;
        this.count = count;

        try {
            Long b = Long.valueOf(before_date);
            this.before_date = new Date(b);
        } catch (NumberFormatException e) {
            this.before_date = null;
        }

        try {
            Long a = Long.valueOf(after_date);
            this.after_date = new Date(a);
        } catch (NumberFormatException e) {
            this.after_date = null;
        }
        LOG.info("Value setup: " + this.toString());
    }

    // GETTER/SETTER
    public UUID getStack_id() {
        return stackid;
    }

    public void setStack_id(UUID stack_id) {
        this.stackid = stack_id;
    }

    public Date getBefore_date() {
        return before_date;
    }

    public void setBefore_date(Date before_date) {
        this.before_date = before_date;
    }

    public Date getAfter_date() {
        return after_date;
    }

    public void setAfter_date(Date after_date) {
        this.after_date = after_date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public UUID getStackid() {
        return stackid;
    }

    public void setStackid(UUID stackid) {
        this.stackid = stackid;
    }

    public UUID getUserid() {
        return userid;
    }

    public void setUserid(UUID userid) {
        this.userid = userid;
    }

    public List<UUID> getCheckids() {
        return checkids;
    }

    public void setCheckids(List<UUID> checkids) {
        this.checkids = checkids;
    }

    public UUID getCheckid() {
    	if(!checkids.isEmpty()) {
    		return checkids.get(0);
    	}
        return null;
    }

    public void setCheckid(UUID checkid) {
        this.checkids.add(checkid);
    }

    
    
    @Override
    public String toString() {
        return "RequestAllCommentsEvent [stackid=" + stackid + ", userid=" + userid + ", checkids=" + checkids +
        	   ", before_date=" + before_date + ", after_date=" + after_date + ", count=" + count + "]";
    }

}
