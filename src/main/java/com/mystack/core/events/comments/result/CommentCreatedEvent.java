package com.mystack.core.events.comments.result;

import java.util.UUID;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.CreatedEvent;

/**
 * This class is named Stack created event, but it is also used when the Stack fails to be created
 * 
 * @author dzd
 */
public class CommentCreatedEvent extends CreatedEvent {
    private CommentDetails details;
    private UUID           newId;

    public CommentCreatedEvent(CommentDetails details) {
        this.details = details;
        this.newId = details.getId();
    }

    public CommentCreatedEvent(UUID newId, CommentDetails details) {
        this.details = details;
        this.newId = newId;
    }

    public CommentDetails getDetails() {
        return this.details;
    }

    public UUID getId() {
        return this.newId;
    }

    /**
     * Static method to be called when creation failed
     * 
     * @param details
     * @return
     */
    public static CommentCreatedEvent notAuthorized(CommentDetails details) {
        CommentCreatedEvent event = new CommentCreatedEvent(null, details);
        event.setFailedNotAuthorized();
        return event;
    }

    public static CommentCreatedEvent creationFailed(CommentDetails details) {
        CommentCreatedEvent event = new CommentCreatedEvent(null, details);
        event.setCreated(false);
        return event;
    }
}
