package com.mystack.core.events.comments.result;

import java.util.ArrayList;
import java.util.List;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.ReadEvent;

public class AllCommentsEvent extends ReadEvent {
    private List<CommentDetails> listDetails;

    public AllCommentsEvent(List<CommentDetails> list) {
        this.listDetails = list;
    }

    public List<CommentDetails> getAllCommentDetails() {
        return this.listDetails;
    }

    public static AllCommentsEvent NotFound() {
        AllCommentsEvent event = new AllCommentsEvent(new ArrayList<CommentDetails>());
        event.entityFound = false;
        return event;
    }
}
