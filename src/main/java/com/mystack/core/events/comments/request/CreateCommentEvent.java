package com.mystack.core.events.comments.request;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.CreateEvent;

public class CreateCommentEvent extends CreateEvent {
    private CommentDetails details;

    public CreateCommentEvent(CommentDetails details, String authenticated_user) {
        this.details = details;
        this.authenticated_user = authenticated_user;
    }

    public CommentDetails getDetails() {
        return this.details;
    }

    public String toString() {
        return this.details.toString();
    }

    public String getAuthenticated_user() {
        return authenticated_user;
    }

}
