package com.mystack.core.events.comments.request;

import java.util.UUID;

import com.mystack.core.events.DeleteEvent;

public class DeleteCommentEvent extends DeleteEvent {

    private UUID id;

    public DeleteCommentEvent(UUID id, String authenticated_user) {
        this.id = id;
        this.authenticated_user = authenticated_user;
    }

    public UUID getId() {
        return id;
    }

}
