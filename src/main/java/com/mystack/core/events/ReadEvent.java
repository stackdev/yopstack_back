package com.mystack.core.events;

import java.util.Optional;

public class ReadEvent extends BaseEvent {
    protected boolean entityFound = true;
    protected long totalCount;

    public void setEntityFound(boolean entityFound) {
        this.entityFound = entityFound;
    }

    public boolean isEntityFound() {
        return entityFound;
    }

	public Optional<Long> getTotalCount() {
		return Optional.ofNullable(totalCount);
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
}
