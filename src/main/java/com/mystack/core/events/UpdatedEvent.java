package com.mystack.core.events;

public class UpdatedEvent {
    private boolean entityFound     = true;
    private boolean updateCompleted = true;

    static public UpdatedEvent notFound() {
        UpdatedEvent event = new UpdatedEvent();
        event.entityFound = false;
        event.updateCompleted = false;
        return event;
    }

    public void setNotUpdated() {
        entityFound = true;
        updateCompleted = false;
    }

    public void setNotFound() {
        entityFound = false;
        updateCompleted = false;
    }

    static public UpdatedEvent NotUpdated() {
        UpdatedEvent event = new UpdatedEvent();
        event.setNotUpdated();
        return event;
    }

    public boolean isUpdated() {
        return entityFound && updateCompleted;
    }

}
