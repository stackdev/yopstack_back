package com.mystack.core.events.mentions.result;
import com.mystack.core.events.UpdatedEvent;
import com.mystack.core.domain.MentionDetails;

public class MentionUpdatedEvent extends UpdatedEvent {
    private MentionDetails details;
    public MentionUpdatedEvent(MentionDetails details) {
        this.details = details;
    }
    
    public MentionDetails getDetails() {
        return details;
    }
    
    public void setDetails(MentionDetails details) {
        this.details = details;
    }
    
    
}
