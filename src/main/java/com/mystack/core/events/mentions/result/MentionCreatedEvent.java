package com.mystack.core.events.mentions.result;

import com.mystack.core.events.CreatedEvent;
import com.mystack.core.domain.MentionDetails;

public class MentionCreatedEvent extends CreatedEvent {
    private MentionDetails details;

    public MentionCreatedEvent(MentionDetails details) {
        this.details = details;
    }

    public MentionDetails getDetails() {
        return details;
    }

    public void setDetails(MentionDetails details) {
        this.details = details;
    }

    public static MentionCreatedEvent creationFailed(MentionDetails details) {
        MentionCreatedEvent event = new MentionCreatedEvent(details);
        event.setCreated(false);
        return event;
    }
}
