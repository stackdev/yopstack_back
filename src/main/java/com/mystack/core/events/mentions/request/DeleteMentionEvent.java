package com.mystack.core.events.mentions.request;
import com.mystack.core.events.DeleteEvent;
import com.mystack.core.domain.MentionDetails;

public class DeleteMentionEvent extends DeleteEvent {
    private MentionDetails details;
    public DeleteMentionEvent(MentionDetails details) {
        this.details = details;
    }
    
    public MentionDetails getDetails() {
        return details;
    }
    
    public void setDetails(MentionDetails details) {
        this.details = details;
    }
    
    
}
