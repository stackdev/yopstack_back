package com.mystack.core.events.mentions.result;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.domain.MentionDetails;
import com.mystack.core.events.ReadEvent;

import java.util.ArrayList;
import java.util.List;

public class AllMentionsEvent extends ReadEvent {
  private List<MentionDetails> listDetails;

  public AllMentionsEvent(List<MentionDetails> list) {
	this.listDetails = list;
  }

  public List<MentionDetails> getAllMentionDetails() {
	return this.listDetails;
  }

  public static AllMentionsEvent NotFound() {
	AllMentionsEvent event = new AllMentionsEvent(new ArrayList<MentionDetails>());
	event.entityFound = false;
	return event;
  }
}
