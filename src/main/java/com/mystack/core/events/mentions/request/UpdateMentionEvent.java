package com.mystack.core.events.mentions.request;
import com.mystack.core.events.UpdateEvent;
import com.mystack.core.domain.MentionDetails;

public class UpdateMentionEvent extends UpdateEvent {
    private MentionDetails details;
    public UpdateMentionEvent(MentionDetails details) {
        this.details = details;
    }
    
    public MentionDetails getDetails() {
        return details;
    }
    
    public void setDetails(MentionDetails details) {
        this.details = details;
    }
    
    
}
