package com.mystack.core.events.mentions.request;

import com.mystack.config.FunctionalConfiguration;
import com.mystack.core.events.ReadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.UUID;

public class RequestAllMentionsEvent extends ReadEvent {
    private static Logger LOG   = LoggerFactory.getLogger(RequestAllMentionsEvent.class);

    private UUID          userId;
    private Date          beforeDate;
    private int           count = FunctionalConfiguration.MENTION_MAX_GET_COUNT;

    public RequestAllMentionsEvent(UUID userId, String beforeDate, int count) {
        init(userId, beforeDate, "", count);
    }

    public RequestAllMentionsEvent(UUID userId, String beforeDate, String afterDate, int count) {
        init(userId, beforeDate, afterDate, count);
    }

    private void init(UUID userId, String beforeDate, String afterDate, int count) {

        LOG.info("Input [userId=" + userId + ", beforeDate=" + beforeDate + ", afterDate=" + afterDate + ", count=" + count + "]");
        this.userId = userId;
        if (count > 0) {
            this.count = count;
        }

        try {
            Long b = Long.valueOf(beforeDate);
            this.beforeDate = new Date(b);
        } catch (NumberFormatException e) {
            this.beforeDate = null;
        }

        LOG.info("Value :" + this);
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Date getBeforeDate() {
        return beforeDate;
    }

    public void setBeforeDate(Date beforeDate) {
        this.beforeDate = beforeDate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "RequestAllMentionsEvent{" +
                "userId=" + userId +
                ", beforeDate=" + beforeDate +
                ", count=" + count +
                '}';
    }
}
