package com.mystack.core.events.mentions.result;
import com.mystack.core.events.ReadEvent;
import com.mystack.core.domain.MentionDetails;

public class MentionReadEvent extends ReadEvent {
    private MentionDetails details;
    public MentionReadEvent(MentionDetails details) {
        this.details = details;
    }
    
    public MentionDetails getDetails() {
        return details;
    }
    
    public void setDetails(MentionDetails details) {
        this.details = details;
    }
    
    
}
