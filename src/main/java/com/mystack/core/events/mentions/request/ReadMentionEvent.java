package com.mystack.core.events.mentions.request;
import com.mystack.core.events.ReadEvent;
import com.mystack.core.domain.MentionDetails;

public class ReadMentionEvent extends ReadEvent {
    private MentionDetails details;
    public ReadMentionEvent(MentionDetails details) {
        this.details = details;
    }
    
    public MentionDetails getDetails() {
        return details;
    }
    
    public void setDetails(MentionDetails details) {
        this.details = details;
    }
    
    
}
