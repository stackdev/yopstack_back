package com.mystack.core.events.mentions.result;
import com.mystack.core.events.DeletedEvent;
import com.mystack.core.domain.MentionDetails;

public class MentionDeletedEvent extends DeletedEvent {
    private MentionDetails details;
    public MentionDeletedEvent(MentionDetails details) {
        this.details = details;
    }
    
    public MentionDetails getDetails() {
        return details;
    }
    
    public void setDetails(MentionDetails details) {
        this.details = details;
    }
    
    
}
