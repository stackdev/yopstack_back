package com.mystack.core.events.mentions.request;
import com.mystack.core.events.CreateEvent;
import com.mystack.core.domain.MentionDetails;

public class CreateMentionEvent extends CreateEvent {
    private MentionDetails details;
    public CreateMentionEvent(MentionDetails details) {
        this.details = details;
    }
    
    public MentionDetails getDetails() {
        return details;
    }
    
    public void setDetails(MentionDetails details) {
        this.details = details;
    }
    
    
}
