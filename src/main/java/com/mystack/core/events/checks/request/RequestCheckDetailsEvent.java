package com.mystack.core.events.checks.request;

import java.util.UUID;

import com.mystack.core.events.ReadEvent;

public class RequestCheckDetailsEvent extends ReadEvent {
    private UUID id;

    public RequestCheckDetailsEvent(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return this.id;
    }
}
