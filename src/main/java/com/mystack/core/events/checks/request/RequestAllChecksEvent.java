package com.mystack.core.events.checks.request;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.config.FunctionalConfiguration;
import com.mystack.core.events.ReadEvent;

public class RequestAllChecksEvent extends ReadEvent {
    private static Logger LOG   = LoggerFactory.getLogger(RequestAllChecksEvent.class);

    private UUID          stack_id;

    private Date          before_date;
    private Date          after_date;
    private int           count = FunctionalConfiguration.CHECK_MAX_GET_COUNT;

    public RequestAllChecksEvent(UUID stack_id, String before_date, int count) {
        init(stack_id, before_date, "", count);
    }

    public RequestAllChecksEvent(UUID stack_id, String before_date, String after_date, int count) {
        init(stack_id, before_date, after_date, count);
    }

    public RequestAllChecksEvent(UUID stack_id, String before_date, String after_date) {
        new RequestAllChecksEvent(stack_id, before_date, after_date, 0);
    }

    public RequestAllChecksEvent(UUID stack_id, Date before_date, Date after_date) {
        this.stack_id = stack_id;
        this.before_date = before_date;
        this.after_date = after_date;
    }

    private void init(UUID stack_id, String before_date, String after_date, int count) {

        LOG.info("Input [stack_id=" + stack_id + ", before_date=" + before_date + ", after_date=" + after_date + ", count=" + count + "]");
        this.stack_id = stack_id;
        if (count > 0) {
            this.count = count;
        }

        try {
            Long b = Long.valueOf(before_date);
            this.before_date = new Date(b);
        } catch (NumberFormatException e) {
            this.before_date = null;
        }

        try {
            Long a = Long.valueOf(after_date);
            this.after_date = new Date(a);
        } catch (NumberFormatException e) {
            this.after_date = null;
        }
        LOG.info("Value setup: [stack_id=" + this.stack_id + ", before_date=" + this.before_date + ", after_date=" + this.after_date + ", count=" + this.count + "]");
    }

    // GETTER/SETTER
    public UUID getStack_id() {
        return stack_id;
    }

    public void setStack_id(UUID stack_id) {
        this.stack_id = stack_id;
    }

    public Date getBefore_date() {
        return before_date;
    }

    public void setBefore_date(Date before_date) {
        this.before_date = before_date;
    }

    public Date getAfter_date() {
        return after_date;
    }

    public void setAfter_date(Date after_date) {
        this.after_date = after_date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "RequestAllChecksEvent [stack_id=" + stack_id + ", before_date=" + before_date + ", after_date=" + after_date + ", count=" + count + "]";
    }

}
