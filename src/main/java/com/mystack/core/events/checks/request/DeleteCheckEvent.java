package com.mystack.core.events.checks.request;

import java.util.UUID;

import com.mystack.core.events.DeleteEvent;

public class DeleteCheckEvent extends DeleteEvent {

    private UUID id;

    public DeleteCheckEvent(UUID id, String authenticated_user) {
        this.id = id;
        this.authenticated_user = authenticated_user;
    }

    public UUID getId() {
        return id;
    }

}
