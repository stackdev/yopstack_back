package com.mystack.core.events.checks.result;

import java.util.Optional;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.UpdatedEvent;

public class CheckUpdatedEvent extends UpdatedEvent {

	private CheckDetails details;
	private Optional<CheckDetails> checkBeforeUpdate;

	public CheckUpdatedEvent(CheckDetails details) {
		this.details = details;
		this.checkBeforeUpdate = Optional.empty();
	}
	
	public CheckUpdatedEvent(CheckDetails details, CheckDetails previousCheck) {
		this.details = details;
		this.checkBeforeUpdate = Optional.ofNullable(previousCheck);
	}

	public CheckDetails getDetails() {
		return this.details;
	}

	public Optional<CheckDetails> getBeforeUpdateCheckDetails() {
		return this.checkBeforeUpdate;
	}

	public void setBeforeUpdateCheckDetails(CheckDetails details) {
		this.checkBeforeUpdate = Optional.ofNullable(details);
	}

	static public CheckUpdatedEvent notUpdated(CheckDetails check) {
		CheckUpdatedEvent event = new CheckUpdatedEvent(check);
		event.setNotUpdated();
		return event;
	}

	static public CheckUpdatedEvent notFound(CheckDetails check) {
		CheckUpdatedEvent event = notUpdated(check);
		event.setNotFound();
		return event;
	}
}
