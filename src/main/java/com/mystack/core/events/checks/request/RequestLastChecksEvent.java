package com.mystack.core.events.checks.request;

import java.util.UUID;

import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.core.events.ReadEvent;

public class RequestLastChecksEvent extends ReadEvent {
	private UUID stackId;
	private CheckStatus checkStatus;
	
	public RequestLastChecksEvent(UUID stackId, CheckStatus status) {
		this.stackId = stackId;
		this.checkStatus = status;
	}
	
	public UUID getStackId() {
		return stackId;
	}
	public void setStackId(UUID stackId) {
		this.stackId = stackId;
	}
	public CheckStatus getCheckStatus() {
		return checkStatus;
	}
	public void setCheckStatus(CheckStatus checkStatus) {
		this.checkStatus = checkStatus;
	}

	
	
}
