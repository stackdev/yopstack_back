package com.mystack.core.events.checks.result;

import java.util.ArrayList;
import java.util.List;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.ReadEvent;

public class AllChecksEvent extends ReadEvent {
  private List<CheckDetails> listDetails;

  public AllChecksEvent(List<CheckDetails> list) {
	this.listDetails = list;
  }

  public List<CheckDetails> getAllCheckDetails() {
	return this.listDetails;
  }

  public static AllChecksEvent NotFound() {
	AllChecksEvent event = new AllChecksEvent(new ArrayList<CheckDetails>());
	event.entityFound = false;
	return event;
  }
}
