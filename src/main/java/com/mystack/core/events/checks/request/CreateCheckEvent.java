package com.mystack.core.events.checks.request;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.CreateEvent;

public class CreateCheckEvent extends CreateEvent {
    private CheckDetails details;

    public CreateCheckEvent(CheckDetails details, String authenticated_user) {
        this.details = details;
        this.authenticated_user = authenticated_user;
    }

    public CheckDetails getDetails() {
        return this.details;
    }

    public String toString() {
        return this.details.toString();
    }

    public String getAuthenticated_user() {
        return authenticated_user;
    }

}
