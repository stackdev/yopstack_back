package com.mystack.core.events.checks.result;

import java.util.UUID;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.ReadEvent;

public class CheckDetailsEvent extends ReadEvent {
    private CheckDetails details;

    public CheckDetailsEvent(CheckDetails details) {
        this.details = details;
    }

    public CheckDetails getDetails() {
        return this.details;
    }

    public static CheckDetailsEvent notFound(UUID id) {

        CheckDetailsEvent event = new CheckDetailsEvent(new CheckDetails(id));
        event.entityFound = false;
        return event;
    }
}
