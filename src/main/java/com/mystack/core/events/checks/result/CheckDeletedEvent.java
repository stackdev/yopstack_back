package com.mystack.core.events.checks.result;

import java.util.UUID;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.DeletedEvent;

public class CheckDeletedEvent extends DeletedEvent {

    private CheckDetails details;

    public CheckDeletedEvent(CheckDetails details) {
        this.details = details;
    }

    public CheckDetails getDetails() {
        return this.details;
    }

    public static CheckDeletedEvent notFound(UUID id) {
        return notFound(new CheckDetails(id));
    }

    public static CheckDeletedEvent notFound(CheckDetails details) {
        CheckDeletedEvent event = new CheckDeletedEvent(details);
        event.entityFound = false;
        event.deletionCompleted = false;
        return event;
    }

    public static CheckDeletedEvent notAuthorized(CheckDetails details) {
        CheckDeletedEvent event = new CheckDeletedEvent(details);
        event.entityFound = true;
        event.deletionCompleted = false;
        return event;
    }
}
