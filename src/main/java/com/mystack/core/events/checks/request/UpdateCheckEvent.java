package com.mystack.core.events.checks.request;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.UpdateEvent;

public class UpdateCheckEvent extends UpdateEvent {

    private CheckDetails check;

    public UpdateCheckEvent(CheckDetails check, String authenticated_user) {
        this.check = check;
        this.authenticated_user = authenticated_user;
    }

    public CheckDetails getCheckDetails() {
        return check;
    }
}
