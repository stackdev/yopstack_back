package com.mystack.core.events.checks.result;

import java.util.UUID;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.CreatedEvent;

/**
 * This class is named Stack created event, but it is also used when the Stack fails to be created
 * 
 * @author dzd
 */
public class CheckCreatedEvent extends CreatedEvent {
    private CheckDetails details;
    private UUID         newId;

    public CheckCreatedEvent(CheckDetails details) {
        this.details = details;
        this.newId = details.getId();
    }

    public CheckCreatedEvent(UUID newId, CheckDetails details) {
        this.details = details;
        this.newId = newId;
    }

    public CheckDetails getDetails() {
        return this.details;
    }

    public UUID getId() {
        return this.newId;
    }

    /**
     * Static method to be called when creation failed
     * 
     * @param details
     * @return
     */
    public static CheckCreatedEvent notAuthorized(CheckDetails details) {
        CheckCreatedEvent event = new CheckCreatedEvent(null, details);
        event.setFailedNotAuthorized();
        return event;
    }

    public static CheckCreatedEvent creationFailed(CheckDetails details) {
        CheckCreatedEvent event = new CheckCreatedEvent(null, details);
        event.setCreated(false);
        return event;
    }
}
