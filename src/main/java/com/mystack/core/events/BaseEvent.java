package com.mystack.core.events;

public class BaseEvent {
    protected String authenticated_user;

    public String getAuthenticated_user() {
        return authenticated_user;
    }

    public void setAuthenticated_user(String authenticated_user) {
        this.authenticated_user = authenticated_user;
    }
}
