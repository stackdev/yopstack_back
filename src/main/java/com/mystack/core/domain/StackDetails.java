package com.mystack.core.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.mystack.config.FunctionalConfiguration;
import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.config.FunctionalConfiguration.StackStatus;
import com.mystack.config.FunctionalConfiguration.StackType;

/**
 * Pivot class between the two Stack classes from core domain and REST domain
 * 
 * @author dzd
 * 
 */
/**
 * @author dzd
 *
 */
public class StackDetails {

    //private static Logger LOG = LoggerFactory.getLogger(StackDetails.class);

    private UUID        id;
    private String      name;
    private String      description;
    private int         periodicity;
    private int         frequency;
    private Date        last_check;
    private Date        last_planned_check;
    private int         last_check_count;
    private Date        creation_date;
    private StackType   type;
    private boolean     isPrivate;
    private StackStatus status;
    private boolean     isShared;
    private List<UUID>  admins;
    private List<UUID>  checkers;
    private Date        target_date;
    private CheckCount  checkCount;
    /*********/
    // empty constructor for conversion purpose
    public StackDetails() {
    }

    // Specific constructor, we are in core domain class, here is the logic
    public StackDetails(UUID id) {
        super();
        init(id, "", "");
    }

    public StackDetails(UUID id, String name, String description, int periodicity,
            int frequency, Date last_check, int last_check_count,
            Date creation_date, StackType type, boolean isPrivate, StackStatus status,
            boolean isShared, List<UUID> admins, List<UUID> checkers, Date target_date, CheckCount checkCount) {
        super();
        init(id, name, description, periodicity, frequency, last_check, last_check_count,
                creation_date, type, isPrivate, status,
                isShared, admins, checkers, target_date, checkCount);
    }

    public StackDetails(UUID id, String name, String description) {
        super();
        init(id, name, description);
    }

    private void init(UUID id, String name, String description) {
        init(id, name, description, FunctionalConfiguration.DEFAULT_STACK_PERIODICITY,
                FunctionalConfiguration.DEFAULT_STACK_FREQUENCY, null,
                0, null, FunctionalConfiguration.DEFAULT_STACK_TYPE,
                FunctionalConfiguration.DEFAULT_STACK_PRIVACY, FunctionalConfiguration.DEFAULT_STACK_STATUS,
                FunctionalConfiguration.DEFAULT_STACK_SHARED_STATE, new ArrayList<UUID>(), new ArrayList<UUID>(), null, null);
    }

    private void init(UUID id, String name, String description, int periodicity, int frequency,
            Date last_check, int last_check_count, Date creation_date,
            StackType type, boolean isPrivate, StackStatus status,
            boolean isShared, List<UUID> admins, List<UUID> checkers, Date target_date, CheckCount checkCount) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.periodicity = periodicity;
        this.frequency = frequency;
        this.last_check = last_check;
        this.last_check_count = last_check_count;
        this.creation_date = creation_date;
        this.type = type;
        this.isPrivate = isPrivate;
        this.status = status;
        this.checkCount = checkCount;
    }

    // convernient methods
    public void addChecker(UUID checker) {
        if (null == this.checkers) {
            this.checkers = new ArrayList<>();
        }
        if (!this.checkers.contains(checker)) {
            this.checkers.add(checker);
        }
    }

    public void addAdmin(UUID admin) {
        if (null == this.admins) {
            this.admins = new ArrayList<>();
        }
        if (!this.admins.contains(admin)) {
            this.admins.add(admin);
        }
    }

    public void removeChecker(UUID checker) {
        if (null != this.checkers &&
                this.checkers.contains(checker)) {
            this.checkers.remove(checker);
        }
    }

    public void removeAdmin(UUID admin) {
        if (null != this.admins &&
                this.admins.contains(admin)) {
            this.admins.remove(admin);
        }
    }

    // setter/getters
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(int periodicity) {
        this.periodicity = periodicity;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public Date getLast_check() {
        return last_check;
    }

    public void setLast_check(Date last_check) {
        this.last_check = last_check;
    }
    
	public void setLastCheckDate(Date date, CheckStatus status) {
		if(CheckStatus.OK.equals(status)) {
			setLast_check(date);
		} else if(CheckStatus.PLANNED.equals(status)) {
			setLast_planned_check(date);
		}
	}
	
	public Optional<Date> getLastCheckDate(CheckStatus status) {
		Optional<Date> lastCheckDate = Optional.empty();
		if(CheckStatus.OK.equals(status)) {
			lastCheckDate = Optional.ofNullable(getLast_check());
		} else if(CheckStatus.PLANNED.equals(status)) {
			lastCheckDate = Optional.ofNullable(getLast_planned_check());
		}
		return lastCheckDate;
	}
    

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public int getLast_check_count() {
        return last_check_count;
    }

    public void setLast_check_count(int last_check_count) {
        this.last_check_count = last_check_count;
    }
    
	public Date getLast_planned_check() {
		return this.last_planned_check;
	}
    
    public void setLast_planned_check(Date last_planned_check) {
		this.last_planned_check = last_planned_check;
	}

    public StackType getType() {
        return type;
    }

    public void setType(StackType type) {
        this.type = type;
    }

    public int getRestType() {
        return type.getValue();
    }

    public void setRestType(int type) {
        this.type = StackType.values()[type];
    }

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public boolean getIsShared() {
        return isShared;
    }

    public void setIsShared(boolean isShared) {
        this.isShared = isShared;
    }

    public List<UUID> getAdmins() {
        return admins;
    }

    public void setAdmins(List<UUID> admins) {
        this.admins = admins;
    }

    public UUID[] getRestAdmins() {
        if (admins != null) {
            return admins.toArray(new UUID[admins.size()]);
        }
        return new UUID[0];
    }

    public void setRestAdmins(UUID[] admins) {

        if (null != admins) {
            setAdmins(new ArrayList<>(Arrays.asList(admins)));
        } else {
            setAdmins(new ArrayList<UUID>());
        }
    }

    public List<UUID> getCheckers() {
        return checkers;
    }

    public void setCheckers(List<UUID> checkers) {
        this.checkers = checkers;
    }

    public UUID[] getRestCheckers() {
        if (checkers != null) {
            return checkers.toArray(new UUID[checkers.size()]);
        }
        return new UUID[0];
    }

    public void setRestCheckers(UUID[] checkers) {

        if (null != checkers) {
            setCheckers(new ArrayList<>(Arrays.asList(checkers)));
        } else {
            setCheckers(new ArrayList<UUID>());
        }
    }

    public Date getTarget_date() {
        return target_date;
    }

    public void setTarget_date(Date target_date) {
        this.target_date = target_date;
    }

    public StackStatus getStatus() {
        return status;
    }

    public void setStatus(StackStatus status) {
        this.status = status;
    }

    public int getRestStatus() {
        return status.getValue();
    }

    public void setRestStatus(int status) {
        this.status = StackStatus.values()[status];
    }


	public CheckCount getCheckCount() {
		return checkCount;
	}

	public void setCheckCount(CheckCount checkCount) {
		this.checkCount = checkCount;
	}
	
	

	@Override
	public String toString() {
		return "StackDetails [id=" + id + ", name=" + name + ", description=" + description + ", periodicity="
				+ periodicity + ", frequency=" + frequency + ", last_check=" + last_check + ", last_planned_check="
				+ last_planned_check + ", last_check_count=" + last_check_count + ", creation_date=" + creation_date
				+ ", type=" + type + ", isPrivate=" + isPrivate + ", status=" + status + ", isShared=" + isShared
				+ ", admins=" + admins + ", checkers=" + checkers + ", target_date=" + target_date + ", checkCount="
				+ checkCount + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((admins == null) ? 0 : admins.hashCode());
		result = prime * result + ((checkers == null) ? 0 : checkers.hashCode());
		result = prime * result + ((creation_date == null) ? 0 : creation_date.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + frequency;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (isPrivate ? 1231 : 1237);
		result = prime * result + (isShared ? 1231 : 1237);
		result = prime * result + ((last_check == null) ? 0 : last_check.hashCode());
		result = prime * result + last_check_count;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + periodicity;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((target_date == null) ? 0 : target_date.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StackDetails other = (StackDetails) obj;
		if (admins == null) {
			if (other.admins != null)
				return false;
		} else if (!admins.equals(other.admins))
			return false;
		if (checkers == null) {
			if (other.checkers != null)
				return false;
		} else if (!checkers.equals(other.checkers))
			return false;
		if (creation_date == null) {
			if (other.creation_date != null)
				return false;
		} else if (!creation_date.equals(other.creation_date))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (frequency != other.frequency)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isPrivate != other.isPrivate)
			return false;
		if (isShared != other.isShared)
			return false;
		if (last_check == null) {
			if (other.last_check != null)
				return false;
		} else if (!last_check.equals(other.last_check))
			return false;
		if (last_check_count != other.last_check_count)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (periodicity != other.periodicity)
			return false;
		if (status != other.status)
			return false;
		if (target_date == null) {
			if (other.target_date != null)
				return false;
		} else if (!target_date.equals(other.target_date))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
}
