package com.mystack.core.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class UserDetails {

    private UUID         id;
    private String       name;
    private List<UUID>   list_friend;
    private List<UUID>   list_stack;

    // all domain except REST
    private String       password;
    private boolean      enabled;
    private List<String> roles;

    // REST only
    private String       setpw;
    private String       oldpw;

    /**
     * Empty constructor for code generation script
     */
    public UserDetails() {

    }

    public UserDetails(UUID id) {
        this.id = id;
    }

    public UserDetails(UUID id, String name, List<UUID> list_friend, List<UUID> list_stack) {
        super();
        this.id = id;
        this.name = name;
        this.list_friend = list_friend;
        this.list_stack = list_stack;
    }

    public UserDetails(UUID id, String name, List<UUID> list_friend, List<UUID> list_stack, String password, boolean enabled, List<String> roles) {
        this.id = id;
        this.name = name;
        this.list_friend = list_friend;
        this.list_stack = list_stack;
        this.password = password;
        this.enabled = enabled;
        this.roles = roles;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UUID> getList_friend() {
        return list_friend;
    }

    public void setList_friend(List<UUID> list_friend) {
        this.list_friend = list_friend;
    }

    public List<UUID> getList_stack() {
        return list_stack;
    }

    public void setRestList_friend(UUID[] list_friend) {

        if (null != list_friend) {
            setList_friend(Arrays.asList(list_friend));
        } else {
            setList_friend(new ArrayList<UUID>());
        }
    }

    public UUID[] getRestList_friend() {
        if (list_friend != null) {
            return list_friend.toArray(new UUID[list_friend.size()]);
        }
        return new UUID[0];
    }

    public void setRestList_stack(UUID[] list_stack) {

        if (null != list_stack) {
            setList_stack(Arrays.asList(list_stack));
        } else {
            setList_stack(new ArrayList<UUID>());
        }
    }

    public UUID[] getRestList_stack() {
        if (list_stack != null) {
            return list_stack.toArray(new UUID[list_stack.size()]);
        }
        return new UUID[0];
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> role) {
        this.roles = role;
    }

    public String getSetpw() {
        return setpw;
    }

    public void setSetpw(String setpw) {
        this.setpw = setpw;
    }

    public String getOldpw() {
        return oldpw;
    }

    public void setOldpw(String oldpw) {
        this.oldpw = oldpw;
    }

    @Override
    public String toString() {
        return "User: [" + " Id= " + id + " Name= " + name + " List_friend= " + list_friend + " List_stack= " +
                list_stack + " Enabled= " + enabled + " Roles= " + roles + " ]";
    }

    /**
     * Add a stack (UUID) to the list of stacks owned by the user
     * 
     * @param stack
     */
    public void add_stack(UUID stack) {
        if (null == list_stack) {
            list_stack = new ArrayList<UUID>();
        }
        this.list_stack.add(stack);
    }

    /**
     * Add a stack (UUID) to the list of stacks owned by the user
     * return true is the stack was successfully removed
     * 
     * @param stack
     */
    public void remove_stack(UUID stack) {
        if (null == list_stack) {
            return;
        }

        if (list_stack.contains(stack)) {
            this.list_stack.remove(stack);
        }
    }

    public void setList_stack(List<UUID> list_stack) {
        this.list_stack = list_stack;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (enabled ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((list_friend == null) ? 0 : list_friend.hashCode());
        result = prime * result + ((list_stack == null) ? 0 : list_stack.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((roles == null) ? 0 : roles.hashCode());
        result = prime * result + ((setpw == null) ? 0 : setpw.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserDetails other = (UserDetails) obj;
        if (enabled != other.enabled)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (list_friend == null) {
            if (other.list_friend != null)
                return false;
        } else if (!list_friend.equals(other.list_friend))
            return false;
        if (list_stack == null) {
            if (other.list_stack != null)
                return false;
        } else if (!list_stack.equals(other.list_stack))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (roles == null) {
            if (other.roles != null)
                return false;
        } else if (!roles.equals(other.roles))
            return false;
        if (setpw == null) {
            if (other.setpw != null)
                return false;
        } else if (!setpw.equals(other.setpw))
            return false;
        return true;
    }

}
