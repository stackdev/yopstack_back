package com.mystack.core.domain;


import java.util.Date;
import java.util.UUID;

public class MentionDetails {

    private UUID id;
    private UUID from;
    private UUID to;
    private UUID comment;
    private Date creation_date;
    private boolean viewed;
    private UUID check;

    //Constructors
    public MentionDetails() {
    }

    public MentionDetails(UUID id, UUID from, UUID to, UUID comment) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.comment = comment;
    }


    //Getter/setters/toString/toDetails/fromDetails


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getFrom() {
        return from;
    }

    public void setFrom(UUID from) {
        this.from = from;
    }

    public UUID getTo() {
        return to;
    }

    public void setTo(UUID to) {
        this.to = to;
    }

    public UUID getComment() {
        return comment;
    }

    public void setComment(UUID comment) {
        this.comment = comment;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }


    public boolean getViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }


    public UUID getCheck() {
        return check;
    }

    public void setCheck(UUID check) {
        this.check = check;
    }


    public MentionDetails toDetails() {
        MentionDetails details = new MentionDetails();

        return details;
    }


}
