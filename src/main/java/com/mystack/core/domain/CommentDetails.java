package com.mystack.core.domain;

import java.util.Date;
import java.util.UUID;

/**
 * @author dzd
 */
public class CommentDetails {

    private UUID   id;
    private UUID   stack_userid;
    private UUID   stackid;
    private UUID   checkid;
    private UUID   comment_userid;
    private String text;
    private Date   timestamp;
    private Date   update_timestamp;

    public CommentDetails(UUID id, UUID stack_userid, UUID stackid, UUID checkid, UUID comment_userid, String text, Date timestamp, Date update_timestamp) {
        init(id,
                stack_userid,
                stackid,
                checkid,
                comment_userid,
                text,
                timestamp,
                update_timestamp);
    }

    public CommentDetails(CommentDetails details) {
        init(details.getId(),
                details.getStack_userid(),
                details.getStackid(),
                details.getCheckid(),
                details.getComment_userid(),
                details.getText(),
                details.getTimestamp(),
                details.getUpdateTimestamp());
    }

    /**
     * Init method
     * 
     * @return
     */
    private void init(UUID id, UUID stack_userid, UUID stackid, UUID checkid, UUID comment_userid, String text, Date timestamp, Date update_timestamp) {

        this.id = id;
        this.stack_userid = stack_userid;
        this.stackid = stackid;
        this.checkid = checkid;
        this.comment_userid = comment_userid;
        this.text = text;

        if (null == timestamp) {
            timestamp = new Date();
        }
        if (null == update_timestamp) {
            update_timestamp = new Date();
        }

        this.timestamp = timestamp;
        this.update_timestamp = update_timestamp;
    }

    public UUID getStack_userid() {
        return stack_userid;
    }

    public void setStack_userid(UUID userid) {
        this.stack_userid = userid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Date getUpdateTimestamp() {
        return update_timestamp;
    }

    public void setUpdateTimestamp(Date update_timestamp) {
        this.update_timestamp = update_timestamp;
    }

    public UUID getStackid() {
        return stackid;
    }

    public void setStackid(UUID stackid) {
        this.stackid = stackid;
    }

    public UUID getCheckid() {
        return checkid;
    }

    public void setCheckid(UUID checkid) {
        this.checkid = checkid;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID commentid) {
        this.id = commentid;
    }

    public UUID getComment_userid() {
        return comment_userid;
    }

    public void setComment_userid(UUID comment_userid) {
        this.comment_userid = comment_userid;
    }

    @Override
    public String toString() {
        return "CommentDetails [id=" + id + ", stack_userid=" + stack_userid + ", stackid=" + stackid + ", checkid=" + checkid + ", comment_userid=" + comment_userid + ", text=" + text
                + ", timestamp=" + timestamp + ", update_timestamp=" + update_timestamp + "]";
    }

}