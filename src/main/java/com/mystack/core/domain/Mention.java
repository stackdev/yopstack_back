package com.mystack.core.domain;
import java.util.UUID;
import java.util.Date;

public class Mention {
    private UUID id;
    private UUID from;
    private UUID to;
    private UUID comment;
    private Date creation_date;
    private boolean viewed;
    private UUID check;
    //Constructors
    public Mention() {
    }
    
    public Mention(UUID id, UUID from, UUID to, UUID comment, Date creation_date, boolean viewed, UUID check) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.comment = comment;
        this.creation_date = creation_date;
        this.viewed = viewed;
        this.check = check;
    }
    
    public Mention(Mention mention) {
        this.id = mention.id;
        this.from = mention.from;
        this.to = mention.to;
        this.comment = mention.comment;
        this.creation_date = mention.creation_date;
        this.viewed = mention.viewed;
        this.check = mention.check;
    }
    
    //Getter/setters/toString/toDetails/fromDetails
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    
    public UUID getFrom() {
        return from;
    }
    
    public void setFrom(UUID from) {
        this.from = from;
    }
    
    
    public UUID getTo() {
        return to;
    }
    
    public void setTo(UUID to) {
        this.to = to;
    }
    
    
    public UUID getComment() {
        return comment;
    }
    
    public void setComment(UUID comment) {
        this.comment = comment;
    }
    
    
    public Date getCreation_date() {
        return creation_date;
    }
    
    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }
    
    
    public boolean getViewed() {
        return viewed;
    }
    
    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }
    
    
    public UUID getCheck() {
        return check;
    }
    
    public void setCheck(UUID check) {
        this.check = check;
    }
    
    
    public String toString() {
        return "Mention: [" + " id: " + id + " from: " + from + " to: " + to + " comment: " + comment + " creation_date: " + creation_date + " viewed: " + viewed + " check: " + check + "]";
    }
    
    public MentionDetails toDetails() {
        MentionDetails details = new MentionDetails();
        details.setId(this.id);
        details.setFrom(this.from);
        details.setTo(this.to);
        details.setComment(this.comment);
        details.setCreation_date(this.creation_date);
        details.setViewed(this.viewed);
        details.setCheck(this.check);
        return details;
    }
    
    public static Mention fromDetails(MentionDetails details) {
        Mention user = new Mention();
        user.id = details.getId();
        user.from = details.getFrom();
        user.to = details.getTo();
        user.comment = details.getComment();
        user.creation_date = details.getCreation_date();
        user.viewed = details.getViewed();
        user.check = details.getCheck();
        return user;
    }
    
    public boolean equals(Object obj) {
        
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        
        Mention other = (Mention) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (from == null) {
            if (other.from != null)
                return false;
        } else if (!from.equals(other.from))
            return false;
        if (to == null) {
            if (other.to != null)
                return false;
        } else if (!to.equals(other.to))
            return false;
        if (comment == null) {
            if (other.comment != null)
                return false;
        } else if (!comment.equals(other.comment))
            return false;
        if (creation_date == null) {
            if (other.creation_date != null)
                return false;
        } else if (!creation_date.equals(other.creation_date))
            return false;
        if (viewed != other.viewed)
            return false;
        if (check == null) {
            if (other.check != null)
                return false;
        } else if (!check.equals(other.check))
            return false;
        return true;
    }
    
}
