package com.mystack.core.domain;

import java.util.Date;
import java.util.UUID;

import com.mystack.config.FunctionalConfiguration.CheckStatus;

public class CheckDetails {
    private UUID        id;
    private Date        date;
    private UUID        stack;
    private String      comment;
    private Date        creation_date;
    private UUID        checker;
    private CheckStatus status = CheckStatus.OK;

    public CheckDetails() {
    }

    public CheckDetails(UUID id) {
        this.id = id;
    }

    public CheckDetails(UUID id, Date date, UUID stack, String comment, Date creation_date, UUID checker, CheckStatus status) {
        this.id = id;
        this.date = date;
        this.stack = stack;
        this.comment = comment;
        this.creation_date = creation_date;
        this.checker = checker;
        if(null != status) {
        	this.status = status;
        }
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UUID getStack() {
        return stack;
    }

    public void setStack(UUID stack) {
        this.stack = stack;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public UUID getChecker() {
        return checker;
    }

    public void setChecker(UUID checker) {
        this.checker = checker;
    }

    public CheckStatus getStatus() {
        return status;
    }

    public void setStatus(CheckStatus status) {
    	if(null != status) {
    		this.status = status;
    	}
    }

    @Override
    public String toString() {
        return "CheckDetails [id=" + id + ", date=" + date + ", stack=" + stack + ", comment=" + comment + ", creation_date=" + creation_date + ", checker=" + checker + ", status=" + status + "]";
    }

}
