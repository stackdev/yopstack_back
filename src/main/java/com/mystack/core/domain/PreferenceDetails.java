package com.mystack.core.domain;

import java.util.List;
import java.util.UUID;

import com.mystack.config.FunctionalConfiguration.Language;
import com.mystack.config.FunctionalConfiguration.StackSorting;
import com.mystack.config.FunctionalConfiguration.Theme;
import com.mystack.config.FunctionalConfiguration.TimeZone;

public class PreferenceDetails {

    private UUID         id;
    private UUID         user;
    private StackSorting stacksorting;    
    private boolean      risingsort;
    private List<UUID> displayCheckPeriod;
    private List<UUID> disableMissedPeriod;
    private List<UUID> disableMarkdownInCheck;
    private List<UUID> disableMarkdownInComment;
    private Language language;
    private TimeZone timezone;
    private Theme theme;

    // Specific constructor, we are in core domain class, here is the logic
    public PreferenceDetails() {

    }

    public PreferenceDetails(UUID id, UUID user, StackSorting sorting) {
        super();
        init(id, user, sorting);
    }

    private void init(UUID id, UUID user, StackSorting sorting) {
        this.id = id;
        this.user = user;
        this.stacksorting = sorting;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUser() {
        return user;
    }

    public void setUser(UUID user) {
        this.user = user;
    }

    public StackSorting getStacksorting() {
        return stacksorting;
    }

    public void setStacksorting(StackSorting stacksorting) {
        this.stacksorting = stacksorting;
    }

    public void setRestStacksorting(int sorting) {
        if (sorting < 0) {
            sorting = 0;
        }
        this.stacksorting = StackSorting.values()[sorting];
    }

    public int getRestStacksorting() {
        return this.stacksorting.getValue();
    }

    public boolean getRisingsort() {
        return risingsort;
    }

    public void setRisingsort(boolean risingsort) {
        this.risingsort = risingsort;
    }

	public List<UUID> getDisplayCheckPeriod() {
		return displayCheckPeriod;
	}

	public void setDisplayCheckPeriod(List<UUID> displayCheckPeriod) {
		this.displayCheckPeriod = displayCheckPeriod;
	}


	public List<UUID> getDisableMarkdownInCheck() {
		return disableMarkdownInCheck;
	}

	public void setDisableMarkdownInCheck(List<UUID> disableMarkdownInCheck) {
		this.disableMarkdownInCheck = disableMarkdownInCheck;
	}

	public List<UUID> getDisableMarkdownInComment() {
		return disableMarkdownInComment;
	}

	public void setDisableMarkdownInComment(List<UUID> disableMarkdownInComment) {
		this.disableMarkdownInComment = disableMarkdownInComment;
	}

	public List<UUID> getDisableMissedPeriod() {
		return disableMissedPeriod;
	}

	public void setDisableMissedPeriod(List<UUID> disableMissedPeriod) {
		this.disableMissedPeriod = disableMissedPeriod;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public TimeZone getTimezone() {
		return timezone;
	}

	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}

	@Override
	public String toString() {
		return "PreferenceDetails [id=" + id + ", user=" + user + ", stacksorting=" + stacksorting + ", risingsort="
				+ risingsort + ", displayCheckPeriod=" + displayCheckPeriod + ", disableMissedPeriod="
				+ disableMissedPeriod + ", disableMarkdownInCheck=" + disableMarkdownInCheck
				+ ", disableMarkdownInComment=" + disableMarkdownInComment + ", language=" + language + ", timezone="
				+ timezone + ", theme=" + theme + "]";
	}




}
