package com.mystack.core.domain;

import com.mystack.config.FunctionalConfiguration.CheckStatus;

public class CheckCount {

	private long ok;
	private long planned;
	private long failed;
	private long skipped;
	
	public CheckCount() {}

	public CheckCount(int ok, int planned, int failed, int skipped) {
		this.ok = ok;
		this.planned = planned;
		this.failed = failed;
		this.skipped = skipped;
	}
	
	public long getTotal() {
		return ok + failed + planned + skipped;
	}

	public long getOk() {
		return ok;
	}

	public void setOk(long ok) {
		this.ok = ok;
	}
	
	public long getCount(CheckStatus status) {
		long count = 0;
		if(CheckStatus.OK.equals(status)) {
			count = getOk();
		} else if (CheckStatus.FAILED.equals(status)) {
			count = getFailed();
		} else if (CheckStatus.SKIPPED.equals(status)) {
			count = getSkipped();
		} else if (CheckStatus.PLANNED.equals(status)) {
			count = getPlanned();
		}
		return count;
	}
	
	public void setCount(CheckStatus status, final long count) {
		if(CheckStatus.OK.equals(status)) {
			setOk(count);
		} else if (CheckStatus.FAILED.equals(status)) {
			setFailed(count);
		} else if (CheckStatus.SKIPPED.equals(status)) {
			setSkipped(count);
		} else if (CheckStatus.PLANNED.equals(status)) {
			setPlanned(count);
		}
	}

	public long getPlanned() {
		return planned;
	}

	public void setPlanned(long planned) {
		this.planned = planned;
	}

	public long getFailed() {
		return failed;
	}

	public void setFailed(long failed) {
		this.failed = failed;
	}

	public long getSkipped() {
		return skipped;
	}

	public void setSkipped(long skipped) {
		this.skipped = skipped;
	}
	
	public void decrement(CheckStatus status) {
		updateCounterValue(status, false);
	}
	public void increment(CheckStatus status) {
		updateCounterValue(status, true);
	}
	
	private void updateCounterValue(CheckStatus status, boolean increment) {
		int inc = 1;
		if(!increment) {
			inc = -1;
		}
		if(CheckStatus.OK.equals(status)) {
			ok += inc;
		} else if (CheckStatus.FAILED.equals(status)) {
			failed += inc;
		} else if (CheckStatus.SKIPPED.equals(status)) {
			skipped += inc;
		} else if (CheckStatus.PLANNED.equals(status)) {
			planned += inc;
		}
	}

	@Override
	public String toString() {
		return "CheckCount [ok=" + ok + ", planned=" + planned + ", failed=" + failed + ", skipped=" + skipped + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (failed ^ (failed >>> 32));
		result = prime * result + (int) (ok ^ (ok >>> 32));
		result = prime * result + (int) (planned ^ (planned >>> 32));
		result = prime * result + (int) (skipped ^ (skipped >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckCount other = (CheckCount) obj;
		if (failed != other.failed)
			return false;
		if (ok != other.ok)
			return false;
		if (planned != other.planned)
			return false;
		if (skipped != other.skipped)
			return false;
		return true;
	}
	
	
}
