package com.mystack.core.domain;
import java.util.UUID;
import java.util.List;
import com.mystack.config.FunctionalConfiguration.StackSorting;
import com.mystack.config.FunctionalConfiguration.Language;
import com.mystack.config.FunctionalConfiguration.TimeZone;
import com.mystack.config.FunctionalConfiguration.Theme;
import com.mystack.core.domain.PreferenceDetails;

public class Preference {
    private UUID id;
    private UUID user;
    private StackSorting stacksorting;
    private boolean risingsort;
    private List<UUID> displayCheckPeriod;
    private List<UUID> disableMissedPeriod;
    private List<UUID> disableMarkdownInCheck;
    private List<UUID> disableMarkdownInComment;
    private Language language;
    private TimeZone timezone;
    private Theme theme;
    //Constructors
    public Preference() {
    }
    
    public Preference(UUID id, UUID user, StackSorting stacksorting, boolean risingsort, List<UUID> displayCheckPeriod, List<UUID> disableMissedPeriod, List<UUID> 
    disableMarkdownInCheck, List<UUID> disableMarkdownInComment, Language language, TimeZone timezone, Theme theme) {
        this.id = id;
        this.user = user;
        this.stacksorting = stacksorting;
        this.risingsort = risingsort;
        this.displayCheckPeriod = displayCheckPeriod;
        this.disableMissedPeriod = disableMissedPeriod;
        this.disableMarkdownInCheck = disableMarkdownInCheck;
        this.disableMarkdownInComment = disableMarkdownInComment;
        this.language = language;
        this.timezone = timezone;
        this.theme = theme;
    }
    
    public Preference(Preference preference) {
        this.id = preference.id;
        this.user = preference.user;
        this.stacksorting = preference.stacksorting;
        this.risingsort = preference.risingsort;
        this.displayCheckPeriod = preference.displayCheckPeriod;
        this.disableMissedPeriod = preference.disableMissedPeriod;
        this.disableMarkdownInCheck = preference.disableMarkdownInCheck;
        this.disableMarkdownInComment = preference.disableMarkdownInComment;
        this.language = preference.language;
        this.timezone = preference.timezone;
        this.theme = preference.theme;
    }
    
    public Preference(UUID id, UUID user) {
        this.id = id;
        this.user = user;
        this.stacksorting = StackSorting.CREATION;
        this.risingsort = true;
        this.displayCheckPeriod = null;
        this.disableMissedPeriod = null;
        this.disableMarkdownInCheck = null;
        this.disableMarkdownInComment = null;
        this.language = Language.EN;
        this.timezone = TimeZone.CET;
        this.theme = Theme.DEFAULT;
    }
    
    //Getter/setters/toString/toDetails/fromDetails
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    
    public UUID getUser() {
        return user;
    }
    
    public void setUser(UUID user) {
        this.user = user;
    }
    
    
    public StackSorting getStacksorting() {
        return stacksorting;
    }
    
    public void setStacksorting(StackSorting stacksorting) {
        this.stacksorting = stacksorting;
    }
    
    
    public boolean getRisingsort() {
        return risingsort;
    }
    
    public void setRisingsort(boolean risingsort) {
        this.risingsort = risingsort;
    }
    
    
    public List<UUID> getDisplayCheckPeriod() {
        return displayCheckPeriod;
    }
    
    public void setDisplayCheckPeriod(List<UUID> displayCheckPeriod) {
        this.displayCheckPeriod = displayCheckPeriod;
    }
    
    
    public List<UUID> getDisableMissedPeriod() {
        return disableMissedPeriod;
    }
    
    public void setDisableMissedPeriod(List<UUID> disableMissedPeriod) {
        this.disableMissedPeriod = disableMissedPeriod;
    }
    
    
    public List<UUID> getDisableMarkdownInCheck() {
        return disableMarkdownInCheck;
    }
    
    public void setDisableMarkdownInCheck(List<UUID> disableMarkdownInCheck) {
        this.disableMarkdownInCheck = disableMarkdownInCheck;
    }
    
    
    public List<UUID> getDisableMarkdownInComment() {
        return disableMarkdownInComment;
    }
    
    public void setDisableMarkdownInComment(List<UUID> disableMarkdownInComment) {
        this.disableMarkdownInComment = disableMarkdownInComment;
    }
    
    
    public Language getLanguage() {
        return language;
    }
    
    public void setLanguage(Language language) {
        this.language = language;
    }
    
    
    public TimeZone getTimezone() {
        return timezone;
    }
    
    public void setTimezone(TimeZone timezone) {
        this.timezone = timezone;
    }
    
    
    public Theme getTheme() {
        return theme;
    }
    
    public void setTheme(Theme theme) {
        this.theme = theme;
    }
    
    
    public String toString() {
        return "Preference: [" + " id: " + id + " user: " + user + " stacksorting: " + stacksorting + " risingsort: " + risingsort + " displayCheckPeriod: " + displayCheckPeriod + " disableMissedPeriod: " + disableMissedPeriod + " disableMarkdownInCheck: " + disableMarkdownInCheck + " disableMarkdownInComment: " + disableMarkdownInComment + " language: " + language + " timezone: " + timezone + " theme: " + theme + "]";
    }
    
    public PreferenceDetails toDetails() {
        PreferenceDetails details = new PreferenceDetails();
        details.setId(this.id);
        details.setUser(this.user);
        details.setStacksorting(this.stacksorting);
        details.setRisingsort(this.risingsort);
        details.setDisplayCheckPeriod(this.displayCheckPeriod);
        details.setDisableMissedPeriod(this.disableMissedPeriod);
        details.setDisableMarkdownInCheck(this.disableMarkdownInCheck);
        details.setDisableMarkdownInComment(this.disableMarkdownInComment);
        details.setLanguage(this.language);
        details.setTimezone(this.timezone);
        details.setTheme(this.theme);
        return details;
    }
    
    public static Preference fromDetails(PreferenceDetails details) {
        Preference user = new Preference();
        user.id = details.getId();
        user.user = details.getUser();
        user.stacksorting = details.getStacksorting();
        user.risingsort = details.getRisingsort();
        user.displayCheckPeriod = details.getDisplayCheckPeriod();
        user.disableMissedPeriod = details.getDisableMissedPeriod();
        user.disableMarkdownInCheck = details.getDisableMarkdownInCheck();
        user.disableMarkdownInComment = details.getDisableMarkdownInComment();
        user.language = details.getLanguage();
        user.timezone = details.getTimezone();
        user.theme = details.getTheme();
        return user;
    }
    
    public boolean equals(Object obj) {
        
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        
        Preference other = (Preference) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (user == null) {
            if (other.user != null)
                return false;
        } else if (!user.equals(other.user))
            return false;
        if (stacksorting == null) {
            if (other.stacksorting != null)
                return false;
        } else if (!stacksorting.equals(other.stacksorting))
            return false;
        if (risingsort != other.risingsort)
            return false;
        if (displayCheckPeriod == null) {
            if (other.displayCheckPeriod != null)
                return false;
        } else if (!displayCheckPeriod.equals(other.displayCheckPeriod))
            return false;
        if (disableMissedPeriod == null) {
            if (other.disableMissedPeriod != null)
                return false;
        } else if (!disableMissedPeriod.equals(other.disableMissedPeriod))
            return false;
        if (disableMarkdownInCheck == null) {
            if (other.disableMarkdownInCheck != null)
                return false;
        } else if (!disableMarkdownInCheck.equals(other.disableMarkdownInCheck))
            return false;
        if (disableMarkdownInComment == null) {
            if (other.disableMarkdownInComment != null)
                return false;
        } else if (!disableMarkdownInComment.equals(other.disableMarkdownInComment))
            return false;
        if (language == null) {
            if (other.language != null)
                return false;
        } else if (!language.equals(other.language))
            return false;
        if (timezone == null) {
            if (other.timezone != null)
                return false;
        } else if (!timezone.equals(other.timezone))
            return false;
        if (theme == null) {
            if (other.theme != null)
                return false;
        } else if (!theme.equals(other.theme))
            return false;
        return true;
    }
    
}
