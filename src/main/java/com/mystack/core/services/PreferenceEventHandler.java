package com.mystack.core.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mystack.config.FunctionalConfiguration.StackSorting;
import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.persistence.services.PreferencePersistenceService;

public class PreferenceEventHandler extends BaseEventHandler implements PreferenceService {

    private static Logger        LOG = LoggerFactory.getLogger(PreferenceEventHandler.class);
    PreferencePersistenceService persistenceService;

    @Autowired
    UserService                  userService;
	private ReadPreferenceEvent readPreferenceEvent;

    public PreferenceEventHandler(PreferencePersistenceService service) {
        this.persistenceService = service;
    }

    @Override
    public PreferenceReadEvent readPreference(ReadPreferenceEvent event) {
        LOG.info("Reading preference for user: " + event.getDetails().getUser().toString());

        UserDetailsEvent user_event = userService.getUserDetails(new RequestUserDetailsEvent(event.getDetails().getUser()));
        if (!user_event.isEntityFound() ||
                !user_event.getDetails().getName().equals(event.getAuthenticated_user())) {
            return PreferenceReadEvent.notAuthorized(event.getDetails());
        }

        PreferenceReadEvent read_event = persistenceService.readPreference(event);
        return read_event;
    }

    @Override
    public PreferenceCreatedEvent createPreference(CreatePreferenceEvent event) {
        LOG.info("Processing preference: " + event.toString());

        UserDetailsEvent user_event = userService.getUserDetails(new RequestUserDetailsEvent(event.getDetails().getUser()));
        if (!user_event.isEntityFound() ||
                !user_event.getDetails().getName().equals(event.getAuthenticated_user())) {
            return PreferenceCreatedEvent.notAuthorized(event.getDetails());
        }
        
        // checking preference existence for the given user
        PreferenceDetails preferenceDetails = new PreferenceDetails(null, user_event.getDetails().getId(),StackSorting.CREATION);
        readPreferenceEvent = new ReadPreferenceEvent(preferenceDetails, event.getAuthenticated_user());
        PreferenceReadEvent readPreference = persistenceService.readPreference(readPreferenceEvent);
        if(readPreference.isEntityFound()) {
        	LOG.error("Preference already exists for user: " + user_event.getDetails().getId()+ ": not created");
        	return PreferenceCreatedEvent.creationFailed(event.getDetails());
        }

        PreferenceCreatedEvent created_event = persistenceService.createPreference(event);
        LOG.info("Returning: " + created_event.toString() + ", " + created_event.isCreated());
        return created_event;
    }

    @Override
    public PreferenceUpdatedEvent updatePreference(UpdatePreferenceEvent event) {
        LOG.info("Updating preference: " + event.getDetails().getId().toString());

        UserDetailsEvent user_event = userService.getUserDetails(new RequestUserDetailsEvent(event.getDetails().getUser()));
        if (!user_event.isEntityFound() ||
                !user_event.getDetails().getName().equals(event.getAuthenticated_user())) {
            return PreferenceUpdatedEvent.notUpdated(event.getDetails());
        }

        PreferenceUpdatedEvent update_event = persistenceService.updatePreference(event);
        return update_event;
    }

    @Override
    public PreferenceDeletedEvent deletePreference(DeletePreferenceEvent event) {
        LOG.info("Deleting preference: " + event.getDetails().getId().toString());

        UserDetailsEvent user_event = userService.getUserDetails(new RequestUserDetailsEvent(event.getDetails().getUser()));
        if (!user_event.isEntityFound() ||
                !user_event.getDetails().getName().equals(event.getAuthenticated_user())) {
            return PreferenceDeletedEvent.notAuthorized(event.getDetails());
        }

        PreferenceDeletedEvent delete_event = persistenceService.deletePreference(event);
        return delete_event;
    }

}
