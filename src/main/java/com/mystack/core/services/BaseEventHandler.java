package com.mystack.core.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mystack.core.domain.*;
import com.mystack.core.events.mentions.request.CreateMentionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;

public class BaseEventHandler {
    private static Logger LOG = LoggerFactory.getLogger(BaseEventHandler.class);

    /**
     * Check if a user owns a stack by providing his name
     *
     * @param stack_id
     * @param checked_user
     * @param stack_service
     * @param user_service
     * @return
     */
    protected StackUserOwnerChecker checkIfOwner(UUID stack_id, String checked_user, StackService stack_service, UserService user_service) {
        return checkIfOwnerAndChecker(stack_id, checked_user, null, stack_service, user_service);
    }

    /**
     * Check if a user owns a stack with his ID
     *
     * @param stack_id
     * @param checked_user
     * @param stack_service
     * @param user_service
     * @return
     */
    protected StackUserOwnerChecker checkIfOwnerAndChecker(UUID stack_id, UUID checked_user, StackService stack_service, UserService user_service) {
        return checkIfOwnerAndChecker(stack_id, "", checked_user, stack_service, user_service);
    }

    /*
     * Checking if a user is owning a stack
     * if username is provided then it is used first. else if provided the userid is used
     */
    private StackUserOwnerChecker checkIfOwnerAndChecker(UUID stack_id, String checked_username, UUID checked_userid, StackService stack_service, UserService user_service) {
        if ((null == checked_username || "".equals(checked_username)) && null == checked_userid) {
            return new StackUserOwnerChecker(null, null, false, false);
        }

        StackDetailsEvent stack_details_event = stack_service.getStackDetails(new RequestStackDetailsEvent(stack_id));

        if (null == stack_details_event || !stack_details_event.isEntityFound()) {
            LOG.info("Stack not found!");
            return new StackUserOwnerChecker(null, null, false, false);
        }

        LOG.info("Stack found! ");
        StackDetails stack_details = stack_details_event.getDetails();

        // get user to be checked
        RequestUserDetailsEvent request_user_event;
        if (!"".equals(checked_username)) {
            request_user_event = new RequestUserDetailsEvent(checked_username);

        } else {
            request_user_event = new RequestUserDetailsEvent(checked_userid);
        }
        UserDetailsEvent result_authenticated_user = user_service.getUserDetails(new RequestUserDetailsEvent(checked_username));

        if (null == result_authenticated_user || !result_authenticated_user.isEntityFound()) {
            LOG.info("Authenticated User: " + result_authenticated_user.getDetails().getName() + " not found...");
            return new StackUserOwnerChecker(stack_details, null, false, false);
        }
        UserDetails authenticated_user_details = result_authenticated_user.getDetails();

        LOG.info("Checking ownership of stack: " + stack_id.toString() + " by auth user: " + result_authenticated_user.getDetails().toString());

        boolean is_owner = result_authenticated_user.getDetails().getList_stack().contains(stack_id);
        boolean is_checker = stack_details.getCheckers() != null && stack_details.getCheckers().contains(result_authenticated_user.getDetails().getId());

        LOG.info("Authenticated user is owner: " + is_owner + ", is_checker: " + is_checker);
        return new StackUserOwnerChecker(stack_details, authenticated_user_details, is_owner, is_checker);

    }

    void processMentions(CommentDetails details, String authenticatedUser, UserService userService,
                         MentionService mentionService) {

        String unProcessedText = details.getText();
        Pattern pattern = Pattern.compile("@(\\w+)");
        Matcher matcher = pattern.matcher(unProcessedText);
        List<String> allMentions = new ArrayList<>();
        while (matcher.find()) {
            allMentions.add(matcher.group(1));
        }

        UserDetailsEvent fromUser = userService.getUserDetails(new RequestUserDetailsEvent(authenticatedUser));

        allMentions.stream().forEach(mention -> {
            UserDetailsEvent mentionedUser = userService.getUserDetails(new RequestUserDetailsEvent(mention));
            if (mentionedUser.isEntityFound()) {
                mentionService.createMention(new CreateMentionEvent(new MentionDetails(UUID.randomUUID(),
                                                                    fromUser.getDetails().getId(),
                                                                    mentionedUser.getDetails().getId(),
                                                                    details.getId())));
            }
        });


    }

    /**
     * Class holding result from checkIfOwner
     *
     * @author dzd
     */
    class StackUserOwnerChecker {
        private StackDetails stack;
        private UserDetails user;
        private boolean isOwner;
        private boolean isChecker;

        public StackUserOwnerChecker(StackDetails stack, UserDetails user, boolean isOwner, boolean isChecker) {
            this.stack = stack;
            this.user = user;
            this.isOwner = isOwner;
            this.isChecker = isChecker;
        }

        public StackDetails getStack() {
            return stack;
        }

        public UserDetails getUser() {
            return user;
        }

        public boolean isOwner() {
            return isOwner;
        }

        public boolean isChecker() {
            return isChecker;
        }

    }

    /**
     * Check is a check is commentable
     *
     * @param details
     * @param authenticated_user
     * @param user_service
     * @param stack_service
     * @param check_service
     * @return
     */
    public boolean checkCommentable(CommentDetails details, String authenticated_user, boolean check_editable, UserService user_service, StackService stack_service, CheckService check_service) {

        UUID stackid = details.getStackid();
        CheckDetailsEvent check_details = check_service.getCheckDetails(new RequestCheckDetailsEvent(details.getCheckid()));
        if (!check_details.getDetails().getStack().equals(stackid)) {
            LOG.warn("Stack provided with event not matching stack of the check: " + check_details.getDetails().getStack() + " != " + stackid);
            return false;
        }

        // 2) are user and stack associated ?
        UserDetailsEvent stackuser = user_service.getUserDetails(new RequestUserDetailsEvent(details.getStack_userid()));
        //        if (!stackuser.getDetails().getList_stack().contains(stackid)) {
        //            LOG.warn("Stack is not owned by the User provided: stack:" + stackid + ", user: " + stackuser.getDetails().getId());
        //            return false;
        //        }

        // 3) is stack public ? or is the user owner ?
        StackDetailsEvent stack_details = stack_service.getStackDetails(new RequestStackDetailsEvent(stackid, authenticated_user));
        if (stack_details.getDetails().getIsPrivate()
                && !stackuser.getDetails().getName().equals(authenticated_user)) {
            LOG.warn("Stack is private and user not owner...");
            return false;
        }
        if (!check_editable) {

            return true;
        }
        // check if editable now => comment onwer == auth user
        UserDetailsEvent commentuser = user_service.getUserDetails(new RequestUserDetailsEvent(details.getComment_userid()));
        if (!commentuser.getDetails().getName().equals(authenticated_user)) {
            return false;
        }
        return true;
    }

    //TODO: move in more general part of the project
    public class NamedObject<T> {
        public final String name;
        public final T object;

        public NamedObject(String name, T object) {
            this.name = name;
            this.object = object;
        }
    }

}
