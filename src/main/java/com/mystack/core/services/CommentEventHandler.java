package com.mystack.core.services;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.config.FunctionalConfiguration;
import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.comments.request.CreateCommentEvent;
import com.mystack.core.events.comments.request.DeleteCommentEvent;
import com.mystack.core.events.comments.request.RequestAllCommentsEvent;
import com.mystack.core.events.comments.request.RequestCommentDetailsEvent;
import com.mystack.core.events.comments.request.UpdateCommentEvent;
import com.mystack.core.events.comments.result.AllCommentsEvent;
import com.mystack.core.events.comments.result.CommentCreatedEvent;
import com.mystack.core.events.comments.result.CommentDeletedEvent;
import com.mystack.core.events.comments.result.CommentDetailsEvent;
import com.mystack.core.events.comments.result.CommentUpdatedEvent;
import com.mystack.persistence.services.CommentPersistenceService;

public class CommentEventHandler extends BaseEventHandler implements CommentService {
    private static Logger LOG = LoggerFactory.getLogger(CommentEventHandler.class);

    CommentPersistenceService persistenceService;
    StackService stackService;
    UserService userService;
    CheckService checkService;
    MentionService mentionService;

    public CommentEventHandler(CommentPersistenceService service,
                               StackService stackService,
                               UserService userService,
                               CheckService checkService,
                               MentionService mentionService) {
        this.persistenceService = service;
        this.stackService = stackService;
        this.userService = userService;
        this.checkService = checkService;
        this.mentionService = mentionService;
    }

    @Override
    public CommentCreatedEvent createComment(CreateCommentEvent event) {
        CommentDetails details = event.getDetails();

        boolean isCommentable = checkCommentable(details, event.getAuthenticated_user(), false, userService, stackService, checkService);
        if (!isCommentable) {
            return CommentCreatedEvent.notAuthorized(details);
        }

        LOG.info("Creating comment: " + details);
        CommentCreatedEvent commentCreated = persistenceService.createComment(event);
        processMentions(commentCreated.getDetails(), event.getAuthenticated_user(), userService, mentionService);

        return commentCreated;
    }

    @Override
    public CommentUpdatedEvent updateComment(UpdateCommentEvent event) {
        CommentDetails details = event.getCommentDetails();
        boolean isCommentable_and_editable = checkCommentable(details, event.getAuthenticated_user(), true, userService, stackService, checkService);
        if (!isCommentable_and_editable) {
            return CommentUpdatedEvent.notUpdated(details);
        }

        return persistenceService.updateComment(event);
    }

    @Override
    public CommentDeletedEvent deleteComment(DeleteCommentEvent event) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AllCommentsEvent getAllComments(RequestAllCommentsEvent event) {
        LOG.info("Event received in core service: " + event.toString());

        setCommentCountAndBeforeDate(event);

        return persistenceService.getAllComments(event);
    }

    @Override
    public AllCommentsEvent getAllCommentsFromChecks(RequestAllCommentsEvent event) {
        LOG.info("Event received in core service: " + event.toString());

        setCommentCountAndBeforeDate(event);

        return persistenceService.getAllCommentsFromChecks(event);
    }

    @Override
    public CommentDetailsEvent getCommentDetails(RequestCommentDetailsEvent event) {
        return persistenceService.getCommentDetails(event);
    }

    private void setCommentCountAndBeforeDate(RequestAllCommentsEvent event) {
        // check the count provided and date in event
        if (event.getCount() <= 0 || event.getCount() >= FunctionalConfiguration.COMMENT_MAX_GET_COUNT) {
            event.setCount(FunctionalConfiguration.COMMENT_MAX_GET_COUNT);
        }
        if (null == event.getBefore_date()) {
            event.setBefore_date(new Date());
        }
    }
}
