package com.mystack.core.services;

import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;

public interface PreferenceService {

    public PreferenceCreatedEvent createPreference(CreatePreferenceEvent event);

    public PreferenceDeletedEvent deletePreference(DeletePreferenceEvent event);

    public PreferenceReadEvent readPreference(ReadPreferenceEvent event);

    public PreferenceUpdatedEvent updatePreference(UpdatePreferenceEvent event);
}
