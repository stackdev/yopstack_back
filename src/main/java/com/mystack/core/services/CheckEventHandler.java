package com.mystack.core.services;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.apache.catalina.authenticator.SpnegoAuthenticator.AuthenticateAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mystack.config.FunctionalConfiguration;
import com.mystack.config.FunctionalConfiguration.ActionType;
import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.core.domain.CheckCount;
import com.mystack.core.domain.CheckDetails;
import com.mystack.core.domain.Stack;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.checks.request.CreateCheckEvent;
import com.mystack.core.events.checks.request.DeleteCheckEvent;
import com.mystack.core.events.checks.request.RequestAllChecksEvent;
import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.request.RequestLastChecksEvent;
import com.mystack.core.events.checks.request.UpdateCheckEvent;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.core.events.checks.result.CheckCreatedEvent;
import com.mystack.core.events.checks.result.CheckDeletedEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.events.checks.result.CheckUpdatedEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.persistence.services.CheckPersistenceService;
import com.mystack.utils.Pair;

public class CheckEventHandler extends BaseEventHandler implements CheckService {
    private static Logger   LOG = LoggerFactory.getLogger(CheckEventHandler.class);

    CheckPersistenceService persistenceService;
    StackService            stack_service;

    @Autowired
    UserService             user_service;

    public CheckEventHandler(CheckPersistenceService service, StackService stack_service) {
        this.persistenceService = service;
        this.stack_service = stack_service;
    }

    /**
     * Create a check, validating that the stack exists
     */
    @Override
    public CheckCreatedEvent createCheck(CreateCheckEvent event) {
        CheckDetails inputCheckDetails = event.getDetails();
        UUID stackId = inputCheckDetails.getStack();

        StackUserOwnerChecker ownerResult = checkIfOwner(stackId, event.getAuthenticated_user(), stack_service, user_service);

        if (null == ownerResult.getStack()) {
            LOG.info("Stack not found!");
            return CheckCreatedEvent.creationFailed(inputCheckDetails);
        } else if (null == ownerResult.getUser()) {
            LOG.info("User not found, check creation blocked");
            return CheckCreatedEvent.notAuthorized(inputCheckDetails);
        } else if (!ownerResult.isOwner() && !ownerResult.isChecker()) {
            LOG.info("User found but not owner or checker of the stack, check creation blocked");
            return CheckCreatedEvent.notAuthorized(inputCheckDetails);
        }
        // forcing the checker to the authenticated  user
        inputCheckDetails.setChecker(ownerResult.getUser().getId());

        if(!isCheckStatusValid(inputCheckDetails)) {
        	LOG.warn("Check not created, status and date are not valid");
        	return CheckCreatedEvent.creationFailed(inputCheckDetails);
        }
        
        StackDetails stackDetails = ownerResult.getStack();

        identifyCheckPeriodDiff(stackDetails);

        CheckCreatedEvent checkResult = persistenceService.createCheck(event);

        if (!checkResult.isCreated()) {
            LOG.warn("Check not created");
            return CheckCreatedEvent.creationFailed(inputCheckDetails);
        }
        
    	CheckStatus status = checkResult.getDetails().getStatus();
    	Date checkDate = checkResult.getDetails().getDate();
    	StackDetails stackForUpdate = Stack.fromDetails(ownerResult.getStack()).toDetails();
    	
		stackForUpdate = getStackCounterUpdateNeededAfterCreateOrDelete(status, ownerResult.getStack(), ActionType.CREATE);
    	stackForUpdate = getStackUpdateRequiredForLastCheckDateAfterCreateOrDelete(ownerResult.getStack(), status, checkDate, ActionType.CREATE);
		StackUpdatedEvent stackUpdatedEvent = stack_service.updateStack(new UpdateStackEvent(stackForUpdate, event.getAuthenticated_user()));
        if (stackUpdatedEvent.isUpdated()) {
            LOG.info("Stack updated sucessfully.");
        }
        return checkResult;
        
    }

    private boolean isCheckStatusValid(CheckDetails check) {
    	final Date now = new Date();
		final boolean isInThePast = check.getDate().before(now);
    	
		if (check.getStatus() == CheckStatus.PLANNED && isInThePast ) {
    		LOG.error("Invalid check status 'PLANNED' and a past date");
    		return false;
    	}
    	
    	if (check.getStatus() == CheckStatus.OK && !isInThePast ) {
    		LOG.error("Invalid check status 'OK' and a future date");
    		return false;
    	}
    	return true;
	}

	private void identifyCheckPeriodDiff(final StackDetails stackDetails) {
        final Date currentCheckDate = new Date();
        //check last_check properties and creation_date and periodicity
        final int currentStackPeriod = getStackPeriod(currentCheckDate, stackDetails.getCreation_date(), stackDetails.getPeriodicity());
        final int lastCheckPeriod = getStackPeriod(stackDetails.getLast_check(), stackDetails.getCreation_date(), stackDetails.getPeriodicity());
        int periodDiff = currentStackPeriod - lastCheckPeriod;
        
        // for the first check set period_diff to 1
        if (null == stackDetails.getLast_check()) {
            periodDiff = 1;
        }

        if (periodDiff < 1 && periodDiff > 0) {
            // get begin and end date for the period
            Pair<Date, Date> begin_end_date = getBeginAndEndDatePeriod(stackDetails.getCreation_date(), currentStackPeriod, stackDetails.getPeriodicity());
            // get number of check already done in this period 
            AllChecksEvent check_in_period = persistenceService.getAllChecks(new RequestAllChecksEvent(stackDetails.getId(), begin_end_date.b, begin_end_date.a));

            int nbCheckInPeriod = 0;
            if (null != check_in_period.getAllCheckDetails()) {
                nbCheckInPeriod = check_in_period.getAllCheckDetails().size();
            }

            LOG.info("Nb of check already done for this period: " + nbCheckInPeriod);
            // compare with freqency
            LOG.error("Stack already checked for this period: " + currentStackPeriod + ", stack_details: " + stackDetails);
            //FIXME:return CheckCreatedEvent.creationFailed(input_check_details);
        }

        if (periodDiff == 1) {
            LOG.info("Correct period to check, score should be increased");
            //TODO:score needs to be incremented
        } else {
            //TODO:score needs to be decremented
            LOG.info("Correct period to check, number of missed checked: " + periodDiff + ", score should be decreased...");
        }		
	}

	@Override
    public CheckDeletedEvent deleteCheck(DeleteCheckEvent event) {

        //retrieve check to get the stack id
        CheckDetailsEvent check_to_be_deleted = persistenceService.getCheckDetails(new RequestCheckDetailsEvent(event.getId()));
        if (null == check_to_be_deleted || !check_to_be_deleted.isEntityFound()) {
            LOG.info("Check not found... no deletion...");
            return CheckDeletedEvent.notFound(new CheckDetails(event.getId()));
        }

        StackUserOwnerChecker owner_result = checkIfOwner(check_to_be_deleted.getDetails().getStack(), event.getAuthenticated_user(), stack_service, user_service);

        if (null == owner_result.getStack()) {
            LOG.info("Stack not found!");
            return CheckDeletedEvent.notFound(event.getId());
        } else if (null == owner_result.getUser()) {
            LOG.info("User not found, check creation blocked");
            return CheckDeletedEvent.notAuthorized(check_to_be_deleted.getDetails());
        } else if (!owner_result.isOwner()) {
            LOG.info("User found but not owner of the stack, check deletion blocked");
            return CheckDeletedEvent.notAuthorized(check_to_be_deleted.getDetails());
        }

        return persistenceService.deleteCheck(event);
    }

    @Override
    public CheckUpdatedEvent updateCheck(UpdateCheckEvent event) {

        final StackUserOwnerChecker owner_result = checkIfOwner(event.getCheckDetails().getStack(), 
        						                          event.getAuthenticated_user(),
        						                          stack_service, user_service);

        if (null == owner_result.getStack()) {
            LOG.info("Stack not found!");
            return CheckUpdatedEvent.notFound(event.getCheckDetails());
        } else if (null == owner_result.getUser()) {
            LOG.info("User not found, check update blocked");
            return CheckUpdatedEvent.notUpdated(event.getCheckDetails());
        } else if (!owner_result.isOwner() && !owner_result.isChecker()) {
            LOG.info("User found but not owner nor checker of the stack, check update blocked");
            return CheckUpdatedEvent.notUpdated(event.getCheckDetails());
        }

        CheckUpdatedEvent updateResult = persistenceService.updateCheck(event);
        
        if(!updateResult.isUpdated()) {
        	return CheckUpdatedEvent.notUpdated(event.getCheckDetails());
        }
		if (!updateResult.getBeforeUpdateCheckDetails().isPresent()) {
			LOG.warn("Missing previous check details in check update result, not updating the stack..." + updateResult);
			
		} else {

			CheckDetails updatedCheck = updateResult.getBeforeUpdateCheckDetails().get();
			CheckStatus status = updateResult.getDetails().getStatus();
			CheckStatus previousStatus = updatedCheck.getStatus();
			LocalDateTime previousDate = LocalDateTime.ofInstant(updatedCheck.getDate().toInstant(), ZoneId.systemDefault());
			LocalDateTime newDate = LocalDateTime.ofInstant(updateResult.getDetails().getDate().toInstant(), ZoneId.systemDefault());
			StackDetails stackForUpdate = getStackCounterUpdateNeededAfterUpdate(status, previousStatus, owner_result.getStack());
			//TODO: add the missing call to the last check update...
			stackForUpdate = getStackUpdateRequiredForLastCheckDateAfterUpdate(stackForUpdate, previousStatus, status, previousDate, newDate);
			stack_service.updateStack(new UpdateStackEvent(stackForUpdate, event.getAuthenticated_user()));
		}
        return updateResult;
    }

    StackDetails getStackCounterUpdateNeededAfterCreateOrDelete(final CheckStatus status, final StackDetails stack, ActionType action) {
    	return getStackCounterUpdateNeeded(status, status, stack, action);
    }
    
    StackDetails getStackCounterUpdateNeededAfterUpdate(final CheckStatus newStatus, final CheckStatus oldStatus, final StackDetails stack) {
    	return getStackCounterUpdateNeeded(newStatus, oldStatus, stack, ActionType.UPDATE);
    }

    
	StackDetails getStackCounterUpdateNeeded(final CheckStatus newStatus, final CheckStatus oldStatus,
			                                         final StackDetails stack, ActionType action) {

		StackDetails stackForUpdate = Stack.fromDetails(stack).toDetails();
		CheckCount currentCount = stackForUpdate.getCheckCount();
		if (null == currentCount) {
			currentCount = new CheckCount();
		}

		if (ActionType.CREATE.equals(action)) {
			currentCount.increment(newStatus);
		} else if(ActionType.DELETE.equals(action)) {
			currentCount.decrement(newStatus);
		} else if (ActionType.UPDATE.equals(action)) {
			if (!newStatus.equals(oldStatus)) {
				currentCount.decrement(oldStatus);
				currentCount.increment(newStatus);
			}
			
		}

		stackForUpdate.setCheckCount(currentCount);

		return stackForUpdate;

	}
	
	
	StackDetails getStackUpdateRequiredForLastCheckDateAfterCreateOrDelete(StackDetails stack, final CheckStatus status,
			final Date checkDate, final ActionType action) {

		if ((!CheckStatus.OK.equals(status) && !CheckStatus.PLANNED.equals(status))) {
			return stack;
		}

		if (!stack.getLastCheckDate(status).isPresent()) {
			CheckDetailsEvent latestCheck = persistenceService
					.getLatestCheck(new RequestLastChecksEvent(stack.getId(), status));
			stack.setLastCheckDate(latestCheck.getDetails().getDate(), status);
		} else {

			Date lastCheckDate = stack.getLastCheckDate(status).get();

			if (ActionType.CREATE.equals(action)) {
				if (lastCheckDate.before(checkDate)) {
					stack.setLastCheckDate(checkDate, status);
				}
			} else if (ActionType.DELETE.equals(action)) {
				if (lastCheckDate.equals(checkDate)) {
					CheckDetailsEvent latestCheck = persistenceService
							.getLatestCheck(new RequestLastChecksEvent(stack.getId(), status));
					stack.setLastCheckDate(latestCheck.getDetails().getDate(), status);
				}
			}
		}
		return stack;
	}
	
	
	public StackDetails getStackUpdateRequiredForLastCheckDateAfterUpdate(StackDetails stack, CheckStatus oldStatus,
			CheckStatus newStatus, LocalDateTime oldCheckDate, LocalDateTime newCheckDate) {
		
		// old or new status is planned, get planned date from persistence
		CheckDetailsEvent latestPlannedCheck;
		CheckDetailsEvent latestOkCheck;
		if(CheckStatus.PLANNED.equals(oldStatus) || CheckStatus.PLANNED.equals(newStatus)) {
			latestPlannedCheck = persistenceService.getLatestCheck(new RequestLastChecksEvent(stack.getId(), CheckStatus.PLANNED));
			stack.setLastCheckDate(latestPlannedCheck.getDetails().getDate(), latestPlannedCheck.getDetails().getStatus());
		}
		if(CheckStatus.OK.equals(oldStatus) || CheckStatus.OK.equals(newStatus)) {
			latestOkCheck = persistenceService.getLatestCheck(new RequestLastChecksEvent(stack.getId(), CheckStatus.OK));
			stack.setLastCheckDate(latestOkCheck.getDetails().getDate(), latestOkCheck.getDetails().getStatus());
		}
		
		
		
		return stack;
	}
    	
 

	@Override
    public AllChecksEvent getAllChecks(RequestAllChecksEvent event) {

        LOG.info("Event received in core service: " + event.toString());

        // check the count provided and date in event
        if (event.getCount() <= 0 || event.getCount() >= FunctionalConfiguration.CHECK_MAX_GET_COUNT) {
            event.setCount(FunctionalConfiguration.CHECK_MAX_GET_COUNT);
        }
        if (null == event.getBefore_date()) {
            event.setBefore_date(new Date());
        }

        return persistenceService.getAllChecks(event);
    }

    @Override
    public CheckDetailsEvent getCheckDetails(RequestCheckDetailsEvent event) {
        return persistenceService.getCheckDetails(event);
    }

    // ----------- private stuff 

    private int getStackPeriod(Date date, Date creation_date, int periodicity_day) {

        if (null == date) {
            LOG.info("No Date provided returning period -1");
            return -1;
        }

        if (null == creation_date) {
            LOG.info("No creation_date provided returning period -1");
            return -1;
        }
        // reference date
        Calendar reference_cal = Calendar.getInstance();
        reference_cal.setTime(creation_date);
        LOG.info("reference date epoch ms: " + reference_cal.getTime());

        // date to be checked
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        LOG.info("date to be checked epoch ms: " + reference_cal.getTime());

        // periodicity in ms
        long periodicity_ms = periodicity_day * 24 * 3600 * 1000;

        long date_diff_ms = cal.getTimeInMillis() - reference_cal.getTimeInMillis();
        float period = (date_diff_ms) / periodicity_ms;

        LOG.info("Date diff in ms: " + date_diff_ms);
        LOG.info("The period for date: " + cal.toString());
        LOG.info(" is: " + period);

        return (int) period;
    }

    private Pair<Date, Date> getBeginAndEndDatePeriod(Date initial_date, int period, int periodicity) {
        if (null == initial_date) {
            LOG.info("No Date provided returning period -1");
            return Pair.makePair(null, null);
        }

        // reference date
        Calendar reference_cal = Calendar.getInstance();
        reference_cal.setTime(initial_date);
        reference_cal.add(Calendar.DATE, periodicity * period);
        Date period_begin = reference_cal.getTime();

        reference_cal.add(Calendar.DATE, periodicity);
        Date period_end = reference_cal.getTime();

        LOG.info("Period begin: " + period_begin + ", Period_end: " + period_end);
        return Pair.makePair(period_begin, period_end);
    }

    private boolean checkerIsStackOwner(String authenticated_user, StackDetails stack) {
        if (null == authenticated_user) {
            return false;
        }
        // get user owner of the stack 
        RequestUserDetailsEvent requestUserEvent = new RequestUserDetailsEvent(authenticated_user);
        UserDetailsEvent result_authenticated_user = user_service.getUserDetails(requestUserEvent);

        if (null == result_authenticated_user || !result_authenticated_user.isEntityFound()) {
            LOG.info("Authenticated User not found...");
            return false;
        }
        if (result_authenticated_user.getDetails().getList_stack().contains(stack.getId())) {
            LOG.info("Authenticated user owns the stack");
            return true;
        }

        return false;

    }

}
