package com.mystack.core.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mystack.config.FunctionalConfiguration;
import com.mystack.core.domain.CheckCount;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksByIdEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksFromUserEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsFromUserEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.AllStacksByIdEvent;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.core.events.stacks.result.AllStacksFromUserEvent;
import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.persistence.services.StackPersistenceService;

public class StackEventHandler extends BaseEventHandler implements StackService {
    private static Logger   LOG = LoggerFactory.getLogger(StackEventHandler.class);
    StackPersistenceService persistence_service;

    //Use autowired instead of constructor to avoid circular dependencies exceptions
    @Autowired
    UserService             user_service;

    public StackEventHandler(StackPersistenceService service) {
        this.persistence_service = service;
    }

    @Override
    public StackCreatedEvent createStack(CreateStackEvent event) {
        UUID requestedUser = event.getRequestingUser();
        LOG.info("Requesting user details for: " + requestedUser.toString());

        UserDetailsEvent userEvent = user_service.getUserDetails(new RequestUserDetailsEvent(requestedUser));

        // check if user exists
        if (!userEvent.isEntityFound()) {
            LOG.info("Stack creation attempt canceled: inexisting user: " + requestedUser.toString());
            return StackCreatedEvent.creationFailed(event.getDetails());
        }

        // check if user and auth_user are the same
        if (null == event.getAuthenticated_user() || !userEvent.getDetails().getName().equals(event.getAuthenticated_user())) {
            LOG.info("Stack creation attempt canceled: auth_user != stack owner :" + userEvent.getDetails().getName() + ", " + event.getAuthenticated_user());
            return StackCreatedEvent.creationFailed(event.getDetails());
        }

        // create the stack -- update creation date
        StackDetails stackDetails = event.getDetails();
        stackDetails.setCreation_date(new Date());
        if (stackDetails.getFrequency() <= 0) {
            stackDetails.setFrequency(FunctionalConfiguration.DEFAULT_STACK_FREQUENCY);
        }
        if (stackDetails.getPeriodicity() <= 0) {
            stackDetails.setPeriodicity(FunctionalConfiguration.DEFAULT_STACK_PERIODICITY);
        }
        // if the stack is shared the creator must be in admins and checkers list
        if (stackDetails.getIsShared()) {
            stackDetails.addAdmin(event.getRequestingUser());
            stackDetails.addChecker(event.getRequestingUser());
        }
        // force checkCount initialization
        stackDetails.setCheckCount(new CheckCount());
        LOG.info("Creating stack: " + stackDetails);

        StackCreatedEvent stackEvent = persistence_service.createStack(new CreateStackEvent(stackDetails, event.getRequestingUser()));

        // stack creation failed
        if (!stackEvent.isCreated()) {
            return stackEvent;
        }

        UserDetails user_to_update = userEvent.getDetails();
        user_to_update.add_stack(stackEvent.getId());
        LOG.info("User before update: " + user_to_update.toString());
        // else add the stack to the user stack list
        UserUpdatedEvent user_update_event = user_service.updateUser(new UpdateUserEvent(user_to_update, event.getAuthenticated_user()));
        if (!user_update_event.isUpdated()) {
            LOG.warn("Stack creation failed as User was not updated..., stack ID: " + stackEvent.getDetails().getId() + ", User ID: " + user_update_event.getDetails().getId());
            return StackCreatedEvent.creationFailed(stackEvent.getDetails());
        }
        LOG.info("User after update: " + user_update_event.getDetails().toString());
        return stackEvent;
    }

    @Override
    public StackDeletedEvent deleteStack(DeleteStackEvent event) {
        UUID requestedUser = event.getRequestingUser();
        LOG.info("Requesting user details for: " + requestedUser.toString());
        UserDetailsEvent userEvent = user_service.getUserDetails(new RequestUserDetailsEvent(requestedUser));
        // check if user exists
        if (!userEvent.isEntityFound()) {
            LOG.info("Stack deletion attempt canceled: inexisting user: " + requestedUser.toString());
            return StackDeletedEvent.notFound(new StackDetails(event.getId()));
        }

        // User found check if it is the same as the authentificated one
        if (!userEvent.getDetails().getName().equals(event.getAuthenticated_user())) {
            LOG.info("User found: " + requestedUser.toString() + ", not the one authenticated: " + event.getAuthenticated_user());
            return StackDeletedEvent.notAuthorized(new StackDetails(event.getId()));

        }

        // User found, check if he own the stack
        if (!userEvent.getDetails().getList_stack().contains(event.getId())) {
            LOG.info("User found: " + requestedUser.toString() + ", but not owning the stack: " + event.getId());
            return StackDeletedEvent.notFound(new StackDetails(event.getId()));
        }

        // delete the stack
        StackDeletedEvent stackEvent = persistence_service.deleteStack(event);

        // stack deletion failed
        if (!stackEvent.isDeletionCompleted()) {
            return stackEvent;
        }

        UserDetails user_to_update = userEvent.getDetails();
        user_to_update.remove_stack(stackEvent.getId());
        LOG.info("User before update: " + user_to_update.toString());
        // else add the stack to the user stack list
        UserUpdatedEvent user_update_event = user_service.updateUser(new UpdateUserEvent(user_to_update, event.getAuthenticated_user()));
        if (!user_update_event.isUpdated()) {
            LOG.warn("Stack deletion failed as User was not updated..., stack ID: " + stackEvent.getDetails().getId() + ", User ID: " + user_update_event.getDetails().getId());
            return StackDeletedEvent.deletionFailed(stackEvent.getDetails());
        }
        LOG.info("User after update: " + user_update_event.getDetails().toString());
        return stackEvent;
    }

    /**
     * Return all the stack related to the user provided in the RequestAllStack event
     */
    @Override
    @Deprecated
    public AllStacksEvent getAllStacks(RequestAllStacksEvent event) {
        //1) list of user in expected in the RequestAllStacksEvent
        if (!event.isUseridProvided()) {
            //TODO: remove this, we keep the current behavior for compatibility:
            // without userid specified we return all the stacks...
            return persistence_service.getAllStacks(event);
        }
        List<UUID> requested_User = event.getUserid_list();
        LOG.info("List of UUID requested:" + requested_User);
        AllStacksEvent workStackEvent;
        //2) UserService is called for all user Id
        List<UserDetails> userDetailsList = user_service.getAllUsers(new RequestAllUsersEvent(requested_User)).getAllUserDetails();
        LOG.info("User service returned: " + userDetailsList.toString());

        // Get list of UUID from UserDetailsList 
        List<UUID> requested_user_uid = new ArrayList<>();
        for (UserDetails user_details : userDetailsList) {
            requested_user_uid.add(user_details.getId());
        }

        LOG.info("Calling Persistence layer with list of requested uid: " + requested_user_uid);
        //3) StackPersistence is called with an updated RequestAllStacksEvent containing StackIds

        AllStacksEvent out_event = persistence_service.getAllStacks(new RequestAllStacksEvent(userDetailsList.get(0).getList_stack(), true, false));

        LOG.info("AllStacksEvent: " + out_event.toString());
        return out_event;

    }

    @Override
    public AllStacksEvent getAllCheckableStacks(RequestAllStacksEvent event) {
        return persistence_service.getAllCheckableStacks(event);
    }

    @Override
    public StackDetailsEvent getStackDetails(RequestStackDetailsEvent event) {
        return persistence_service.getStackDetails(event);
    }

    public StackDetailsEvent getStackDetails(RequestStackDetailsFromUserEvent event) {

        UUID user_id = event.getUserId();
        UUID stack_id = event.getStackId();

        // check user in path existence
        UserDetailsEvent userDetailsEvent = user_service.getUserDetails(new RequestUserDetailsEvent(user_id));
        if (userDetailsEvent.isEntityFound() == false) {
            return StackDetailsEvent.notFound(stack_id);
        }

        // check user ownership of the requested stack
        //        if (!userDetailsEvent.getDetails().getList_stack().contains(stack_id)) {
        //            return StackDetailsEvent.notFound(stack_id);
        //        }

        StackUserOwnerChecker result = checkIfOwner(stack_id, event.getAuthenticated_user(), this, user_service);

        StackDetails result_stack = result.getStack();
        if (null != result_stack) {
            if (!result_stack.getIsPrivate() || result.isOwner() || result.isChecker()) {
                return new StackDetailsEvent(result_stack);
            }
        }
        return StackDetailsEvent.notFound(stack_id);
    }

    @Override
    public AllStacksFromUserEvent getAllStacks(RequestAllStacksFromUserEvent event) {
        UUID uid = event.getRequested_user();
        LOG.info("Getting all stacks for user: " + uid.toString());

        // 1) get user details list from user service
        UserDetailsEvent userDetailsEvent = user_service.getUserDetails(new RequestUserDetailsEvent(uid));
        // User not found
        if (!userDetailsEvent.isEntityFound()) {
            LOG.info("No info found for user : " + uid.toString());
            return AllStacksFromUserEvent.NotFound(new UserDetails(uid));
        }
        // Call the persistence
        UserDetails userDetails = userDetailsEvent.getDetails();

        //if user is different than authenticated one, request only public stacks
        RequestAllStacksByIdEvent request_event = new RequestAllStacksByIdEvent(userDetails.getList_stack(), event.getAuthenticated_user());
        if (!event.getAuthenticated_user().equals(userDetails.getName())) {
            request_event.setOnly_public(true);
        }
        List<StackDetails> all_stack_details = new ArrayList<>();

        AllStacksByIdEvent allstackByIdEvent = persistence_service.getAllStacks(request_event);
        if (allstackByIdEvent.getStackDetailsList().size() > 0) {
            all_stack_details.addAll(allstackByIdEvent.getStackDetailsList());
        }
        // FIXME: this is just an experiment on how to use the get all checkable stacks
        UserDetailsEvent auth_user_event = user_service.getUserDetails(new RequestUserDetailsEvent(event.getAuthenticated_user()));
        if (auth_user_event.isEntityFound()) {
            RequestAllStacksEvent request_all_stack_event = new RequestAllStacksEvent();
            List<UUID> user_id_list = new ArrayList<>();
            user_id_list.add(auth_user_event.getDetails().getId());
            request_all_stack_event.setUserid_list(user_id_list);
            AllStacksEvent checkable_event = persistence_service.getAllCheckableStacks(request_all_stack_event);
            LOG.debug(checkable_event.toString());
            
            if (checkable_event.getAllStackDetails().size() > 0)
                all_stack_details.addAll(checkable_event.getAllStackDetails());
            	all_stack_details = all_stack_details.stream()
            	.distinct()
            	.collect(Collectors.toList());
        }
        /////////////////
        if (all_stack_details.isEmpty()) {
            LOG.info("No stack found!");
            return AllStacksFromUserEvent.NotFound(userDetails);
        }

        // convert Event from AllStacksBy Id to AllStacksFromUser
        AllStacksFromUserEvent resultEvent = new AllStacksFromUserEvent(all_stack_details);
        return resultEvent;
    }

    private boolean isStackCheckable(StackDetails stack, Date check_date) {
        // 
        return false;
    }

    @Override
    public StackUpdatedEvent updateStack(UpdateStackEvent event) {
        // TODO purge fields which we don't want to update before giving the stack to the persistence
        //FIXME: temporary: handle here the list of fields that we want to be updatable
        // last_check
        // last_check_count 
        // title 
        // description 

        // check if user is owner or checker of the stack 
        StackUserOwnerChecker result = checkIfOwner(event.getStackDetails().getId(), event.getAuthenticated_user(), this, user_service);

        if (!result.isOwner() && !result.isChecker()) {
            LOG.info("User is not owner nor checker, not updating the stack");
            return StackUpdatedEvent.notUpdated(event.getStackDetails());
        }
        return persistence_service.updateStack(event);
    }
}
