package com.mystack.core.services;

import com.mystack.core.events.comments.request.CreateCommentEvent;
import com.mystack.core.events.comments.request.DeleteCommentEvent;
import com.mystack.core.events.comments.request.RequestAllCommentsEvent;
import com.mystack.core.events.comments.request.RequestCommentDetailsEvent;
import com.mystack.core.events.comments.request.UpdateCommentEvent;
import com.mystack.core.events.comments.result.AllCommentsEvent;
import com.mystack.core.events.comments.result.CommentCreatedEvent;
import com.mystack.core.events.comments.result.CommentDeletedEvent;
import com.mystack.core.events.comments.result.CommentDetailsEvent;
import com.mystack.core.events.comments.result.CommentUpdatedEvent;

public interface CommentService {

    CommentCreatedEvent createComment(CreateCommentEvent event);
    CommentUpdatedEvent updateComment(UpdateCommentEvent event);
    CommentDeletedEvent deleteComment(DeleteCommentEvent event);
    AllCommentsEvent getAllComments(RequestAllCommentsEvent event);
    CommentDetailsEvent getCommentDetails(RequestCommentDetailsEvent event);
	AllCommentsEvent getAllCommentsFromChecks(RequestAllCommentsEvent event);

}
