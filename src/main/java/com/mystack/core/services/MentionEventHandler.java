package com.mystack.core.services;

import com.mystack.core.events.mentions.request.CreateMentionEvent;
import com.mystack.core.events.mentions.request.DeleteMentionEvent;
import com.mystack.core.events.mentions.request.RequestAllMentionsEvent;
import com.mystack.core.events.mentions.request.UpdateMentionEvent;
import com.mystack.core.events.mentions.result.AllMentionsEvent;
import com.mystack.core.events.mentions.result.MentionCreatedEvent;
import com.mystack.core.events.mentions.result.MentionDeletedEvent;
import com.mystack.core.events.mentions.result.MentionUpdatedEvent;
import com.mystack.persistence.services.MentionPersistenceService;

public class MentionEventHandler implements MentionService {

  MentionPersistenceService persistenceService;

  public MentionEventHandler(MentionPersistenceService service) {
    this.persistenceService = service;
  }
  @Override
  public MentionCreatedEvent createMention(CreateMentionEvent event) {
    return persistenceService.createMention(event);
  }

  @Override
  public MentionDeletedEvent deleteMention(DeleteMentionEvent event) {
    return null;
  }

  @Override
  public MentionUpdatedEvent updateMention(UpdateMentionEvent event) {
    return persistenceService.updateMention(event);
  }

  @Override
  public AllMentionsEvent getAllMentions(RequestAllMentionsEvent event) {
    return persistenceService.getAllMentions(event);
  }
}
