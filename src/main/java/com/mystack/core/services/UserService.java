package com.mystack.core.services;

import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;

public interface UserService {

  public UserCreatedEvent createUser(CreateUserEvent event);

  public UserUpdatedEvent updateUser(UpdateUserEvent event);

  public UserDeletedEvent deleteUser(DeleteUserEvent event);

  public AllUsersEvent getAllUsers(RequestAllUsersEvent event);

  public UserDetailsEvent getUserDetails(RequestUserDetailsEvent event);

  public UserDetailsEvent applyPolicy(UserDetailsEvent event);
}
