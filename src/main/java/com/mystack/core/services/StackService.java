package com.mystack.core.services;

import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksFromUserEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsFromUserEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.core.events.stacks.result.AllStacksFromUserEvent;
import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;

public interface StackService {

    public StackCreatedEvent createStack(CreateStackEvent event);

    public StackDeletedEvent deleteStack(DeleteStackEvent event);

    public AllStacksEvent getAllStacks(RequestAllStacksEvent event);

    public AllStacksEvent getAllCheckableStacks(RequestAllStacksEvent event);

    public AllStacksFromUserEvent getAllStacks(RequestAllStacksFromUserEvent event);

    public StackDetailsEvent getStackDetails(RequestStackDetailsEvent event);

    public StackDetailsEvent getStackDetails(RequestStackDetailsFromUserEvent event);

    public StackUpdatedEvent updateStack(UpdateStackEvent event);
}
