package com.mystack.core.services;

import com.mystack.core.events.checks.request.*;
import com.mystack.core.events.checks.result.*;
import com.mystack.core.events.mentions.request.CreateMentionEvent;
import com.mystack.core.events.mentions.request.DeleteMentionEvent;
import com.mystack.core.events.mentions.request.RequestAllMentionsEvent;
import com.mystack.core.events.mentions.request.UpdateMentionEvent;
import com.mystack.core.events.mentions.result.AllMentionsEvent;
import com.mystack.core.events.mentions.result.MentionCreatedEvent;
import com.mystack.core.events.mentions.result.MentionDeletedEvent;
import com.mystack.core.events.mentions.result.MentionUpdatedEvent;

public interface MentionService {

  MentionCreatedEvent createMention(CreateMentionEvent event);
  MentionDeletedEvent deleteMention(DeleteMentionEvent event);
  MentionUpdatedEvent updateMention(UpdateMentionEvent event);

  AllMentionsEvent getAllMentions(RequestAllMentionsEvent event);
  // public MentionDetailsEvent getMentionDetails(RequestMentionDetailsEvent event);

}
