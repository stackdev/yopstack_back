package com.mystack.core.services;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.persistence.services.UserPersistenceService;

public class UserEventHandler implements UserService {
    private static Logger  LOG = LoggerFactory.getLogger(UserEventHandler.class);

    UserPersistenceService persistenceService;

    @Autowired
    StackService           stackService;

    @Autowired
    PasswordEncoder        password_encoder;

    public UserEventHandler(UserPersistenceService service) {
        this.persistenceService = service;
    }

    /**
     * Domain service method to create a user. Interact with stack domain service to validate the list of stack provided in input
     */
    @Override
    public UserCreatedEvent createUser(CreateUserEvent event) {
        // check list of stack provided with user creation
        UserDetails input_details = event.getDetails();
        UUID user_id = input_details.getId();
        List<UUID> stack_list = input_details.getList_stack();
        List<UUID> verified_stack_list = new ArrayList<>();
        boolean stack_list_modified = false;
        UserDetails output_details = input_details;
        setUserCredentials(output_details, output_details.getSetpw());

        if (null != stack_list) {
            for (UUID stack_id : stack_list) {
                StackDetailsEvent checked_stack = stackService.getStackDetails(new RequestStackDetailsEvent(stack_id));
                if (checked_stack.isEntityFound()) {
                    verified_stack_list.add(stack_id);
                } else {
                    stack_list_modified = true;
                    LOG.warn("User:" + user_id + " Stack provided in stack_list but not existing: " + stack_id);
                }
            }
            if (stack_list_modified) {
                LOG.warn("User:" + user_id + " Stack list provided with user creation was containing inexisting stack, content corrected.");
                output_details.setList_stack(verified_stack_list);
            }
        }
        return persistenceService.createUser(new CreateUserEvent(output_details));
    }

    @Override
    public UserDeletedEvent deleteUser(DeleteUserEvent event) {
        return persistenceService.deleteUser(event);
    }

    @Override
    public AllUsersEvent getAllUsers(RequestAllUsersEvent event) {
        LOG.info("Get All Users: " + event.toString());
        return persistenceService.getAllUsers(event);
    }

    @Override
    public UserDetailsEvent getUserDetails(RequestUserDetailsEvent event) {
        return persistenceService.getUserDetails(event);
    }

    @Override
    public UserUpdatedEvent updateUser(UpdateUserEvent event) {

        // get user by id,
        UserDetailsEvent existing_user_event = getUserDetails(new RequestUserDetailsEvent(event.getUserDetails().getId()));

        // check User existence
        if (!existing_user_event.isEntityFound()) {
            return UserUpdatedEvent.notFound(event.getUserDetails());
        }

        // check existing user against logged in user
        if (!existing_user_event.getDetails().getName().equals(event.getAuthenticated_user())) {
            LOG.warn("Mismatch between logged in user and target user: " + existing_user_event.getDetails().getName() + " != " + event.getAuthenticated_user());
            return UserUpdatedEvent.notFound(event.getUserDetails());
        }

        // forcing name in user for update
        UserDetails user_for_update = event.getUserDetails();
        user_for_update.setName(existing_user_event.getDetails().getName());

        // password modification check
        String new_password = user_for_update.getSetpw();
        String old_password = user_for_update.getOldpw();

        // check if update is password related
        if (null != new_password || null != old_password) {
            LOG.info("User update is password related");
            // is password related, check old password
            if (password_encoder.matches(old_password, existing_user_event.getDetails().getPassword())) {
                // update user including password
                LOG.info("correct old password, update can continue");
                setUserCredentials(user_for_update, new_password);
            } else {
                // block completely the update
                LOG.info("Incorrect old password, blocking user update");
                return UserUpdatedEvent.notUpdated(event.getUserDetails());
            }
        } else {
            // password need to be preserved --> might be generalized to all null or invalid or protected fields
            user_for_update.setPassword(existing_user_event.getDetails().getPassword());
            user_for_update.setRoles(existing_user_event.getDetails().getRoles());
            user_for_update.setEnabled(existing_user_event.getDetails().getEnabled());
        }

        UpdateUserEvent update_event = new UpdateUserEvent(user_for_update, event.getAuthenticated_user());
        return persistenceService.updateUser(update_event);

    }

    @Override
    public UserDetailsEvent applyPolicy(UserDetailsEvent event) {
        // TODO Auto-generated method stub
        return null;
    }

    private void setUserCredentials(UserDetails details, String password) {
        details.setPassword(password_encoder.encode(password));
        List<String> roles = new ArrayList<String>();
        roles.add("ROLE_USER");
        details.setRoles(roles);
    }

}
