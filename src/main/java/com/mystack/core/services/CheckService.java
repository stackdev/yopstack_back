package com.mystack.core.services;

import com.mystack.core.events.checks.request.CreateCheckEvent;
import com.mystack.core.events.checks.request.DeleteCheckEvent;
import com.mystack.core.events.checks.request.RequestAllChecksEvent;
import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.request.UpdateCheckEvent;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.core.events.checks.result.CheckCreatedEvent;
import com.mystack.core.events.checks.result.CheckDeletedEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.events.checks.result.CheckUpdatedEvent;

public interface CheckService {

  public CheckCreatedEvent createCheck(CreateCheckEvent event);

  public CheckDeletedEvent deleteCheck(DeleteCheckEvent event);

  public CheckUpdatedEvent updateCheck(UpdateCheckEvent event);

  public AllChecksEvent getAllChecks(RequestAllChecksEvent event);

  public CheckDetailsEvent getCheckDetails(RequestCheckDetailsEvent event);

}
