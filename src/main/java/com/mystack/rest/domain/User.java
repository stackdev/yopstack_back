package com.mystack.rest.domain;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mystack.core.domain.UserDetails;

@JsonInclude(Include.NON_NULL)
public class User {
    private UUID   id;
    private String name;
    private UUID[] list_friend;
    private UUID[] list_stack;
    private String setpw;
    private String oldpw;

    //Constructors
    public User() {
    }

    public User(UUID id, String name, UUID[] list_friend, UUID[] list_stack, String setpw, String oldpw) {
        this.id = id;
        this.name = name;
        this.list_friend = list_friend;
        this.list_stack = list_stack;
        this.setpw = setpw;
        this.oldpw = oldpw;
    }

    public User(User user) {
        this.id = user.id;
        this.name = user.name;
        this.list_friend = user.list_friend;
        this.list_stack = user.list_stack;
        this.setpw = user.setpw;
        this.oldpw = user.oldpw;
    }

    public User(UUID id, String name, UUID[] list_friend, UUID[] list_stack) {
        this.id = id;
        this.name = name;
        this.list_friend = list_friend;
        this.list_stack = list_stack;
        this.setpw = null;
        this.oldpw = null;
    }

    //Getter/setters/toString/toDetails/fromDetails
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID[] getList_friend() {
        return list_friend;
    }

    public void setList_friend(UUID[] list_friend) {
        this.list_friend = list_friend;
    }

    public UUID[] getList_stack() {
        return list_stack;
    }

    public void setList_stack(UUID[] list_stack) {
        this.list_stack = list_stack;
    }

    public String getSetpw() {
        return setpw;
    }

    public void setSetpw(String setpw) {
        this.setpw = setpw;
    }

    public String getOldpw() {
        return oldpw;
    }

    public void setOldpw(String oldpw) {
        this.oldpw = oldpw;
    }

    public String toString() {
        return "User: [" + " Id: " + id + " Name: " + name + " List_friend: " + list_friend + " List_stack: " + list_stack + " Setpw: " + setpw + " Oldpw: " + oldpw + "]";
    }

    public UserDetails toDetails() {
        UserDetails details = new UserDetails();
        details.setId(this.id);
        details.setName(this.name);
        details.setRestList_friend(this.list_friend);
        details.setRestList_stack(this.list_stack);
        details.setSetpw(this.setpw);
        details.setOldpw(this.oldpw);
        return details;
    }

    public static User fromDetails(UserDetails details) {
        User user = new User();
        user.id = details.getId();
        user.name = details.getName();
        user.list_friend = details.getRestList_friend();
        user.list_stack = details.getRestList_stack();
        user.setpw = details.getSetpw();
        user.oldpw = details.getOldpw();
        return user;
    }

    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        User other = (User) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (list_friend == null) {
            if (other.list_friend != null)
                return false;
        } else if (!list_friend.equals(other.list_friend))
            return false;
        if (list_stack == null) {
            if (other.list_stack != null)
                return false;
        } else if (!list_stack.equals(other.list_stack))
            return false;
        if (setpw == null) {
            if (other.setpw != null)
                return false;
        } else if (!setpw.equals(other.setpw))
            return false;
        if (oldpw == null) {
            if (other.oldpw != null)
                return false;
        } else if (!oldpw.equals(other.oldpw))
            return false;
        return true;
    }

}
