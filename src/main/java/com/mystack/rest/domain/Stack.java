package com.mystack.rest.domain;
import java.util.Date;
import java.util.UUID;
import com.mystack.core.domain.CheckCount;
import com.mystack.core.domain.StackDetails;
import javax.xml.bind.annotation.XmlRootElement;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement
@JsonInclude(Include.NON_NULL)
public class Stack {
    private UUID id;
    private String name;
    private String description;
    private int periodicity;
    private int frequency;
    private Date last_check;
    private Date last_planned_check;
    private int last_check_count;
    private CheckCount checkCount;
    private Date creation_date;
    private int type;
    private boolean isPrivate;
    private int status;
    private boolean isShared;
    private UUID[] admins;
    private UUID[] checkers;
    private Date target_date;
    //Constructors
    public Stack() {
    }
    
    public Stack(UUID id, String name, String description, int periodicity, int frequency, Date last_check, Date last_planned_check, int last_check_count, CheckCount checkCount, Date 
    creation_date, int type, boolean isPrivate, int status, boolean isShared, UUID[] admins, UUID[] checkers, Date target_date) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.periodicity = periodicity;
        this.frequency = frequency;
        this.last_check = last_check;
        this.last_planned_check = last_planned_check;
        this.last_check_count = last_check_count;
        this.checkCount = checkCount;
        this.creation_date = creation_date;
        this.type = type;
        this.isPrivate = isPrivate;
        this.status = status;
        this.isShared = isShared;
        this.admins = admins;
        this.checkers = checkers;
        this.target_date = target_date;
    }
    
    public Stack(Stack stack) {
        this.id = stack.id;
        this.name = stack.name;
        this.description = stack.description;
        this.periodicity = stack.periodicity;
        this.frequency = stack.frequency;
        this.last_check = stack.last_check;
        this.last_planned_check = stack.last_planned_check;
        this.last_check_count = stack.last_check_count;
        this.checkCount = stack.checkCount;
        this.creation_date = stack.creation_date;
        this.type = stack.type;
        this.isPrivate = stack.isPrivate;
        this.status = stack.status;
        this.isShared = stack.isShared;
        this.admins = stack.admins;
        this.checkers = stack.checkers;
        this.target_date = stack.target_date;
    }
    
    //Getter/setters/toString/toDetails/fromDetails
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    public int getPeriodicity() {
        return periodicity;
    }
    
    public void setPeriodicity(int periodicity) {
        this.periodicity = periodicity;
    }
    
    
    public int getFrequency() {
        return frequency;
    }
    
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
    
    
    public Date getLast_check() {
        return last_check;
    }
    
    public void setLast_check(Date last_check) {
        this.last_check = last_check;
    }
    
    
    public Date getLast_planned_check() {
        return last_planned_check;
    }
    
    public void setLast_planned_check(Date last_planned_check) {
        this.last_planned_check = last_planned_check;
    }
    
    
    public int getLast_check_count() {
        return last_check_count;
    }
    
    public void setLast_check_count(int last_check_count) {
        this.last_check_count = last_check_count;
    }
    
    
    public CheckCount getCheckCount() {
        return checkCount;
    }
    
    public void setCheckCount(CheckCount checkCount) {
        this.checkCount = checkCount;
    }
    
    
    public Date getCreation_date() {
        return creation_date;
    }
    
    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }
    
    
    public int getType() {
        return type;
    }
    
    public void setType(int type) {
        this.type = type;
    }
    
    
    public boolean getIsPrivate() {
        return isPrivate;
    }
    
    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
    
    
    public int getStatus() {
        return status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    
    
    public boolean getIsShared() {
        return isShared;
    }
    
    public void setIsShared(boolean isShared) {
        this.isShared = isShared;
    }
    
    
    public UUID[] getAdmins() {
        return admins;
    }
    
    public void setAdmins(UUID[] admins) {
        this.admins = admins;
    }
    
    
    public UUID[] getCheckers() {
        return checkers;
    }
    
    public void setCheckers(UUID[] checkers) {
        this.checkers = checkers;
    }
    
    
    public Date getTarget_date() {
        return target_date;
    }
    
    public void setTarget_date(Date target_date) {
        this.target_date = target_date;
    }
    
    
    public String toString() {
        return "Stack: [" + " id: " + id + " name: " + name + " description: " + description + " periodicity: " + periodicity + " frequency: " + frequency + " last_check: " + last_check + " last_planned_check: " + last_planned_check + " last_check_count: " + last_check_count + " checkCount: " + checkCount + " creation_date: " + creation_date + " type: " + type + " isPrivate: " + isPrivate + " status: " + status + " isShared: " + isShared + " admins: " + admins + " checkers: " + checkers + " target_date: " + target_date + "]";
    }
    
    public StackDetails toDetails() {
        StackDetails details = new StackDetails();
        details.setId(this.id);
        details.setName(this.name);
        details.setDescription(this.description);
        details.setPeriodicity(this.periodicity);
        details.setFrequency(this.frequency);
        details.setLast_check(this.last_check);
        details.setLast_planned_check(this.last_planned_check);
        details.setLast_check_count(this.last_check_count);
        details.setCheckCount(this.checkCount);
        details.setCreation_date(this.creation_date);
        details.setRestType(this.type);
        details.setIsPrivate(this.isPrivate);
        details.setRestStatus(this.status);
        details.setIsShared(this.isShared);
        details.setRestAdmins(this.admins);
        details.setRestCheckers(this.checkers);
        details.setTarget_date(this.target_date);
        return details;
    }
    
    public static Stack fromDetails(StackDetails details) {
        Stack user = new Stack();
        user.id = details.getId();
        user.name = details.getName();
        user.description = details.getDescription();
        user.periodicity = details.getPeriodicity();
        user.frequency = details.getFrequency();
        user.last_check = details.getLast_check();
        user.last_planned_check = details.getLast_planned_check();
        user.last_check_count = details.getLast_check_count();
        user.checkCount = details.getCheckCount();
        user.creation_date = details.getCreation_date();
        user.type = details.getRestType();
        user.isPrivate = details.getIsPrivate();
        user.status = details.getRestStatus();
        user.isShared = details.getIsShared();
        user.admins = details.getRestAdmins();
        user.checkers = details.getRestCheckers();
        user.target_date = details.getTarget_date();
        return user;
    }
    
    public boolean equals(Object obj) {
        
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        
        Stack other = (Stack) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (periodicity != other.periodicity)
            return false;
        if (frequency != other.frequency)
            return false;
        if (last_check == null) {
            if (other.last_check != null)
                return false;
        } else if (!last_check.equals(other.last_check))
            return false;
        if (last_planned_check == null) {
            if (other.last_planned_check != null)
                return false;
        } else if (!last_planned_check.equals(other.last_planned_check))
            return false;
        if (last_check_count != other.last_check_count)
            return false;
        if (checkCount == null) {
            if (other.checkCount != null)
                return false;
        } else if (!checkCount.equals(other.checkCount))
            return false;
        if (creation_date == null) {
            if (other.creation_date != null)
                return false;
        } else if (!creation_date.equals(other.creation_date))
            return false;
        if (type != other.type)
            return false;
        if (isPrivate != other.isPrivate)
            return false;
        if (status != other.status)
            return false;
        if (isShared != other.isShared)
            return false;
        if (admins == null) {
            if (other.admins != null)
                return false;
        } else if (!admins.equals(other.admins))
            return false;
        if (checkers == null) {
            if (other.checkers != null)
                return false;
        } else if (!checkers.equals(other.checkers))
            return false;
        if (target_date == null) {
            if (other.target_date != null)
                return false;
        } else if (!target_date.equals(other.target_date))
            return false;
        return true;
    }
    
}
