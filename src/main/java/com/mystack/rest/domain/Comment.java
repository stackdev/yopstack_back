package com.mystack.rest.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mystack.core.domain.CommentDetails;

@JsonInclude(Include.NON_NULL)
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    private UUID              id;
    private UUID              stack_userid;
    private UUID              stackid;
    private UUID              checkid;
    private UUID              comment_userid;
    private String            text;
    private Long              timestamp;
    private Long              update_timestamp;

    // No specific constructor, preferred method is From details, default constructor might been used by Jackson to convert JSON

    // Dummy constructor for JSON convertion
    //    public Comment() {
    //
    //    }

    //    public Comment(UUID id, UUID stack_userid, UUID stackid, UUID checkid, UUID comment_userid, String text, Long timestamp, Long update_timestamp) {
    //        super();
    //        this.id = id;
    //        this.stack_userid = stack_userid;
    //        this.stackid = stackid;
    //        this.checkid = checkid;
    //        this.comment_userid = comment_userid;
    //        this.text = text;
    //        this.timestamp = timestamp;
    //        this.update_timestamp = update_timestamp;
    //
    //    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getUpdate_timestamp() {
        return update_timestamp;
    }

    public void setUpdate_timestamp(Long update_timestamp) {
        this.update_timestamp = update_timestamp;
    }

    public UUID getStackid() {
        return stackid;
    }

    public void setStackid(UUID stackid) {
        this.stackid = stackid;
    }

    public UUID getCheckid() {
        return checkid;
    }

    public void setCheckid(UUID checkid) {
        this.checkid = checkid;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID commentid) {
        this.id = commentid;
    }

    public UUID getStack_userid() {
        return stack_userid;
    }

    public void setStack_userid(UUID stack_userid) {
        this.stack_userid = stack_userid;
    }

    public UUID getComment_userid() {
        return comment_userid;
    }

    public void setComment_userid(UUID comment_userid) {
        this.comment_userid = comment_userid;
    }

    @Override
    public String toString() {
        return "Comment [id=" + id + ", stack_userid=" + stack_userid + ", stackid=" + stackid + ", checkid=" + checkid + ", comment_userid=" + comment_userid + ", text=" + text + ", timestamp="
                + timestamp + ", update_timestamp=" + update_timestamp + "]";
    }

    public CommentDetails toDetails() {
        return new CommentDetails(id, stack_userid, stackid, checkid, comment_userid, text, new Date(timestamp), new Date(update_timestamp));
    }

    public static Comment FromDetails(CommentDetails details) {

        Comment comment = new Comment();
        comment.setId(details.getId());
        comment.setStack_userid(details.getStack_userid());
        comment.setStackid(details.getStackid());
        comment.setCheckid(details.getCheckid());
        comment.setComment_userid(details.getComment_userid());
        comment.setText(details.getText());
        comment.setTimestamp(details.getTimestamp().getTime());
        comment.setUpdate_timestamp(details.getUpdateTimestamp().getTime());
        return comment;
    }

    public static Comment[] FromListDetails(List<CommentDetails> details) {
        Comment[] output = new Comment[details.size()];
        int i = 0;
        for (CommentDetails comment_detail : details) {
            output[i] = FromDetails(comment_detail);
            i++;
        }
        return output;
    }

    public static List<CommentDetails> ToListDetails(List<Comment> details) {
        List<CommentDetails> output = new ArrayList<>();
        for (Comment comment : details) {
            output.add(comment.toDetails());
        }
        return output;
    }

    public static List<CommentDetails> ToListDetails(Comment[] details) {
        List<CommentDetails> output = new ArrayList<>();
        if (null != details) {
            for (Comment comment : details) {
                output.add(comment.toDetails());
            }
        }
        return output;
    }

}
