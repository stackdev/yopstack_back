package com.mystack.rest.controller;

import java.security.Principal;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.mystack.core.events.CreatedEvent;
import com.mystack.core.events.comments.request.CreateCommentEvent;
import com.mystack.core.events.comments.request.DeleteCommentEvent;
import com.mystack.core.events.comments.request.UpdateCommentEvent;
import com.mystack.core.events.comments.result.CommentCreatedEvent;
import com.mystack.core.events.comments.result.CommentDeletedEvent;
import com.mystack.core.events.comments.result.CommentUpdatedEvent;
import com.mystack.core.services.CommentService;
import com.mystack.rest.domain.Comment;

@Controller
@RequestMapping("/aggregators/users/{uid}/stacks/{sid}/checks/{check_id}/comments")
public class CommentCommandsController {

    @Autowired
    private CommentService service;

    private static Logger  LOG = LoggerFactory.getLogger(CommentCommandsController.class);

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Comment> createComment(
            @PathVariable("uid") String uid,
            @PathVariable("sid") String sid,
            @PathVariable("check_id") String check_id,
            @RequestBody Comment comment,
            UriComponentsBuilder builder,
            Principal authenticated_user) {

        ///all id from the rest URL
        comment.setStack_userid(UUID.fromString(uid));
        comment.setStackid(UUID.fromString(sid));
        comment.setCheckid(UUID.fromString(check_id));
        comment.setTimestamp(new Date().getTime());

        LOG.info("let's create a comment !: " + comment.toString() + "with authenticated user: " + authenticated_user.getName());
        CommentCreatedEvent created_event = service.createComment(new CreateCommentEvent(comment.toDetails(), authenticated_user.getName()));

        if (!created_event.isCreated()) {
            if (created_event.getCreationCode() == CreatedEvent.CODE_CREATION_FAILED_NOT_AUTHORIZED) {
                LOG.warn("Comment not authorized: " + created_event.toString());
                return new ResponseEntity<Comment>(HttpStatus.FORBIDDEN);
            }

            LOG.warn("Comment not created: " + created_event.getId());
            return new ResponseEntity<Comment>(HttpStatus.NOT_FOUND);
        }

        LOG.info("CommentCreatedEvent's details's comment: " + created_event.getDetails());

        Comment newComment = Comment.FromDetails(created_event.getDetails());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path("/aggregators/users/{uid}/stacks/{sid}/checks/{cid}/comments/{id}")
                        .buildAndExpand(created_event.getDetails().getStack_userid().toString(),
                                created_event.getDetails().getStackid().toString(),
                                created_event.getDetails().getCheckid().toString(),
                                created_event.getDetails().getId().toString())
                        .toUri());
        LOG.info("Comment created: " + newComment.toString());

        return new ResponseEntity<Comment>(newComment, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Comment> cancelComment(
            @PathVariable("uid") String uid,
            @PathVariable("sid") String sid,
            @PathVariable("check_id") String check_id,
            @PathVariable("id") String comment_id,
            Principal authenticated_user) {

        CommentDeletedEvent deleted_event = service.deleteComment(new DeleteCommentEvent(UUID.fromString(sid), authenticated_user.getName()));

        if (!deleted_event.isEntityFound()) {
            LOG.info("Comment not found");
            return new ResponseEntity<Comment>(HttpStatus.NOT_FOUND);
        }
        LOG.info("Comment found, try to delete");
        Comment comment = Comment.FromDetails(deleted_event.getDetails());

        if (deleted_event.isDeletionCompleted()) {
            LOG.info("Comment deleted");
            return new ResponseEntity<Comment>(comment, HttpStatus.OK);
        }
        LOG.info("Comment not deleted");
        return new ResponseEntity<Comment>(comment, HttpStatus.FORBIDDEN);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Comment> updateComment(
            @PathVariable("uid") String uid,
            @PathVariable("sid") String sid,
            @PathVariable("check_id") String check_id,
            @PathVariable("id") String comment_id,
            @RequestBody Comment comment,
            UriComponentsBuilder builder,
            Principal authenticated_user) {
        LOG.info("let's update a comment !: " + comment.toString());

        ///all id from the rest URL
        comment.setId(UUID.fromString(comment_id));
        comment.setStack_userid(UUID.fromString(uid));
        comment.setStackid(UUID.fromString(sid));
        comment.setCheckid(UUID.fromString(check_id));

        CommentUpdatedEvent updated_event = service.updateComment(new UpdateCommentEvent(comment.toDetails(), authenticated_user.getName()));
        if (null != updated_event && !updated_event.isUpdated()) {
            return new ResponseEntity<>(comment, HttpStatus.NOT_MODIFIED);
        }

        Comment newComment = Comment.FromDetails(updated_event.getDetails());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path("/aggregators/users/{uid}/stacks/{sid}/checks/{cid}/comments/{id}")
                        .buildAndExpand(newComment.getStack_userid().toString(),
                                newComment.getStackid().toString(),
                                newComment.getCheckid().toString(),
                                newComment.getId().toString())
                        .toUri());
        LOG.info("Comment updated: " + newComment.toString());

        return new ResponseEntity<Comment>(newComment, headers, HttpStatus.OK);
    }
}
