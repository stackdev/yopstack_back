package com.mystack.rest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.checks.request.RequestAllChecksEvent;
import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.services.CheckService;
import com.mystack.rest.domain.Check;

@Controller
@RequestMapping("/aggregators/")
public class CheckQueriesController {

    private static Logger LOG = LoggerFactory.getLogger(CheckQueriesController.class);

    @Autowired
    private CheckService  service;

    /**
     * REST URI for retrieval of check for a given stack @param stack_id between @param before_date and @param after_date
     * 
     * @param stack_id
     * @param before_date
     * @param after_date
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "checks", params = { "stack" })
    public ResponseEntity<List<Check>> getListBetweenDate(
            @RequestParam(value = "stack") String stack_id,
            @RequestParam(value = "before_date", required = false) String before_date,
            @RequestParam(value = "after_date", required = false) String after_date,
            @RequestParam(value = "count", required = false, defaultValue = "0") int count) {

        UUID searched_stack_id = UUID.fromString(stack_id);
        LOG.info("Getlist check called with searched_name: " + stack_id + ", before_date: " + before_date + " ,after_date: " + after_date + ", count: " + count);

        if (!"".equals(before_date) || !"".equals(after_date)) {
            return getListCheck(new RequestAllChecksEvent(searched_stack_id, before_date, after_date, count));
        }
        // else only provide count
        return getListCheck(new RequestAllChecksEvent(searched_stack_id, "", count));
    }

    /**
     * REST URI for retrieval of check with a giveb id @param id
     * 
     * @param id
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "checks/{id}")
    public ResponseEntity<Check> viewCheck(@PathVariable("id") String id) {

        LOG.info("View Check called with id:" + id);

        CheckDetailsEvent details_event = service.getCheckDetails(new RequestCheckDetailsEvent(UUID.fromString(id)));

        if (!details_event.isEntityFound()) {
            return new ResponseEntity<Check>(HttpStatus.NOT_FOUND);
        }

        Check check = Check.fromDetails(details_event.getDetails());

        return new ResponseEntity<Check>(check, HttpStatus.OK);

    }

    /**
     * Doing the interaction with the service layer
     * 
     * @param request_event
     * @return
     */
    private ResponseEntity<List<Check>> getListCheck(RequestAllChecksEvent request_event) {
        try {
            AllChecksEvent allchecks_event = service.getAllChecks(request_event);

            LOG.info("result: " + allchecks_event);
            if (!allchecks_event.isEntityFound()) {
                return new ResponseEntity<List<Check>>(HttpStatus.NOT_FOUND);
            }

            List<Check> result = new ArrayList<>();

            for (CheckDetails details : allchecks_event.getAllCheckDetails()) {
                result.add(Check.fromDetails(details));
            }

            return new ResponseEntity<List<Check>>(result, HttpStatus.OK);

        } catch (IllegalArgumentException e) {
            LOG.error("Invalid id provided: " + request_event.getStack_id().toString() + ", " + e.getMessage());
            return new ResponseEntity<List<Check>>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error("Unexpected Exception encountered: " + e.getMessage());
            return new ResponseEntity<List<Check>>(HttpStatus.BAD_REQUEST);
        }
    }

}