package com.mystack.rest.controller;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.domain.Mention;
import com.mystack.core.events.comments.request.RequestAllCommentsEvent;
import com.mystack.core.events.comments.request.RequestCommentDetailsEvent;
import com.mystack.core.events.comments.result.AllCommentsEvent;
import com.mystack.core.events.comments.result.CommentDetailsEvent;
import com.mystack.core.events.mentions.request.RequestAllMentionsEvent;
import com.mystack.core.events.mentions.result.AllMentionsEvent;
import com.mystack.core.services.CommentService;
import com.mystack.core.services.MentionService;
import com.mystack.rest.domain.Comment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
//@RequestMapping("/aggregators/users/{uid}/stacks/{sid}/checks/{check_id}/comments")
public class MentionQueriesController {

    private static Logger  LOG = LoggerFactory.getLogger(MentionQueriesController.class);


    private MentionService service;


    public MentionQueriesController(MentionService service) {
        this.service = service;
    }

    @RequestMapping(path = "/aggregators/mentions",
    	        	method = RequestMethod.GET, params = { "to", "beforeDate", "afterDate" })
    public ResponseEntity<List<Mention>> getListBetweenDate(

            @RequestParam(value = "to") String to,
            @RequestParam(value = "beforeDate", required = false) String beforeDate,
            @RequestParam(value = "afterDate", required = false) String afterDate) {

        UUID userid = UUID.fromString(to);
        LOG.info("Request to retrieve all mentions for userid: " + userid + ", beforeDate: " + beforeDate + " ,afterDate: " + afterDate);

        AllMentionsEvent result = service.getAllMentions(new RequestAllMentionsEvent(userid, "", 10));
        if (!result.isEntityFound()){
            return new ResponseEntity<List<Mention>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<Mention>>(
                result.getAllMentionDetails().stream()
                        .map(details -> Mention.fromDetails(details))
                        .collect(Collectors.toList()),
                HttpStatus.OK);
    }



}
