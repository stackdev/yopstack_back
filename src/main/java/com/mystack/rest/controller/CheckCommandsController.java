package com.mystack.rest.controller;

import java.security.Principal;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.mystack.config.ConstantString;
import com.mystack.core.events.CreatedEvent;
import com.mystack.core.events.checks.request.CreateCheckEvent;
import com.mystack.core.events.checks.request.DeleteCheckEvent;
import com.mystack.core.events.checks.request.UpdateCheckEvent;
import com.mystack.core.events.checks.result.CheckCreatedEvent;
import com.mystack.core.events.checks.result.CheckDeletedEvent;
import com.mystack.core.events.checks.result.CheckUpdatedEvent;
import com.mystack.core.services.CheckService;
import com.mystack.rest.domain.Check;

@Controller
@RequestMapping("/aggregators/checks")
public class CheckCommandsController {

    private static Logger LOG = LoggerFactory.getLogger(CheckCommandsController.class);

    @Autowired
    private CheckService  service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Check> createCheck(@RequestBody Check check, UriComponentsBuilder builder, Principal authenticated_user) {
        LOG.info("let's create a check !: " + check.toString() + "with authenticated user: " + authenticated_user.getName());
        CheckCreatedEvent checkCreated = service.createCheck(new CreateCheckEvent(check.toDetails(), authenticated_user.getName()));

        if (!checkCreated.isCreated()) {
            if (checkCreated.getCreationCode() == CreatedEvent.CODE_CREATION_FAILED_NOT_AUTHORIZED) {
                LOG.warn("Check not authorized: " + checkCreated.toString());
                return new ResponseEntity<Check>(HttpStatus.FORBIDDEN);
            }

            LOG.warn("Check not created: " + checkCreated.getId());
            return new ResponseEntity<Check>(HttpStatus.NOT_FOUND);
        }

        LOG.info("CheckCreatedEvent's details's comment: " + checkCreated.getDetails().getComment());

        Check newCheck = Check.fromDetails(checkCreated.getDetails());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path(ConstantString.ChecksFullPath() + "{id}")
                        .buildAndExpand(checkCreated.getDetails().getId().toString())
                        .toUri());
        LOG.info("Check created: " + newCheck.toString());

        return new ResponseEntity<Check>(newCheck, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Check> cancelCheck(@PathVariable("id") String sid, Principal authenticated_user) {

        CheckDeletedEvent deleted_event = service.deleteCheck(new DeleteCheckEvent(UUID.fromString(sid), authenticated_user.getName()));

        if (!deleted_event.isEntityFound()) {
            LOG.info("Check not found");
            return new ResponseEntity<Check>(HttpStatus.NOT_FOUND);
        }
        LOG.info("Check found, try to delete");
        Check check = Check.fromDetails(deleted_event.getDetails());

        if (deleted_event.isDeletionCompleted()) {
            LOG.info("Check deleted");
            return new ResponseEntity<Check>(check, HttpStatus.OK);
        }
        LOG.info("Check not deleted");
        return new ResponseEntity<Check>(check, HttpStatus.FORBIDDEN);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Check> updateCheck(@PathVariable("id") String sid, @RequestBody Check check, UriComponentsBuilder builder, Principal authenticated_user) {
        LOG.info("let's update a check !: " + check.toString());

        ///forcing the stack id from the rest URL
        check.setId(UUID.fromString(sid));

        CheckUpdatedEvent result = service.updateCheck(new UpdateCheckEvent(check.toDetails(), authenticated_user.getName()));
        if (null != result && !result.isUpdated()) {
            return new ResponseEntity<>(check, HttpStatus.NOT_MODIFIED);
        }

        Check newCheck = Check.fromDetails(result.getDetails());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path(ConstantString.ChecksFullPath() + "{id}")
                        .buildAndExpand(result.getDetails().getId().toString())
                        .toUri());
        LOG.info("Check updated: " + newCheck.toString());

        return new ResponseEntity<Check>(newCheck, headers, HttpStatus.OK);
    }
}
