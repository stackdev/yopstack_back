package com.mystack.rest.controller;

import com.mystack.config.ConstantString;
import com.mystack.core.events.mentions.request.CreateMentionEvent;
import com.mystack.core.events.mentions.request.UpdateMentionEvent;
import com.mystack.core.events.mentions.result.MentionCreatedEvent;
import com.mystack.core.events.mentions.result.MentionUpdatedEvent;
import com.mystack.core.services.MentionService;
import com.mystack.rest.domain.Mention;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.Principal;

@Controller
@RequestMapping("/aggregators/mentions")
public class MentionCommandsController {

    private static final Logger LOG = LoggerFactory.getLogger(MentionCommandsController.class);

    private final MentionService  service;

    public MentionCommandsController(MentionService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Mention> createMention(@RequestBody Mention mention, UriComponentsBuilder builder, Principal authenticated_user) {
        LOG.info("new mention: " + mention.toString() + "with authenticated user: " + authenticated_user.getName());


        MentionCreatedEvent mentionCreated = service.createMention(new CreateMentionEvent(mention.toDetails()));


        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path(ConstantString.MentionsFullPath(mentionCreated.getDetails().getId().toString()))
                        .build()
                        .toUri());

        return new ResponseEntity<>(Mention.fromDetails(mentionCreated.getDetails()), headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Mention> updateMention(@RequestBody Mention mention, UriComponentsBuilder builder, Principal authenticated_user) {

        MentionUpdatedEvent mentionUpdatedEvent = service.updateMention(new UpdateMentionEvent(mention.toDetails()));


        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path(ConstantString.MentionsFullPath(mentionUpdatedEvent.getDetails().getId().toString()))
                        .build()
                        .toUri());

        return new ResponseEntity<>(Mention.fromDetails(mentionUpdatedEvent.getDetails()), headers, HttpStatus.OK);
    }


}
