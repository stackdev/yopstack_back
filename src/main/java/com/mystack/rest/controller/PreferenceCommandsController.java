package com.mystack.rest.controller;

import java.security.Principal;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.mystack.config.ConstantString;
import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;
import com.mystack.core.services.PreferenceService;
import com.mystack.rest.domain.Preference;

@Controller
@RequestMapping("/aggregators/users/{uid}/preferences")
public class PreferenceCommandsController {

    private static Logger     LOG = LoggerFactory.getLogger(PreferenceCommandsController.class);

    @Autowired
    private PreferenceService service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Preference> createPreference(@RequestBody Preference preference, UriComponentsBuilder builder, @PathVariable("uid") String uid, Principal authenticated_user) {
        LOG.info("let's create a preference !: " + preference.toString() + " requesting user: " + authenticated_user);
        PreferenceCreatedEvent result = service.createPreference(new CreatePreferenceEvent(preference.toDetails(), authenticated_user.getName()));

        if (!result.isCreated()) {
            return new ResponseEntity<Preference>(null, null, HttpStatus.NOT_FOUND);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path(ConstantString.PreferencesFullPath(result.getDetails().getUser().toString(), result.getDetails().getId().toString()))
                        .build()
                        .toUri());

        LOG.info("Preference created");

        return new ResponseEntity<Preference>(Preference.fromDetails(result.getDetails()), headers, HttpStatus.CREATED);

    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{pid}")
    public ResponseEntity<Preference> updatePreference(@RequestBody Preference preference, UriComponentsBuilder builder, @PathVariable("uid") String uid, @PathVariable("pid") String pid,
            Principal authenticated_user) {
        LOG.info("let's update a preference !: " + preference.toString() + " requesting user: " + authenticated_user);
        preference.setId(UUID.fromString(pid));
        PreferenceUpdatedEvent result = service.updatePreference(new UpdatePreferenceEvent(preference.toDetails(), authenticated_user.getName()));

        if (!result.isUpdated()) {
            LOG.info("Preference not updated");
            return new ResponseEntity<Preference>(HttpStatus.NOT_MODIFIED);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path(ConstantString.PreferencesFullPath(result.getDetails().getUser().toString(), pid))
                        .build()
                        .toUri());

        LOG.info("Preference updated");

        return new ResponseEntity<Preference>(Preference.fromDetails(result.getDetails()), headers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{pid}")
    public ResponseEntity<Preference> cancelStack(@PathVariable("uid") String uid, @PathVariable("pid") String pid, UriComponentsBuilder builder, Principal authenticated_user) {
        PreferenceDetails details = new PreferenceDetails(UUID.fromString(pid), UUID.fromString(uid), null);
        PreferenceDeletedEvent result = service.deletePreference(new DeletePreferenceEvent(details, authenticated_user.getName()));

        if (!result.isDeletionCompleted()) {
            LOG.info("Preference not deleted");
            return new ResponseEntity<Preference>(HttpStatus.NOT_MODIFIED);
        }

        LOG.info("Preference deleted");

        return new ResponseEntity<Preference>(HttpStatus.OK);
    }

}
