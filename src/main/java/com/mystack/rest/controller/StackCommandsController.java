package com.mystack.rest.controller;

import java.security.Principal;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.core.services.StackService;
import com.mystack.rest.domain.Stack;

@Controller
@RequestMapping("/aggregators/users/{uid}/stacks")
public class StackCommandsController {

    private static Logger LOG = LoggerFactory.getLogger(StackCommandsController.class);

    @Autowired
    private StackService  service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Stack> createStack(@RequestBody Stack stack, UriComponentsBuilder builder, @PathVariable("uid") String uid, Principal authenticated_user) {
        LOG.info("let's create a stack !: " + stack.toString() + " requesting user: " + authenticated_user);
        StackCreatedEvent stackCreated = service.createStack(new CreateStackEvent(stack.toDetails(), UUID.fromString(uid), authenticated_user.getName()));

        if (!stackCreated.isCreated()) {
            LOG.info("Stack not created");
            return new ResponseEntity<Stack>(null, null, HttpStatus.NOT_FOUND);
        }

        LOG.info("StackCreatedEvent's details's description: " + stackCreated.getDetails().getDescription());

        Stack newStack = Stack.fromDetails(stackCreated.getDetails());

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path("/aggregators/users/{uid}/stacks/{sid}")
                        .buildAndExpand(uid, stackCreated.getDetails().getId().toString())
                        .toUri());
        LOG.info("Stack created: " + newStack.toString());

        return new ResponseEntity<Stack>(newStack, headers, HttpStatus.CREATED);
    }

    // TODO: dzd check if javac debug option is suitable for production. If yes, set it and remove " ("id") "
    @RequestMapping(method = RequestMethod.DELETE, value = "/{sid}")
    public ResponseEntity<Stack> cancelStack(@PathVariable("uid") String uid, @PathVariable("sid") String sid, Principal authenticated_user) {

        StackDeletedEvent deleted_event = service.deleteStack(new DeleteStackEvent(UUID.fromString(sid), UUID.fromString(uid), authenticated_user.getName()));

        if (!deleted_event.isEntityFound()) {
            LOG.info("Stack not found");
            return new ResponseEntity<Stack>(HttpStatus.NOT_FOUND);
        }
        LOG.info("Stack found, try to delete");
        Stack stack = Stack.fromDetails(deleted_event.getDetails());

        if (deleted_event.isDeletionCompleted()) {
            LOG.info("Stack deleted");
            return new ResponseEntity<Stack>(stack, HttpStatus.OK);
        }
        LOG.info("Stack not deleted");
        return new ResponseEntity<Stack>(stack, HttpStatus.FORBIDDEN);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{sid}")
    public ResponseEntity<Stack> updateStack(@PathVariable("uid") String uid, @PathVariable("sid") String sid, @RequestBody Stack stack, UriComponentsBuilder builder, Principal authenticated_user) {
        LOG.info("let's update a stack !: " + stack.toString());

        try {
            stack.setId(UUID.fromString(sid));
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(stack, HttpStatus.NOT_MODIFIED);
        }

        StackUpdatedEvent result = service.updateStack(new UpdateStackEvent(stack.toDetails(), authenticated_user.getName()));
        if (null != result && !result.isUpdated()) {
            return new ResponseEntity<>(stack, HttpStatus.NOT_MODIFIED);
        }

        Stack newStack = Stack.fromDetails(result.getDetails());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path("/aggregators/users/{uid}/stacks/{sid}")
                        .buildAndExpand(uid, result.getDetails().getId().toString())
                        .toUri());
        LOG.info("Stack updated: " + newStack.toString());

        return new ResponseEntity<Stack>(newStack, headers, HttpStatus.OK);
    }
}
