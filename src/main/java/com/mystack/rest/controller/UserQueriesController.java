package com.mystack.rest.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.services.UserService;
import com.mystack.rest.domain.User;

@Controller

@RequestMapping("/aggregators/")
public class UserQueriesController {

    private static Logger LOG = LoggerFactory.getLogger(UserQueriesController.class);

    @Autowired
    private UserService   service;

    @RequestMapping(method = RequestMethod.GET, value = "users", params = { "name" })
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<List<User>> getListUsers(
            @RequestParam(value = "name") String searched_name,
            @RequestParam(value = "exact", defaultValue = "true") String exact,
            Principal principal) {

        boolean exact_match = !exact.equals("false");
        LOG.info("ViewUser called with searched_name: " + searched_name + "(logged in user: " + principal.getName() + ")");
        AllUsersEvent alluser_event = service.getAllUsers(new RequestAllUsersEvent(searched_name, exact_match));

        if (!alluser_event.isEntityFound()) {
            return new ResponseEntity<List<User>>(HttpStatus.NOT_FOUND);
        }

        List<User> result = new ArrayList<>();

        for (UserDetails details : alluser_event.getAllUserDetails()) {
            result.add(User.fromDetails(details));
        }

        return new ResponseEntity<List<User>>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.HEAD, value = "users/{name}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<User> checkUsernameAvailability(@PathVariable("name") String name) {

        AllUsersEvent alluser_event = service.getAllUsers(new RequestAllUsersEvent(name, true));

        if (!alluser_event.isEntityFound()) {
            return new ResponseEntity<User>(HttpStatus.OK);
        }
        return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = "users/{uid}")
    public ResponseEntity<User> viewUser(@PathVariable("uid") String uid) {

        LOG.info("ViewUser called for user:" + uid);

        UserDetailsEvent details_event = service.getUserDetails(new RequestUserDetailsEvent(UUID.fromString(uid)));

        if (!details_event.isEntityFound()) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }

        User user = User.fromDetails(details_event.getDetails());

        return new ResponseEntity<User>(user, HttpStatus.OK);

    }
}