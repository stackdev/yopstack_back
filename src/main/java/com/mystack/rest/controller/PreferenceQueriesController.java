package com.mystack.rest.controller;

import java.security.Principal;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.mystack.config.ConstantString;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.services.PreferenceService;
import com.mystack.rest.domain.Preference;

@Controller
@RequestMapping("/aggregators/users/{uid}")
public class PreferenceQueriesController {

    private static Logger     LOG = LoggerFactory.getLogger(PreferenceQueriesController.class);

    @Autowired
    private PreferenceService service;

    @RequestMapping(method = RequestMethod.GET, value = "/preferences")
    public ResponseEntity<Preference> readPreference(@PathVariable("uid") String uid, UriComponentsBuilder builder, Principal authenticated_user) {

        UUID user_id = UUID.fromString(uid);
        Preference preference = new Preference(user_id);

        LOG.info("Reading preference called for user:" + uid + "and preference: " + preference);

        PreferenceReadEvent result = service.readPreference(new ReadPreferenceEvent(preference.toDetails(), authenticated_user.getName()));

        LOG.info("Result::: " + result.getDetails().toString());

        if (!result.isEntityFound()) {
            return new ResponseEntity<Preference>(HttpStatus.NOT_FOUND);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path(ConstantString.PreferencesFullPath(uid.toString(), result.getDetails().getId().toString()))
                        .build()
                        .toUri());

        return new ResponseEntity<Preference>(Preference.fromDetails(result.getDetails()), headers, HttpStatus.OK);
    }
}
