package com.mystack.rest.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.stacks.request.RequestAllStacksFromUserEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsFromUserEvent;
import com.mystack.core.events.stacks.result.AllStacksFromUserEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.services.StackService;
import com.mystack.rest.domain.Stack;

@Controller
@RequestMapping("/aggregators/users/{uid}")
public class StackQueriesController {

    private static Logger LOG = LoggerFactory.getLogger(StackQueriesController.class);

    @Autowired
    private StackService  service;

    @RequestMapping(method = RequestMethod.GET, value = "/stacks")
    public ResponseEntity<List<Stack>> getAllStacks(@PathVariable("uid") String uid, Principal authenticated_user) {
        try {

            UUID uid_u = UUID.fromString(uid);
            LOG.info("GetAllStacks called for user:" + uid);
            List<Stack> list = new ArrayList<Stack>();
            AllStacksFromUserEvent event = service.getAllStacks(new RequestAllStacksFromUserEvent(uid_u, authenticated_user.getName()));
            if (!event.isEntityFound()) {
                LOG.info("No Stack list returned by service layer");
                return new ResponseEntity<List<Stack>>(HttpStatus.NOT_FOUND);
            }
            LOG.info("Service layer returned a list of stacks");
            //convert the list of stack details to list of stack
            for (StackDetails detail : event.getStackDetailsList()) {
                list.add(Stack.fromDetails(detail));
            }
            return new ResponseEntity<List<Stack>>(list, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            LOG.error("Unable to convert %s as an UUID, no user identified, bad request", uid);
            return new ResponseEntity<List<Stack>>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error("Unexpected exception raised while getting stack for user: %s, exception: %s", uid, e.getMessage());
            return new ResponseEntity<List<Stack>>(HttpStatus.BAD_REQUEST);
        }
    }

    // TODO: dzd check if javac debug option is suitable for production. If yes, set it and remove " ("id") "
    @RequestMapping(method = RequestMethod.GET, value = "/stacks/{sid}")
    public ResponseEntity<Stack> viewStack(@PathVariable("uid") String uid, @PathVariable("sid") String sid, Principal authenticated_user) {
        try {
            UUID stack_id = UUID.fromString(sid);
            UUID user_id = UUID.fromString(uid);

            LOG.info("ViewStack called for user:" + uid + " and stack: " + sid);

            StackDetailsEvent details_event = service.getStackDetails(new RequestStackDetailsFromUserEvent(user_id, stack_id, authenticated_user.getName()));

            if (details_event == null || !details_event.isEntityFound()) {
                return new ResponseEntity<Stack>(HttpStatus.NOT_FOUND);
            }

            Stack stack = Stack.fromDetails(details_event.getDetails());

            return new ResponseEntity<Stack>(stack, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            LOG.error("Unable to use: %s or %s as a UUID, %s", uid, sid, e.getMessage());
            return new ResponseEntity<Stack>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error("Unexpected exception raised: " + e.getMessage());
            return new ResponseEntity<Stack>(HttpStatus.BAD_REQUEST);
        }
    }
}