package com.mystack.rest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.comments.request.RequestAllCommentsEvent;
import com.mystack.core.events.comments.request.RequestCommentDetailsEvent;
import com.mystack.core.events.comments.result.AllCommentsEvent;
import com.mystack.core.events.comments.result.CommentDetailsEvent;
import com.mystack.core.services.CommentService;
import com.mystack.rest.domain.Comment;

@Controller
//@RequestMapping("/aggregators/users/{uid}/stacks/{sid}/checks/{check_id}/comments")
public class CommentQueriesController {

    private static Logger  LOG = LoggerFactory.getLogger(CommentQueriesController.class);

    @Autowired
    private CommentService service;

    /**
     * REST URI for retrieval of comment for a given stack @param stack_id between @param before_date and @param after_date
     * 
     * @param check_id
     * @param before_date
     * @param after_date
     * @return
     */
    @RequestMapping(path = "/aggregators/users/{uid}/stacks/{sid}/checks/{check_id}/comments",
    	        	method = RequestMethod.GET, params = { "before_date", "after_date" })
    public ResponseEntity<List<Comment>> getListBetweenDate(
            @PathVariable("uid") String uid,
            @PathVariable("sid") String sid,
            @PathVariable("check_id") String check_id,
            @RequestParam(value = "before_date", required = false) String before_date,
            @RequestParam(value = "after_date", required = false) String after_date) {

        UUID userid = UUID.fromString(uid);
        UUID stackid = UUID.fromString(sid);
        UUID checkid = UUID.fromString(check_id);
        LOG.info("Getlist comment called with searched_name: " + checkid + ", before_date: " + before_date + " ,after_date: " + after_date);

        return getListComment(new RequestAllCommentsEvent(stackid, userid, checkid, before_date, after_date));

    }

    /**
     * REST URI for retrieval of comment for a given stack @param stack_id limiting the number of comment return with @param count
     * 
     * @param stack_id
     * @param count
     * @return
     */
    @RequestMapping(path = "/aggregators/users/{uid}/stacks/{sid}/checks/{check_id}/comments",
    		        method = RequestMethod.GET)
    public ResponseEntity<List<Comment>> getListComments(
            @PathVariable("uid") String uid,
            @PathVariable("sid") String sid,
            @PathVariable("check_id") String check_id,
            @RequestParam(value = "count", required = false, defaultValue = "0") int count) {

        UUID userid = UUID.fromString(uid);
        UUID stackid = UUID.fromString(sid);
        UUID checkid = UUID.fromString(check_id);
        LOG.info("Getlist comment called with count: " + String.valueOf(count));

        return getListComment(new RequestAllCommentsEvent(stackid, userid, checkid, "", count));

    }
    
    @RequestMapping(path = "/aggregators/users/{uid}/stacks/{sid}/comments/" ,
	                method = RequestMethod.GET)
    public ResponseEntity<List<Comment>> getListCommentsFromListChecks(
            @PathVariable("uid") String uid,
            @PathVariable("sid") String sid,
            @RequestParam List<String> checkids,
            @RequestParam(value = "count", required = false, defaultValue = "0") int count) {

        UUID userid = UUID.fromString(uid);
        UUID stackid = UUID.fromString(sid);
        List<UUID> checkidsUUID = checkids.stream().map(id -> UUID.fromString(id)).collect(Collectors.toList());
        
        LOG.info("Getlist comment called with count: " + String.valueOf(count));
        
        AllCommentsEvent event = service.getAllCommentsFromChecks(new RequestAllCommentsEvent(stackid, userid, checkidsUUID, "", count));
        
        
        // WIP to be cleaned up /refactor
        List<Comment> result = new ArrayList<>();

        for (CommentDetails details : event.getAllCommentDetails()) {
            result.add(Comment.FromDetails(details));
        }

        return new ResponseEntity<List<Comment>>(result, HttpStatus.OK);

    }

    /**
     * REST URI for retrieval of comment with a giveb id @param id
     * 
     * @param comid
     * @return
     */
    @RequestMapping(path = "/aggregators/users/{uid}/stacks/{sid}/checks/{check_id}/comments/{comid}",
    		        method = RequestMethod.GET)
    public ResponseEntity<Comment> viewComment(
            @PathVariable("uid") String uid,
            @PathVariable("sid") String sid,
            @PathVariable("check_id") String check_id,
            @PathVariable("comid") String comid) {

        return handleCommentQuery(uid, sid, check_id, comid);

    }

    @RequestMapping(path = "/aggregators/comments/{comid}",
            method = RequestMethod.GET)
    public ResponseEntity<Comment> viewComment(
            @PathVariable("comid") String comid) {

        return handleCommentQuery(UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                                  UUID.randomUUID().toString(), comid);
    }

    private ResponseEntity<Comment> handleCommentQuery(String uid, String sid, String check_id, String comid) {
        LOG.info("View Comment called with comid:" + comid);

        CommentDetailsEvent details_event = service.getCommentDetails(new RequestCommentDetailsEvent(UUID.fromString(uid),
                UUID.fromString(sid),
                UUID.fromString(check_id),
                UUID.fromString(comid)));

        if (!details_event.isEntityFound()) {
            return new ResponseEntity<Comment>(HttpStatus.NOT_FOUND);
        }

        Comment comment = Comment.FromDetails(details_event.getDetails());

        return new ResponseEntity<Comment>(comment, HttpStatus.OK);
    }

    /**
     * Doing the interaction with the service layer
     * 
     * @param request_event
     * @return
     */
    private ResponseEntity<List<Comment>> getListComment(RequestAllCommentsEvent request_event) {
        try {
            AllCommentsEvent allcomments_event = service.getAllComments(request_event);

            LOG.info("result: " + allcomments_event);
            if (!allcomments_event.isEntityFound()) {
                return new ResponseEntity<List<Comment>>(HttpStatus.NOT_FOUND);
            }

            List<Comment> result = new ArrayList<>();

            for (CommentDetails details : allcomments_event.getAllCommentDetails()) {
                result.add(Comment.FromDetails(details));
            }

            return new ResponseEntity<List<Comment>>(result, HttpStatus.OK);

        } catch (IllegalArgumentException e) {
            LOG.error("Invalid id provided: " + request_event.getStack_id().toString() + ", " + e.getMessage());
            return new ResponseEntity<List<Comment>>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error("Unexpected Exception encountered: " + e.getMessage());
            return new ResponseEntity<List<Comment>>(HttpStatus.BAD_REQUEST);
        }
    }

}
