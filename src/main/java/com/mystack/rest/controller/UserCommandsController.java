package com.mystack.rest.controller;

import java.security.Principal;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.mystack.core.events.CreatedEvent;
import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.core.services.UserService;
import com.mystack.rest.domain.User;

@Controller
@RequestMapping("/aggregators/users")
public class UserCommandsController {

    private static Logger LOG = LoggerFactory.getLogger(UserCommandsController.class);

    @Autowired
    private UserService   service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> createUser(@RequestBody User user, UriComponentsBuilder builder, Principal authenticated_user) {
        LOG.info("let's create a user !: " + user.toString());
        UserCreatedEvent userCreated = service.createUser(new CreateUserEvent(user.toDetails()));

        //check if the creation was successful
        if (CreatedEvent.CODE_OK != userCreated.getCreationCode()) {
            LOG.info("Creation failed");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }

        LOG.info("UserCreatedEvent's details's description: " + userCreated.getDetails().toString());

        User newUser = User.fromDetails(userCreated.getDetails());
        LOG.info("User created, to be returned: " + newUser.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path("/aggregators/users/{uid}")
                        .buildAndExpand(userCreated.getDetails().getId().toString())
                        .toUri());

        return new ResponseEntity<User>(newUser, headers, HttpStatus.CREATED);

    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{uid}")
    public ResponseEntity<User> updateUser(@PathVariable("uid") String uid, @RequestBody User user, UriComponentsBuilder builder, Principal authenticated_user) {
        LOG.info("let's update a user !: " + uid + ", " + user.toString());
        UserUpdatedEvent userUpdated = service.updateUser(new UpdateUserEvent(user.toDetails(), authenticated_user.getName()));
        User newUser = User.fromDetails(userUpdated.getDetails());

        if (!userUpdated.isUpdated()) {
            return new ResponseEntity<User>(newUser, new HttpHeaders(), HttpStatus.NOT_FOUND);
        }

        LOG.info("UserUpdatedEvent's details's description: " + userUpdated.getDetails().toString());

        LOG.info("User updated, to be returned: " + newUser.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path("/aggregators/users/{uid}")
                        .buildAndExpand(userUpdated.getDetails().getId().toString())
                        .toUri());

        return new ResponseEntity<User>(newUser, headers, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{uid}")
    public ResponseEntity<User> deleteUser(@PathVariable("uid") String uid, Principal authenticated_user) {

        UserDeletedEvent deleted_event = service.deleteUser(new DeleteUserEvent(UUID.fromString(uid)));

        if (!deleted_event.isEntityFound()) {
            LOG.info("User not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        LOG.info("User found, try to delete");
        User user = User.fromDetails(deleted_event.getDetails());

        if (deleted_event.isDeletionCompleted()) {
            LOG.info("User deleted");
            return new ResponseEntity<User>(user, HttpStatus.OK);
        }
        LOG.info("User not deleted");
        return new ResponseEntity<User>(user, HttpStatus.FORBIDDEN);

    }

    //TODO: handle update user
}
