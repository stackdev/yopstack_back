package com.mystack.config;

public class ConstantString {

    final static String base_path   = "/aggregators/";
    final static String users       = "users/";
    final static String checks      = "checks/";
    final static String stacks      = "stacks/";
    final static String preferences = "preferences/";
    final static String metions = "mentions/";

    public static String BasePath() {
        return base_path;
    }

    /** --- stack -- **/
    public static String StacksShortPath() {
        return stacks;
    }

    public static String StacksFullPath(String uid) {
        return StacksFullPath(uid, "");
    }

    public static String StacksFullPath(String uid, String sid) {
        return base_path + users + uid + "/" + stacks + sid;
    }

    /** --- user -- **/
    public static String UsersShortPath() {
        return users;
    }

    public static String UsersFullPath() {
        return UsersFullPath("");
    }

    public static String UsersFullPath(String uid) {
        return base_path + users + uid;
    }

    /** --- check -- **/
    public static String ChecksShortPath() {
        return checks;
    }

    public static String ChecksFullPath() {
        return ChecksFullPath("");
    }

    public static String ChecksFullPath(String cid) {
        return base_path + checks + cid;
    }

    /** --- preference -- **/
    public static String PreferencesShortPath() {
        return preferences;
    }

    public static String PreferencesFullPath(String uid) {
        return PreferencesFullPath(uid, "");
    }

    public static String PreferencesFullPath(String uid, String pid) {
        return base_path + users + uid + "/" + preferences + pid;
    }

    public static String MentionsFullPath(String mid) {
        return base_path +  "/" + metions + mid;
    }

}
