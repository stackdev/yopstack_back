package com.mystack.config;

import java.util.logging.Logger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

// @Configuration
@PropertySource("classpath:application.properties")
public class FunctionalConfiguration {

    private static Logger     LOG                        = Logger.getLogger(FunctionalConfiguration.class.toString());

    //TODO: to be done via a bean and properties, temporary harcoded

    //@Value("#{stack.default.frequency}")
    public static int         DEFAULT_STACK_FREQUENCY    = 1;

    //@Value("#{stack.default.periodicity}")
    public static int         DEFAULT_STACK_PERIODICITY  = 2;
    public static StackType   DEFAULT_STACK_TYPE         = StackType.TIME;
    public static boolean     DEFAULT_STACK_PRIVACY      = false;
    public static StackStatus DEFAULT_STACK_STATUS       = StackStatus.RUNNING;
    public static boolean     DEFAULT_STACK_SHARED_STATE = false;

    // Maximum number of checks return in a get all check request
    public static int         CHECK_MAX_GET_COUNT        = 10;

    public static int         COMMENT_MAX_GET_COUNT      = 10;

    public static int         MENTION_MAX_GET_COUNT        = 10;

    // stack types
    public enum StackType {
        TIME(0), NOTES(1), SHARED(2);
        private int value;

        private StackType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    // Stack status
    public enum StackStatus {
        RUNNING(0), PAUSED(1), ARCHIVED(2);
        private int value;

        private StackStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    // stack sorting types
    public enum StackSorting {
        CREATION(0), TYPE(1), LASTCHECK(2);
        private int value;

        private StackSorting(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public enum StackSortingDirection {
        FIRST(0), LAST(1);
        private int value;

        private StackSortingDirection(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    // Check status
    public enum CheckStatus {
        PLANNED(0), OK(1), FAILED(2), SKIPPED(3);
        private int value;

        private CheckStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }
    
    //Action type, used in shared logic between create/delete and update service methods
    public enum ActionType {
    	CREATE(0), UPDATE(1), DELETE(2);
        private int value;

        private ActionType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }    	
    }
    
    public enum Theme {
    	DEFAULT(0), DARK(1);
        private int value;

        private Theme(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }    	
    }
    
    public enum TimeZone {
    	WET(0), CET(1), EST(-5);
        private int value;

        private TimeZone(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }    	
    }
    
    public enum Language {
    	EN(0), FR(1), SP(1), DE(2);
        private int value;

        private Language(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }    	
    }


    // Needed to replace the properties name by the value read in PropertySource
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}