package com.mystack.config;

import com.mystack.core.services.*;
import com.mystack.persistence.repository.MentionCRUDRepository;
import com.mystack.persistence.services.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class CoreConfig {

    @Bean
    public StackService createStackService(StackPersistenceService stackPersistenceService) {
        return new StackEventHandler(stackPersistenceService);
    }

    @Bean
    public StackPersistenceService createStackPersistenceService() {
        return new StackPersistenceEventHandler();
    }

    @Bean
    public UserService createUserService(UserPersistenceService userPersistenceService) {
        return new UserEventHandler(userPersistenceService);
    }

    @Bean
    public UserPersistenceService createUserPersistenceService() {
        return new UserPersistenceEventHandler();
    }

    @Bean
    public CheckService createCheckService(CheckPersistenceService check_persistence_service, StackService stack_service) {
        return new CheckEventHandler(check_persistence_service, stack_service);
    }

    @Bean
    public CheckPersistenceService createCheckPersistenceService() {
        return new CheckPersistenceEventHandler();
    }

    @Bean
    public CommentService createCommentService(CommentPersistenceService persistence,
                                               StackService stackService,
                                               CheckService checkService,
                                               UserService userService,
                                               MentionService mentionService) {
        return new CommentEventHandler(persistence, stackService, userService, checkService, mentionService);
    }

    @Bean
    public CommentPersistenceService createCommentPersistenceService() {
        return new CommentPersistenceEventHandler();
    }

    @Bean
    public PreferenceService createPreferenceService(PreferencePersistenceService preference_persistence_service) {
        return new PreferenceEventHandler(preference_persistence_service);
    }

    @Bean
    public PreferencePersistenceService createPreferencePersistenceService() {
        return new PreferencePersistenceEventHandler();
    }

    @Bean
    public StackUserAuthenticationManager createStackUserAuthenticationManager() {
        return new StackUserAuthenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    public MentionPersistenceService createMentionPersistenceService(MentionCRUDRepository repo) {
        return new MentionPersistenceEventHandler(repo);
    }


    @Bean
    public MentionService createMentionService(MentionPersistenceService persistenceService) {
        return new MentionEventHandler(persistenceService);
    }

}