package com.mystack.config;

import static com.mongodb.connection.ClusterSettings.builder;
import static java.util.Arrays.asList;

import java.net.UnknownHostException;

import com.mongodb.Block;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClients;
import com.mongodb.connection.ClusterSettings;
import com.mystack.persistence.repository.*;
import org.bson.UuidRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.client.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@Configuration
@PropertySource("classpath:application.properties")
@EnableMongoRepositories(basePackages = "com.mystack.persistence.repository",
        includeFilters = @ComponentScan.Filter(value = { StackCRUDRepository.class, UserCRUDRepository.class,
                CheckCRUDRepository.class, CommentCRUDRepository.class, PreferenceCRUDRepository.class,
                MentionCRUDRepository.class },
                type = FilterType.ASSIGNABLE_TYPE))
public class MongoConfiguration extends AbstractMongoClientConfiguration {

    private static Logger LOG = LoggerFactory.getLogger(MongoConfiguration.class);

    //Needed to have system env variable registered as source of properties
    @Autowired
    Environment           environment;

    @Value("${mongodb.uri}")
    String                mongodbUri;

    @Value("${mongodb.host}")
    String                mongodbHost;

    @Value("${mongodb.port}")
    String                mongodbPort;

    @Value("${stack.db.name}")
    String                stackDbName;
    @Value("${stack.db.user}")
    String                stackDbUser;
    @Value("${stack.db.pass}")
    String                stackDbPass;

    public @Bean MongoTemplate mongoTemplate(MongoClient mongoClient) throws UnknownHostException {

        try {
            LOG.info("Mongo db name: " + stackDbName);
            return new MongoTemplate(mongoClient, stackDbName);
        } catch (Exception e) {
            LOG.info("Exception??: " + e.toString() + ", text: " + e.getMessage());
            return null;
        }
    }

    @Bean
    public MongoClient mongoClient() {

        Block<ClusterSettings.Builder> clusterSettingsBlock = new Block<>() {
            @Override
            public void apply(ClusterSettings.Builder b) {
                b.applySettings(ClusterSettings.builder()
                        .hosts(asList(new ServerAddress(mongodbHost, Integer.parseInt(mongodbPort)))).build());
            }
        };
        MongoClientSettings.Builder settingsBuilder = MongoClientSettings.builder().
                applyToClusterSettings(clusterSettingsBlock)
                .uuidRepresentation(UuidRepresentation.JAVA_LEGACY);


        if ("" != stackDbUser && "" != stackDbPass) {
            LOG.info("Using stack user:" + stackDbUser + ", db pass: " + stackDbPass);
            settingsBuilder.credential(MongoCredential.createCredential(stackDbUser, stackDbName, stackDbPass.toCharArray()));
        }

        return MongoClients.create(settingsBuilder.build());
    }

    // Needed to replace the properties name by the value read in PropertySource
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Override
    protected String getDatabaseName() {
        return stackDbName;
    }

    @Override
    protected boolean autoIndexCreation() {
        return true;
    }
}
