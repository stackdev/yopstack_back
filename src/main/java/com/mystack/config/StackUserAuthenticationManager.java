package com.mystack.config;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;

import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.services.UserService;

public class StackUserAuthenticationManager implements UserDetailsManager {

    private static Logger LOG = LoggerFactory.getLogger(StackUserAuthenticationManager.class);

    @Autowired
    private UserService   user_service;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.mystack.core.events.users.result.UserDetailsEvent event = user_service.getUserDetails(new RequestUserDetailsEvent(username));

        if (!event.isEntityFound()) {
            LOG.info("user: " + username + " not found, not processing the authentication further");
            throw new UsernameNotFoundException(username);
        }

        com.mystack.core.domain.UserDetails user = event.getDetails();
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (String role : user.getRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role));
        }

        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), grantedAuthorities);
    }

    @Override
    public void createUser(UserDetails user) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateUser(UserDetails user) {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteUser(String username) {
        // TODO Auto-generated method stub

    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean userExists(String username) {
        LOG.info("userExists called for user: " + username);
        return false;
    }

}
