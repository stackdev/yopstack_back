package com.mystack.config;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity(debug = false)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static Logger          LOG = LoggerFactory.getLogger(SecurityConfig.class);

    @Autowired
    StackUserAuthenticationManager stack_user_auth_manager;

    @Autowired
    PasswordEncoder                password_encoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(stack_user_auth_manager).passwordEncoder(password_encoder);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .cors()
                .and()
                .httpBasic()
                .and()
                .authorizeRequests()
                	//.antMatchers("/aggregators/**").hasRole("USER")
                	.antMatchers(HttpMethod.POST, "/aggregators/users").permitAll()
                	.antMatchers(HttpMethod.POST, "/aggregators/users/").permitAll()
                	.antMatchers(HttpMethod.HEAD, "/aggregators/users/**").permitAll()
                    .antMatchers(HttpMethod.GET, "/actuator/info").permitAll()
                    .antMatchers(HttpMethod.GET, "/actuator/metrics").permitAll()
                    .antMatchers(HttpMethod.GET, "/actuator/prometheus").permitAll()
                	.anyRequest().authenticated()
                .and()
                    .logout()
                .and()
                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
                	.csrf()
                	.ignoringAntMatchers("/aggregators/users")
                	.csrfTokenRepository(csrfTokenRepository());


    }


    /*
     * Specify header name expected by angular for CSRF protection
     */
    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200", "https://www.yopstack.com"));
        configuration.setAllowedMethods(Arrays.asList("GET", "HEAD", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token", "x-requested-with", "x-xsrf-token"));
        configuration.setAllowCredentials(true);
        configuration.setExposedHeaders(Arrays.asList("x-requested-with"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

}