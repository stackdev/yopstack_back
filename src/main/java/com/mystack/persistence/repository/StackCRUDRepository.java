package com.mystack.persistence.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mystack.persistence.domain.Stack;

public interface StackCRUDRepository extends CrudRepository<Stack, UUID> {
    public List<Stack> findById(UUID... id);

    @Query("{id:{$in: ?0 }, isPrivate: false}")
    public List<Stack> findAllPublic(List<UUID> stackIdList);

    public List<Stack> findStackByCheckers(UUID checker);
}