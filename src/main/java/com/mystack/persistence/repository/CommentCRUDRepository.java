package com.mystack.persistence.repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mystack.persistence.domain.Comment;

public interface CommentCRUDRepository extends PagingAndSortingRepository<Comment, UUID> {
    List<Comment> findByCheckid(UUID checkid);

    List<Comment> findByCheckidOrderByTimestampDesc(UUID checkid, Pageable pageable);

    @Query("{checkid: {$in: ?0}}")
    List<Comment> findByCheckidOrderByTimestampDesc(List<UUID> checkid, Pageable pageable);
    
    @Query("{checkid: ?0, timestamp:{ $lt: ?1, $gte: ?2}}")
    List<Comment> findByCheckidBetweenTimestamps(UUID checkid, Date timestamp_before, Date timestamp_after);

    @Query("{checkid: {$in: ?0}, timestamp:{ $lt: ?1, $gte: ?2}}")
    List<Comment> findByCheckidsBetweenTimestamps(List<UUID> checkid, Date timestamp_before, Date timestamp_after);

}
