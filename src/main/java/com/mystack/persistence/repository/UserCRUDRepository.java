package com.mystack.persistence.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.mystack.persistence.domain.User;

public interface UserCRUDRepository extends CrudRepository<User, UUID> {
    List<User> findByName(String name);

    List<User> findByNameLike(String name);

}
