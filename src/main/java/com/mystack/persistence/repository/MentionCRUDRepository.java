package com.mystack.persistence.repository;

import com.mystack.persistence.domain.Mention;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MentionCRUDRepository extends PagingAndSortingRepository<Mention, UUID> {
    List<Mention> findByTo(UUID to);

    //@Query("{checkid: {$in: ?0}}")
    //List<Comment> findByCheckidOrderByTimestampDesc(List<UUID> checkid, Pageable pageable);
    //
    //@Query("{checkid: ?0, timestamp:{ $lt: ?1, $gte: ?2}}")
    //List<Comment> findByCheckidBetweenTimestamps(UUID checkid, Date timestamp_before, Date timestamp_after);
    //
    //@Query("{checkid: {$in: ?0}, timestamp:{ $lt: ?1, $gte: ?2}}")
    //List<Comment> findByCheckidsBetweenTimestamps(List<UUID> checkid, Date timestamp_before, Date timestamp_after);

}
