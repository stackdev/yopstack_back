package com.mystack.persistence.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.persistence.domain.Check;

public interface CheckCRUDRepository extends PagingAndSortingRepository<Check, UUID> {
    List<Check> findByStack(UUID stack);

    List<Check> findByStackOrderByDateDesc(UUID stack, Pageable pageable);

    @Query("{stack: ?0, date:{ $lt: ?1, $gte: ?2}}")
    List<Check> findByStackBetweenDates(UUID stack, Date date_before, Date date_after, Pageable pageable);

    @Query("{stack: ?0, date:{ $lt: ?1}}")
    List<Check> findByStackBeforeDate(UUID stack, Date date_before, Pageable pageable);
    
    Page<Check> findByStackAndStatusOrderByDateDesc(UUID stack, CheckStatus status, Pageable pageable);
}
