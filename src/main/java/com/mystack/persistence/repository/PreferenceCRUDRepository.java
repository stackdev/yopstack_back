package com.mystack.persistence.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.mystack.persistence.domain.Preference;

public interface PreferenceCRUDRepository extends CrudRepository<Preference, UUID> {
    public Preference findByUser(UUID user);
}