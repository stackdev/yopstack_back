package com.mystack.persistence.services;

import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;

public interface PreferencePersistenceService {

    PreferenceReadEvent readPreference(ReadPreferenceEvent event);

    PreferenceCreatedEvent createPreference(CreatePreferenceEvent event);

    PreferenceDeletedEvent deletePreference(DeletePreferenceEvent event);

    PreferenceUpdatedEvent updatePreference(UpdatePreferenceEvent event);
}
