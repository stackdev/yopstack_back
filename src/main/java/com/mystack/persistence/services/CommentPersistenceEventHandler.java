package com.mystack.persistence.services;

import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.comments.request.*;
import com.mystack.core.events.comments.result.*;
import com.mystack.persistence.domain.Comment;
import com.mystack.persistence.repository.CommentCRUDRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.*;
import java.util.stream.Collectors;

public class CommentPersistenceEventHandler implements CommentPersistenceService {

    private static Logger         LOG = LoggerFactory.getLogger(CommentPersistenceEventHandler.class);

    @Autowired
    private CommentCRUDRepository repo;

    @Override
    public AllCommentsEvent getAllComments(RequestAllCommentsEvent event) {
        List<Comment> result;
        LOG.info("Event: --- " + event.toString());

        if (null != event.getAfter_date() && null != event.getBefore_date()) {
            LOG.info("Both date_after and date_before are provided, will return check between those dates: " + event.getAfter_date().getTime() + " - " + event.getBefore_date().getTime());
            result = repo.findByCheckidBetweenTimestamps(event.getCheckid(), event.getBefore_date(), event.getAfter_date());
        } else {
            LOG.info("Getting all comment for check: " + event.getCheckid() + "with a page size of " + event.getCount());
            // search for all users in repo 
            result = repo.findByCheckidOrderByTimestampDesc(event.getCheckid(), PageRequest.of(0, event.getCount()));
        }
        // create a list of Stack details as integration domain does not know about core domain
        List<CommentDetails> result_details = new ArrayList<>();
        for (Comment comment : result) {
            result_details.add(comment.toDetails());
        }
        if (result_details.size() == 0) {
            LOG.info("No Comment found");
            return AllCommentsEvent.NotFound();
        }
        return new AllCommentsEvent(result_details);
    }
    
    @Override
    public AllCommentsEvent getAllCommentsFromChecks(RequestAllCommentsEvent event) {
        List<Comment> result;
        List<UUID> checkids = event.getCheckids();
        
        if (null != event.getAfter_date() && null != event.getBefore_date()) {
            LOG.info("Both date_after and date_before are provided, will return check between those dates: " + event.getAfter_date().getTime() + " - " + event.getBefore_date().getTime());
            
            result = checkids.stream()
            .flatMap(checkid -> 
            	repo.findByCheckidBetweenTimestamps(checkid, event.getBefore_date(), event.getAfter_date()).stream())
            .collect(Collectors.toList());
        } else {
            LOG.info("Getting all comment for check: " + event.getCheckids() + "with a page size of " + event.getCount());
            
            result = checkids.stream()
            .flatMap(checkid -> 
            	repo.findByCheckidOrderByTimestampDesc(checkid, PageRequest.of(0, event.getCount())).stream())
            .collect(Collectors.toList());
        }
        // create a list of Stack details as integration domain does not know about core domain
        List<CommentDetails> result_details = new ArrayList<>();
        for (Comment comment : result) {
            result_details.add(comment.toDetails());
        }
        if (result_details.size() == 0) {
            LOG.info("No Comment found");
            return AllCommentsEvent.NotFound();
        }
        return new AllCommentsEvent(result_details);
    }
    

    @Override
    public CommentDetailsEvent getCommentDetails(RequestCommentDetailsEvent event) {
        UUID commentId = event.getCommentid();
        if ( commentId == null) {
            commentId = event.getCommentid();
            LOG.info("Searching for comment with id:" + commentId);
        }
        CommentDetails details;
        Optional<Comment> comment = repo.findById(commentId);
        if (comment.isPresent()) {
            LOG.info("Comment found");
            details = comment.get().toDetails();
            return new CommentDetailsEvent(details);
        }
        LOG.info("Comment not found");
        return CommentDetailsEvent.notFound(commentId);
    }

    @Override
    public CommentCreatedEvent createComment(CreateCommentEvent event) {
        Comment comment = Comment.FromDetails(event.getDetails());
        //get a new id before saving 
        comment.setId(UUID.randomUUID());
        Comment savedComment = repo.save(comment);
        return new CommentCreatedEvent(savedComment.toDetails());
    }

    @Override
    public CommentDeletedEvent deleteComment(DeleteCommentEvent event) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CommentUpdatedEvent updateComment(UpdateCommentEvent event) {

        Optional<Comment> comment_to_update = repo.findById(event.getCommentDetails().getId());

        if (!comment_to_update.isPresent()) {
            return CommentUpdatedEvent.notFound(event.getCommentDetails());
        }
        Comment comment_to_save = comment_to_update.get();

        comment_to_save.setUpdateTimestamp(new Date());
        comment_to_save.setText(event.getCommentDetails().getText());

        Comment saved_comment = repo.save(comment_to_save);
        return new CommentUpdatedEvent(saved_comment.toDetails());
    }

    @Override
    public CommentUpdatedEvent updateCommentComment(UpdateCommentEvent event) {
        // TODO Auto-generated method stub
        return null;
    }

}
