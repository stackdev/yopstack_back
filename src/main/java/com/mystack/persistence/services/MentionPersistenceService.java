package com.mystack.persistence.services;

import com.mystack.core.events.mentions.request.CreateMentionEvent;
import com.mystack.core.events.mentions.request.ReadMentionEvent;
import com.mystack.core.events.mentions.request.RequestAllMentionsEvent;
import com.mystack.core.events.mentions.request.UpdateMentionEvent;
import com.mystack.core.events.mentions.result.AllMentionsEvent;
import com.mystack.core.events.mentions.result.MentionCreatedEvent;
import com.mystack.core.events.mentions.result.MentionReadEvent;
import com.mystack.core.events.mentions.result.MentionUpdatedEvent;

public interface MentionPersistenceService {

    MentionReadEvent getMentionDetails(ReadMentionEvent event);
    MentionCreatedEvent createMention(CreateMentionEvent event);

    AllMentionsEvent getAllMentions(RequestAllMentionsEvent event);
    MentionUpdatedEvent updateMention(UpdateMentionEvent event);


    //CommentDeletedEvent deleteMention(DeleteMentionEvent event);
    //CommentUpdatedEvent updateCommentComment(UpdateCommentEvent event);
}
