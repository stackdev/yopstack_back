package com.mystack.persistence.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksByIdEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.AllStacksByIdEvent;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.persistence.domain.Stack;
import com.mystack.persistence.repository.StackCRUDRepository;

public class StackPersistenceEventHandler implements StackPersistenceService {

    @Autowired
    private StackCRUDRepository repo;

    private static Logger       LOG = LoggerFactory.getLogger(StackPersistenceEventHandler.class);

    @Override
    public AllStacksEvent getAllStacks(RequestAllStacksEvent event) {
        Iterable<Stack> result;
        if (!event.isListidProvided()) {
            LOG.info("Stack List not provided");
            //TODO: remove this findAll without param, it is kept for backward compatibility
            // search for all stacks in repo
            result = repo.findAll();
        } else {
            LOG.info("Stack List provided");
            // Search for the details of specified stack id
            result = repo.findAllById(event.getStackid_list());
        }
        // create a list of Stack details as integration domain does not know about core domain
        List<StackDetails> result_details = new ArrayList<>();
        for (Stack stack : result) {
            result_details.add(stack.toDetails());
        }
        LOG.info(result_details.size() + " result found.");
        return new AllStacksEvent(result_details);
    }

    @Override
    public AllStacksEvent getAllCheckableStacks(RequestAllStacksEvent event) {
        LOG.info("getting all checkable stacks for user: " + event.getUserid_list().get(0));
        Iterable<Stack> result = repo.findStackByCheckers(event.getUserid_list().get(0));
        // create a list of Stack details as integration domain does not know about core domain
        List<StackDetails> result_details = new ArrayList<>();
        for (Stack stack : result) {
            result_details.add(stack.toDetails());
        }
        LOG.info(result_details.size() + " result found.");
        return new AllStacksEvent(result_details);
    }

    /**l
     * Get All stacks providing a list of Stack Id
     */
    @Override
    public AllStacksByIdEvent getAllStacks(RequestAllStacksByIdEvent event) {
        LOG.info("Received Event" + event);
        Iterable<Stack> result;
        if (event.isOnly_public()) {
            result = repo.findAllPublic(event.getStackid_list());
        } else {
            result = repo.findAllById(event.getStackid_list());
        }
        // create a list of Stack details as integration domain does not know about core domain
        List<StackDetails> result_details = new ArrayList<>();
        for (Stack stack : result) {
            result_details.add(stack.toDetails());
        }
        LOG.info(result_details.size() + " result found.");
        return new AllStacksByIdEvent(result_details);
    }

    /**
     * Return stack details if found by id. Else returns null
     */
    @Override
    public StackDetailsEvent getStackDetails(RequestStackDetailsEvent event) {
        UUID id = event.getId();
        LOG.info("Persistence getStackDetails called with stack id:" + id);
        StackDetails details = null;
        Optional<Stack> result = repo.findById(id);
        if (result.isPresent()) {
            LOG.info("Stack found");
            details = result.get().toDetails();
            //TODO: log here extra result returned
            return new StackDetailsEvent(details);
        }
        LOG.info("Stack not found");
        return StackDetailsEvent.notFound(id);
    }

    @Override
    public StackCreatedEvent createStack(CreateStackEvent event) {
        // create stack from details
        StackDetails details = event.getDetails();
        Stack stack = Stack.fromDetails(details);

        LOG.info("Creating stack in persistence: " + details.toString());
        LOG.info("Persistence stack description:" + stack.getDescription());

        //TODO: check where the id should be created
        //TODO: how to garanty ID unicity ? 
        UUID newId = UUID.randomUUID();
        stack.setId(newId);

        // TODO: add a control here to know if addition went good !!
        Stack savedStack = repo.save(stack);
        if (savedStack == null) {
            LOG.warn("No stack saved");
            return StackCreatedEvent.creationFailed(details);
        }
        LOG.info("Stack successfully saved: id " + savedStack.toString());
        return new StackCreatedEvent(savedStack.getId(), savedStack.toDetails());
    }

    @Override
    public StackDeletedEvent deleteStack(DeleteStackEvent event) {
        UUID id = event.getId();
        LOG.info("Trying to delete stack with id: " + id.toString());
        StackDeletedEvent result;
        Optional<Stack> stack = repo.findById(id);
        StackDetails details = null;
        if (!stack.isPresent()) {
            LOG.info("Stack with id " + id.toString() + " not found");
            return StackDeletedEvent.notFound(new StackDetails(id));
        }

        details = stack.get().toDetails();
        LOG.info("Stack to be deleted found: " + details.getId().toString());
        repo.deleteById(id);
        result = new StackDeletedEvent(details);
        return result;
    }

    public List<UUID> checkStackList(List<UUID> list) {
        LOG.info("Checking stack list of " + list.size() + " stacks");
        Iterator<UUID> it = list.iterator();
        UUID id;
        List<UUID> result = new ArrayList<>();
        while (it.hasNext()) {
            id = it.next();
            LOG.info("in while: searching for: " + id.toString());
            if (repo.existsById(id)) {
                LOG.info("exists");
                result.add(id);
            }
        }

        LOG.info("Returning a list of " + result.size() + " stacks");
        return result;
    }

    @Override
    public StackUpdatedEvent updateStack(UpdateStackEvent event) {

        // create stack from details
        StackDetails details = event.getStackDetails();

        // search for the stack in repo
        Optional<Stack> stack_to_update = repo.findById(details.getId());
        if (!stack_to_update.isPresent()) {
            return StackUpdatedEvent.notFound(event.getStackDetails());
        }

        //FIXME: temporary here for update we keep only stack_id and creation date un-updatable
        Stack stack = Stack.fromDetails(details);
        stack.setId(stack_to_update.get().getId());
        stack.setCreation_date(stack_to_update.get().getCreation_date());
        // ----

        LOG.info("Updating stack in persistence: " + stack.toString());

        Stack saved_stack = repo.save(stack);
        if (saved_stack == null) {
            LOG.warn("No stack saved");
            return StackUpdatedEvent.notUpdated(stack.toDetails());
        }
        LOG.info("Stack successfully updated: " + saved_stack.toString());
        return new StackUpdatedEvent(saved_stack.toDetails());
    }
}
