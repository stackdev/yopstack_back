package com.mystack.persistence.services;

import com.mystack.core.domain.MentionDetails;
import com.mystack.core.events.mentions.request.*;
import com.mystack.core.events.mentions.result.*;
import com.mystack.persistence.domain.Mention;
import com.mystack.persistence.repository.MentionCRUDRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MentionPersistenceEventHandler implements MentionPersistenceService {

    private static Logger         LOG = LoggerFactory.getLogger(MentionPersistenceEventHandler.class);

    private MentionCRUDRepository repo;

    public MentionPersistenceEventHandler(MentionCRUDRepository repo){
        this.repo = repo;
    }

    @Override
    public MentionReadEvent getMentionDetails(ReadMentionEvent event) {
        return null;
    }

    @Override
    public MentionCreatedEvent createMention(CreateMentionEvent event) {
        Mention saved = repo.save(Mention.fromDetails(event.getDetails()));
        if (saved == null) {
            LOG.warn("No Mention saved");
            return MentionCreatedEvent.creationFailed(event.getDetails());
        }
        LOG.info("Mention successfully saved: id " + saved.toString());
        return new MentionCreatedEvent(saved.toDetails());
    }

    @Override
    public AllMentionsEvent getAllMentions(RequestAllMentionsEvent event) {
        List<Mention> result = repo.findByTo(event.getUserId());
        if (result.isEmpty()) {
            return AllMentionsEvent.NotFound();
        }
        return new AllMentionsEvent(result.stream().map(mention -> mention.toDetails()).collect(Collectors.toList()));
    }

    @Override
    public MentionUpdatedEvent updateMention(UpdateMentionEvent event) {
        MentionDetails updatedEvent = event.getDetails();
        Optional<Mention> mention = repo.findById(updatedEvent.getId());
        if (!mention.isPresent()) {
            return (MentionUpdatedEvent) MentionUpdatedEvent.notFound();
        }

        // only a few fields can be updated
        Mention mention1 = mention.get();
        mention1.setViewed(updatedEvent.getViewed());

        return new MentionUpdatedEvent(mention1.toDetails());
    }

}
