package com.mystack.persistence.services;

import com.mystack.core.events.comments.request.CreateCommentEvent;
import com.mystack.core.events.comments.request.DeleteCommentEvent;
import com.mystack.core.events.comments.request.RequestAllCommentsEvent;
import com.mystack.core.events.comments.request.RequestCommentDetailsEvent;
import com.mystack.core.events.comments.request.UpdateCommentEvent;
import com.mystack.core.events.comments.result.AllCommentsEvent;
import com.mystack.core.events.comments.result.CommentCreatedEvent;
import com.mystack.core.events.comments.result.CommentDeletedEvent;
import com.mystack.core.events.comments.result.CommentDetailsEvent;
import com.mystack.core.events.comments.result.CommentUpdatedEvent;

public interface CommentPersistenceService {

    AllCommentsEvent getAllComments(RequestAllCommentsEvent event);

    CommentDetailsEvent getCommentDetails(RequestCommentDetailsEvent event);

    CommentCreatedEvent createComment(CreateCommentEvent event);

    CommentDeletedEvent deleteComment(DeleteCommentEvent event);

    CommentUpdatedEvent updateComment(UpdateCommentEvent event);

    CommentUpdatedEvent updateCommentComment(UpdateCommentEvent event);

	AllCommentsEvent getAllCommentsFromChecks(RequestAllCommentsEvent event);
}
