package com.mystack.persistence.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.persistence.domain.User;
import com.mystack.persistence.repository.UserCRUDRepository;

public class UserPersistenceEventHandler implements UserPersistenceService {

    @Autowired
    private UserCRUDRepository repo;

    private static Logger      LOG = LoggerFactory.getLogger(UserPersistenceEventHandler.class);

    @Override
    public AllUsersEvent getAllUsers(RequestAllUsersEvent event) {
        Iterable<User> result;
        String searched_name = event.getSearchedName();
        if (null != searched_name && !"".equals(searched_name)) {
            LOG.info("Calling persistence with searched_name: " + event.getSearchedName());
            if (event.getExactmatch()) {
                // search for all users in repo
                LOG.info("Exact match search");
                result = repo.findByName(event.getSearchedName());
            } else {
                LOG.info("Non exact match search");
                result = repo.findByNameLike(event.getSearchedName());
            }
        } else {
            LOG.info("Calling persistence to search for all users");
            // search for all users in repo
            result = repo.findAll();
        }
        LOG.info("result: " + result);

        if (null == result) {
            LOG.info("Null result returned by persistence");
            return AllUsersEvent.NotFound();
        }

        // create a list of User details as integration domain does not know about core domain
        List<UserDetails> result_details = new ArrayList<>();
        for (User user : result) {
            result_details.add(user.toDetails());
        }

        if (result_details.isEmpty()) {
            LOG.info("result_details list is empty");
            return AllUsersEvent.NotFound();
        }

        return new AllUsersEvent(result_details);
    }

    /**
     * Return stack details if found by id. Else returns null
     */
    @Override
    public UserDetailsEvent getUserDetails(RequestUserDetailsEvent event) {

        UUID id = event.getId();
        String name = event.getSearchedName();

        LOG.info("Searching for user with id:" + id);
        UserDetails details = null;
        try {
            User result;
            if (null != name && !"".equals(name)) {
                result = getUserByName(name);
            } else {
                result = getUserById(id);
            }
            if (result != null) {
                LOG.info("User found");
                details = result.toDetails();
                return new UserDetailsEvent(details);
            }
            LOG.info("User not found");
            return UserDetailsEvent.notFound(id);

        } catch (Exception e) {
            LOG.info("Exception raise will trying to retrieve user with id: " + id);
            return UserDetailsEvent.notFound(id);
        }

    }

    @Override
    public UserCreatedEvent createUser(CreateUserEvent event) {
        // create stack from details
        UserDetails details = event.getDetails();
        User user = User.fromDetails(details);
        LOG.info("Creating user in persistence: " + details.toString());
        LOG.info("Created user created:" + user.toString());

        // TODO: how to garanty ID unicity ?
        UUID new_id = UUID.randomUUID();
        user.setId(new_id);

        // check existence of user id in friend list
        List<UUID> friendList = user.getList_friend();
        if (null != friendList) {
            user.setList_friend(checkUserList(friendList));
        }

        // TODO: factorize code with the same test in update
        // Check if user with same name does not exist
        List<User> same_name_user = repo.findByName(details.getName());
        if (null != same_name_user && same_name_user.size() != 0) {
            LOG.info("Name already in use: " + details.getName() + ", creation failed");
            UserCreatedEvent failed_event = UserCreatedEvent.creationFailed(details);
            failed_event.setFailedAlreadyExists();
            return failed_event;
        }

        User saved_user = repo.save(user);
        if (saved_user == null) {
            LOG.warn("No User saved");
            return UserCreatedEvent.creationFailed(details);
        }
        LOG.info("Input user             : id " + user.toString());
        LOG.info("User successfully saved: id " + saved_user.toString());
        return new UserCreatedEvent(saved_user.getId(), saved_user.toDetails());
    }

    @Override
    public UserDeletedEvent deleteUser(DeleteUserEvent event) {

        UUID id = event.getId();
        Optional<User> user = repo.findById(id);
        UserDetails details = null;

        UserDeletedEvent result_event;

        if (!user.isPresent()) {
            LOG.info("User not found with ID: " + id.toString());
            return UserDeletedEvent.notFound(id);
        }

        details = user.get().toDetails();
        LOG.info("Deleting User with ID: " + details.getId());
        repo.deleteById(id);
        return new UserDeletedEvent(details);

    }

    public List<UUID> checkUserList(List<UUID> list) {
        LOG.info("Checking friend list of " + list.size() + " friends");
        Iterator<UUID> it = list.iterator();
        UUID id;
        List<UUID> result = new ArrayList<>();
        while (it.hasNext()) {
            id = it.next();
            LOG.info("in while: searching for: " + id.toString());
            if (repo.existsById(id)) {
                LOG.info("exists");
                result.add(id);
            }
        }

        LOG.info("Returning a list of " + result.size() + " friends");
        return result;
    }

    @Override
    public UserUpdatedEvent updateUser(UpdateUserEvent event) {
        LOG.info("Updating user: " + event.getUserDetails().getName() + " with ID: " + event.getUserDetails().getId().toString());
        // check user existence
        UserDetails details = event.getUserDetails();
        User user = User.fromDetails(details);
        try {
            Optional<User> existing_user = repo.findById(details.getId());
            if (!existing_user.isPresent()) {
                LOG.info("User not found in repo: " + user.toString());
                return UserUpdatedEvent.notFound(user.toDetails());
            }

            // check existence of user id in friend list
            user.setList_friend(checkUserList(user.getList_friend()));

            // TODO: factorize code with the same test in create
            // Check if user name is modified, and if this name is not already in use
            String event_name = details.getName();
            if (!event_name.equals(existing_user.get().getName())) {
                List<User> same_name_user = repo.findByName(event_name);

                if (null != same_name_user && same_name_user.size() != 0) {
                    LOG.info("Name already in use: " + details.getName() + ", creation failed");
                    UserUpdatedEvent failed_event = UserUpdatedEvent.notUpdated(details);
                    // TODO: set code in update event like in create
                    // event.setFailedAlreadyExists();
                    return failed_event;
                }
            }
            // save user details
            User saved_user = repo.save(user);
            if (saved_user == null) {
                LOG.warn("User not updated: " + user.toString());
                return UserUpdatedEvent.notUpdated(user.toDetails());
            }
            LOG.info("User succesfully updated: " + user.toString());
            return new UserUpdatedEvent(saved_user.toDetails());

        } catch (Exception e) {
            LOG.warn("Issue while updating user : " + user.toString() + "exception: " + e.getLocalizedMessage());
            return UserUpdatedEvent.notUpdated(user.toDetails());
        }

    }

    private User getUserById(UUID id) {
        LOG.info("Searching for user with id:" + id);
        UserDetails details = null;
        Optional<User> result = repo.findById(id);
        if (result.isPresent()) {
            LOG.info("User found");
            return result.get();
        }
        LOG.info("User not found");
        return null;
    }

    private User getUserByName(String name) {
        LOG.info("Searching for user with name: " + name);
        List<User> result = repo.findByName(name);

        if (result != null) {
            if (result.size() != 1) {
                LOG.error("Unexpected number of User returned when querying with name: " + name + " ,count: " + result.size());
                return null;
            } else {
                LOG.info("User found");
                return result.get(0);
            }
        }
        LOG.info("User not found");
        return null;
    }

}
