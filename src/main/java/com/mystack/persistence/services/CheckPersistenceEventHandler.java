package com.mystack.persistence.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.checks.request.CreateCheckEvent;
import com.mystack.core.events.checks.request.DeleteCheckEvent;
import com.mystack.core.events.checks.request.RequestAllChecksEvent;
import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.request.RequestLastChecksEvent;
import com.mystack.core.events.checks.request.UpdateCheckEvent;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.core.events.checks.result.CheckCreatedEvent;
import com.mystack.core.events.checks.result.CheckDeletedEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.events.checks.result.CheckUpdatedEvent;
import com.mystack.persistence.domain.Check;
import com.mystack.persistence.repository.CheckCRUDRepository;

public class CheckPersistenceEventHandler implements CheckPersistenceService {

    @Autowired
    private CheckCRUDRepository repo;

    private static Logger       LOG = LoggerFactory.getLogger(CheckPersistenceEventHandler.class);

    @Override
    public AllChecksEvent getAllChecks(RequestAllChecksEvent event) {

        List<Check> result;
        LOG.info("Event: --- " + event.toString());

        if (null != event.getAfter_date() && null != event.getBefore_date()) {
            LOG.info("Both date_after and date_before are provided, will return check between those dates: "
            		 + event.getAfter_date().getTime() + " - " + event.getBefore_date().getTime());
            result = repo.findByStackBetweenDates(event.getStack_id(), event.getBefore_date(), event.getAfter_date(),
            		                              PageRequest.of(0, event.getCount(), Sort.Direction.DESC, "date"));
        } else if (null != event.getBefore_date()) {
            LOG.info("Only before date is provided: " + event.getBefore_date().getTime());
            result = repo.findByStackBeforeDate(event.getStack_id(), event.getBefore_date(),
            		                            PageRequest.of(0, event.getCount(), Sort.Direction.DESC, "date"));
        } else {
            LOG.info("Getting all check for stack: " + event.getStack_id() + "with a page size of " + event.getCount());
            // search for all users in repo 
            result = repo.findByStackOrderByDateDesc(event.getStack_id(),
            		                                 PageRequest.of(0, event.getCount()));
        }
        // create a list of Stack details as integration domain does not know about core domain
        List<CheckDetails> result_details = new ArrayList<>();
        for (Check check : result) {
            result_details.add(check.toDetails());
        }
        if (result_details.size() == 0) {
            LOG.info("No Checks found");
            return AllChecksEvent.NotFound();
        }
        return new AllChecksEvent(result_details);
    }

    /**
     * Return stack details if found by id. Else returns null
     */
    @Override
    public CheckDetailsEvent getCheckDetails(RequestCheckDetailsEvent event) {

        UUID id = event.getId();
        LOG.info("Searching for check with id:" + id);
        CheckDetails details = null;
        Optional<Check> check = repo.findById(id);
        if (check.isPresent()) {
            LOG.info("Check found");
            details = check.get().toDetails();
            return new CheckDetailsEvent(details);
        }
        LOG.info("Check not found");
        return CheckDetailsEvent.notFound(id);
    }
    
    @Override
    public CheckDetailsEvent getLatestCheck(RequestLastChecksEvent event) {
    	Page<Check> check = repo.findByStackAndStatusOrderByDateDesc(event.getStackId(), event.getCheckStatus(),
    																 PageRequest.of(0,1));
    	if (check.hasContent()) {
    		CheckDetailsEvent details = new CheckDetailsEvent(check.getContent().get(0).toDetails());
    		details.setTotalCount(check.getTotalElements());
    		return details;
    	}
    	return CheckDetailsEvent.notFound(event.getStackId());
    }
    
    @Override
    public CheckCreatedEvent createCheck(CreateCheckEvent event) {
        // create stack from details
        CheckDetails details = event.getDetails();
        Check check = Check.fromDetails(details);
        LOG.info("Creating check in persistence: " + details.toString());
        LOG.info("Created check:" + check.toString());

        // TODO: how to garanty ID unicity ?
        UUID new_id = UUID.randomUUID();
        check.setId(new_id);

        Check saved_check = repo.save(check);
        if (saved_check == null) {
            LOG.warn("No Check saved, save operation failed for check id: " + new_id + ", Stack: " + check.getStack());
            return CheckCreatedEvent.creationFailed(details);
        }
        LOG.info("Input Check             : id " + check.toString());
        LOG.info("Check successfully saved: id " + saved_check.toString());
        return new CheckCreatedEvent(saved_check.getId(), saved_check.toDetails());
    }

    @Override
    public CheckDeletedEvent deleteCheck(DeleteCheckEvent event) {

        UUID id = event.getId();
        Optional<Check> check = repo.findById(id);
        CheckDetails details = null;

        if (check.isPresent()) {
            details = check.get().toDetails();
            LOG.info("Deleting Check with ID: " + id.toString());
            repo.deleteById(id);
            return new CheckDeletedEvent(details);
        }

        LOG.info("Check not found with ID: " + id.toString());
        return CheckDeletedEvent.notFound(id);
    }

    @Override
    public CheckUpdatedEvent updateCheck(UpdateCheckEvent event) {

        // create check from details
        Check updateCheckData = Check.fromDetails(event.getCheckDetails());

        // search the Check
        Optional<Check> checkToBeUpdated = repo.findById(updateCheckData.getId());
        if (!checkToBeUpdated.isPresent()) {
            LOG.warn("No Check updated");
            return CheckUpdatedEvent.notUpdated(updateCheckData.toDetails());
        }
        Check checkToSave = checkToBeUpdated.get();

        if (null != updateCheckData.getComment()) {
            checkToSave.setComment(updateCheckData.getComment());
        }
        if (null != updateCheckData.getDate()) {
            checkToSave.setDate(updateCheckData.getDate());
        }

        if (null != updateCheckData.getStatus()) {
            checkToSave.setStatus(updateCheckData.getStatus());
        }
        
        Check updatedCheck = repo.save(checkToSave);
        if (null == updatedCheck) {
            LOG.warn("Failed to update check: " + checkToBeUpdated + " with: " + updateCheckData);
            return CheckUpdatedEvent.notUpdated(updateCheckData.toDetails());
        }
        LOG.info("Check successfully updated: " + updatedCheck);
        CheckUpdatedEvent checkUpdatedEvent = new CheckUpdatedEvent(updatedCheck.toDetails(), checkToSave.toDetails());
        return checkUpdatedEvent;
    }



}
