package com.mystack.persistence.services;

import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;

public interface UserPersistenceService {

    AllUsersEvent getAllUsers(RequestAllUsersEvent event);

    UserDetailsEvent getUserDetails(RequestUserDetailsEvent event);

    UserCreatedEvent createUser(CreateUserEvent event);

    UserDeletedEvent deleteUser(DeleteUserEvent event);

    UserUpdatedEvent updateUser(UpdateUserEvent event);
}
