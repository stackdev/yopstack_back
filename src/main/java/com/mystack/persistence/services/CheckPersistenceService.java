package com.mystack.persistence.services;

import com.mystack.core.events.checks.request.CreateCheckEvent;
import com.mystack.core.events.checks.request.DeleteCheckEvent;
import com.mystack.core.events.checks.request.RequestAllChecksEvent;
import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.request.RequestLastChecksEvent;
import com.mystack.core.events.checks.request.UpdateCheckEvent;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.core.events.checks.result.CheckCreatedEvent;
import com.mystack.core.events.checks.result.CheckDeletedEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.events.checks.result.CheckUpdatedEvent;

public interface CheckPersistenceService {

    AllChecksEvent getAllChecks(RequestAllChecksEvent event);

    CheckDetailsEvent getCheckDetails(RequestCheckDetailsEvent event);
    
    CheckDetailsEvent getLatestCheck(RequestLastChecksEvent event);

    CheckCreatedEvent createCheck(CreateCheckEvent event);

    CheckDeletedEvent deleteCheck(DeleteCheckEvent event);

    CheckUpdatedEvent updateCheck(UpdateCheckEvent event);

}
