package com.mystack.persistence.services;

import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;
import com.mystack.persistence.domain.Preference;
import com.mystack.persistence.repository.PreferenceCRUDRepository;

public class PreferencePersistenceEventHandler implements PreferencePersistenceService {

    @Autowired
    private PreferenceCRUDRepository repo;

    private static Logger            LOG = LoggerFactory.getLogger(PreferencePersistenceEventHandler.class);

    @Override
    public PreferenceReadEvent readPreference(ReadPreferenceEvent event) {
        UUID id = event.getDetails().getId();
        UUID user = event.getDetails().getUser();
        Optional<Preference> result = null;
        if (null != id) {
            LOG.info("Persistence get Preference by id:" + id);
            result = repo.findById(id);
        } else if (null != user) {
            LOG.info("Persistence get Preference by user:" + user);
            result = Optional.ofNullable(repo.findByUser(user));
        }
        if (result.isPresent()) {
            LOG.info("Preference found");
            PreferenceDetails details = result.get().toDetails();
            return new PreferenceReadEvent(details);
        }
        LOG.info("Preference not found");
        return PreferenceReadEvent.NotFound();
    }

    @Override
    public PreferenceCreatedEvent createPreference(CreatePreferenceEvent event) {
        // create stack from details
        PreferenceDetails details = event.getDetails();
        Preference pref = Preference.fromDetails(details);

        LOG.info("Creating preference in persistence: " + details.toString());

        UUID new_id = UUID.randomUUID();
        pref.setId(new_id);

        Preference saved = repo.save(pref);
        if (saved == null) {
            LOG.warn("No preference saved");
            return PreferenceCreatedEvent.creationFailed(details);
        }
        LOG.info("Preference successfully saved: id " + saved.toString());
        return new PreferenceCreatedEvent(saved.toDetails());
    }

    @Override
    public PreferenceDeletedEvent deletePreference(DeletePreferenceEvent event) {

        UUID id = event.getDetails().getId();
        LOG.info("Trying to delete preference with id: " + id.toString());
        PreferenceDeletedEvent result;
        Optional<Preference> pref = repo.findById(id);

        if (!pref.isPresent()) {
            LOG.info("Preference with id " + id.toString() + " not found");
            return PreferenceDeletedEvent.notFound(event.getDetails());
        }
        LOG.info("Preference to be deleted found: " + pref.get().getId().toString());
        repo.deleteById(id);
        result = new PreferenceDeletedEvent(pref.get().toDetails());
        return result;

    }

    @Override
    public PreferenceUpdatedEvent updatePreference(UpdatePreferenceEvent event) {
        PreferenceDetails details = event.getDetails();

        Optional<Preference> pref_to_update = repo.findById(details.getId());
        if (!pref_to_update.isPresent()) {
            return PreferenceUpdatedEvent.notFound(details);
        }

        Preference saved_pref = repo.save(Preference.fromDetails(details));
        if (saved_pref == null) {
            LOG.warn("Failed to saved preference");
            return PreferenceUpdatedEvent.notUpdated(details);
        }
        LOG.info("Preference successfully updated: " + saved_pref.toString());
        return new PreferenceUpdatedEvent(saved_pref.toDetails());

    }

}
