package com.mystack.persistence.services;

import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksByIdEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.AllStacksByIdEvent;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;

public interface StackPersistenceService {

    AllStacksEvent getAllStacks(RequestAllStacksEvent event);

    AllStacksEvent getAllCheckableStacks(RequestAllStacksEvent event);

    AllStacksByIdEvent getAllStacks(RequestAllStacksByIdEvent event);

    StackDetailsEvent getStackDetails(RequestStackDetailsEvent event);

    StackCreatedEvent createStack(CreateStackEvent event);

    StackDeletedEvent deleteStack(DeleteStackEvent event);

    StackUpdatedEvent updateStack(UpdateStackEvent event);

}
