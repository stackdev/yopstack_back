package com.mystack.persistence.domain;
import java.util.UUID;
import java.lang.String;
import java.util.List;
import java.util.ArrayList;
import com.mystack.core.domain.UserDetails;

public class User {
    private UUID id;
    private String name;
    private List<UUID> list_friend;
    private List<UUID> list_stack;
    private String password;
    private boolean enabled;
    private List<String> roles;
    //Constructors
    public User() {
    }
    
    public User(UUID id, String name, List<UUID> list_friend, List<UUID> list_stack, String password, boolean enabled, List<String> roles) {
        this.id = id;
        this.name = name;
        this.list_friend = list_friend;
        this.list_stack = list_stack;
        this.password = password;
        this.enabled = enabled;
        this.roles = roles;
    }
    
    public User(User user) {
        this.id = user.id;
        this.name = user.name;
        this.list_friend = user.list_friend;
        this.list_stack = user.list_stack;
        this.password = user.password;
        this.enabled = user.enabled;
        this.roles = user.roles;
    }
    
    public User(UUID id, String name, List<UUID> list_friend, List<UUID> list_stack) {
        this.id = id;
        this.name = name;
        this.list_friend = list_friend;
        this.list_stack = list_stack;
        this.password = "no_password";
        this.enabled = false;
        this.roles = new ArrayList<String>();
    }
    
    //Getter/setters/toString/toDetails/fromDetails
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    
    public List<UUID> getList_friend() {
        return list_friend;
    }
    
    public void setList_friend(List<UUID> list_friend) {
        this.list_friend = list_friend;
    }
    
    
    public List<UUID> getList_stack() {
        return list_stack;
    }
    
    public void setList_stack(List<UUID> list_stack) {
        this.list_stack = list_stack;
    }
    
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    
    public boolean getEnabled() {
        return enabled;
    }
    
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    
    public List<String> getRoles() {
        return roles;
    }
    
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    
    
    public String toString() {
        return "User: ["+" Id: "+id+" Name: "+name+" List_friend: "+list_friend+" List_stack: "+list_stack+"]";
    }
    
    public UserDetails toDetails() {
        UserDetails details = new UserDetails();
        details.setId(this.id);
        details.setName(this.name);
        details.setList_friend(this.list_friend);
        details.setList_stack(this.list_stack);
        details.setPassword(this.password);
        details.setEnabled(this.enabled);
        details.setRoles(this.roles);
        return details;
    }
    
    public static User fromDetails(UserDetails details) {
        User user = new User();
        user.id = details.getId();
        user.name = details.getName();
        user.list_friend = details.getList_friend();
        user.list_stack = details.getList_stack();
        user.password = details.getPassword();
        user.enabled = details.getEnabled();
        user.roles = details.getRoles();
        return user;
    }
    
    public boolean equals(Object obj) {
        
    if (this == obj)
        return true;
    if (obj == null)
        return false;
    if (getClass() != obj.getClass())
        return false;

    User other = (User) obj;
            if (id == null) {
        if (other.id != null)
            return false;
    }
    else if (!id.equals(other.id))
        return false;
            if (name == null) {
        if (other.name != null)
            return false;
    }
    else if (!name.equals(other.name))
        return false;
            if (list_friend == null) {
        if (other.list_friend != null)
            return false;
    }
    else if (!list_friend.equals(other.list_friend))
        return false;
            if (list_stack == null) {
        if (other.list_stack != null)
            return false;
    }
    else if (!list_stack.equals(other.list_stack))
        return false;
            if (password == null) {
        if (other.password != null)
            return false;
    }
    else if (!password.equals(other.password))
        return false;
            if ( enabled != other.enabled)
        return false;
            if (roles == null) {
        if (other.roles != null)
            return false;
    }
    else if (!roles.equals(other.roles))
        return false;
            return true;
    }
    
}
