package com.mystack.persistence.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.core.domain.CommentDetails;

public class Comment {
    private static Logger LOG = LoggerFactory.getLogger(Comment.class);

    private UUID          id;
    private UUID          stack_userid;
    private UUID          stackid;
    private UUID          checkid;
    private UUID          comment_userid;
    private String        text;
    private Date          timestamp;
    private Date          update_timestamp;

    // Dummy constructor for the convertion from persistence
    public Comment() {

    }

    public Comment(UUID id, UUID stack_userid, UUID stackid, UUID checkid, UUID comment_userid, Date timestamp, String text, Date update_timestamp) {
        super();
        this.id = id;
        this.stack_userid = stack_userid;
        this.stackid = stackid;
        this.checkid = checkid;
        this.comment_userid = comment_userid;
        this.text = text;
        this.timestamp = timestamp;
        this.update_timestamp = update_timestamp;
    }

    public UUID getUserid() {
        return stack_userid;
    }

    public void setUserid(UUID userid) {
        this.stack_userid = userid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Date getUpdateTimestamp() {
        return update_timestamp;
    }

    public void setUpdateTimestamp(Date update_timestamp) {
        this.update_timestamp = update_timestamp;
    }

    public UUID getStackid() {
        return stackid;
    }

    public void setStackid(UUID stackid) {
        this.stackid = stackid;
    }

    public UUID getCheckid() {
        return checkid;
    }

    public void setCheckid(UUID checkid) {
        this.checkid = checkid;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID commentid) {
        this.id = commentid;
    }

    public UUID getStack_userid() {
        return stack_userid;
    }

    public void setStack_userid(UUID stack_userid) {
        this.stack_userid = stack_userid;
    }

    public UUID getComment_userid() {
        return comment_userid;
    }

    public void setComment_userid(UUID comment_userid) {
        this.comment_userid = comment_userid;
    }

    @Override
    public String toString() {
        return "Comment [id=" + id + ", stack_userid=" + stack_userid + ", stackid=" + stackid + ", checkid=" + checkid + ", comment_userid=" + comment_userid + ", text=" + text + ", timestamp="
                + timestamp + ", update_timestamp=" + update_timestamp + "]";
    }

    public CommentDetails toDetails() {
        return new CommentDetails(id, stack_userid, stackid, checkid, comment_userid, text, timestamp, update_timestamp);
    }

    public static Comment FromDetails(CommentDetails details) {
        return new Comment(details.getId(),
                details.getStack_userid(),
                details.getStackid(),
                details.getCheckid(),
                details.getComment_userid(),
                details.getTimestamp(),
                details.getText(),
                details.getUpdateTimestamp());
    }

    public static List<Comment> FromListDetails(List<CommentDetails> details) {
        List<Comment> output = new ArrayList<>();
        for (CommentDetails comment_detail : details) {
            output.add(FromDetails(comment_detail));
        }
        return output;
    }

    public static List<CommentDetails> ToListDetails(List<Comment> details) {
        List<CommentDetails> output = new ArrayList<>();
        if (null == details) {
            return output;
        }
        for (Comment comment : details) {
            output.add(comment.toDetails());
        }
        return output;
    }

}
