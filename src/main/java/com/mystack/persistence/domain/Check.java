package com.mystack.persistence.domain;
import java.util.Date;
import java.util.UUID;
import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.core.domain.CheckDetails;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

//@Document
public class Check {
    @Id
    private UUID id;
    @Indexed
    private Date date;
    private UUID stack;
    @TextIndexed
    private String comment;
    private Date creation_date;
    private UUID checker;
    private CheckStatus status;
    //Constructors
    public Check() {
    }
    
    public Check(UUID id, Date date, UUID stack, String comment, Date creation_date, UUID checker, CheckStatus status) {
        this.id = id;
        this.date = date;
        this.stack = stack;
        this.comment = comment;
        this.creation_date = creation_date;
        this.checker = checker;
        this.status = status;
    }
    
    public Check(Check check) {
        this.id = check.id;
        this.date = check.date;
        this.stack = check.stack;
        this.comment = check.comment;
        this.creation_date = check.creation_date;
        this.checker = check.checker;
        this.status = check.status;
    }
    
    //Getter/setters/toString/toDetails/fromDetails
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    
    public Date getDate() {
        return date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    
    public UUID getStack() {
        return stack;
    }
    
    public void setStack(UUID stack) {
        this.stack = stack;
    }
    
    
    public String getComment() {
        return comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    public Date getCreation_date() {
        return creation_date;
    }
    
    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }
    
    
    public UUID getChecker() {
        return checker;
    }
    
    public void setChecker(UUID checker) {
        this.checker = checker;
    }
    
    
    public CheckStatus getStatus() {
        return status;
    }
    
    public void setStatus(CheckStatus status) {
        this.status = status;
    }
    
    
    public String toString() {
        return "Check: [" + " id: " + id + " date: " + date + " stack: " + stack + " comment: " + comment + " creation_date: " + creation_date + " checker: " + checker + " status: " + status + "]";
    }
    
    public CheckDetails toDetails() {
        CheckDetails details = new CheckDetails();
        details.setId(this.id);
        details.setDate(this.date);
        details.setStack(this.stack);
        details.setComment(this.comment);
        details.setCreation_date(this.creation_date);
        details.setChecker(this.checker);
        details.setStatus(this.status);
        return details;
    }
    
    public static Check fromDetails(CheckDetails details) {
        Check user = new Check();
        user.id = details.getId();
        user.date = details.getDate();
        user.stack = details.getStack();
        user.comment = details.getComment();
        user.creation_date = details.getCreation_date();
        user.checker = details.getChecker();
        user.status = details.getStatus();
        return user;
    }
    
    public boolean equals(Object obj) {
        
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        
        Check other = (Check) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (stack == null) {
            if (other.stack != null)
                return false;
        } else if (!stack.equals(other.stack))
            return false;
        if (comment == null) {
            if (other.comment != null)
                return false;
        } else if (!comment.equals(other.comment))
            return false;
        if (creation_date == null) {
            if (other.creation_date != null)
                return false;
        } else if (!creation_date.equals(other.creation_date))
            return false;
        if (checker == null) {
            if (other.checker != null)
                return false;
        } else if (!checker.equals(other.checker))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        return true;
    }
    
}
