package com.mystack.persistence.domain;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "score")
public class Score {
    private static Logger LOG = LoggerFactory.getLogger(Score.class);

    @Id
    private UUID          id;
    @Field
    @Indexed
    private UUID          stack;
    private int           value;
    private Date          date;
    private String        comment;

    // Dummy constructor for the convertion from persistence
    public Score() {

    }

    // TODO: to be cleaned up once new_comment is in used
    public Score(UUID id, Date date, UUID stack, String comment) {
        init_check(id, date, stack, comment, null);
    }

    public Score(UUID id, Date date, UUID stack, String comment, List<Comment> new_comment) {
        init_check(id, date, stack, comment, new_comment);
    }

    private void init_check(UUID id, Date date, UUID stack, String comment, List<Comment> new_comment) {
        this.id = id;
        this.date = date;
        this.stack = stack;
        this.comment = comment;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UUID getStack() {
        return stack;
    }

    public void setStack(UUID stack) {
        this.stack = stack;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Check [id=" + id + ", date=" + date + ", stack=" + stack + ", comment=" + comment + "]";
    }

}
