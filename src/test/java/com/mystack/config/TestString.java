package com.mystack.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TestString {

    String       http_addressPort;
    final String war_name    = "";
    final String base_path   = "/aggregators";
    final String users       = "users";
    final String checks      = "/checks/";
    final String stacks      = "/stacks/";
    final String comments    = "/comments/";
    final String preferences = "/preferences/";
    final String info = "/actuator/info";

    public String getHttp_addressPort() {
        return http_addressPort;
    }

    public String getBase_path() {
        return base_path;
    }

    /** --- stack -- **/
    public String getStacks() {
        return stacks;
    }

    public String getStacks_path(String uid) {
        return getStacks_path(uid, "");
    }

    public String getStacks_path(String uid, String sid) {
        return getUsers_path(uid) + stacks + sid;
    }

    public String getStacks_fullPath(String uid) {
        return getStacks_fullPath(uid, "");
    }

    public String getStacks_fullPath(String uid, String sid) {
        return http_addressPort + war_name + getStacks_path(uid, sid);
    }

    /** --- user -- **/
    public String getUsers() {
        return users;
    }

    public String getUsers_path() {
        return getUsers_path(null);
    }

    public String getUsers_path(String uid) {
    	String a = Arrays.asList(base_path, users, uid)
    			.stream()
    			.filter(v-> v != null)
    			.collect(Collectors.joining("/"));
        //return base_path + users + uid;
    	return a;
    }

    public String getUsers_fullPath() {
        return getUsers_fullPath(null);
    }

    public String getUsers_fullPath(String uid) {
        return http_addressPort + war_name + getUsers_path(uid);
    }

    /** --- check -- **/
    public String getChecks() {
        return checks;
    }

    public String getChecks_path() {
        return getChecks_path("");
    }

    public String getChecks_path(String cid) {
        return base_path + checks + cid;
    }

    public String getChecks_fullPath() {
        return getChecks_fullPath("");
    }

    public String getChecks_fullPath(String cid) {
        return http_addressPort + war_name + getChecks_path(cid);
    }

    /** --- comment -- **/
    public String getComments() {
        return comments;
    }

    public String getComments_path(String uid, String sid, String checkid) {
        return getComments_path(uid, sid, checkid, "");
    }

    public String getComments_path(String uid, String sid, String checkid, String comid) {
        return getStacks_path(uid, sid) + checks + checkid + comments + comid;
    }

    public String getComments_fullPath(String uid, String sid, String checkid) {
        return getComments_fullPath(uid, sid, checkid, "");
    }

    public String getComments_fullPath(String uid, String sid, String checkid, String comid) {
        return http_addressPort + war_name + getComments_path(uid, sid, checkid, comid);
    }

    /** --- preferences -- **/
    public String getPreferences() {
        return preferences;
    }

    public String getPreferences_path(String uid) {
        return getPreferences_path(uid, "");
    }

    public String getPreferences_path(String uid, String pid) {
        return getUsers_path(uid) + preferences + pid;
    }

    public String getPreferences_fullPath(String uid) {
        return getPreferences_fullPath(uid, "");
    }

    public String getPreferences_fullPath(String uid, String sid) {
        return http_addressPort + war_name + getPreferences_path(uid, sid);
    }

    /** -- **/

    /** Info **/
    public String getInfo_path() {
        return this.info;
    }
    public String getInfo_fullPath() {
    	return  http_addressPort + war_name + getInfo_path();
    }
    /** -- **/    
    
    /** --- url param --- **/
    public String getParam(HashMap<String, String> params) {
        String param_str = "";
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (param_str.length() > 0) {
                param_str += "&";
            }
            param_str += entry.getKey() + "=" + entry.getValue();
        }
        if (param_str.length() > 0) {
            param_str = "?" + param_str;
        }
        return param_str;
    }

    /**
     * Simply take http_addressPort as parameter ( comming from the configuration class)
     * 
     * @param http_addressPort
     */
    public TestString(String http_addressPort) {
        this.http_addressPort = http_addressPort;
    }

}
