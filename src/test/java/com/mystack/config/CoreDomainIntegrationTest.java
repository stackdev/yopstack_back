package com.mystack.config;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.domain.fixtures.StacksFixtures;
import com.mystack.core.domain.fixtures.UsersFixtures;
import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.services.StackService;
import com.mystack.core.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CoreConfig.class, MongoConfiguration.class, FunctionalConfiguration.class })
public class CoreDomainIntegrationTest {

    private static Logger LOG = Logger.getLogger(CoreDomainIntegrationTest.class.toString());

    @Autowired
    StackService          stack_service;

    @Autowired
    UserService           user_service;

    /**
     * Similar test to services test, but this time repository is not mocked but configured
     */
    @Test
    public void addAStackToTheSystem() {
        UserDetails details = UsersFixtures.getStandardUserNoList().toDetails();
        // TODO: find something clean to do that ...
        details.setSetpw(details.getPassword());
        UserCreatedEvent user_created_event = user_service.createUser(new CreateUserEvent(details));

        assertTrue(user_created_event.isCreated());

        CreateStackEvent event = new CreateStackEvent(StacksFixtures.getStandardStackDetails(), user_created_event.getId(), user_created_event.getDetails().getName());
        AllStacksEvent initial_state = stack_service.getAllStacks(new RequestAllStacksEvent());
        // add a stack
        stack_service.createStack(event);

        // check the number of stack in repo, must be one
        AllStacksEvent result = stack_service.getAllStacks(new RequestAllStacksEvent());
        assertEquals(initial_state.getAllStackDetails().size() + 1, result.getAllStackDetails().size());
        assertEquals(1, FunctionalConfiguration.DEFAULT_STACK_FREQUENCY);
    }
}