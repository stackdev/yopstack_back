package com.mystack.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:test.properties")
public class TestStringConfig {
    private static Logger LOG = LoggerFactory.getLogger(TestStringConfig.class);
    //Needed to have system env variable registered as source of properties
    @Autowired
    Environment           environment;

    //TODO: make this configuration work in EndToEnd test: RunWith Spring ?
    @Value("${http.addressPort}")
    private String        http_addressPort;                                     // = "http://localhost:8080/";

    // Needed to replace the properties name by the value read in PropertySource
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        return pspc;
        //return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public TestString testString() {
        return new TestString(http_addressPort);
    }

}
