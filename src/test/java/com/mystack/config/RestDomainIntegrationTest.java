package com.mystack.config;

import static com.mystack.rest.fixture.RestCheckDataFixtures.customizedCheckJSON;
import static com.mystack.rest.fixture.RestStackDataFixtures.standardStackJSON;
import static com.mystack.rest.fixture.RestUserDataFixtures.customUserDetails;
import static com.mystack.rest.fixture.RestUserDataFixtures.customizedUserJSON;
import static com.mystack.rest.fixture.RestUserDataFixtures.userJSONFromDetails;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.domain.UserDetails;
import com.mystack.tool.MyPrincipal;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = { CoreConfig.class, MVCConfig.class, MongoConfiguration.class, TestStringConfig.class })
public class RestDomainIntegrationTest {

    private static Logger LOG = LoggerFactory.getLogger(RestDomainIntegrationTest.class);

    @Autowired
    WebApplicationContext webApplicationContext;

    private MockMvc       mockMvc;

    @Autowired
    TestString            testString;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    /**
     * The MVC is mocked, this means that test trigger controller via this fake mvc,<br/>
     * the other components are configured (service, repo)
     * 
     * @throws Exception
     */
    @Test
    public void addAStackToTheSystem() throws Exception {
        HashMap<String, String> user_created = postAUSer();
        String uid = user_created.get("id");
        this.mockMvc.perform(
                post(testString.getStacks_path(uid)).principal(new MyPrincipal(user_created.get("name")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(standardStackJSON())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());

        this.mockMvc.perform(
                get(testString.getStacks_path(uid))
                        .accept(MediaType.APPLICATION_JSON)
                        .principal(new MyPrincipal("plop")))
                .andDo(print())
                .andExpect(status().isOk());

    }

    /*
     * Add a User to the system, referencing an existing Stack
     */
    @Test
    public void addAUserToTheSystem() throws Exception {

        String stack_created_id = postAStack();
        LOG.info("Stack created for User creation: " + stack_created_id);
        String user_created_id = "No_id_set";
        MvcResult result = this.mockMvc.perform(
                post(testString.getUsers_path())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(customizedUserJSON(null, "MockMVcUserAdded", 0, 0, "MockMVcUserAdded_password"))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn();
        String[] location = result.getResponse().getHeader("Location").split(testString.getUsers());
        if (location.length > 0)
            user_created_id = location[1];

        this.mockMvc.perform(
                get(testString.getUsers_path(user_created_id))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /*
     * Add a Check to the system, referencing an existing Stack
     */
    @Test
    public void addACheckToTheSystem() throws Exception {

        String cid = postACheck();
        LOG.info("Check created, with id: " + cid);

        this.mockMvc.perform(
                get(testString.getChecks_path(cid))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    // End of tests
    ///////////////////////////////

    private HashMap<String, String> postAUSer() {

        UUID id = UUID.randomUUID();
        UserDetails details = customUserDetails(id, "user_" + id.toString(), 2, 1, "password_" + id.toString());
        try {
            MvcResult result =
                    this.mockMvc
                            .perform(
                                    post(testString.getUsers_path())
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .content(userJSONFromDetails(details))
                                            .accept(MediaType.APPLICATION_JSON))
                            .andDo(print())
                            .andExpect(status().isCreated())
                            .andReturn();

            // status is 200 OK
            String body = result.getResponse().getContentAsString();
            //TODO: this to be done more cleanely (use Jackson here ?)
            Map json_body = new Gson().fromJson(body, Map.class);
            if (json_body.containsKey("id")) {
                HashMap<String, String> result_map = new HashMap<>();
                result_map.put("id", (String) json_body.get("id"));
                result_map.put("name", (String) json_body.get("name"));
                return result_map;
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    /*
     * This method create a stack and return the id of this new stack
     */
    private String postAStack() {
        HashMap<String, String> user_created = postAUSer();
        String uid = user_created.get("id");
        try {
            MvcResult result =
                    this.mockMvc.perform(
                            post(testString.getStacks_path(uid)).principal(new MyPrincipal(user_created.get("name")))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(standardStackJSON())
                                    .accept(MediaType.APPLICATION_JSON))
                            .andDo(print())
                            .andExpect(status().isCreated())
                            .andReturn();

            // status is 200 OK
            String body = result.getResponse().getContentAsString();
            //TODO: this to be done more cleanely (use Jackson here ?)
            Map json_body = new Gson().fromJson(body, Map.class);
            if (json_body.containsKey("id")) {
                return (String) json_body.get("id");
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    /*
     * This method create a stack and return a Map containing user and stack id and name
     */
    private HashMap<String, String> postAStackAndUser() {
        HashMap<String, String> user_created = postAUSer();
        HashMap<String, String> result_map = new HashMap<>();
        //prefix all user keys with u
        for (String key : user_created.keySet()) {
            result_map.put("u" + key, user_created.get(key));
        }

        String uid = result_map.get("uid");
        try {
            MvcResult result =
                    this.mockMvc.perform(
                            post(testString.getStacks_path(uid)).principal(new MyPrincipal(result_map.get("uname")))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(standardStackJSON())
                                    .accept(MediaType.APPLICATION_JSON))
                            .andDo(print())
                            .andExpect(status().isCreated())
                            .andReturn();

            // status is 200 OK
            String body = result.getResponse().getContentAsString();
            //TODO: this to be done more cleanely (use Jackson here ?)
            Map json_body = new Gson().fromJson(body, Map.class);
            if (json_body.containsKey("id")) {
                result_map.put("sid", (String) json_body.get("id"));
            }
            if (json_body.containsKey("name")) {
                result_map.put("sname", (String) json_body.get("name"));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return result_map;
    }

    private HashMap<String, String> postACheck_uid_sid_cid() {
        return postACheck(null, null);
    }

    private String postACheck() {
        HashMap<String, String> result = postACheck(null, null);
        if (null == result) {
            return "";
        }
        return result.get("cid");
    }

    private HashMap<String, String> postACheck(UserDetails owner, StackDetails stack) {

        // post body: {"stack":"f5a0b15e-0b06-4c0d-92c3-26395bf7d241","comment":"plop","date":123456786}

        String uid, uname, sid;
        HashMap<String, String> stack_user_data = new HashMap<>();
        if (null == owner || null == stack) {
            stack_user_data = postAStackAndUser();
            uid = stack_user_data.get("uid");
            uname = stack_user_data.get("uname");
            sid = stack_user_data.get("sid");
        } else {
            uid = owner.getId().toString();
            uname = owner.getName();
            sid = stack.getId().toString();
        }
        try {
            MvcResult result =
                    this.mockMvc.perform(
                            post(testString.getChecks_path()).principal(new MyPrincipal(uname))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(customizedCheckJSON(UUID.randomUUID(), new Date(), UUID.fromString(sid), "plop"))
                                    .accept(MediaType.APPLICATION_JSON))
                            .andDo(print())
                            .andExpect(status().isCreated())
                            .andReturn();

            // status is 200 OK
            String body = result.getResponse().getContentAsString();
            //TODO: this to be done more cleanely (use Jackson here ?)
            Map json_body = new Gson().fromJson(body, Map.class);
            if (json_body.containsKey("id")) {

                stack_user_data.put("cid", (String) json_body.get("id"));
                return stack_user_data;
            }
        } catch (Exception e) {
            return stack_user_data;
        }
        return stack_user_data;
    }
}