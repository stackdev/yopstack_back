package com.mystack.persistence.domain.fixture;

import java.util.Date;
import java.util.UUID;

import com.mystack.persistence.domain.Check;

public class PersistenceCheckFixture {

    public static Check standardCheck() {

        Check check = new Check();
        check.setId(UUID.randomUUID());
        check.setDate(new Date());
        check.setStack(UUID.randomUUID());
        check.setComment("This is a standard check");
        return check;
    }
}
