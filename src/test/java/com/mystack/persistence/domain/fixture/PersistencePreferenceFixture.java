package com.mystack.persistence.domain.fixture;

import java.util.UUID;

import com.mystack.persistence.domain.Preference;

public class PersistencePreferenceFixture {

    public static Preference standardPreference() {

        return new Preference(UUID.randomUUID(), UUID.randomUUID());
    }
}
