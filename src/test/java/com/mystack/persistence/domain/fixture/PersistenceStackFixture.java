package com.mystack.persistence.domain.fixture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.mystack.persistence.domain.Stack;

public class PersistenceStackFixture {

    public static int STACK_DEFAULT_PERIODICITY = 2;
    public static int STACK_DEFAULT_FREQUENCY   = 1;

    //TODO:to be removed
    public static Stack standardStack() {
        Stack stack = standardStack(null, 0, 0);
        stack.setDescription("This is the description of my stack !");
        return stack;
    }

    //TODO: to be replaced in all tests by standardStack
    @Deprecated
    public static Stack standardStackRandomDesc() {
        return standardStack(null, 0, 0);
    }

    public static Stack standardStack(Date creation_date, int periodicity, int frequency) {
        if (periodicity == 0) {
            periodicity = STACK_DEFAULT_PERIODICITY;
        }

        if (frequency == 0) {
            frequency = STACK_DEFAULT_FREQUENCY;
        }
        if (null == creation_date) {
            creation_date = new Date();
        }
        Stack stack = new Stack();
        stack.setName("Standard stack");
        stack.setId(UUID.randomUUID());
        stack.setCreation_date(new Date());
        stack.setPeriodicity(periodicity);
        stack.setFrequency(frequency);
        String desc = String.format("This is a random desc ! ({})", UUID.randomUUID());
        stack.setDescription(desc);
        return stack;

    }

    public static List<Stack> getListStandardCheck(int nb) {
        List<Stack> list_stack = new ArrayList<Stack>(nb);
        for (int i = 0; i < nb; i++) {
            list_stack.add(standardStack(new Date(), 2, 2));
        }
        return list_stack;
    }
}