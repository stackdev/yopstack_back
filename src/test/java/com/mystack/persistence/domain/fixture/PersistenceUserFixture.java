package com.mystack.persistence.domain.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mystack.persistence.domain.User;
import com.mystack.tool.Tool;

public class PersistenceUserFixture {

    public static User standardUser(int friend_nb, int stack_nb) {
        return standardUser(null, friend_nb, stack_nb);
    }

    public static User standardUser(String user_name, int friend_nb, int stack_nb) {
        if (null == user_name || "".equals(user_name)) {
            user_name = "Standard user" + UUID.randomUUID();
        }
        return new User(UUID.randomUUID(), user_name, Tool.getRandomUUIDList(friend_nb), Tool.getRandomUUIDList(stack_nb));
    }

    public static List<User> listStandardUser(int nb_user, int friend_nb, int stack_nb) {
        List<User> output_list = new ArrayList<>();
        for (int i = 0; i < nb_user; i++) {
            output_list.add(standardUser(friend_nb, stack_nb));
        }
        return output_list;
    }

}
