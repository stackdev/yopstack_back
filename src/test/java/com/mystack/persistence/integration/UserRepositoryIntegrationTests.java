package com.mystack.persistence.integration;

import static com.mystack.persistence.domain.fixture.PersistenceUserFixture.standardUser;
import static junit.framework.TestCase.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.config.MongoConfiguration;
import com.mystack.persistence.domain.User;
import com.mystack.persistence.integration.fixture.UserServiceFixture;
import com.mystack.persistence.repository.StackCRUDRepository;
import com.mystack.persistence.repository.UserCRUDRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MongoConfiguration.class })
public class UserRepositoryIntegrationTests {

    //TODO: read that in a global properties file
    private static String coll_name = "user";

    @Autowired
    UserCRUDRepository    repo;

    @Autowired
    StackCRUDRepository   stack_repo;

    @Autowired
    MongoOperations       mongo;

    @Before
    public void setup() throws Exception {
        mongo.dropCollection(coll_name);
    }

    // at the end of integration test we get standard user back
    @After
    public void teardown() {
        mongo.dropCollection(coll_name);
        List<String> list_basic_user = Arrays.asList("dzd", "etienne", "test1", "test2", "test3");
        mongo.insert(UserServiceFixture.getListStandardUser(list_basic_user, 0, 0), coll_name);
    }

    @Test
    public void thatItemIsInsertedIntoRepoWorks() throws Exception {

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        int nb_friend = 2;
        int nb_stack = 1;
        // create a user with two friends, no stack
        User input_user = standardUser(nb_friend, nb_stack);

        User result = repo.save(input_user);

        assertEquals(1, mongo.getCollection(coll_name).countDocuments());

        assertEquals(nb_friend, result.getList_friend().size());
        assertEquals(nb_stack, result.getList_stack().size());
    }
}
