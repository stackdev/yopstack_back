package com.mystack.persistence.integration;

import static com.mystack.persistence.domain.fixture.PersistencePreferenceFixture.standardPreference;
import static junit.framework.TestCase.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.config.MongoConfiguration;
import com.mystack.persistence.domain.Preference;
import com.mystack.persistence.repository.PreferenceCRUDRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MongoConfiguration.class })
public class PreferenceRepositoryIntegrationTests {

    private static Logger    LOG       = LoggerFactory.getLogger(PreferenceRepositoryIntegrationTests.class);

    private static String    coll_name = "preference";

    @Autowired
    PreferenceCRUDRepository repo;

    @Autowired
    MongoOperations          mongo;

    @Before
    public void setup() throws Exception {
        mongo.dropCollection(coll_name);
    }

    @After
    public void teardown() {
        mongo.dropCollection(coll_name);
    }

    @Test
    public void thatItemIsInsertedIntoRepoWorks() throws Exception {

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        Preference input = standardPreference();
        repo.save(input);

        assertEquals(1, mongo.getCollection(coll_name).countDocuments());
    }

}
