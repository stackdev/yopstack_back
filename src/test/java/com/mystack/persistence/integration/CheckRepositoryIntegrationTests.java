package com.mystack.persistence.integration;

import static com.mystack.persistence.domain.fixture.PersistenceCheckFixture.standardCheck;
import static junit.framework.TestCase.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.config.MongoConfiguration;
import com.mystack.persistence.domain.Check;
import com.mystack.persistence.repository.CheckCRUDRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MongoConfiguration.class })
public class CheckRepositoryIntegrationTests {

    private static Logger LOG       = LoggerFactory.getLogger(CheckRepositoryIntegrationTests.class);
    private static String coll_name = "check";

    @Autowired
    CheckCRUDRepository   repo;

    @Autowired
    MongoOperations       mongo;

    @Before
    public void setup() throws Exception {
        mongo.dropCollection(coll_name);
    }

    @After
    public void teardown() {
        mongo.dropCollection(coll_name);
    }

    @Test
    public void thatItemIsInsertedIntoRepoWorks() throws Exception {

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        Check input_check = standardCheck();
        repo.save(input_check);

        assertEquals(1, mongo.getCollection(coll_name).countDocuments());
    }

    @Test
    public void thatCheckSelectionBetweenDatesWorks() throws Exception {

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        Check input_check = standardCheck();
        repo.save(input_check);
        Date check_date = input_check.getDate();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();
        cal.add(Calendar.DATE, 2);
        Date tomorrow = cal.getTime();

        List<Check> result_check = repo.findByStackBetweenDates(input_check.getStack(), tomorrow, yesterday, PageRequest.of(0, 10));
        assertEquals(result_check.size(), 1);
        LOG.info("result_check:" + result_check);
    }

    @Test
    public void thatCheckSelectionBeforeDateWorks() throws Exception {

        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();
        cal.add(Calendar.DATE, 2);
        Date tomorrow = cal.getTime();

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        UUID stack_id = UUID.randomUUID();

        Check check_yesterday = standardCheck();
        check_yesterday.setStack(stack_id);
        check_yesterday.setDate(yesterday);

        Check check_tomorrow = standardCheck();
        check_tomorrow.setStack(stack_id);
        check_tomorrow.setDate(tomorrow);

        repo.save(check_yesterday);
        repo.save(check_tomorrow);

        List<Check> result_check = repo.findByStackBeforeDate(stack_id, today, PageRequest.of(0, 10));
        assertEquals(1, result_check.size());
        LOG.info("result_check:" + result_check);
    }

}
