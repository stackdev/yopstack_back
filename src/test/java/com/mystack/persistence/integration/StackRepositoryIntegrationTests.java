package com.mystack.persistence.integration;

import static com.mystack.persistence.domain.fixture.PersistenceStackFixture.standardStack;
import static junit.framework.TestCase.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.config.MongoConfiguration;
import com.mystack.persistence.domain.Stack;
import com.mystack.persistence.domain.fixture.PersistenceStackFixture;
import com.mystack.persistence.repository.StackCRUDRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MongoConfiguration.class })
public class StackRepositoryIntegrationTests {

    private static String coll_name = "stack";

    @Autowired
    StackCRUDRepository   stackCRUDRepository;

    @Autowired
    MongoOperations       mongo;

    @Before
    public void setup() throws Exception {
        mongo.dropCollection(coll_name);
    }

    @After
    public void teardown() {
        mongo.dropCollection(coll_name);
    }

    @Test
    public void thatItemIsInsertedIntoRepoWorks() throws Exception {

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        stackCRUDRepository.save(standardStack());

        assertEquals(1, mongo.getCollection(coll_name).countDocuments());
    }

    @Test
    public void thatGetAllPublicStacksRepoWorks() throws Exception {
        int nb = 3;
        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        List<Stack> stacks_to_save = PersistenceStackFixture.getListStandardCheck(nb);
        stacks_to_save.get(0).setIsPrivate(true);
        List<UUID> list_stackid = new ArrayList<>();

        Stack saved_stack;
        for (Stack stack : stacks_to_save) {
            saved_stack = stackCRUDRepository.save(stack);
            list_stackid.add(saved_stack.getId());
        }

        List<Stack> result_list = stackCRUDRepository.findAllPublic(list_stackid);

        assertEquals(nb - 1, result_list.size());
        assertEquals(nb, mongo.getCollection(coll_name).countDocuments());
    }
}
