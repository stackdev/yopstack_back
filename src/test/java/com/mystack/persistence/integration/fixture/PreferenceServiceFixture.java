package com.mystack.persistence.integration.fixture;

import java.util.UUID;

import com.mystack.persistence.domain.Preference;

public class PreferenceServiceFixture {

    private static String default_name = "PreferencePersisFix";

    public static Preference standardPreference() {
        return new Preference(UUID.randomUUID(), UUID.randomUUID());
    }
}
