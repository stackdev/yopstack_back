package com.mystack.persistence.integration.fixture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.mystack.persistence.domain.Check;
import com.mystack.persistence.domain.Comment;

public class CheckServiceFixture {

    private static String default_name = "CheckPersisFix";

    public static List<Comment> standardCommentList(int nb) {
        List<Comment> output = new ArrayList<>();
        for (int i = 0; i < nb; i++) {
            output.add(standardComment());
        }
        return output;
    }

    public static Comment standardComment() {
        return new Comment(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), new Date(), "txt", null);
    }

    public static Check standardCheck() {
        Check check = new Check();
        check.setId(UUID.randomUUID());
        check.setDate(new Date());
        check.setStack(UUID.randomUUID());
        check.setComment("This is a standard check");
        return check;
    }

    // TODO: create a template method a this is common between different object
    public static List<Check> getListStandardCheck(int nb) {
        if (nb <= 0) {
            return null;
        }
        Check tmp_check;
        List<Check> output = new ArrayList<>();
        for (int i = 0; i < nb; i++) {
            tmp_check = standardCheck();
            output.add(tmp_check);
        }
        return output;
    }
}
