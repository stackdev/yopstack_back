package com.mystack.persistence.integration.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mystack.config.FunctionalConfiguration.StackType;
import com.mystack.persistence.domain.Stack;

public class StackServiceFixture {

    private static String default_name = "PersFixted";
    private static String default_desc = "Persistence Stack fixtured";

    public static Stack uniqueUserStack() {
        return uniqueUserStack(2, 1, StackType.TIME, false);
    }

    public static Stack uniqueUserStack(int periodicity, int frequency, StackType type, boolean isPrivate) {
        Stack stack = new Stack();
        stack.setDescription(default_desc);
        stack.setName(default_name);
        stack.setId(UUID.randomUUID());
        stack.setPeriodicity(periodicity);
        stack.setFrequency(frequency);
        stack.setType(type);
        stack.setIsPrivate(isPrivate);

        return stack;
    }

    public static List<Stack> getListStandardStack(int nb) {
        if (nb <= 0) {
            return null;
        }
        Stack tmp_stack;
        List<Stack> output = new ArrayList<Stack>();
        for (int i = 0; i < nb; i++) {
            tmp_stack = uniqueUserStack();
            tmp_stack.setName(default_name + i);
            tmp_stack.setDescription(default_desc + i);
            output.add(tmp_stack);
        }
        return output;
    }
}
