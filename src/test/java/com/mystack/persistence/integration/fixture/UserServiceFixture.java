package com.mystack.persistence.integration.fixture;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.springframework.security.crypto.bcrypt.BCrypt;

import com.mystack.persistence.domain.User;
import com.mystack.tool.Tool;

public class UserServiceFixture {

    private static String default_name = "UserPersisFix";

    /**
     * Standard User with default name
     * 
     * @param nb_friend
     * @param nb_stack
     * @return
     */
    public static User standardUser(int nb_friend, int nb_stack)
    {
        return standardUser(default_name, nb_friend, nb_stack);
    }

    /**
     * Standard user with custom name
     * 
     * @param name
     * @param nb_friend
     * @param nb_stack
     * @return
     */
    public static User standardUser(String name, int nb_friend, int nb_stack)
    {
        return standardUser(name, name, nb_friend, nb_stack);
    }

    public static User standardUser(String name, String password, int nb_friend, int nb_stack)
    {
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");
        String password_enc = BCrypt.hashpw(password, BCrypt.gensalt());
        return new User(UUID.randomUUID(), name, Tool.getRandomUUIDList(nb_friend), Tool.getRandomUUIDList(nb_stack), password_enc, true, roles);
    }

    public static List<User> getListStandardUser(List<String> list_usernames) {
        return getListStandardUser(list_usernames, 3, 2);
    }

    public static List<User> getListStandardUser(List<String> list_usernames, int nb_friend, int nb_stack) {
        return getListStandardUser(list_usernames, list_usernames, nb_friend, nb_stack);
    }

    public static List<User> getListStandardUser(List<String> list_usernames, List<String> list_passwords, int nb_friend, int nb_stack) {
        if (list_usernames.size() <= 0 || list_passwords.size() <= 0) {
            return null;
        }
        List<User> output = new ArrayList<>();
        Iterator<String> names_it = list_usernames.iterator();
        Iterator<String> password_it = list_passwords.iterator();
        String pass;
        while (names_it.hasNext()) {
            if (password_it.hasNext()) {
                pass = password_it.next();
            } else {
                pass = "";
            }
            output.add(standardUser(names_it.next(), nb_friend, nb_stack));
        }
        return output;

    }

    /**
     * List of standard user with default names
     * 
     * @param nb
     * @return
     */
    public static List<User> getListStandardUser(int nb) {
        if (nb <= 0) {
            return null;
        }
        // generate a list of random names
        ArrayList<String> list_usernames = new ArrayList<>();
        for (int i = 0; i < nb; i++) {
            list_usernames.add(default_name + "_" + UUID.randomUUID());
        }
        return getListStandardUser(list_usernames);
    }
}
