package com.mystack.persistence.integration;

import static com.mystack.persistence.domain.fixture.MongoAssertions.usingMongo;
import static com.mystack.persistence.domain.fixture.PersistenceStackFixture.standardStack;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClients;
import org.bson.UuidRepresentation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

public class StackMappingIntegrationTests {
    private String  coll_name = "stack";
    MongoOperations mongo;

    @Before
    public void setup() throws Exception {
        ConnectionString connectionString = new ConnectionString("mongodb://app_user:app_password@localhost:27017/mystack?uuidrepresentation=javaLegacy");
        SimpleMongoClientDatabaseFactory mongoDbFactory = new SimpleMongoClientDatabaseFactory(connectionString);

        mongo = new MongoTemplate(mongoDbFactory);
        mongo.dropCollection(coll_name);
    }

    @After
    public void teardown() {
        mongo.dropCollection(coll_name);
    }

    @Test
    public void thatItemIsInsertedIntoCollectionHasCorrectIndexes() throws Exception {

        mongo.insert(standardStack());

        assertEquals(1, mongo.getCollection(coll_name).countDocuments());

        assertTrue(usingMongo(mongo).collection(coll_name).hasIndexOn("_id"));
        assertTrue(usingMongo(mongo).collection(coll_name).first().hasField("description"));
    }

    @Test
    public void thatItemCustomMappingWorks() throws Exception {
        mongo.insert(standardStack());

        assertTrue(usingMongo(mongo).collection(coll_name).first().hasField("name"));
        assertTrue(usingMongo(mongo).collection(coll_name).first().hasField("description"));
    }

}
