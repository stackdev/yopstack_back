package com.mystack.persistence.integration;

import static com.mystack.persistence.domain.fixture.PersistenceStackFixture.standardStack;
import static com.mystack.persistence.domain.fixture.PersistenceStackFixture.standardStackRandomDesc;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.config.MongoConfiguration;
import com.mystack.persistence.domain.Stack;
import com.mystack.persistence.repository.StackCRUDRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MongoConfiguration.class })
public class StackRepositoryMapReduceIntegrationTests {

    private static String coll_name = "stack";

    @Autowired
    StackCRUDRepository   stackRepository;

    @Autowired
    MongoOperations       mongo;

    @Before
    public void setup() throws Exception {
        mongo.dropCollection(coll_name);
    }

    @After
    public void teardown() {
        mongo.dropCollection(coll_name);
    }

    @Test
    public void thatItemIsInsertedIntoRepoWorks() throws Exception {

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        Stack standard1 = standardStack();
        Stack standard2 = standardStackRandomDesc();
        UUID standardId1 = standard1.getId();
        UUID standardId2 = standard2.getId();
        stackRepository.save(standard1);
        stackRepository.save(standard2);
        ArrayList<UUID> l = new ArrayList<>();
        l.add(standardId1);
        l.add(standardId2);
        ArrayList<Stack> list = (ArrayList<Stack>) stackRepository.findAllById(l);

        assertEquals(l.size(), list.size());
    }

    @Test
    public void thatStackDescriptionIsInsertedIntoRepoWorks() throws Exception {

        assertEquals(0, mongo.getCollection(coll_name).countDocuments());

        Stack standard2 = standardStackRandomDesc();
        UUID standardId2 = standard2.getId();
        stackRepository.save(standard2);
        Optional<Stack> result = stackRepository.findById(standardId2);

        assertTrue(result.isPresent());
        assertEquals(result.get().getDescription(), standard2.getDescription());

    }
}
