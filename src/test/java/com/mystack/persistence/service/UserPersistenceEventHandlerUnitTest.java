package com.mystack.persistence.service;

import static com.mystack.persistence.integration.fixture.UserServiceFixture.getListStandardUser;
import static com.mystack.persistence.integration.fixture.UserServiceFixture.standardUser;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.persistence.domain.User;
import com.mystack.persistence.domain.fixture.PersistenceUserFixture;
import com.mystack.persistence.repository.StackCRUDRepository;
import com.mystack.persistence.repository.UserCRUDRepository;
import com.mystack.persistence.services.UserPersistenceEventHandler;

// @ContextConfiguration(classes = { MongoConfiguration.class })
@RunWith(MockitoJUnitRunner.class)
public class UserPersistenceEventHandlerUnitTest {
    private static Logger               LOG = LoggerFactory.getLogger(UserPersistenceEventHandlerUnitTest.class);

    @Mock
    private UserCRUDRepository          repo_mocked;

    @Mock
    private StackCRUDRepository         repo_stack_mocked;

    @InjectMocks
    private UserPersistenceEventHandler service;

    @Before
    public void init() {
        service = new UserPersistenceEventHandler();
        MockitoAnnotations.initMocks(this);
    }

    // Two tests for User creation

    /**
     * Basic test for CreateStackEvent Processing
     */
    @Test
    public void thatUserCreateEventAreProcessed() {
        User user = standardUser(2, 1);
        // our User is returned by the mock when findById is called

        when(repo_mocked.findByName(any(String.class))).thenReturn(null);
        when(repo_mocked.save(any(User.class))).thenReturn(user);

        // create event with a generated UserDetails i.e. with another UUID
        CreateUserEvent event = new CreateUserEvent(user.toDetails());

        UserCreatedEvent result_event = service.createUser(event);

        // assert on event content
        assertTrue(result_event.isCreated());
        assertEquals(result_event.getDetails().getId(), user.toDetails().getId());
        assertEquals(result_event.getDetails().getName(), user.toDetails().getName());
        assertEquals(result_event.getDetails().getList_friend(), user.toDetails().getList_friend());
        assertEquals(result_event.getDetails().getList_stack(), user.toDetails().getList_stack());
    }

    @Test
    public void thatUserCreateEvent_UserNameAlreadyUsed() {
        String user_name = "user1";
        int friend_nb = 1, stack_nb = 1;
        User user = PersistenceUserFixture.standardUser(user_name, friend_nb, stack_nb);
        // our User is returned by the mock when findById is called
        List<User> result = PersistenceUserFixture.listStandardUser(1, friend_nb, stack_nb);

        when(repo_mocked.findByName(user_name)).thenReturn(result);

        // create event with a generated UserDetails i.e. with another UUID
        CreateUserEvent event = new CreateUserEvent(user.toDetails());

        UserCreatedEvent result_event = service.createUser(event);

        verify(repo_mocked, times(0)).save(any(User.class));

        assertFalse(result_event.isCreated());
    }

    // TODO: to be moved in a place where we don't mock db call...
    @Test
    public void thatCreateUser_FriendListIsVerified() {
        User input_user = standardUser(2, 1);

        when(repo_mocked.findByName(any(String.class))).thenReturn(null);
        when(repo_mocked.existsById(any(UUID.class))).thenReturn(true).thenReturn(false);
        // check if save is called with the User with less friends
        // when(repo_mocked.save(any(User.class))).thenReturn(user_to_be_saved);

        // tested call
        service.createUser(new CreateUserEvent(input_user.toDetails()));

        // check that all friend in list are verified
        verify(repo_mocked, times(1)).save(any(User.class));
        verify(repo_mocked, times(input_user.getList_friend().size())).existsById(any(UUID.class));
    }

    // Three tests for DeleteUser event processing

    @Test
    public void thatUserDeleteEventAreProcessed() {
        // We create a User, UUID auto generated
        User user = standardUser(1, 3);

        // create event with a generated UserDetails i.e. with another UUID
        DeleteUserEvent event = new DeleteUserEvent(user.getId());
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(user));

        UserDeletedEvent result_event = service.deleteUser(event);

        verify(repo_mocked).findById(any(UUID.class));
        verify(repo_mocked).deleteById(any(UUID.class));

        assertEquals(user.getId(), result_event.getDetails().getId());
        assertTrue(result_event.isDeletionCompleted());
        assertTrue(result_event.isEntityFound());
    }

    @Test
    public void thatUserDeleteEventAreProcessed_UserNotFound() {
        // We create a User, UUID auto generated
        User user = standardUser(2, 2);
        // our User is returned by the mock when findById is called
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.ofNullable(null));

        // create event with a generated UserDetails i.e. with another UUID
        DeleteUserEvent event = new DeleteUserEvent(user.getId());

        UserDeletedEvent result_event = service.deleteUser(event);

        assertFalse(result_event.isDeletionCompleted());
        assertFalse(result_event.isEntityFound());
    }

    // Test getAllUser operation
    @Test
    public void thatGetAllUserEventIsProcessed() {
        int nb = 4;
        List<User> list = getListStandardUser(nb);

        // our user is returned by the mock when findById is called
        when(repo_mocked.findAll()).thenReturn(list);

        // create event with a generated UserDetails i.e. with another UUID
        RequestAllUsersEvent event = new RequestAllUsersEvent();

        AllUsersEvent result_event = service.getAllUsers(event);

        // assert on event content
        assertTrue(result_event.isEntityFound());
        assertEquals(result_event.getAllUserDetails().size(), nb);
    }

    // Test getAllUser operation
    @Test
    public void thatGetAllUserEventIsProcessed_WithUserName() {
        int nb = 4;
        String searched_name = "plop";
        List<User> list = getListStandardUser(nb);

        // our user is returned by the mock when findById is called
        when(repo_mocked.findByName(any(String.class))).thenReturn(list);

        // create event with a generated UserDetails i.e. with another UUID
        RequestAllUsersEvent event = new RequestAllUsersEvent(searched_name);

        AllUsersEvent result_event = service.getAllUsers(event);

        // assert on event content
        verify(repo_mocked, times(1)).findByName(any(String.class));
        assertTrue(result_event.isEntityFound());
        assertEquals(result_event.getAllUserDetails().size(), nb);
    }

    /**
     * Tests that : - persistence service call findone() method - that userdetailsEvent is created with findone() result
     */
    // Test getUserDetails operation
    @Test
    public void thatRequestUserDetailsEventIsProcessed() {
        User user = standardUser(2, 1);
        // our User is returned by the mock when findById is called
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(user));

        // create event with a generated UserDetails i.e. with another UUID
        RequestUserDetailsEvent event = new RequestUserDetailsEvent(user.getId());

        UserDetailsEvent result_event = service.getUserDetails(event);
        assertEquals(result_event.getDetails().getId(), user.toDetails().getId());
        assertEquals(result_event.getDetails().getName(), user.toDetails().getName());
        assertEquals(result_event.getDetails().getList_friend(), user.toDetails().getList_friend());
        assertEquals(result_event.getDetails().getList_stack(), user.toDetails().getList_stack());
        // assert on event content
        LOG.info("Result: " + result_event.getDetails().getName());

    }

    // user existing update ok,
    @Test
    public void thatUserUpdateEventAreProcessed() {
        // We create a User, UUID auto generated
        User user = standardUser(1, 3);

        // create event with a generated UserDetails i.e. with another UUID
        UpdateUserEvent event = new UpdateUserEvent(user.toDetails(), "");
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(user));
        when(repo_mocked.save(any(User.class))).thenReturn(user);

        UserUpdatedEvent result_event = service.updateUser(event);

        verify(repo_mocked).findById(any(UUID.class));
        verify(repo_mocked).save(any(User.class));

        assertEquals(user.getId(), result_event.getDetails().getId());
        assertTrue(result_event.isUpdated());
    }

    @Test
    public void thatUserUpdateEventAreProcessed_UserNotFound() {
        // We create a User, UUID auto generated
        User user = standardUser(1, 3);

        // create event with a generated UserDetails i.e. with another UUID
        UpdateUserEvent event = new UpdateUserEvent(user.toDetails(), "");
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.ofNullable(null));

        UserUpdatedEvent result_event = service.updateUser(event);

        verify(repo_mocked).findById(any(UUID.class));
        verifyNoMoreInteractions(repo_mocked);

        assertFalse(result_event.isUpdated());
    }

    @Test
    public void thatUserUpdateEventAreProcessed_UserNameAlreadyUsed() {
        String user_name1 = UUID.randomUUID().toString();
        String user_name2 = UUID.randomUUID().toString();
        List<User> list_existing_user = PersistenceUserFixture.listStandardUser(1, 1, 1);

        User user = PersistenceUserFixture.standardUser(user_name1, 1, 1);
        User user_updated = PersistenceUserFixture.standardUser(user_name2, 1, 1);

        // create event with a generated UserDetails i.e. with another UUID
        UpdateUserEvent event = new UpdateUserEvent(user_updated.toDetails(), "");
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(user));
        when(repo_mocked.findByName(user_updated.getName())).thenReturn(list_existing_user);

        UserUpdatedEvent result_event = service.updateUser(event);

        verify(repo_mocked, times(1)).findById(any(UUID.class));
        verify(repo_mocked, times(1)).findByName(any(String.class));
        verify(repo_mocked, times(0)).save(any(User.class));

        assertFalse(result_event.isUpdated());
    }

}
