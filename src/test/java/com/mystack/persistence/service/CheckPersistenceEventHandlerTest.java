package com.mystack.persistence.service;

import static com.mystack.persistence.integration.fixture.CheckServiceFixture.getListStandardCheck;
import static com.mystack.persistence.integration.fixture.CheckServiceFixture.standardCheck;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.core.events.checks.request.CreateCheckEvent;
import com.mystack.core.events.checks.request.DeleteCheckEvent;
import com.mystack.core.events.checks.request.RequestAllChecksEvent;
import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.request.RequestLastChecksEvent;
import com.mystack.core.events.checks.request.UpdateCheckEvent;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.core.events.checks.result.CheckCreatedEvent;
import com.mystack.core.events.checks.result.CheckDeletedEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.events.checks.result.CheckUpdatedEvent;
import com.mystack.persistence.domain.Check;
import com.mystack.persistence.integration.fixture.CheckServiceFixture;
import com.mystack.persistence.repository.CheckCRUDRepository;
import com.mystack.persistence.services.CheckPersistenceEventHandler;
import com.mystack.tool.MyPrincipal;

// @ContextConfiguration(classes = { MongoConfiguration.class })
// @RunWith(MockitoJUnitRunner.class)
public class CheckPersistenceEventHandlerTest {
    private static Logger                LOG = LoggerFactory.getLogger(CheckPersistenceEventHandlerTest.class);

    @Mock
    private CheckCRUDRepository          repo_mocked;

    @InjectMocks
    private CheckPersistenceEventHandler service;

    @Before
    public void init() {
        service = new CheckPersistenceEventHandler();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void thatCheckCreateEventAreProcessed() {
        Check check = standardCheck();

        when(repo_mocked.save(any(Check.class))).thenReturn(check);

        // create event with a generated Details i.e. with another UUID
        CreateCheckEvent event = new CreateCheckEvent(check.toDetails(), new MyPrincipal("plop").getName());

        CheckCreatedEvent result_event = service.createCheck(event);

        // assert on event content
        assertTrue(result_event.isCreated());
        assertEquals(result_event.getDetails().getId(), check.toDetails().getId());
        assertEquals(result_event.getDetails().getStack(), check.toDetails().getStack());
        assertEquals(result_event.getDetails().getComment(), check.toDetails().getComment());
        assertEquals(result_event.getDetails().getDate(), check.toDetails().getDate());
    }

    // Three tests for Delete event processing

    @Test
    public void thatCheckDeleteEventAreProcessed() {
        // We create a check, UUID auto generated
        Check check = standardCheck();

        // create event with a generated UserDetails i.e. with another UUID
        DeleteCheckEvent event = new DeleteCheckEvent(check.getId(), new MyPrincipal("plop").getName());
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(check));

        CheckDeletedEvent result_event = service.deleteCheck(event);

        verify(repo_mocked).findById(any(UUID.class));
        verify(repo_mocked).deleteById(any(UUID.class));

        assertEquals(check.getId(), result_event.getDetails().getId());
        assertTrue(result_event.isDeletionCompleted());
        assertTrue(result_event.isEntityFound());
    }

    @Test
    public void thatCheckDeleteEventAreProcessed_CheckNotFound() {
        Check check = standardCheck();

        // create event with a generated UserDetails i.e. with another UUID
        DeleteCheckEvent event = new DeleteCheckEvent(check.getId(), new MyPrincipal("plop").getName());
        when(repo_mocked.existsById(any(UUID.class))).thenReturn(false);

        CheckDeletedEvent result_event = service.deleteCheck(event);

        verify(repo_mocked).findById(any(UUID.class));

        assertEquals(check.getId(), result_event.getDetails().getId());
        assertFalse(result_event.isEntityFound());
    }

    @Test
    public void thatGetAllChecksEventIsProcessed() {
        int nb = 4;
        List<Check> list = getListStandardCheck(nb);

        // our user is returned by the mock when findById is called
        when(repo_mocked.findByStackOrderByDateDesc(any(UUID.class), any(Pageable.class))).thenReturn(list);

        // create event with a generated UserDetails i.e. with another UUID
        RequestAllChecksEvent event = new RequestAllChecksEvent(UUID.randomUUID(), "", 10);

        AllChecksEvent result_event = service.getAllChecks(event);

        verify(repo_mocked, times(0)).findAll();
        verify(repo_mocked, times(1)).findByStackOrderByDateDesc(any(UUID.class), any(Pageable.class));
        // assert on event content
        assertTrue(result_event.isEntityFound());
        assertEquals(result_event.getAllCheckDetails().size(), nb);
    }
    
    @Test
    public void that_get_last_check_returns_count_of_events() {
        int nb = 4;
        Page<Check> checkPage = new PageImpl<Check>(getListStandardCheck(nb));
        

        // our user is returned by the mock when findById is called
        when(repo_mocked.findByStackAndStatusOrderByDateDesc(any(UUID.class), any(CheckStatus.class), any(PageRequest.class)))
        		.thenReturn(checkPage);

        // create event with a generated UserDetails i.e. with another UUID
        RequestLastChecksEvent event = new RequestLastChecksEvent(UUID.randomUUID(), CheckStatus.OK);

        CheckDetailsEvent resultEvent = service.getLatestCheck(event);


        verify(repo_mocked, times(1)).findByStackAndStatusOrderByDateDesc(any(UUID.class), any(CheckStatus.class), any(PageRequest.class));
        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        assertEquals(resultEvent.getTotalCount().get(), Long.valueOf(nb));
    }

    @Test
    public void thatGetAllCheckEventIsProcessed_WithStackId() {
        int nb = 4;
        UUID searched_stack = UUID.randomUUID();
        List<Check> list = CheckServiceFixture.getListStandardCheck(nb);

        // we will check that findByStack is called when searching check with the stack as a param
        when(repo_mocked.findByStackOrderByDateDesc(any(UUID.class), any(Pageable.class))).thenReturn(list);

        // create event with a generated UserDetails i.e. with another UUID
        RequestAllChecksEvent event = new RequestAllChecksEvent(searched_stack, "", 10);

        AllChecksEvent result_event = service.getAllChecks(event);

        // assert on event content
        verify(repo_mocked, times(0)).findByStack(any(UUID.class));
        verify(repo_mocked, times(1)).findByStackOrderByDateDesc(any(UUID.class), any(Pageable.class));

        verify(repo_mocked, times(0)).findAll();
        assertTrue(result_event.isEntityFound());
        assertEquals(result_event.getAllCheckDetails().size(), nb);
    }

    @Test
    public void thatRequestCheckDetailsEventIsProcessed() {
        Check check = standardCheck();
        // our User is returned by the mock when findById is called
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(check));

        RequestCheckDetailsEvent event = new RequestCheckDetailsEvent(check.getId());

        CheckDetailsEvent result_event = service.getCheckDetails(event);
        assertEquals(result_event.getDetails().getId(), check.toDetails().getId());
        assertEquals(result_event.getDetails().getDate(), check.toDetails().getDate());
        assertEquals(result_event.getDetails().getComment(), check.toDetails().getComment());
        assertEquals(result_event.getDetails().getStack(), check.toDetails().getStack());
        // assert on event content
        LOG.info("Result: " + result_event.getDetails().getComment());

    }

    @Test
    public void thatCheckUpdateEventAreProcessed_CheckNotFound() {

        Check update_check = standardCheck();

        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.ofNullable(null));
        when(repo_mocked.save(any(Check.class))).thenReturn(update_check);

        UpdateCheckEvent event = new UpdateCheckEvent(update_check.toDetails(), new MyPrincipal("plop").getName());
        CheckUpdatedEvent result_event = service.updateCheck(event);

        // assert on event content
        assertFalse(result_event.isUpdated());
        verify(repo_mocked, times(0)).save(any(Check.class));
    }

}
