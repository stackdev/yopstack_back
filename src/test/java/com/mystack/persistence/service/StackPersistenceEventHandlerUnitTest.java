package com.mystack.persistence.service;

import static com.mystack.persistence.integration.fixture.StackServiceFixture.getListStandardStack;
import static com.mystack.persistence.integration.fixture.StackServiceFixture.uniqueUserStack;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.config.FunctionalConfiguration.StackType;
import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.persistence.domain.Stack;
import com.mystack.persistence.repository.StackCRUDRepository;
import com.mystack.persistence.services.StackPersistenceEventHandler;

// @ContextConfiguration(classes = { MongoConfiguration.class })
@RunWith(MockitoJUnitRunner.class)
public class StackPersistenceEventHandlerUnitTest {
    private static Logger                LOG = LoggerFactory.getLogger(StackPersistenceEventHandlerUnitTest.class);

    @Mock
    private StackCRUDRepository          repo_mocked;

    @InjectMocks
    private StackPersistenceEventHandler service;

    @Before
    public void init() {
        service = new StackPersistenceEventHandler();
        MockitoAnnotations.initMocks(this);
    }

    // Two tests for stack creation

    /**
     * Basic test for CreateStackEvent Processing
     */
    @Test
    public void thatStackCreateEventAreProcessed() {
        boolean isPrivate = true;
        int periodicity = 12, frequency = 1;
        StackType type = StackType.NOTES;
        Stack stack = uniqueUserStack(periodicity, frequency, type, isPrivate);

        // our stack is returned by the mock when findById is called
        when(repo_mocked.save(any(Stack.class))).thenReturn(stack);
        ArgumentCaptor<Stack> argument = ArgumentCaptor.forClass(Stack.class);

        // create event with a generated StackDetails i.e. with another UUID
        CreateStackEvent event = new CreateStackEvent(stack.toDetails(), UUID.randomUUID());

        StackCreatedEvent result_event = service.createStack(event);

        verify(repo_mocked).save(argument.capture());

        // assert on event content
        assertTrue(result_event.isCreated());
        assertEquals(result_event.getDetails().getDescription(), stack.toDetails().getDescription());
        assertEquals(result_event.getDetails().getId(), stack.toDetails().getId());
        assertEquals(result_event.getDetails().getName(), stack.toDetails().getName());

        assertThat(stack.getPeriodicity(), equalTo(argument.getValue().getPeriodicity()));
        assertThat(stack.getFrequency(), equalTo(argument.getValue().getFrequency()));
        assertThat(stack.getIsPrivate(), equalTo(argument.getValue().getIsPrivate()));
        assertThat(stack.getType(), equalTo(argument.getValue().getType()));

        LOG.info("Result: " + result_event.getDetails().getDescription());
    }

    // Three tests for DeleteStack event processing

    @Test
    public void thatStackDeleteEventAreProcessed() {
        // We create a stack, UUID auto generated
        Stack stack = uniqueUserStack();

        // create event with a generated StackDetails i.e. with another UUID
        DeleteStackEvent event = new DeleteStackEvent(stack.getId(), UUID.randomUUID());
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(stack));

        StackDeletedEvent resultEvent = service.deleteStack(event);

        verify(repo_mocked).findById(any(UUID.class));
        verify(repo_mocked).deleteById(any(UUID.class));

        assertEquals(stack.getId(), resultEvent.getDetails().getId());
        assertTrue(resultEvent.isDeletionCompleted());
        assertTrue(resultEvent.isEntityFound());
    }

    @Test
    public void thatStackDeleteEventAreProcessed_StackNotFound() {
        // We create a stack, UUID auto generated
        Stack stack = uniqueUserStack();
        // our stack is returned by the mock when findById is called
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.ofNullable(null));

        // create event with a generated StackDetails i.e. with another UUID
        DeleteStackEvent event = new DeleteStackEvent(stack.getId(), UUID.randomUUID());

        StackDeletedEvent resultEvent = service.deleteStack(event);

        assertFalse(resultEvent.isDeletionCompleted());
        assertFalse(resultEvent.isEntityFound());
    }

    // Test getAllStack operation
    @Test
    public void thatGetAllStackEventIsProcessed() {
        int nb = 4;
        List<Stack> list = getListStandardStack(nb);
        //AllStacksEvent mocked_event = new AllStacksEvent(list);

        // our stack is returned by the mock when findById is called
        when(repo_mocked.findAll()).thenReturn(list);

        // create event with a generated StackDetails i.e. with another UUID
        RequestAllStacksEvent event = new RequestAllStacksEvent();

        AllStacksEvent resultEvent = service.getAllStacks(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        assertEquals(nb, resultEvent.getAllStackDetails().size());
    }

    /**
     * Test getAllStack operation
     * Check that repo is calling the findAll method with the list of stack id as param
     */
    @Test
    public void thatGetAllStack_UsesListOfStackId() {
        int nb = 4;
        List<Stack> expected_stacks = getListStandardStack(nb);
        //AllStacksEvent mocked_event = new AllStacksEvent(list);
        List<UUID> expected_stacks_ids = new ArrayList<UUID>();
        for (Stack s : expected_stacks) {
            expected_stacks_ids.add(s.getId());
        }

        // our stack is returned by the mock when findById is called
        when(repo_mocked.findAllById(expected_stacks_ids)).thenReturn(expected_stacks);

        // create event with the list of stack ids requested
        RequestAllStacksEvent event = new RequestAllStacksEvent(expected_stacks_ids, true, false);

        AllStacksEvent resultEvent = service.getAllStacks(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        assertEquals(nb, resultEvent.getAllStackDetails().size());
    }

    /**
     * Test getAllStack operation
     * Check that repo is calling the findAll and returns only public stacks
     */
    @Test
    public void thatGetAllStack_OnlyPublic() {
        int nb = 4;
        List<Stack> expected_stacks = getListStandardStack(nb);
        //AllStacksEvent mocked_event = new AllStacksEvent(list);
        List<UUID> expected_stacks_ids = new ArrayList<UUID>();
        for (Stack s : expected_stacks) {
            expected_stacks_ids.add(s.getId());
        }

        // our stack is returned by the mock when findById is called
        when(repo_mocked.findAllById(expected_stacks_ids)).thenReturn(expected_stacks);
        // else return nothing

        // create event with the list of stack ids requested
        RequestAllStacksEvent event = new RequestAllStacksEvent(expected_stacks_ids, true, false);

        AllStacksEvent resultEvent = service.getAllStacks(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        assertEquals(nb, resultEvent.getAllStackDetails().size());
    }

    /**
     * Tests that :
     * - persistence service call findone() method
     * - that stackdetailsEvent is created with findone() result
     */
    // Test getStackDetails operation
    @Test
    public void thatRequestStackDetailsEventIsProcessed() {
        Stack stack = uniqueUserStack();
        // our stack is returned by the mock when findById is called
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(stack));

        // create event with a generated StackDetails i.e. with another UUID
        RequestStackDetailsEvent event = new RequestStackDetailsEvent(stack.getId());

        StackDetailsEvent resultEvent = service.getStackDetails(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        assertEquals(resultEvent.getDetails().getDescription(), stack.toDetails().getDescription());
        assertEquals(resultEvent.getDetails().getId(), stack.toDetails().getId());
        assertEquals(resultEvent.getDetails().getName(), stack.toDetails().getName());
        LOG.info("Result: " + resultEvent.getDetails().getDescription());

    }

    /**
     * Tests that :
     * - persistence service call save() method
     * - that stackUpdatedEvent contains the result from the save
     */
    @Test
    public void thatUpdateStackEventIsProcessed() {
        Stack stack = uniqueUserStack();
        Stack stack2 = uniqueUserStack();
        // our stack is returned by the mock when findById is called
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(stack));
        when(repo_mocked.save(any(Stack.class))).thenReturn(stack2);

        // create event with a generated StackDetails i.e. with another UUID
        UpdateStackEvent event = new UpdateStackEvent(stack.toDetails());

        StackUpdatedEvent resultEvent = service.updateStack(event);

        // assert on event content
        verify(repo_mocked, times(1)).findById(any(UUID.class));
        verify(repo_mocked, times(1)).save(any(Stack.class));
        assertTrue(resultEvent.isUpdated());
        assertEquals(resultEvent.getDetails().getDescription(), stack2.toDetails().getDescription());
        assertEquals(resultEvent.getDetails().getId(), stack2.toDetails().getId());
        assertEquals(resultEvent.getDetails().getName(), stack2.toDetails().getName());
        LOG.info("Result: " + resultEvent.getDetails().getDescription());

    }
}
