package com.mystack.persistence.service;

import static com.mystack.persistence.integration.fixture.PreferenceServiceFixture.standardPreference;
import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;
import com.mystack.persistence.domain.Preference;
import com.mystack.persistence.repository.PreferenceCRUDRepository;
import com.mystack.persistence.services.PreferencePersistenceEventHandler;
import com.mystack.tool.MyPrincipal;

// @ContextConfiguration(classes = { MongoConfiguration.class })
// @RunWith(MockitoJUnitRunner.class)
public class PreferencePersistenceEventHandlerUnitTest {
    private static Logger                     LOG = LoggerFactory.getLogger(PreferencePersistenceEventHandlerUnitTest.class);

    @Mock
    private PreferenceCRUDRepository          repo_mocked;

    @InjectMocks
    private PreferencePersistenceEventHandler service;

    @Before
    public void init() {
        service = new PreferencePersistenceEventHandler();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void thatPreferenceReadEventAreProcessed() {
        Preference pref = standardPreference();

        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.ofNullable(pref));

        // create event with a generated Details i.e. with another UUID
        ReadPreferenceEvent event = new ReadPreferenceEvent(pref.toDetails(), new MyPrincipal("plop").getName());

        PreferenceReadEvent result_event = service.readPreference(event);

        // assert on event content
        assertTrue(result_event.isEntityFound());
        assertEquals(result_event.getDetails().getStacksorting(), pref.toDetails().getStacksorting());
    }

    @Test
    public void thatPreferenceCreateEventAreProcessed() {
        Preference pref = standardPreference();

        when(repo_mocked.save(any(Preference.class))).thenReturn(pref);

        // create event with a generated Details i.e. with another UUID
        CreatePreferenceEvent event = new CreatePreferenceEvent(pref.toDetails(), new MyPrincipal("plop").getName());

        PreferenceCreatedEvent result_event = service.createPreference(event);

        // assert on event content
        assertTrue(result_event.isCreated());
        assertEquals(result_event.getDetails().getId(), pref.toDetails().getId());
        assertEquals(result_event.getDetails().getUser(), pref.toDetails().getUser());
        assertEquals(result_event.getDetails().getStacksorting(), pref.toDetails().getStacksorting());
    }

    
    @Test
    public void thatPreferenceDeleteEventAreProcessed() {
        Preference pref = standardPreference();

        DeletePreferenceEvent event = new DeletePreferenceEvent(pref.toDetails(), new MyPrincipal("plop").getName());
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(pref));

        PreferenceDeletedEvent result_event = service.deletePreference(event);

        verify(repo_mocked).deleteById(any(UUID.class));
        assertTrue(result_event.isDeletionCompleted());
    }

    @Test
    public void thatPreferenceDeleteEventAreProcessed_CheckNotFound() {
        Preference pref = standardPreference();

        DeletePreferenceEvent event = new DeletePreferenceEvent(pref.toDetails(), new MyPrincipal("plop").getName());
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.ofNullable(null));

        PreferenceDeletedEvent result_event = service.deletePreference(event);

        verify(repo_mocked).findById(any(UUID.class));

        verify(repo_mocked, times(0)).deleteById(any(UUID.class));
        assertFalse(result_event.isDeletionCompleted());
    }

    @Test
    public void thatPreferenceUpdateEventAreProcessed() {
        Preference pref = standardPreference();

        when(repo_mocked.save(any(Preference.class))).thenReturn(pref);
        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(pref));

        // create event with a generated Details i.e. with another UUID
        UpdatePreferenceEvent event = new UpdatePreferenceEvent(pref.toDetails(), new MyPrincipal("plop").getName());

        PreferenceUpdatedEvent result_event = service.updatePreference(event);

        // assert on event content
        assertTrue(result_event.isUpdated());

        assertEquals(result_event.getDetails().getId(), pref.toDetails().getId());
    }

    @Test
    public void thatPreferenceUpdate_only_if_it_exists() {
        Preference pref = standardPreference();

        when(repo_mocked.findById(any(UUID.class))).thenReturn(Optional.ofNullable(null));

        // create event with a generated Details i.e. with another UUID
        UpdatePreferenceEvent event = new UpdatePreferenceEvent(pref.toDetails(), new MyPrincipal("plop").getName());

        PreferenceUpdatedEvent result_event = service.updatePreference(event);

        verify(repo_mocked, times(0)).save(any(Preference.class));
        // assert on event content
        assertFalse(result_event.isUpdated());

        assertEquals(result_event.getDetails().getId(), pref.toDetails().getId());
    }

}
