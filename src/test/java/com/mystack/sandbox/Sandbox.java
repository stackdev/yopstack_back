package com.mystack.sandbox;

import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Sandbox {
    private static Logger LOG = LoggerFactory.getLogger(Sandbox.class);

    @Test
    public void playWithDate() {

        long ref_epoch = new Long("1449417691000");
        long date_epoch = new Long("1451145691000");

        // 6 dec 2015 17:01:31
        GregorianCalendar reference_cal = new GregorianCalendar();
        reference_cal.setTime(new Date(ref_epoch));
        LOG.info("reference date epoch ms: " + reference_cal.getTimeInMillis());

        //26 dec 2015 17:01:31  
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(new Date(date_epoch));
        LOG.info("date to be checked epoch ms: " + reference_cal.getTimeInMillis());

        // periodicity 2 jours
        long periodicity = 2 * 24 * 3600 * 1000;

        long date_diff_ms = cal.getTimeInMillis() - reference_cal.getTimeInMillis();
        float period = (date_diff_ms) / periodicity;

        LOG.info("Date diff in ms: " + date_diff_ms);
        LOG.info("The period for date: " + cal.toString());
        LOG.info(" is: " + period);

    }
}
