package com.mystack.functional;

import static org.mockito.Mockito.mock;

import com.mystack.persistence.repository.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MockConfig {

    @Bean
    public StackCRUDRepository createStackCRUDRepositoryMock() {
        return mock(StackCRUDRepository.class);
    }

    @Bean
    public UserCRUDRepository createUserCRUDRepositoryMock() {
        return mock(UserCRUDRepository.class);
    }

    @Bean
    public CheckCRUDRepository createCheckCRUDRepositoryMock() {
        return mock(CheckCRUDRepository.class);
    }

    @Bean
    public CommentCRUDRepository createCommentCRUDRepositoryMock() {
        return mock(CommentCRUDRepository.class);
    }

    @Bean
    public PreferenceCRUDRepository createPreferenceCRUDRepositoryMock() {
        return mock(PreferenceCRUDRepository.class);
    }

    @Bean
    public MentionCRUDRepository createMentionCRUDRepositoryMock() {
        return mock(MentionCRUDRepository.class);
    }
}
