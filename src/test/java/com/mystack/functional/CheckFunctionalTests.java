package com.mystack.functional;

import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.mystack.config.CoreConfig;
import com.mystack.core.services.CheckService;
import com.mystack.persistence.repository.CheckCRUDRepository;
import com.mystack.persistence.repository.StackCRUDRepository;
import com.mystack.persistence.repository.UserCRUDRepository;

/**
 * Tests from behind the REST controller, throught all the layer until the persistence
 * DB is mocked
 * 
 * @author dzd
 * 
 */

//TODO: temporaary desactivated, the test strategy needs to be reviewed, we can not use DI for mock, because it seems to keep state between tests (??)

//@RunWith(SpringJUnit4ClassRunner.class)
@Ignore
@ContextConfiguration(classes = { CoreConfig.class, MockConfig.class })
public class CheckFunctionalTests {

    private static Logger       LOG = LoggerFactory.getLogger(CheckFunctionalTests.class);

    @Autowired
    private StackCRUDRepository stack_repo_mocked;

    @Autowired
    private UserCRUDRepository  user_repo_mocked;

    @Autowired
    private CheckCRUDRepository check_repo_mocked;

    // SUT
    @Autowired
    CheckService                checkService;

    //@Test
    //    public void that_StackLastCheck_isUpdated_onCheck() {
    //        com.mystack.persistence.domain.Stack stack = PersistenceStackFixture.standardStack();
    //        LOG.info("Stack fixture: " + stack);
    //        //WIP
    //        when(stack_repo_mocked.findOne(any(UUID.class))).thenReturn(stack);
    //        when(check_repo_mocked.save(any(com.mystack.persistence.domain.Check.class))).thenReturn(new com.mystack.persistence.domain.Check());
    //        when(stack_repo_mocked.save(any(com.mystack.persistence.domain.Stack.class))).thenReturn(stack);
    //
    //        //provide an existing stack 
    //        // do a check
    //        CheckDetails details = ChecksFixtures.getStandardCheckDetails();
    //        CreateCheckEvent createCheckEvent = new CreateCheckEvent(details);
    //        checkService.createCheck(createCheckEvent);
    //        // verify that the update of the stack is called
    //        // capture the stack object sent to the persistence and compare the last_check value with current time
    //        ArgumentCaptor<com.mystack.persistence.domain.Stack> argument = ArgumentCaptor.forClass(com.mystack.persistence.domain.Stack.class);
    //
    //        //The stack should be updated with last check information
    //        verify(stack_repo_mocked, times(1)).save(argument.capture());
    //        verify(check_repo_mocked, times(1)).save(any(com.mystack.persistence.domain.Check.class));
    //        assertTrue(argument.getValue().getLast_check().compareTo(new Date()) < 3);
    //
    //    }
}
