package com.mystack.functional;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.config.CoreConfig;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.domain.fixtures.StacksFixtures;
import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.services.StackService;
import com.mystack.persistence.domain.fixture.PersistenceUserFixture;
import com.mystack.persistence.repository.StackCRUDRepository;
import com.mystack.persistence.repository.UserCRUDRepository;

/**
 * Tests from behind the REST controller, throught all the layer until the persistence
 * DB is mocked
 * 
 * @author dzd
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CoreConfig.class, MockConfig.class })
public class StackFunctionalTests {

    private static Logger       LOG = LoggerFactory.getLogger(StackFunctionalTests.class);

    @Autowired
    private StackCRUDRepository stack_repo_mocked;

    @Autowired
    private UserCRUDRepository  user_repo_mocked;

    // SUT
    @Autowired
    StackService                stackService;

    @Before
    public void init() {
        // MockitoAnnotations.initMocks(this);
    }

    @Test
    public void thatLastCheckDate_StackAttributes_IsPropagatedFromPersistence() {

        Date now = new Date();
        com.mystack.persistence.domain.Stack persistenceStack = new com.mystack.persistence.domain.Stack();
        persistenceStack.setDescription("plop");
        persistenceStack.setLast_check(now);
        LOG.info("Returned by persistence mock: " + persistenceStack);
        when(stack_repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(persistenceStack));

        RequestStackDetailsEvent requestStackEvent = new RequestStackDetailsEvent(UUID.randomUUID());
        StackDetailsEvent resultEvent = stackService.getStackDetails(requestStackEvent);
        LOG.info("Result: " + resultEvent.getDetails().toString());
        assertEquals(resultEvent.getDetails().getLast_check(), now);
    }

    @Test
    public void that_Stack_CreationDate_IsSetbyServiceLayer() {

        // a fake user owning one stack
        com.mystack.persistence.domain.User stack_owner = PersistenceUserFixture.standardUser(0, 1);

        StackDetails details = StacksFixtures.getStandardStack().toDetails();
        CreateStackEvent event = new CreateStackEvent(details, stack_owner.getList_stack().get(0), stack_owner.getName());

        when(user_repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(stack_owner));
        //get the Stack passed to the repo to verify the value for creation date
        ArgumentCaptor<com.mystack.persistence.domain.Stack> argument = ArgumentCaptor.forClass(com.mystack.persistence.domain.Stack.class);

        stackService.createStack(event);

        verify(stack_repo_mocked, times(1)).save(argument.capture());
        assertTrue(null != argument.getValue().getCreation_date());
        assertTrue(new Date().compareTo(argument.getValue().getCreation_date()) < 3);
    }

    @Test
    public void thatUp() {

        Date now = new Date();
        com.mystack.persistence.domain.Stack persistenceStack = new com.mystack.persistence.domain.Stack();
        persistenceStack.setDescription("plop");
        persistenceStack.setLast_check(now);
        LOG.info("Returned by persistence mock: " + persistenceStack);
        when(stack_repo_mocked.findById(any(UUID.class))).thenReturn(Optional.of(persistenceStack));

        RequestStackDetailsEvent requestStackEvent = new RequestStackDetailsEvent(UUID.randomUUID());
        StackDetailsEvent resultEvent = stackService.getStackDetails(requestStackEvent);
        LOG.info("Result: " + resultEvent.getDetails().toString());
        assertEquals(resultEvent.getDetails().getLast_check(), now);
    }
}
