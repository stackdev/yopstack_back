package com.mystack.rest.EndToEnd;

import static com.mystack.rest.fixture.RestStackDataFixtures.nowStackStackDetails;
import static com.mystack.rest.fixture.RestStackDataFixtures.stackJSONFromDetails;
import static com.mystack.rest.fixture.RestStackDataFixtures.standardStackJSON;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mystack.config.FunctionalConfiguration.StackStatus;
import com.mystack.config.FunctionalConfiguration.StackType;
import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.domain.StackDetails;
import com.mystack.rest.domain.Stack;
import com.mystack.rest.domain.User;
import com.mystack.rest.fixture.RestStackDataFixtures;
import com.mystack.tool.UtilsEndToEnd;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class StackEndToEndTests {
    private static Logger LOG      = LoggerFactory.getLogger(StackEndToEndTests.class);

    @Autowired
    TestString            testString;

    public UUID           existing_user_id;

    String                username = "dzd";
    String                password = "dzd";
    String                auth     = username + ":" + password;

    @Before
    public void setup() {
        // init the testString class for UtilsEndToEnd as it is a static class
        UtilsEndToEnd.setTestString(testString);

        // user that can be used for Stack creation
        //Not creating dzd....
        try {
            existing_user_id = UtilsEndToEnd.getAUserByName_id(username);
        } catch (HttpClientErrorException e) {
            LOG.info("Not authorized... keep going");
        }
        if (null == existing_user_id) {
            existing_user_id = UtilsEndToEnd.postAUser_id(username, username);
        }

    }

    @Test
    public void thatStacksCanBeAdded() {

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        StackDetails input_data = nowStackStackDetails();
        input_data.setIsPrivate(true);
        input_data.setType(StackType.TIME);
        input_data.setStatus(StackStatus.ARCHIVED);
        input_data.setIsShared(true);

        String userid = existing_user_id.toString();
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                stackJSONFromDetails(input_data),
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

        LOG.info("to be sent: " + requestEntity);

        RestTemplate template = new RestTemplate();
        ResponseEntity<Stack> entity = template.postForEntity(
                testString.getStacks_fullPath(userid),
                requestEntity, Stack.class);

        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getStacks_path(userid)));
        Stack result_stack = entity.getBody();

        LOG.info("Result stack looks like: " + result_stack.toString());
        LOG.info("The Location is " + entity.getHeaders().getLocation());
        assertEquals(input_data.getName(), result_stack.getName());
        assertEquals(input_data.getDescription(), result_stack.getDescription());
        assertEquals(true, result_stack.getIsPrivate());
        assertEquals(StackStatus.ARCHIVED.getValue(), result_stack.getStatus());
        assertEquals(true, result_stack.getIsShared());
    }

    @Test
    public void thatStacksCanBeAddedAndQueried() {
        StackDetails input_data = nowStackStackDetails();
        String userid = existing_user_id.toString();
        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);

        // POST a new stack first
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                stackJSONFromDetails(input_data),
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

        RestTemplate template = new RestTemplate();
        ResponseEntity<Stack> entity = template.postForEntity(
                testString.getStacks_fullPath(userid),
                requestEntity, Stack.class);

        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getStacks_path(userid)));
        Stack result_stack = entity.getBody();

        assertEquals(input_data.getName(), result_stack.getName());
        assertEquals(input_data.getDescription(), result_stack.getDescription());

        // POST end

        // GET the stack
        auth_cookie = UtilsEndToEnd.authenticate(username, password);
        requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        template = new RestTemplate();
        entity = template.exchange(testString.getStacks_fullPath(userid, result_stack.getId().toString()),
                HttpMethod.GET, requestEntity, Stack.class);
        Stack result_stack2 = entity.getBody();
        LOG.info("Result stack2 looks like: " + result_stack2.toString());
        assertEquals(input_data.getName(), result_stack2.getName());
        assertEquals(input_data.getDescription(), result_stack2.getDescription());

    }

    /**
     * Create 1 stack for 3 differents users Query all stack for one user, expect only one stack
     */
    @Test
    public void thatStackCanBeAdded_And_AllStackFromAnUserQueried() {

        User user_before = UtilsEndToEnd.getAUserById(existing_user_id).getBody();
        UtilsEndToEnd.postAStack(existing_user_id);
        User user_after = UtilsEndToEnd.getAUserById(existing_user_id).getBody();

        assertEquals(user_before.getList_stack().length + 1, user_after.getList_stack().length);
    }

    @Test
    public void thatStacksCannotBeAdded_WithoutExistingUser() {

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        // Random UUID passed as user, --> not existing in db
        String userid = UUID.randomUUID().toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                standardStackJSON(),
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<Stack> entity = template.postForEntity(
                    testString.getStacks_fullPath(userid),
                    requestEntity, Stack.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void thatStacksCannotBeViewed_WithoutExistingUser() {

        // create a stack for an existing user
        Stack existing_user_stack = UtilsEndToEnd.postAStack_Stack(existing_user_id);

        // Random UUID passed as user, --> not existing in db
        String userid = UUID.randomUUID().toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeaders("dzd" + ":" + "dzd"));

        RestTemplate template = new RestTemplate();
        // pass generic information to rest template
        ParameterizedTypeReference<Stack> responseType = new ParameterizedTypeReference<Stack>() {
        };
        try {
            String getUrl = testString
                    .getStacks_fullPath(userid, existing_user_stack
                            .getId()
                            .toString());
            ResponseEntity<Stack> entity = template.exchange(getUrl, HttpMethod.GET, requestEntity, responseType);
            // Stack result = entity.getBody();
            // assertEquals(1, result.size());

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        } catch (Exception e) {
            // TMP debug
            e.printStackTrace();
        }
    }

    @Test
    public void thatPrivateStacksCannotBeViewed_WithExistingUserNotOwner() {
        // prepare one private and one public stack 
        StackDetails public_stack = RestStackDataFixtures.nowStackStackDetails();
        public_stack.setIsPrivate(false);
        public_stack.setName("PUBLIC");
        StackDetails private_stack = RestStackDataFixtures.nowStackStackDetails();
        private_stack.setIsPrivate(true);
        private_stack.setName("PRIVATE");

        // create a stack for an existing user
        Stack private_stack_saved = UtilsEndToEnd.postAStack(existing_user_id, private_stack, username, password).getBody();
        Stack public_stack_saved = UtilsEndToEnd.postAStack(existing_user_id, public_stack, username, password).getBody();

        //getting the stack with the username and password from the owner should work
        ResponseEntity<Stack> response1 = UtilsEndToEnd.getAStack(private_stack_saved.getId(), existing_user_id, username, password);
        assertEquals(response1.getStatusCode(), HttpStatus.OK);

        ResponseEntity<Stack> response2 = UtilsEndToEnd.getAStack(public_stack_saved.getId(), existing_user_id, username, password);
        assertEquals(response2.getStatusCode(), HttpStatus.OK);

        //getting the stack with the credentials of another user won't work
        ResponseEntity<Stack> response3 = UtilsEndToEnd.getAStack(public_stack_saved.getId(), existing_user_id, "etienne", "etienne");
        assertEquals(response3.getStatusCode(), HttpStatus.OK);
        ResponseEntity<Stack> response4 = UtilsEndToEnd.getAStack(private_stack_saved.getId(), existing_user_id, "etienne", "etienne");
        assertEquals(HttpStatus.NOT_FOUND, response4.getStatusCode());

        //getting getAllStack with owner
        ResponseEntity<List<Stack>> response5 = UtilsEndToEnd.getAllStack(existing_user_id, username, password);
        int nb_stack_from_owner = response5.getBody().size();

        //getting getAllStack now, only one should be returned
        ResponseEntity<List<Stack>> response6 = UtilsEndToEnd.getAllStack(existing_user_id, "etienne", "etienne");
        assert (nb_stack_from_owner > response6.getBody().size());

    }

    @Test
    public void thatStacksCannotBeAddedAndQueriedWithBadAuthentication() {

        String userid = UUID.randomUUID().toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                standardStackJSON(),
                getHeaders("dzd" + ":" + "BADPASSWORD"));

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<Stack> entity = template.postForEntity(
                    testString.getStacks_fullPath(userid),
                    requestEntity, Stack.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    @Test
    public void thatStacksCannotBeAddedAndQueriedAnonymously() {

        String userid = existing_user_id.toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                standardStackJSON());

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<Stack> entity = template.postForEntity(
                    testString.getStacks_fullPath(userid),
                    requestEntity, Stack.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    @Test
    public void thatStacksCannotBeDeleted_WithoutExistingUser() {

        // Random UUID passed as user, --> not existing in db
        String userid = UUID.randomUUID().toString();

        // Add a stack to an user
        // existing_user_id
        ResponseEntity<Stack> stack_entity = UtilsEndToEnd.postAStack(existing_user_id);
        Stack stack_to_delete = stack_entity.getBody();
        assertEquals(HttpStatus.CREATED, stack_entity.getStatusCode());

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                standardStackJSON(),
                getHeaders("dzd" + ":" + "dzd"));

        RestTemplate template = new RestTemplate();
        try {
            template.delete(testString.getStacks_fullPath(userid, stack_to_delete.getId().toString()));

            fail("Delete completed with unexisting user");
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    @Test
    public void thatStacksCannotBeDeleted_IfNotOwner() {

        String username = "etienne";
        String password = "etienne";
        String auth = username + ":" + password;

        String userid = existing_user_id.toString();

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

        // Add a stack to an user
        // existing_user_id
        ResponseEntity<Stack> stack_entity = UtilsEndToEnd.postAStack(existing_user_id);
        Stack stack_to_delete = stack_entity.getBody();
        assertEquals(HttpStatus.CREATED, stack_entity.getStatusCode());

        RestTemplate template = new RestTemplate();

        try {
            template.exchange(testString.getStacks_fullPath(userid, stack_to_delete.getId().toString()), HttpMethod.DELETE, requestEntity, Stack.class);

            fail("Delete completed with authenticated user not owner");
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
        }
    }

    @Test
    public void thatStacksCanBeUpdated() {

        Stack stack_to_be_updated = UtilsEndToEnd.postAStack_Stack(existing_user_id);

        String name_before = stack_to_be_updated.getName();
        boolean isPrivate_before = stack_to_be_updated.getIsPrivate();

        stack_to_be_updated.setName(name_before + "updated");
        stack_to_be_updated.setIsPrivate(!isPrivate_before);

        LOG.info("Before: " + stack_to_be_updated);

        ResponseEntity<Stack> result_stack = UtilsEndToEnd.updateAStack(existing_user_id, stack_to_be_updated.toDetails());

        LOG.info("result: " + result_stack.getBody());
        assertEquals(HttpStatus.OK, result_stack.getStatusCode());
        assertFalse(name_before.equals(result_stack.getBody().getName()));
        assert (isPrivate_before == !result_stack.getBody().getIsPrivate());

    }

    @Test
    public void thatStacksCannotBeUpdated_IfNotOwner() {

        Stack stack_to_be_updated = UtilsEndToEnd.postAStack_Stack(existing_user_id);

        String name_before = stack_to_be_updated.getName();

        stack_to_be_updated.setName(name_before + "updated");
        ResponseEntity<Stack> result_stack = UtilsEndToEnd.updateAStack(existing_user_id, stack_to_be_updated.toDetails(), "etienne", "etienne");

        assertEquals(HttpStatus.NOT_MODIFIED, result_stack.getStatusCode());

    }

    // ///////////// END OF TESTS //////////////////////

    static HttpHeaders getHeaders(String auth) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        byte[] encodedAuthorisation = Base64.encode(auth.getBytes());
        headers.add("Authorization", "Basic " + new String(encodedAuthorisation));

        return headers;
    }
}
