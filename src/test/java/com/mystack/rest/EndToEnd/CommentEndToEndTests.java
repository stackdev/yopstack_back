package com.mystack.rest.EndToEnd;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotSame;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.domain.CommentDetails;
import com.mystack.rest.domain.Comment;
import com.mystack.rest.fixture.RestCommentFixtures;
import com.mystack.tool.UtilsEndToEnd;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentEndToEndTests {
    private static Logger LOG      = LoggerFactory.getLogger(CommentEndToEndTests.class);
    private UUID          stack_to_be_commented;
    private UUID          check_to_be_commented;

    @Autowired
    TestString            testString;

    String                username = "dzd";
    String                password = "dzd";
    UUID                  existing_user_id;
    String                auth     = username + ":" + password;

    @Before
    public void setup() {
        // init the testString class for UtilsEndToEnd as it is a static class
        UtilsEndToEnd.setTestString(testString);

        existing_user_id = UtilsEndToEnd.getAUserByName_id(username);
        stack_to_be_commented = UtilsEndToEnd.postAStack_id(existing_user_id);
        check_to_be_commented = UtilsEndToEnd.postACheck_UUID(stack_to_be_commented);
    }

    @Test
    public void addComment() {

        CommentDetails input_comment_details = RestCommentFixtures.customCommentDetails(existing_user_id, stack_to_be_commented, check_to_be_commented, existing_user_id);

        LOG.info("Input Comment Details " + input_comment_details);
        String json_Comment = RestCommentFixtures.CommentJSONFromDetails(input_comment_details);
        LOG.info("Adding Comment with data: " + json_Comment);

        ResponseEntity<Comment> entity = UtilsEndToEnd.postAComment(input_comment_details);

        String path = entity.getHeaders().getLocation().getPath();
        LOG.info("Path: " + path);
        assertTrue(path.contains(testString.getComments_path(existing_user_id.toString(), stack_to_be_commented.toString(), check_to_be_commented.toString())));
        Comment result_Comment = entity.getBody();

        LOG.info("Result Comment looks like: " + result_Comment.toString());
        LOG.info("The Location is " + entity.getHeaders().getLocation());
        assertEquals(input_comment_details.getStackid(), result_Comment.getStackid());
        assertNotSame(input_comment_details.getId(), result_Comment.getId());
        assertNotSame(input_comment_details.getTimestamp(), result_Comment.getTimestamp());

    }

    @Test
    public void addAndQueryComment() {
        // POST a new Comment first
        CommentDetails input_comment = RestCommentFixtures.customCommentDetails(existing_user_id, stack_to_be_commented, check_to_be_commented, existing_user_id);

        ResponseEntity<Comment> entity = UtilsEndToEnd.postAComment(input_comment);
        Comment result_Comment1 = entity.getBody();
        // POST end

        // GET the stack
        entity = UtilsEndToEnd.getAComment(result_Comment1.getStack_userid(), result_Comment1.getStackid(), result_Comment1.getCheckid(), result_Comment1.getId());

        Comment result_Comment2 = entity.getBody();
        LOG.info("Result Comment2 looks like: " + result_Comment2.toString());

        assertEquals(result_Comment1.getId(), result_Comment2.getId());
        assertEquals(input_comment.getStackid(), result_Comment2.getStackid());
        assertNotSame(input_comment.getTimestamp(), result_Comment2.getTimestamp());

    }

    @Test
    public void addThreeCommentAndQueryOne() throws InterruptedException {
        // we post 2 and request one
        int count_requested = 1;

        // POST a new Comment first
        CommentDetails input_Comment1 = RestCommentFixtures.customCommentDetails(existing_user_id, stack_to_be_commented, check_to_be_commented, existing_user_id);
        Thread.sleep(2);
        CommentDetails input_Comment2 = RestCommentFixtures.customCommentDetails(existing_user_id, stack_to_be_commented, check_to_be_commented, existing_user_id);
        Thread.sleep(2);
        CommentDetails input_Comment3 = RestCommentFixtures.customCommentDetails(existing_user_id, stack_to_be_commented, check_to_be_commented, existing_user_id);

        ResponseEntity<Comment> entity1 = UtilsEndToEnd.postAComment(input_Comment1);
        ResponseEntity<Comment> entity2 = UtilsEndToEnd.postAComment(input_Comment2);
        ResponseEntity<Comment> entity3 = UtilsEndToEnd.postAComment(input_Comment3);
        Comment result_post2 = entity2.getBody();
        Comment result_post3 = entity3.getBody();
        // POST end

        // GET the stack
        ResponseEntity<List<Comment>> getall_entity = UtilsEndToEnd.getAllComment(existing_user_id, stack_to_be_commented, check_to_be_commented, null, count_requested);

        List<Comment> result_getall = getall_entity.getBody();

        assertEquals(result_getall.size(), count_requested);

        // Get all Comments between Comment2 and 3 date
        Date date_Comment_2 = new Date(result_post2.getTimestamp());
        Date date_Comment_3 = new Date(result_post3.getTimestamp());

        Calendar cal = Calendar.getInstance();
        cal.setTime(date_Comment_2);
        cal.add(Calendar.MILLISECOND, -1);
        Date after = cal.getTime();

        cal.add(Calendar.MILLISECOND, 2);
        Date before = cal.getTime();

        ResponseEntity<List<Comment>> getall_between_entity = UtilsEndToEnd.getAllComment(existing_user_id, stack_to_be_commented, check_to_be_commented, before, after, 0);

        assertEquals(1, getall_between_entity.getBody().size());
    }

    //
    //    @Test
    //    public void Comment_cannot_be_added_ifnotowner() {
    //
    //        try {
    //            // authent user != owner
    //            String username = "etienne";
    //            String password = "etienne";
    //
    //            // POST a new Comment first
    //            CommentDetails input_Comment = standardCommentDetails();
    //            input_Comment.setStack(stack_to_be_Commented);
    //            ResponseEntity<Comment> entity = UtilsEndToEnd.postAComment_(stack_to_be_Commented, input_Comment.getComment(), input_Comment.getNew_comment(), username, password);
    //
    //            fail("Request Passed incorrectly with status " + entity.getStatusCode());
    //        } catch (HttpClientErrorException ex) {
    //            assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
    //        }
    //    }
    //
    //    // TODO add deletion test
    //
    //    @Test
    //    public void CommentSecurityComment() {
    //
    //        String Commentid = UUID.randomUUID().toString();
    //        try {
    //            ResponseEntity<Comment> entity = UtilsEndToEnd.postAComment_(stack_to_be_Commented, "", new ArrayList<CommentDetails>(), "bob", "BAD_PASSWORD");
    //
    //            fail("Request Passed incorrectly with status " + entity.getStatusCode());
    //        } catch (HttpClientErrorException ex) {
    //            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
    //        }
    //    }
    //
    //    @Test
    //    public void addAndSearchCommentByStack() {
    //        // POST a new Comment first
    //        String my_Comment_comment = "special comment: " + UUID.randomUUID().toString();
    //        ResponseEntity<Comment> entity = UtilsEndToEnd.postAComment_(stack_to_be_Commented, my_Comment_comment);
    //        String path = entity.getHeaders().getLocation().getPath();
    //
    //        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
    //        assertTrue(path.startsWith(testString.getComments_path()));
    //        Comment result_Comment = entity.getBody();
    //
    //        // POST end
    //
    //        // Search the Comment
    //        HttpEntity<String> requestEntity = new HttpEntity<String>(
    //                getHeaders("dzd" + ":" + "dzd"));
    //        RestTemplate template = new RestTemplate();
    //        HashMap<String, String> params = new HashMap<>();
    //        params.put("stack", stack_to_be_Commented.toString());
    //        ResponseEntity<List<Comment>> entity2 = template.exchange(testString.getComments_fullPath() + testString.getParam(params),
    //                HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Comment>>() {
    //                });
    //        List<Comment> result_list = entity2.getBody();
    //
    //        // FIXME: this is crappy, get the last added Comment...
    //        Comment result_Comment2 = result_list.get(result_list.size() - 1);
    //        LOG.info("Result Comment2 looks like: " + result_Comment2.toString());
    //
    //        assertEquals(result_Comment2.getStack(), stack_to_be_Commented);
    //        assertEquals(result_Comment2.getComment(), my_Comment_comment);
    //    }
    //
    //    @Test
    //    public void addAndSearchCommentByStack_StackDoesNotExist() {
    //        try {
    //            UUID random_stack_id = UUID.randomUUID();
    //            ResponseEntity<Comment> entity = UtilsEndToEnd.postAComment_(random_stack_id);
    //
    //            fail("Request Passed incorrectly with status " + entity.getStatusCode());
    //        } catch (HttpClientErrorException ex) {
    //            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
    //        }
    //    }
    //
    //    @Test
    //    public void addAndSearchCommentByStack_StackHasNoComment() {
    //
    //        // POST a new Comment first
    //        UUID stack_no_Comments_id = UtilsEndToEnd.postAStack_id(existing_user_id);
    //        // POST end
    //        try {
    //
    //            // Search the Comment
    //            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
    //            HttpEntity<String> requestEntity = new HttpEntity<String>(
    //                    UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
    //
    //            RestTemplate template = new RestTemplate();
    //            HashMap<String, String> params = new HashMap<>();
    //            params.put("stack", stack_no_Comments_id.toString());
    //            ResponseEntity<List<Comment>> entity2 = template.exchange(testString.getComments_fullPath() + testString.getParam(params),
    //                    HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Comment>>() {
    //                    });
    //            fail("Test failed with satus code: " + entity2.getStatusCode());
    //        } catch (HttpClientErrorException e) {
    //            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
    //        }
    //    }
    //
    @Test
    public void updateComment() {

        CommentDetails details = RestCommentFixtures.customCommentDetails(existing_user_id, stack_to_be_commented, check_to_be_commented, existing_user_id);
        ResponseEntity<Comment> creation_entity = UtilsEndToEnd.postAComment(details);
        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());

        CommentDetails comment_created = creation_entity.getBody().toDetails();
        CommentDetails comment_for_update = new CommentDetails(comment_created);
        Date update_date = new Date();
        Date creation_date = comment_for_update.getTimestamp();

        comment_for_update.setText("some new text");
        comment_for_update.setTimestamp(update_date);

        assertTrue(update_date.after(creation_date));

        LOG.info("initial comment: " + details);
        LOG.info("Comment for update: " + comment_for_update);

        // Update Comment
        ResponseEntity<Comment> update_entity = UtilsEndToEnd.updateAComment(comment_for_update);

        assertEquals(HttpStatus.OK, update_entity.getStatusCode());
        Comment updated_comment = update_entity.getBody();

        LOG.info("Updated comment : " + updated_comment);

        LOG.info("... " + creation_date + " == " + updated_comment.getTimestamp() + "...");
        assertFalse(comment_created.getText().equals(updated_comment.getText()));
        assertTrue(creation_date.equals(new Date(updated_comment.getTimestamp())));
        assertTrue(creation_date.before(new Date(updated_comment.getUpdate_timestamp())));

    }
    //
    //    @Test
    //    public void updateComment() {
    //        int nb_comment_added = 0;
    //
    //        ResponseEntity<Comment> creation_entity = UtilsEndToEnd.postAComment_(stack_to_be_Commented);
    //        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());
    //
    //        CommentDetails Comment_created = creation_entity.getBody().toDetails();
    //
    //        Date new_date = new Date();
    //        String initial_comment = Comment_created.getComment();
    //        String new_comment = initial_comment + new_date.toString();
    //
    //        CommentDetails Comment_for_update = RestCommentFixtures.customCommentDetails(Comment_created.getId(), new_date, stack_to_be_Commented, new_comment, nb_comment_added);
    //        //int nb_comment_initial = Comment_created.getNew_comment().size();
    //        // Update Comment
    //        ResponseEntity<Comment> update_entity = UtilsEndToEnd.updateAComment(Comment_for_update);
    //
    //        assertEquals(update_entity.getStatusCode(), HttpStatus.OK);
    //        Comment updated_Comment = update_entity.getBody();
    //
    //        assertEquals(updated_Comment.getComment(), new_comment);
    //
    //    }
    //
    //    @Test
    //    public void updateComment_AddComment() {
    //        int nb_comment_added = 1;
    //
    //        ResponseEntity<Comment> creation_entity = UtilsEndToEnd.postAComment_(stack_to_be_Commented);
    //        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());
    //
    //        CommentDetails Comment_created = creation_entity.getBody().toDetails();
    //        int initial_nb_new_comment = Comment_created.getNew_comment().size();
    //
    //        Date new_date = new Date();
    //        String initial_comment = Comment_created.getComment();
    //        String new_comment = initial_comment + new_date.toString();
    //
    //        CommentDetails Comment_for_update = RestCommentFixtures.customCommentDetails(Comment_created.getId(), new_date, stack_to_be_Commented, new_comment, nb_comment_added);
    //        //int nb_comment_initial = Comment_created.getNew_comment().size();
    //        // Update Comment
    //        ResponseEntity<Comment> update_entity = UtilsEndToEnd.updateAComment(Comment_for_update);
    //
    //        assertEquals(update_entity.getStatusCode(), HttpStatus.OK);
    //        Comment updated_Comment = update_entity.getBody();
    //
    //        assertEquals(initial_nb_new_comment + nb_comment_added, updated_Comment.getNew_comment().length);
    //
    //    }
    //
    //    @Test
    //    public void updateComment_AddComment_NotExistingComment() {
    //        int nb_comment_added = 1;
    //
    //        CommentDetails non_existing_Comment = RestCommentFixtures.getCommentDetails(new Date());
    //        non_existing_Comment.setId(null);
    //        int initial_nb_new_comment = non_existing_Comment.getNew_comment().size();
    //
    //        Date new_date = new Date();
    //        String initial_comment = non_existing_Comment.getComment();
    //        String new_comment = initial_comment + new_date.toString();
    //
    //        CommentDetails Comment_for_update = RestCommentFixtures.customCommentDetails(non_existing_Comment.getId(), new_date, stack_to_be_Commented, new_comment, nb_comment_added);
    //        //int nb_comment_initial = Comment_created.getNew_comment().size();
    //
    //        try {
    //            // Update Comment
    //            ResponseEntity<Comment> update_entity = UtilsEndToEnd.updateAComment(Comment_for_update);
    //
    //            assertEquals(update_entity.getStatusCode(), HttpStatus.NOT_FOUND);
    //            Comment updated_Comment = update_entity.getBody();
    //
    //            assertEquals(initial_nb_new_comment + nb_comment_added, updated_Comment.getNew_comment().length);
    //            fail("Request Passed incorrectly with status " + update_entity.getStatusCode());
    //        } catch (HttpClientErrorException ex) {
    //            assertEquals(HttpStatus.METHOD_NOT_ALLOWED, ex.getStatusCode());
    //        }
    //    }
    //
    //    @Test
    //    public void updateComment_NotStackOwner() {
    //        int nb_comment_added = 0;
    //
    //        ResponseEntity<Comment> creation_entity = UtilsEndToEnd.postAComment_(stack_to_be_Commented);
    //        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());
    //
    //        CommentDetails Comment_created = creation_entity.getBody().toDetails();
    //
    //        Date new_date = new Date();
    //        String initial_comment = Comment_created.getComment();
    //        String new_comment = initial_comment + new_date.toString();
    //
    //        CommentDetails Comment_for_update = RestCommentFixtures.customCommentDetails(Comment_created.getId(), new_date, stack_to_be_Commented, new_comment, nb_comment_added);
    //        //int nb_comment_initial = Comment_created.getNew_comment().size();
    //        // Update Comment
    //        ResponseEntity<Comment> update_entity = UtilsEndToEnd.updateAComment(Comment_for_update, "etienne", "etienne");
    //
    //        assertEquals(HttpStatus.NOT_MODIFIED, update_entity.getStatusCode());
    //
    //    }
    //
    //    // END OF TEST
    //    // /////////////////////
    //
    //    /**
    //     * 
    //     * @param auth
    //     * @return
    //     */
    //    static HttpHeaders getHeaders(String auth) {
    //        HttpHeaders headers = new HttpHeaders();
    //        headers.setContentType(MediaType.APPLICATION_JSON);
    //        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    //
    //        byte[] encodedAuthorisation = Base64.encode(auth.getBytes());
    //        headers.add("Authorization", "Basic " + new String(encodedAuthorisation));
    //
    //        return headers;
    //    }
}
