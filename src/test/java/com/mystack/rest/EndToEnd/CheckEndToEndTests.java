package com.mystack.rest.EndToEnd;

import static com.mystack.rest.fixture.RestCheckDataFixtures.checkJSONFromDetails;
import static com.mystack.rest.fixture.RestCheckDataFixtures.standardCheckDetails;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotSame;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.domain.CheckDetails;
import com.mystack.rest.domain.Check;
import com.mystack.rest.fixture.RestCheckDataFixtures;
import com.mystack.tool.UtilsEndToEnd;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class CheckEndToEndTests {
    private static Logger LOG      = LoggerFactory.getLogger(CheckEndToEndTests.class);
    private UUID          stack_to_be_checked;

    @Autowired
    TestString            testString;

    String                username = "dzd";
    String                password = "dzd";
    UUID                  existing_user_id;
    String                auth     = username + ":" + password;

    @Before
    public void setup() {
        // init the testString class for UtilsEndToEnd as it is a static class
        UtilsEndToEnd.setTestString(testString);

        existing_user_id = UtilsEndToEnd.getAUserByName_id(username);
        stack_to_be_checked = UtilsEndToEnd.postAStack(existing_user_id, username, password).getBody().getId();
    }

    @Test
    public void addCheck() {

        CheckDetails input_check_details = standardCheckDetails();
        input_check_details.setStack(stack_to_be_checked);
        LOG.info("Input Check Details " + input_check_details);
        String json_check = checkJSONFromDetails(input_check_details);
        LOG.info("Adding check with data: " + json_check);
        LOG.info("Using url: " + testString.getChecks_fullPath());

        ResponseEntity<Check> entity = UtilsEndToEnd.postACheck_(input_check_details.getStack(), input_check_details.getComment());

        String path = entity.getHeaders().getLocation().getPath();
        assertTrue(path.contains(testString.getChecks_path()));
        Check result_check = entity.getBody();

        LOG.info("Result check looks like: " + result_check.toString());
        LOG.info("The Location is " + entity.getHeaders().getLocation());
        assertEquals(input_check_details.getComment(), result_check.getComment());
        assertEquals(input_check_details.getStack(), result_check.getStack());
        assertNotSame(input_check_details.getId(), result_check.getId());
        assertNotSame(input_check_details.getDate(), result_check.getDate());

    }

    @Test
    public void addAndQueryCheck() {
        // POST a new check first
        CheckDetails input_check = standardCheckDetails();
        input_check.setStack(stack_to_be_checked);
        ResponseEntity<Check> entity = UtilsEndToEnd.postACheck_(stack_to_be_checked, input_check.getComment());
        Check result_check1 = entity.getBody();
        // POST end

        // GET the stack
        entity = UtilsEndToEnd.getACheck(result_check1.getId());

        Check result_check2 = entity.getBody();
        LOG.info("Result check2 looks like: " + result_check2.toString());

        assertEquals(result_check1.getId(), result_check2.getId());
        assertEquals(input_check.getComment(), result_check2.getComment());
        assertEquals(input_check.getStack(), result_check2.getStack());
        assertNotSame(input_check.getId(), result_check2.getId());
        assertNotSame(input_check.getDate(), result_check2.getDate());

    }

    @Test
    public void addThreeCheckAndQueryOne() {
        // we post 2 and request one
        int count_requested = 1;

        // POST a new check first
        CheckDetails input_check1 = standardCheckDetails();
        input_check1.setComment("1--" + input_check1.getComment());
        CheckDetails input_check2 = standardCheckDetails();
        input_check2.setComment("2--" + input_check2.getComment());
        CheckDetails input_check3 = standardCheckDetails();
        input_check3.setComment("3--" + input_check3.getComment());

        input_check1.setStack(stack_to_be_checked);
        input_check2.setStack(stack_to_be_checked);
        ResponseEntity<Check> entity1 = UtilsEndToEnd.postACheck_(stack_to_be_checked, input_check1.getComment());
        ResponseEntity<Check> entity2 = UtilsEndToEnd.postACheck_(stack_to_be_checked, input_check2.getComment());
        ResponseEntity<Check> entity3 = UtilsEndToEnd.postACheck_(stack_to_be_checked, input_check3.getComment());
        Check result_post2 = entity2.getBody();
        Check result_post3 = entity3.getBody();
        // POST end

        // GET the stack
        ResponseEntity<List<Check>> getall_entity = UtilsEndToEnd.getAllCheck(stack_to_be_checked, null, count_requested);

        List<Check> result_getall = getall_entity.getBody();

        assertEquals(result_getall.size(), count_requested);

        // Get all checks between check2 and 3 date
        Date date_check_2 = result_post2.getDate();
        Date date_check_3 = result_post3.getDate();

        Calendar cal = Calendar.getInstance();
        cal.setTime(date_check_2);
        cal.add(Calendar.MILLISECOND, -1);
        Date after = cal.getTime();

        cal.add(Calendar.MILLISECOND, 2);
        Date before = cal.getTime();

        LOG.info("TMP: before: " + before + ", after: " + after);

        ResponseEntity<List<Check>> getall_between_entity = UtilsEndToEnd.getAllCheck(stack_to_be_checked, before, after, 0);

        assertEquals(1, getall_between_entity.getBody().size());

        // rerun the get now specifying the count and with a before = now
        getall_between_entity = UtilsEndToEnd.getAllCheck(stack_to_be_checked, new Date(), null, 1);

        assertEquals(1, getall_between_entity.getBody().size());
        assertEquals(input_check3.getComment(), getall_between_entity.getBody().get(0).getComment());
    }

    @Test
    public void check_cannot_be_added_ifnotowner() {

        try {
            // authent user != owner
            String username = "etienne";
            String password = "etienne";

            // POST a new check first
            CheckDetails input_check = standardCheckDetails();
            input_check.setStack(stack_to_be_checked);
            ResponseEntity<Check> entity = UtilsEndToEnd.postACheck_(stack_to_be_checked, input_check.getComment(), username, password);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
        }
    }

    // TODO add deletion test

    @Test
    public void checkSecurityCheck() {

        String checkid = UUID.randomUUID().toString();
        try {
            ResponseEntity<Check> entity = UtilsEndToEnd.postACheck_(stack_to_be_checked, "", "bob", "BAD_PASSWORD");

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    @Test
    public void addAndSearchCheckByStack() {
        // POST a new check first
        String my_check_comment = "special comment: " + UUID.randomUUID().toString();
        ResponseEntity<Check> entity = UtilsEndToEnd.postACheck_(stack_to_be_checked, my_check_comment);
        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getChecks_path()));
        Check result_check = entity.getBody();

        // POST end

        // Search the check
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeaders("dzd" + ":" + "dzd"));
        RestTemplate template = new RestTemplate();
        HashMap<String, String> params = new HashMap<>();
        params.put("stack", stack_to_be_checked.toString());
        ResponseEntity<List<Check>> entity2 = template.exchange(testString.getChecks_fullPath() + testString.getParam(params),
                HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Check>>() {
                });
        List<Check> result_list = entity2.getBody();

        // FIXME: this is crappy, get the last added check...
        Check result_check2 = result_list.get(result_list.size() - 1);
        LOG.info("Result check2 looks like: " + result_check2.toString());

        assertEquals(result_check2.getStack(), stack_to_be_checked);
        assertEquals(result_check2.getComment(), my_check_comment);
    }

    @Test
    public void addAndSearchCheckByStack_StackDoesNotExist() {
        try {
            UUID random_stack_id = UUID.randomUUID();
            ResponseEntity<Check> entity = UtilsEndToEnd.postACheck_(random_stack_id);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void addAndSearchCheckByStack_StackHasNoCheck() {

        // POST a new check first
        UUID stack_no_checks_id = UtilsEndToEnd.postAStack_id(existing_user_id);
        // POST end
        try {

            // Search the check
            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

            RestTemplate template = new RestTemplate();
            HashMap<String, String> params = new HashMap<>();
            params.put("stack", stack_no_checks_id.toString());
            ResponseEntity<List<Check>> entity2 = template.exchange(testString.getChecks_fullPath() + testString.getParam(params),
                    HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Check>>() {
                    });
            fail("Test failed with satus code: " + entity2.getStatusCode());
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }
    }

    @Test
    public void updateCheck() {

        ResponseEntity<Check> creation_entity = UtilsEndToEnd.postACheck_(stack_to_be_checked);
        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());

        CheckDetails check_created = creation_entity.getBody().toDetails();

        Date new_date = new Date();
        String initial_comment = check_created.getComment();
        String new_text = initial_comment + new_date.toString();

        CheckDetails check_for_update = RestCheckDataFixtures.customCheckDetails(check_created.getId(), new_date, stack_to_be_checked, new_text);
        //int nb_comment_initial = check_created.getNew_comment().size();
        // Update check
        ResponseEntity<Check> update_entity = UtilsEndToEnd.updateACheck(check_for_update);

        assertEquals(update_entity.getStatusCode(), HttpStatus.OK);
        Check updated_check = update_entity.getBody();

        assertEquals(updated_check.getComment(), new_text);

    }

    @Test
    public void updateCheck_NotStackOwner() {

        ResponseEntity<Check> creation_entity = UtilsEndToEnd.postACheck_(stack_to_be_checked);
        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());

        CheckDetails check_created = creation_entity.getBody().toDetails();

        Date new_date = new Date();
        String initial_text = check_created.getComment();
        String new_text = initial_text + new_date.toString();

        CheckDetails check_for_update = RestCheckDataFixtures.customCheckDetails(check_created.getId(), new_date, stack_to_be_checked, new_text);
        //int nb_comment_initial = check_created.getNew_comment().size();
        // Update check
        ResponseEntity<Check> update_entity = UtilsEndToEnd.updateACheck(check_for_update, "etienne", "etienne");

        assertEquals(HttpStatus.NOT_MODIFIED, update_entity.getStatusCode());

    }

    // END OF TEST
    // /////////////////////

    /**
     * 
     * @param auth
     * @return
     */
    static HttpHeaders getHeaders(String auth) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        byte[] encodedAuthorisation = Base64.encode(auth.getBytes());
        headers.add("Authorization", "Basic " + new String(encodedAuthorisation));

        return headers;
    }
}