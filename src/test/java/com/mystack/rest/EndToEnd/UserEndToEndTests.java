package com.mystack.rest.EndToEnd;

import static com.mystack.rest.fixture.RestUserDataFixtures.standardUserDetails;
import static com.mystack.rest.fixture.RestUserDataFixtures.userJSONFromDetails;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotSame;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.config.WebAppInitializer;
import com.mystack.core.domain.UserDetails;
import com.mystack.rest.domain.User;
import com.mystack.tool.UtilsEndToEnd;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestStringConfig.class })
public class UserEndToEndTests {
    private static Logger LOG      = LoggerFactory.getLogger(WebAppInitializer.class);

    @Autowired
    TestString            testString;

    String                username = "dzd";
    String                password = "dzd";
    String                auth     = username + ":" + password;

    @Before
    public void setUp() {
        // init the testString class for UtilsEndToEnd as it is a static class
        UtilsEndToEnd.setTestString(testString);
    }

    @Test
    public void addUser() {

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);

        UserDetails input_user = standardUserDetails();

        LOG.info("Input UserDetails " + input_user);
        String json_user = userJSONFromDetails(input_user);
        LOG.info("Adding user with data: " + json_user);

        LOG.info("user path: " + testString.getUsers_fullPath());

        //We are not using UtisEndToEnd.PostAUser here, to have a distinct source of test for the creation
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                json_user,
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        LOG.info("POST headers: " + requestEntity);
        RestTemplate template = new RestTemplate();
        ResponseEntity<User> entity = template.postForEntity(
                testString.getUsers_fullPath(),
                requestEntity, User.class);

        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getUsers_path()));
        User result_user = entity.getBody();

        LOG.info("Result user looks like: " + result_user.toString());
        LOG.info("The Location is " + entity.getHeaders().getLocation());
        assertEquals(input_user.getName(), result_user.getName());
        assertTrue(!input_user.getId().toString().equals(result_user.getId().toString()));
        assertEquals(0, result_user.getList_friend().length);
        assertEquals(0, result_user.getList_stack().length);

    }

    @Test
    public void addAndQuerieUser() {
        Map<String, String> csrfAndSessionValues = UtilsEndToEnd.getCsrfTokenAndSessionFromSetCookies();
        
        String new_user_name = "name_" + UUID.randomUUID();
        String new_user_password = "password_" + UUID.randomUUID();

        // POST a new user first
        UserDetails input_user_details = standardUserDetails();
        input_user_details.setName(new_user_name);
        input_user_details.setSetpw(new_user_password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                userJSONFromDetails(input_user_details),
                UtilsEndToEnd.getHeadersLoggedIn(null, csrfAndSessionValues));

        RestTemplate template = new RestTemplate();
        ResponseEntity<User> entity = template.postForEntity(
                testString.getUsers_fullPath(),
                requestEntity, User.class);

        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getUsers_path()));
        User result_user1 = entity.getBody();

        // POST end

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(new_user_name, new_user_password);
        // GET the user
        String userid = result_user1.getId().toString();
        requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        template = new RestTemplate();
        entity = template.exchange(testString.getUsers_fullPath(userid),
                HttpMethod.GET, requestEntity, User.class);
        User result_user2 = entity.getBody();
        LOG.info("Result user2 looks like: " + result_user2.toString());

        assertEquals(result_user2.getId(), result_user1.getId());

        assertEquals(result_user2.getName(), input_user_details.getName());
        // User needs to be transform in details for comparison
        // both list of stack and friend are invalid
        assertEquals(result_user2.toDetails().getList_friend().size(), 0);
        assertEquals(result_user2.toDetails().getList_stack().size(), 0);

    }

    @Test
    public void addAndSearchUserByName() {
        String new_user_name = "user_addAndSearchUserByName" + UUID.randomUUID().toString();
        String new_user_password = "password_" + UUID.randomUUID().toString();

        // POST a new user first
        ResponseEntity<User> entity = UtilsEndToEnd.postAUser_(new_user_name, new_user_password);
        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getUsers_path()));
        User result_user1 = entity.getBody();

        // POST end

        // Search the user
        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        RestTemplate template = new RestTemplate();
        HashMap<String, String> params = new HashMap<>();
        params.put("name", new_user_name);
        ResponseEntity<List<User>> entity2 = template.exchange(testString.getUsers_fullPath() + testString.getParam(params),
                HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<User>>() {
                });
        List<User> result_list = entity2.getBody();

        //FIXME: this is crappy, get the last added user...
        User result_user2 = result_list.get(result_list.size() - 1);
        LOG.info("Result user2 looks like: " + result_user2.toString());

        assertEquals(result_user2.getName(), new_user_name);
        // User needs to be transform in details for comparison
        // both list of stack and friend are invalid
        assertEquals(result_user2.toDetails().getList_friend().size(), 0);
        assertEquals(result_user2.toDetails().getList_stack().size(), 0);

    }

    @Test
    public void addAndSearchUserByName_NotExactMatch() {
        String new_user_name = "user_addAndSearchUserByName" + UUID.randomUUID().toString();
        String new_user_password = "password_" + UUID.randomUUID().toString();

        // POST a new user first
        ResponseEntity<User> entity = UtilsEndToEnd.postAUser_(new_user_name, new_user_password);
        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getUsers_path()));
        User result_user1 = entity.getBody();

        // POST end

        // Search the user
        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        RestTemplate template = new RestTemplate();
        HashMap<String, String> params = new HashMap<>();
        String search_name_shorter = new_user_name.substring(0, new_user_name.length() - 2);

        params.put("name", search_name_shorter);
        params.put("exact", "false");

        ResponseEntity<List<User>> entity2 = template.exchange(testString.getUsers_fullPath() + testString.getParam(params),
                HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<User>>() {
                });
        List<User> result_list = entity2.getBody();

        User result_user2 = result_list.get(result_list.size() - 1);
        LOG.info("Result user2 looks like: " + result_user2.toString());

        assertEquals(result_user2.getName(), new_user_name);
        // User needs to be transform in details for comparison
        // both list of stack and friend are invalid
        assertEquals(result_user2.toDetails().getList_friend().size(), 0);
        assertEquals(result_user2.toDetails().getList_stack().size(), 0);

    }

    @Test
    public void updateUserNameIsBlocked() {

        String random_str = UUID.randomUUID().toString();
        String initial_name = "Bob_" + random_str, modified_name = "Bobi_modified_" + random_str;
        String user_password = "pass" + random_str;
        //create a user
        ResponseEntity<User> creation_entity = UtilsEndToEnd.postAUser_(initial_name, user_password);

        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());
        User created_user = creation_entity.getBody();

        assertEquals(initial_name, created_user.getName());

        UserDetails modified_user = created_user.toDetails();
        modified_user.setName(modified_name);

        // Update user
        ResponseEntity<User> update_entity = UtilsEndToEnd.updateAUser(modified_user, initial_name, user_password);

        assertEquals(update_entity.getStatusCode(), HttpStatus.OK);
        User updated_user = update_entity.getBody();

        assertFalse(modified_name.equals(updated_user.getName()));

    }

    @Test
    public void updateUser_password_basic() {

        String random_str = UUID.randomUUID().toString();
        String user_name = "Bob_" + random_str;
        String user_password = "pass" + random_str;
        String new_password = "new_pass" + random_str;
        //create a user
        ResponseEntity<User> creation_entity = UtilsEndToEnd.postAUser_(user_name, user_password);

        assertEquals(HttpStatus.CREATED, creation_entity.getStatusCode());
        User created_user = creation_entity.getBody();

        UserDetails modified_user = created_user.toDetails();
        modified_user.setOldpw(user_password);
        modified_user.setSetpw(new_password);

        // Update user
        ResponseEntity<User> update_entity = UtilsEndToEnd.updateAUser(modified_user, user_name, user_password);
        assertEquals(update_entity.getStatusCode(), HttpStatus.OK);

        // check that we can get the user using the new password and not with the old one
        ResponseEntity<User> get_user_oldpw = UtilsEndToEnd.getAUserById(modified_user.getId(), user_name, user_password);
        assertEquals(null, get_user_oldpw);

        // check that we can get the user using the new password and not with the old one
        ResponseEntity<User> get_user_newpw = UtilsEndToEnd.getAUserById(modified_user.getId(), user_name, new_password);
        assertNotSame(null, get_user_newpw);
        // the new password is not transmitted back to the REST domain, so just check if the user name is the good one.
        assertEquals(user_name, get_user_newpw.getBody().getName());

    }

    //TODO add deletion test

    @Test
    public void checkSecurity() {

        String userid = UUID.randomUUID().toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLogin("dzd" + ":" + "BADPASSWORD"));

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<User> entity = template.postForEntity(
                    testString.getStacks_fullPath(userid),
                    requestEntity, User.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    @Test
    public void checkThatUserCannotBeAddedAnonymously() {

        String userid = UUID.randomUUID().toString();

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<User> entity = template.postForEntity(
                    testString.getStacks_fullPath(userid),
                    null, User.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    // END OF TESTS 

}