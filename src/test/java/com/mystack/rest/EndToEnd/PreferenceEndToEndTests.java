package com.mystack.rest.EndToEnd;

import static com.mystack.rest.fixture.RestPreferenceFixtures.preferenceJSON;
import static com.mystack.rest.fixture.RestPreferenceFixtures.randomPreference;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mystack.config.FunctionalConfiguration.StackSorting;
import com.mystack.config.MongoConfiguration;
import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.rest.domain.Preference;
import com.mystack.rest.domain.User;
import com.mystack.tool.UtilsEndToEnd;

@ContextConfiguration(classes = { TestStringConfig.class, MongoConfiguration.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class PreferenceEndToEndTests {
    private static Logger LOG      = LoggerFactory.getLogger(PreferenceEndToEndTests.class);

    @Autowired
    TestString            testString;

    public UUID           existing_user_id;

    String                username = "dzd";
    String                password = "dzd";
    String                auth     = username + ":" + password;

    @Autowired
    MongoOperations       mongo;

    @Before
    public void setup() {
        // Droping all preferences between tests.
        mongo.dropCollection("preference");
        // init the testString class for UtilsEndToEnd as it is a static class
        UtilsEndToEnd.setTestString(testString);

        // user that can be used for Stack creation
        //Not creating dzd....
        try {
            existing_user_id = UtilsEndToEnd.getAUserByName_id(username);

        } catch (HttpClientErrorException e) {
            LOG.info("Not authorized... keep going");
        }
        if (null == existing_user_id) {
            existing_user_id = UtilsEndToEnd.postAUser_id(username, username);
        }

    }

    @Test
    public void thatPreferencesCanBeAdded() {
        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        Preference input_data = randomPreference();
        input_data.setStacksorting(StackSorting.TYPE.getValue());
        input_data.setUser(existing_user_id);
        input_data.setRisingsort(false);

        String userid = existing_user_id.toString();
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                preferenceJSON(input_data),
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        LOG.info("to be sent: " + requestEntity);

        RestTemplate template = new RestTemplate();
        ResponseEntity<Preference> entity = template.postForEntity(
                testString.getPreferences_fullPath(userid),
                requestEntity, Preference.class);

        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getPreferences_path(userid)));
        Preference result = entity.getBody();

        LOG.info("Result stack looks like: " + result.toString());
        LOG.info("The Location is " + entity.getHeaders().getLocation());
        assertTrue(input_data.getUser().toString().equals(result.getUser().toString()));
        assertEquals(input_data.getStacksorting(), result.getStacksorting());
        assertEquals(input_data.getRisingsort(), result.getRisingsort());

    }

    @Test
    public void thatPreferencesCanBeAddedAndQueried() {

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        Preference input_data = randomPreference();
        input_data.setStacksorting(StackSorting.LASTCHECK.getValue());
        input_data.setUser(existing_user_id);

        String userid = existing_user_id.toString();
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                preferenceJSON(input_data),
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        LOG.info("to be sent: " + requestEntity);

        // POST a new preference
        RestTemplate template = new RestTemplate();
        ResponseEntity<Preference> entity = template.postForEntity(
                testString.getPreferences_fullPath(userid),
                requestEntity, Preference.class);

        String path = entity.getHeaders().getLocation().getPath();

        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertTrue(path.contains(testString.getPreferences_path(userid)));
        Preference result = entity.getBody();

        LOG.info("Result pref looks like: " + result.toString());
        LOG.info("The Location is " + entity.getHeaders().getLocation());
        assertTrue(input_data.getUser().toString().equals(result.getUser().toString()));
        assertEquals(input_data.getStacksorting(), result.getStacksorting());
        // POST END

        // GET the stack
        auth_cookie = UtilsEndToEnd.authenticate(username, password);
        requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
        template = new RestTemplate();
        entity = template.exchange(testString.getPreferences_fullPath(userid),
                HttpMethod.GET, requestEntity, Preference.class);
        Preference result2 = entity.getBody();
        LOG.info("Result looks like: " + result2.toString());

        assertTrue(input_data.getUser().toString().equals(result2.getUser().toString()));
        assertEquals(input_data.getStacksorting(), result2.getStacksorting());

    }

    @Test
    public void thatPreferencesCannotBeAdded_WithoutExistingUser() {

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        // Random UUID passed as user, --> not existing in db
        UUID userid = UUID.randomUUID();
        Preference some_pref = randomPreference();
        some_pref.setUser(userid);

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                preferenceJSON(some_pref),
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<Preference> entity = template.postForEntity(
                    testString.getPreferences_fullPath(userid.toString()),
                    requestEntity, Preference.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void thatPreferencesCannotBeViewed_WithoutExistingUser() {

        // create a stack for an existing user
        Preference existing_user_preference = UtilsEndToEnd.postAPreference(existing_user_id, username, password);

        // Random UUID passed as user, --> not existing in db
        String userid = UUID.randomUUID().toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeaders("dzd" + ":" + "dzd"));

        RestTemplate template = new RestTemplate();
        // pass generic information to rest template
        ParameterizedTypeReference<Preference> responseType = new ParameterizedTypeReference<Preference>() {
        };
        try {
            String getUrl = testString
                    .getPreferences_fullPath(userid);
            ResponseEntity<Preference> entity = template.exchange(getUrl, HttpMethod.GET, requestEntity, responseType);
            // Stack result = entity.getBody();
            // assertEquals(1, result.size());

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        } catch (Exception e) {
            // TMP debug
            e.printStackTrace();
        }
    }

    @Test
    public void thatPreferenceCannotBeViewed_WithExistingUserNotOwner_inUrl() {

        // create a stack for an existing user
        Preference existing_user_preference = UtilsEndToEnd.postAPreference(existing_user_id, username, password);

        // Random UUID passed as user, --> not existing in db
        String user_name_pass = UUID.randomUUID().toString();
        User another_existing_user = UtilsEndToEnd.postAUser_User(user_name_pass, user_name_pass);

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

        RestTemplate template = new RestTemplate();
        // pass generic information to rest template
        ParameterizedTypeReference<Preference> responseType = new ParameterizedTypeReference<Preference>() {
        };
        try {
            String getUrl = testString.getPreferences_fullPath(another_existing_user.getId().toString());
            ResponseEntity<Preference> entity = template.exchange(getUrl, HttpMethod.GET, requestEntity, responseType);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }

    @Test
    public void thatPreferencesCannotBeAddedAndQueriedWithBadAuthentication() {

        String userid = UUID.randomUUID().toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                preferenceJSON(randomPreference()),
                getHeaders("dzd" + ":" + "BADPASSWORD"));

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<Preference> entity = template.postForEntity(
                    testString.getPreferences_fullPath(userid),
                    requestEntity, Preference.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    @Test
    public void thatPreferencesCannotBeAddedAndQueriedAnonymously() {

        String userid = existing_user_id.toString();

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                preferenceJSON(randomPreference()));

        RestTemplate template = new RestTemplate();
        try {
            ResponseEntity<Preference> entity = template.postForEntity(
                    testString.getPreferences_fullPath(userid),
                    requestEntity, Preference.class);

            fail("Request Passed incorrectly with status " + entity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    @Test
    public void thatPreferencesCannotBeDeleted_WithoutExistingUser() {

        // Random UUID passed as user, --> not existing in db
        String userid = UUID.randomUUID().toString();

        // Add a stack to an user
        // existing_user_id
        Preference pref_to_delete = UtilsEndToEnd.postAPreference(existing_user_id, username, password);

        RestTemplate template = new RestTemplate();
        try {
            template.delete(testString.getPreferences_fullPath(userid, pref_to_delete.getId().toString()));

            fail("Delete completed with unexisting user");
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.UNAUTHORIZED, ex.getStatusCode());
        }
    }

    //
    //    @Test
    //    public void thatStacksCannotBeDeleted_IfNotOwner() {
    //
    //        String username = "etienne";
    //        String password = "etienne";
    //        String auth = username + ":" + password;
    //
    //        String userid = existing_user_id.toString();
    //
    //        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
    //
    //        HttpEntity<String> requestEntity = new HttpEntity<String>(
    //                UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
    //
    //        // Add a stack to an user
    //        // existing_user_id
    //        ResponseEntity<Stack> stack_entity = UtilsEndToEnd.postAStack(existing_user_id);
    //        Stack stack_to_delete = stack_entity.getBody();
    //        assertEquals(HttpStatus.CREATED, stack_entity.getStatusCode());
    //
    //        RestTemplate template = new RestTemplate();
    //
    //        try {
    //            template.exchange(testString.getStacks_fullPath(userid, stack_to_delete.getId().toString()), HttpMethod.DELETE, requestEntity, Stack.class);
    //
    //            fail("Delete completed with authenticated user not owner");
    //        } catch (HttpClientErrorException ex) {
    //            assertEquals(HttpStatus.FORBIDDEN, ex.getStatusCode());
    //        }
    //    }
    //
    @Test
    public void thatPreferencesCanBeUpdated() {

        Preference pref_to_update = UtilsEndToEnd.postAPreference(existing_user_id, username, password);

        int sorting_before = pref_to_update.getStacksorting();
        if (sorting_before == StackSorting.CREATION.getValue()) {
            pref_to_update.setStacksorting(StackSorting.LASTCHECK.getValue());
        } else {
            pref_to_update.setStacksorting(StackSorting.CREATION.getValue());
        }

        Preference result = UtilsEndToEnd.updateAPreference(existing_user_id, pref_to_update.toDetails(), username, password);

        assertFalse(sorting_before == result.getStacksorting());

    }

    @Test
    public void thatPreferenceCannotBeUpdated_IfNotOwner() {

        Preference pref_to_update = UtilsEndToEnd.postAPreference(existing_user_id, username, password);

        int sorting_before = pref_to_update.getStacksorting();
        if (sorting_before == StackSorting.CREATION.getValue()) {
            pref_to_update.setStacksorting(StackSorting.LASTCHECK.getValue());
        } else {
            pref_to_update.setStacksorting(StackSorting.CREATION.getValue());
        }

        Preference result = UtilsEndToEnd.updateAPreference(existing_user_id, pref_to_update.toDetails(), "etienne", "etienne");

        assertEquals(null, result);

    }

    // ///////////// END OF TESTS //////////////////////

    static HttpHeaders getHeaders(String auth) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        byte[] encodedAuthorisation = Base64.encode(auth.getBytes());
        headers.add("Authorization", "Basic " + new String(encodedAuthorisation));

        return headers;
    }
}