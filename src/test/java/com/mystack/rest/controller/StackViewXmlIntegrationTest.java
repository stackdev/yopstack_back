package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestStackEventFixtures.stackDetailsEvent;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.stacks.request.RequestStackDetailsFromUserEvent;
import com.mystack.core.services.StackService;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class StackViewXmlIntegrationTest {

    MockMvc                mockMvc;

    @InjectMocks
    StackQueriesController controller;

    @Mock
    StackService           service;

    @Autowired
    TestString             testString;

    UUID                   id          = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    UUID                   user        = UUID.fromString("f1425d26-1111-2222-8888-ffffffffffff");
    String                 name        = "View xml stack";
    String                 description = "Xml stack used for integration purpose";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter(),
                        new Jaxb2RootElementHttpMessageConverter()).build();
    }

    @Test
    public void thatViewStackRendersXmlCorrectly() throws Exception {

        when(service.getStackDetails(any(RequestStackDetailsFromUserEvent.class))).thenReturn(
                stackDetailsEvent(id, name, description));

        this.mockMvc.perform(
                get(testString.getStacks_path(user.toString(), id.toString()))
                        .accept(MediaType.TEXT_XML)
                        .principal(new MyPrincipal("plop")))
                .andDo(print())
                .andExpect(content().contentType(MediaType.TEXT_XML))
                .andExpect(xpath("/stack/id").string(id.toString()));
    }

    @Test
    public void thatViewStackRendersJsonCorrectly() throws Exception {

        when(service.getStackDetails(any(RequestStackDetailsFromUserEvent.class))).thenReturn(
                stackDetailsEvent(id, name, description));

        // TODOCUMENT JSON Path in use here (really like this)

        this.mockMvc.perform(
                get(testString.getStacks_path(user.toString(), id.toString()))
                        .accept(MediaType.APPLICATION_JSON)
                        .principal(new MyPrincipal("plop")))
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id.toString()));
    }
}