package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestCheckDataFixtures.customCheckDetails;
import static com.mystack.rest.fixture.RestCheckEventFixtures.CheckDetailsNotFound;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.checks.request.RequestAllChecksEvent;
import com.mystack.core.events.checks.request.RequestCheckDetailsEvent;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;
import com.mystack.core.services.CheckService;
import com.mystack.rest.fixture.RestCheckDataFixtures;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class CheckViewIntegrationTest {
    MockMvc                mockMvc;

    @InjectMocks
    CheckQueriesController controller;

    @Mock
    CheckService           service;

    @Autowired
    TestString             testString;

    UUID                   id       = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    UUID                   stack_id = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc14");

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller).setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
    }

    @Test
    public void thatViewCheckUsesHttpNotFound() throws Exception {

        when(service.getCheckDetails(any(RequestCheckDetailsEvent.class))).thenReturn(CheckDetailsNotFound(id));

        this.mockMvc.perform(get(testString.getChecks_path(id.toString()))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void thatViewUserUsesHttpOK() throws Exception {

        CheckDetailsEvent event = new CheckDetailsEvent(customCheckDetails(id, new Date(), stack_id, "Check view Check"));
        when(service.getCheckDetails(any(RequestCheckDetailsEvent.class))).thenReturn(event);

        this.mockMvc.perform(get(testString.getChecks_path(id.toString()))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.stack").value(stack_id.toString()));
    }

    @Test
    public void thatViewCheckUsesStackIdAsParameter() throws Exception {

        int nb = 2;
        UUID stack_id = UUID.randomUUID();
        List<CheckDetails> custom_list_check_details = RestCheckDataFixtures.getListCheckDetails(nb);
        AllChecksEvent event = new AllChecksEvent(custom_list_check_details);
        when(service.getAllChecks(any(RequestAllChecksEvent.class))).thenReturn(event);

        this.mockMvc.perform(get(testString.getChecks_path()).param("stack", stack_id.toString())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        verify(service).getAllChecks(
                argThat(arg -> null != arg.getStack_id()));
        //Matchers.<RequestAllChecksEvent> hasProperty("stack_id", Matchers.equalTo(stack_id))));
    }

    @Test
    public void thatViewCeckUsesStackidAsParameter_NotFound() throws Exception {
        UUID stack_id = UUID.randomUUID();
        AllChecksEvent event = AllChecksEvent.NotFound();
        when(service.getAllChecks(any(RequestAllChecksEvent.class))).thenReturn(event);

        this.mockMvc.perform(get(testString.getUsers_path()).param("stack", stack_id.toString())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}