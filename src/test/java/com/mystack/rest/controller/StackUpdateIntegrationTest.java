package com.mystack.rest.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.core.services.StackService;
import com.mystack.rest.fixture.RestStackDataFixtures;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class StackUpdateIntegrationTest {
    private static Logger   LOG    = LoggerFactory.getLogger(StackUpdateIntegrationTest.class);

    MockMvc                 mockMvc;

    @Mock
    StackService            service;

    @InjectMocks
    StackCommandsController controller;

    @Autowired
    TestString              testString;

    UUID                    userId = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    int                     g_nb_friend = 2, g_nb_stack = 3;
    StackUpdatedEvent       event;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        event = new StackUpdatedEvent(RestStackDataFixtures.standardStackDetails());
        when(service.updateStack(any(UpdateStackEvent.class))).thenReturn(event);

    }

    // updateStack - validation?

    @Test
    public void thatUpdateStackUsesHttpOK() throws Exception {
        String name = "plop";
        //UUID id doesn't matter in this context, the user service is mocked
        UUID fake_user_id = UUID.randomUUID();
        UUID fake_stack_id = UUID.randomUUID();
        String input_json = RestStackDataFixtures.customizedStackJSON(name, "plop");
        LOG.info("Input JSON:" + input_json);

        this.mockMvc.perform(
                put(testString.getStacks_path(fake_user_id.toString(), fake_stack_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .content(input_json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string("Location", Matchers.endsWith(testString.getStacks_path(fake_user_id.toString(), event.getDetails().getId().toString()))));

        verify(service, times(1)).updateStack(any(UpdateStackEvent.class));
    }

}
