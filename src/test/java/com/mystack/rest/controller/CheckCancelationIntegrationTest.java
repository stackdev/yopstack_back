package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestCheckEventFixtures.customCheckDeleted;
import static com.mystack.rest.fixture.RestCheckEventFixtures.customCheckDeletedFailed;
import static com.mystack.rest.fixture.RestCheckEventFixtures.customCheckDeletedNotFound;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.checks.request.DeleteCheckEvent;
import com.mystack.core.services.CheckService;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class CheckCancelationIntegrationTest {

    MockMvc                 mockMvc;

    @InjectMocks
    CheckCommandsController controller;

    @Mock
    CheckService            service;

    @Autowired
    TestString              testString;

    UUID                    checkId = UUID.fromString("fff65384-1247-e8c9-1112-456ef78abbbb");

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

    }

    @Test
    public void thatDeleteCheckUsesHttpOkOnSuccess() throws Exception {

        when(service.deleteCheck(any(DeleteCheckEvent.class)))
                .thenReturn(customCheckDeleted(checkId));

        this.mockMvc.perform(
                delete(testString.getChecks_path(checkId.toString()))
                        .principal(new MyPrincipal("plop"))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        verify(service).deleteCheck(argThat(event -> null != event.getId()));
    }

    @Test
    public void thatDeleteCheckUsesHttpNotFoundOnEntityLookupFailure() throws Exception {

        when(service.deleteCheck(any(DeleteCheckEvent.class))).thenReturn(customCheckDeletedNotFound(checkId));

        this.mockMvc.perform(delete(testString.getChecks_path(checkId.toString()))
                .principal(new MyPrincipal("plop"))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void thatDeleteCheckUsesHttpForbiddenOnEntityDeletionFailure() throws Exception {

        when(service.deleteCheck(any(DeleteCheckEvent.class))).thenReturn(customCheckDeletedFailed(checkId));

        this.mockMvc.perform(delete(testString.getChecks_path(checkId.toString()))
                .principal(new MyPrincipal("plop"))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

}