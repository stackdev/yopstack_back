package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestUserEventFixtures.customUserDeleted;
import static com.mystack.rest.fixture.RestUserEventFixtures.customUserDeletedFailed;
import static com.mystack.rest.fixture.RestUserEventFixtures.customUserDeletedNotFound;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.services.UserService;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserCancelationIntegrationTest {

    MockMvc                mockMvc;

    @InjectMocks
    UserCommandsController controller;

    @Mock
    UserService            service;

    @Autowired
    TestString             testString;

    UUID                   userId = UUID.fromString("fff65384-1247-e8c9-1112-456ef78abbbb");

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

    }

    @Test
    public void thatDeleteUserUsesHttpOkOnSuccess() throws Exception {

        when(service.deleteUser(any(DeleteUserEvent.class)))
                .thenReturn(customUserDeleted(userId));

        this.mockMvc.perform(
                delete(testString.getUsers_path(userId.toString()))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        verify(service).deleteUser(argThat(
                arg -> null != arg.getId()));
        //Matchers.<DeleteUserEvent> hasProperty("id",
        //        Matchers.equalTo(userId))));
    }

    @Test
    public void thatDeleteUserUsesHttpNotFoundOnEntityLookupFailure() throws Exception {

        when(service.deleteUser(any(DeleteUserEvent.class))).thenReturn(customUserDeletedNotFound(userId));

        this.mockMvc.perform(delete(testString.getUsers_path(userId.toString()))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void thatDeleteUserUsesHttpForbiddenOnEntityDeletionFailure() throws Exception {

        when(service.deleteUser(any(DeleteUserEvent.class))).thenReturn(customUserDeletedFailed(userId));

        this.mockMvc.perform(delete(testString.getUsers_path(userId.toString()))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

}