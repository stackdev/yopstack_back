package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestCheckDataFixtures.customizedCheckJSON;
import static com.mystack.rest.fixture.RestCheckEventFixtures.customCheckCreated;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.Date;
import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.checks.request.CreateCheckEvent;
import com.mystack.core.services.CheckService;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class CheckCreationIntegrationTest {
    private static Logger   LOG      = LoggerFactory.getLogger(CheckCreationIntegrationTest.class);

    MockMvc                 mockMvc;

    @InjectMocks
    CheckCommandsController controller;

    @Mock
    CheckService            service;

    @Autowired
    TestString              testString;

    UUID                    check_id = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    UUID                    stack_id = UUID.fromString("abababab-7712-8765-aaaa-3245647adfee");
    String                  comment  = "Stack used for integration purpose";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        when(service.createCheck(any(CreateCheckEvent.class))).thenReturn(
                customCheckCreated(check_id, new Date(), stack_id, comment));
    }

    @Test
    public void thatCreateCheckUsesHttpCreated() throws Exception {

        String json = customizedCheckJSON(check_id, new Date(), stack_id, comment);
        LOG.info("Check data to be sent: " + json);

        this.mockMvc.perform(
                post(testString.getChecks_path()).principal(new MyPrincipal("plop"))
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.comment").value(comment))
                .andExpect(jsonPath("$.stack").value(stack_id.toString()));

        //TODO: check why logs are not working here
        LOG.info("Body received: " + content().toString());

    }

    @Test
    public void thatCreateCheckPassesLocationHeader() throws Exception {

        this.mockMvc.perform(
                post(testString.getChecks_path()).principal(new MyPrincipal("plop"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(customizedCheckJSON(check_id, new Date(), stack_id, comment))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(header().string("Location",
                        Matchers.endsWith(testString.getChecks_path(check_id.toString()))));
    }
}
