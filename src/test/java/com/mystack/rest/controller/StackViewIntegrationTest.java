package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestStackEventFixtures.stackDetailsNotFound;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksFromUserEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsFromUserEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.services.StackService;
import com.mystack.core.services.UserService;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class StackViewIntegrationTest {
    MockMvc                mockMvc;

    @InjectMocks
    StackQueriesController controller;

    @Mock
    StackService           stackService;

    @Mock
    UserService            userService;

    @Autowired
    TestString             testString;

    UUID                   id   = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    UUID                   user = UUID.fromString("f1425d26-1111-2222-8888-ffffffffffff");

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
    }

    @Test
    public void thatViewStackUsesHttpNotFound() throws Exception {

        when(stackService.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(
                stackDetailsNotFound(id));

        this.mockMvc.perform(
                get(testString.getStacks_path(user.toString(), id.toString()))
                        .accept(MediaType.APPLICATION_JSON)
                        .principal(new MyPrincipal("plop")))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void thatViewStackUsesHttpOK() throws Exception {

        String name = "plop";
        String desc = "plop stack";
        when(stackService.getStackDetails(any(RequestStackDetailsFromUserEvent.class))).thenReturn(
                new StackDetailsEvent(new StackDetails(id, name, desc)));

        this.mockMvc.perform(
                get(testString.getStacks_path(user.toString(), id.toString()))
                        .accept(MediaType.APPLICATION_JSON)
                        .principal(new MyPrincipal("plop")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description").value(desc));
    }

    @Test
    public void thatViewStack_withUserNotUUID_returnsBadRequest() throws Exception {

        String name = "plop";
        String desc = "plop stack";
        String user_non_UUID = "notAnUUID";
        when(stackService.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(
                new StackDetailsEvent(new StackDetails(id, name, desc)));

        this.mockMvc.perform(
                get(testString.getStacks_path(user_non_UUID, id.toString()))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void thatViewAllStack_withUserNotUUID_returnsNotFound() throws Exception {

        String name = "plop";
        String desc = "plop stack";
        String user_non_UUID = "notAnUUID";

        verify(stackService, times(0)).getAllStacks(any(RequestAllStacksEvent.class));
        verify(stackService, times(0)).getAllStacks(any(RequestAllStacksFromUserEvent.class));

        this.mockMvc.perform(
                get(testString.getStacks_path(user_non_UUID))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }
}