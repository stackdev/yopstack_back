package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestUserDataFixtures.customizedUserJSON;
import static com.mystack.rest.fixture.RestUserDataFixtures.userJSONFromDetails;
import static com.mystack.rest.fixture.RestUserEventFixtures.customUserCreated;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.services.UserService;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserCreationIntegrationTest {
    private static Logger  LOG           = LoggerFactory.getLogger(UserCreationIntegrationTest.class);

    MockMvc                mockMvc;

    @InjectMocks
    UserCommandsController controller;

    @Mock
    UserService            service;

    @Autowired
    TestString             testString;

    UUID                   userId        = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    String                 user_name     = "name_" + userId.toString();
    String                 user_password = "password_" + userId.toString();
    int                    g_nb_friend   = 2, g_nb_stack = 3;
    UserCreatedEvent       event;
    List<String>           user_roles;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        user_roles = new ArrayList<>();
        user_roles.add("ROLE_USER");
        event = customUserCreated(userId, user_name, g_nb_friend, g_nb_stack, user_password, true, user_roles);
        when(service.createUser(any(CreateUserEvent.class))).thenReturn(event);

    }

    // createStack - validation?

    @Test
    public void thatCreateUserUsesHttpCreated() throws Exception {
        String json_user = userJSONFromDetails(event.getDetails());

        LOG.info("Input json: " + json_user);
        this.mockMvc.perform(
                post(testString.getUsers_path())
                        .content(json_user)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(json_user))
                .andExpect(status().isCreated());
    }

    @Test
    public void thatCreateUserPassesLocationHeader() throws Exception {
        int nb_friend = 3, nb_stack = 2;
        String name = "Usssseerrrr222";
        String password = "passs222";
        String input_json = customizedUserJSON(null, name, nb_friend, nb_stack, password);
        this.mockMvc.perform(
                post(testString.getUsers_path())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(input_json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(header().string("Location", Matchers.endsWith(testString.getUsers_path(userId.toString()))));
    }
}
