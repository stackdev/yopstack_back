package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestStackEventFixtures.stackDeleted;
import static com.mystack.rest.fixture.RestStackEventFixtures.stackDeletedFailed;
import static com.mystack.rest.fixture.RestStackEventFixtures.stackDeletedNotFound;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.services.StackService;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class StackCancelationIntegrationTest {

    MockMvc                 mockMvc;

    @InjectMocks
    StackCommandsController controller;

    @Mock
    StackService            service;

    @Autowired
    TestString              testString;

    UUID                    stackId = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    UUID                    userId  = UUID.fromString("fff65384-1247-e8c9-1112-456ef78abbbb");

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

    }

    @Test
    public void thatDeleteStackUsesHttpOkOnSuccess() throws Exception {

        when(service.deleteStack(any(DeleteStackEvent.class)))
                .thenReturn(
                        stackDeleted(stackId));

        this.mockMvc.perform(
                delete(testString.getStacks_path(userId.toString(), stackId.toString())).principal(new MyPrincipal("plop"))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        verify(service).deleteStack(argThat(
                arg -> null != arg.getId()));
        //Matchers.<DeleteStackEvent> hasProperty("id",
        //        Matchers.equalTo(stackId))));
    }

    @Test
    public void thatDeleteStackUsesHttpNotFoundOnEntityLookupFailure() throws Exception {

        when(service.deleteStack(any(DeleteStackEvent.class))).thenReturn(stackDeletedNotFound(stackId));

        this.mockMvc.perform(delete(testString.getStacks_path(userId.toString(), stackId.toString())).principal(new MyPrincipal("plop"))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    public void thatDeleteStackUsesHttpForbiddenOnEntityDeletionFailure() throws Exception {

        when(service.deleteStack(any(DeleteStackEvent.class))).thenReturn(stackDeletedFailed(stackId));

        this.mockMvc.perform(delete(testString.getStacks_path(userId.toString(), stackId.toString())).principal(new MyPrincipal("plop"))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

}