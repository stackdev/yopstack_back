package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestUserDataFixtures.customUserDetails;
import static com.mystack.rest.fixture.RestUserDataFixtures.getListCustomIdUserDetails;
import static com.mystack.rest.fixture.RestUserEventFixtures.UserDetailsNotFound;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.services.UserService;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserViewIntegrationTest {
    private static Logger LOG  = LoggerFactory.getLogger(UserViewIntegrationTest.class);

    MockMvc               mockMvc;

    @InjectMocks
    UserQueriesController controller;

    @Mock
    UserService           service;

    @Autowired
    TestString            testString;

    UUID                  id   = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    UUID                  user = UUID.fromString("f1425d26-1111-2222-8888-ffffffffffff");

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
    }

    @Test
    public void thatViewUserUsesHttpNotFound() throws Exception {

        when(service.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(
                UserDetailsNotFound(id));

        this.mockMvc.perform(
                get(testString.getUsers_path(user.toString()))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void thatViewUserUsesHttpOK() throws Exception {

        String name = "plop";
        int nb_friend = 2, nb_stack = 3;
        UserDetailsEvent event = new UserDetailsEvent(customUserDetails(null, name, nb_friend, nb_stack));
        when(service.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(event);

        this.mockMvc.perform(
                get(testString.getUsers_path(user.toString()))
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(name));
    }

    /*
     * Check if param in user url are processed
     */
    @Test
    public void thatViewUserUsesUsernameAsParameter() throws Exception {

        String name = "plop";
        int nb_friend = 2, nb_stack = 3, nb = 1;
        List<UserDetails> custom_list_userdetails = getListCustomIdUserDetails(nb, nb_friend, nb_stack);
        AllUsersEvent event = new AllUsersEvent(custom_list_userdetails);
        when(service.getAllUsers(any(RequestAllUsersEvent.class))).thenReturn(event);
        LOG.info("TestString: " + testString.getUsers_path());

        this.mockMvc.perform(
                get(testString.getUsers_path()).principal(new MyPrincipal(name)).param("name", name)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value(custom_list_userdetails.get(0).getName()));
    }

    /*
     * Check if param in user url are processed
     */
    @Test
    public void thatViewUserUsesUsernameAsParameter_NotFound() throws Exception {

        String name = "plop";
        AllUsersEvent event = AllUsersEvent.NotFound();
        when(service.getAllUsers(any(RequestAllUsersEvent.class))).thenReturn(event);

        this.mockMvc.perform(
                get(testString.getUsers_path()).principal(new MyPrincipal(name)).param("name", name)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}