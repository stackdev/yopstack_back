package com.mystack.rest.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.ConstantString;
import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;
import com.mystack.core.services.PreferenceService;
import com.mystack.rest.fixture.RestPreferenceFixtures;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class PreferenceIntegrationTest {
    private static Logger        LOG     = LoggerFactory.getLogger(PreferenceIntegrationTest.class);

    MockMvc                      mock_mvc_commands;
    MockMvc                      mock_mvc_queries;

    @Mock
    PreferenceService            service;

    @InjectMocks
    PreferenceCommandsController commands_controller;

    @InjectMocks
    PreferenceQueriesController  queries_controller;

    @Autowired
    TestString                   testString;

    UUID                         user_id = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");;
    PreferenceCreatedEvent       created_event;
    PreferenceReadEvent          read_event;
    PreferenceUpdatedEvent       updated_event;
    PreferenceDeletedEvent       deleted_event;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mock_mvc_commands = standaloneSetup(commands_controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        this.mock_mvc_queries = standaloneSetup(queries_controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        //** query **//
        read_event = new PreferenceReadEvent(RestPreferenceFixtures.randomPreference().toDetails());
        when(service.readPreference(any(ReadPreferenceEvent.class))).thenReturn(read_event);

        //** commands **//
        created_event = new PreferenceCreatedEvent(RestPreferenceFixtures.randomPreference().toDetails());
        when(service.createPreference(any(CreatePreferenceEvent.class))).thenReturn(created_event);

        updated_event = new PreferenceUpdatedEvent(RestPreferenceFixtures.randomPreference().toDetails());
        when(service.updatePreference(any(UpdatePreferenceEvent.class))).thenReturn(updated_event);

        deleted_event = new PreferenceDeletedEvent(RestPreferenceFixtures.randomPreference().toDetails());
        when(service.deletePreference(any(DeletePreferenceEvent.class))).thenReturn(deleted_event);

    }

    //------  READ  -------//
    @Test
    public void thatReadPreferenceUsesHttpOK() throws Exception {
        String json = RestPreferenceFixtures.randomPreferenceJSON();
        LOG.info("Input JSON:" + json);

        this.mock_mvc_queries.perform(
                get(testString.getPreferences_path(user_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string("Location", Matchers.endsWith(ConstantString.PreferencesFullPath(user_id.toString(), read_event.getDetails().getId().toString()))));

        verify(service, times(1)).readPreference(any(ReadPreferenceEvent.class));
    }

    //------  CREATE  --------//
    @Test
    public void thatCreatePreferenceUsesHttpOK() throws Exception {
        String json = RestPreferenceFixtures.randomPreferenceJSON();
        LOG.info("Input JSON:" + json);

        this.mock_mvc_commands.perform(
                post(testString.getPreferences_path(user_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(
                        header().string("Location",
                                Matchers.endsWith(ConstantString.PreferencesFullPath(created_event.getDetails().getUser().toString(),
                                        created_event.getDetails().getId().toString()))));

        verify(service, times(1)).createPreference(any(CreatePreferenceEvent.class));
    }

    //------  UPDATE  -------//
    @Test
    public void thatUpdatePreferenceUsesHttpOK() throws Exception {
        String json = RestPreferenceFixtures.randomPreferenceJSON();
        LOG.info("Input JSON:" + json);
        UUID some_id = UUID.randomUUID();

        this.mock_mvc_commands.perform(
                put(testString.getPreferences_path(user_id.toString(), some_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string("Location",
                        Matchers.endsWith(ConstantString.PreferencesFullPath(updated_event.getDetails().getUser().toString(),
                                some_id.toString()))));

        verify(service, times(1)).updatePreference(any(UpdatePreferenceEvent.class));
    }

    //------  DELETE  -------//
    @Test
    public void thatDeletePreferenceUsesHttpOK() throws Exception {
        String json = RestPreferenceFixtures.randomPreferenceJSON();
        LOG.info("Input JSON:" + json);

        this.mock_mvc_commands.perform(
                delete(testString.getPreferences_path(user_id.toString(), user_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        verify(service, times(1)).deletePreference(any(DeletePreferenceEvent.class));
    }

}
