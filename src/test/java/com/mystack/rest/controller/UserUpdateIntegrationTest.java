package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestUserDataFixtures.customizedUserJSON;
import static com.mystack.rest.fixture.RestUserDataFixtures.userJSONFromDetails;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.core.services.UserService;
import com.mystack.rest.fixture.RestUserDataFixtures;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserUpdateIntegrationTest {
    private static Logger  LOG    = LoggerFactory.getLogger(UserUpdateIntegrationTest.class);

    MockMvc                mockMvc;

    @InjectMocks
    UserCommandsController controller;

    @Mock
    UserService            service;

    @Autowired
    TestString             testString;

    UUID                   userId = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    int                    g_nb_friend = 2, g_nb_stack = 3;
    UserUpdatedEvent       event;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        event = new UserUpdatedEvent(RestUserDataFixtures.standardUserDetails());
        when(service.updateUser(any(UpdateUserEvent.class))).thenReturn(event);

    }

    // updateStack - validation?

    @Test
    public void thatUpdateUserUsesHttpOK() throws Exception {
        int nb_friend = 1, nb_stack = 1;
        String name = "plop";
        String password = "pass";
        //UUID id doesn't matter in this context, the user service is mocked
        UUID fake_id = UUID.randomUUID();
        String input_json = customizedUserJSON(fake_id, name, nb_friend, nb_stack, password);
        LOG.info("Input JSON:" + input_json);
        this.mockMvc.perform(
                put(testString.getUsers_path(fake_id.toString()))
                        .content(input_json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .principal(new MyPrincipal("plop")))
                .andDo(print())
                .andExpect(content().string(userJSONFromDetails(event.getDetails())))
                .andExpect(status().isOk());
    }

    @Test
    public void thatUpdateUserPassesLocationHeader() throws Exception {
        int nb_friend = 3, nb_stack = 2;
        String name = "Usssseerrrr222";
        String password = "pass";
        UUID fake_id = UUID.randomUUID();
        String input_json = customizedUserJSON(null, name, nb_friend, nb_stack, password);
        this.mockMvc.perform(
                put(testString.getUsers_path(fake_id.toString()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(input_json)
                        .accept(MediaType.APPLICATION_JSON)
                        .principal(new MyPrincipal("plop")))
                .andExpect(header().string("Location", Matchers.endsWith(testString.getUsers_path(event.getDetails().getId().toString()))));
    }
}
