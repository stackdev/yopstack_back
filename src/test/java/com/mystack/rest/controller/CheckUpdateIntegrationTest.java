package com.mystack.rest.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.checks.request.UpdateCheckEvent;
import com.mystack.core.events.checks.result.CheckUpdatedEvent;
import com.mystack.core.services.CheckService;
import com.mystack.rest.fixture.RestCheckDataFixtures;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class CheckUpdateIntegrationTest {
    private static Logger   LOG     = LoggerFactory.getLogger(CheckUpdateIntegrationTest.class);

    MockMvc                 mockMvc;

    @InjectMocks
    CheckCommandsController controller;

    @Mock
    CheckService            service;

    @Autowired
    TestString              testString;

    UUID                    checkId = UUID.randomUUID();
    UUID                    stackId = UUID.randomUUID();
    CheckUpdatedEvent       event;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        event = new CheckUpdatedEvent(RestCheckDataFixtures.standardCheckDetails());
        when(service.updateCheck(any(UpdateCheckEvent.class))).thenReturn(event);

    }

    @Test
    public void thatUpdateCheckUsesHttpOK() throws Exception {
        // UUID id doesn't matter in this context, the check service is mocked
        UUID fake_id = UUID.randomUUID();
        String input_json = RestCheckDataFixtures.checkJSONFromDetails(RestCheckDataFixtures.standardCheckDetails());
        LOG.info("Input JSON:" + input_json);
        this.mockMvc.perform(
                put(testString.getChecks_path(fake_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .content(input_json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().string(RestCheckDataFixtures.checkJSONFromDetails(event.getDetails())))
                .andExpect(status().isOk());
    }

    @Test
    public void thatUpdateCheckPassesLocationHeader() throws Exception {
        UUID fake_id = UUID.randomUUID();
        String input_json = RestCheckDataFixtures.checkJSONFromDetails(RestCheckDataFixtures.standardCheckDetails());
        this.mockMvc.perform(
                put(testString.getChecks_path(fake_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(input_json)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(header().string("Location", Matchers.endsWith(testString.getChecks_path(event.getDetails().getId().toString()))));
    }

    @Test
    public void thatUpdateUserWithoutMETHODPUT_areRefused() throws Exception {
        UUID fake_id = UUID.randomUUID();
        String input_json = RestCheckDataFixtures.checkJSONFromDetails(RestCheckDataFixtures.standardCheckDetails());
        LOG.info("Input JSON:" + input_json);
        this.mockMvc.perform(
                post(testString.getChecks_path(fake_id.toString()))
                        .principal(new MyPrincipal("plop"))
                        .content(input_json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isMethodNotAllowed());
    }

}
