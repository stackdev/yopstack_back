package com.mystack.rest.controller;

import static com.mystack.rest.fixture.RestStackDataFixtures.standardStackJSON;
import static com.mystack.rest.fixture.RestStackEventFixtures.stackCreated;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.mystack.config.FunctionalConfiguration.StackType;
import com.mystack.config.TestString;
import com.mystack.config.TestStringConfig;
import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.services.StackService;
import com.mystack.tool.MyPrincipal;

@ContextConfiguration(classes = { TestStringConfig.class })
@RunWith(SpringJUnit4ClassRunner.class)
public class StackCreationIntegrationTest {
    private static Logger   LOG         = LoggerFactory.getLogger(StackCreationIntegrationTest.class);

    MockMvc                 mockMvc;

    @InjectMocks
    StackCommandsController controller;

    @Mock
    StackService            service;

    @Autowired
    TestString              testString;

    UUID                    userId      = UUID.fromString("f3512d26-72f6-4290-9265-63ad69eccc13");
    UUID                    newStackId  = UUID.fromString("abababab-7712-8765-aaaa-3245647adfee");
    String                  name        = "integration_stack";
    String                  description = "Stack used for integration purpose";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter()).build();

        when(service.createStack(any(CreateStackEvent.class))).thenReturn(
                stackCreated(newStackId, name, description));
    }

    @Test
    public void thatCreateStackUsesHttpCreated() throws Exception {

        this.mockMvc.perform(
                post(testString.getStacks_path(userId.toString())).principal(new MyPrincipal("plop"))
                        .content(standardStackJSON())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.description").value(description));

        //TODO: check why logs are not working here
        LOG.info("Body received: " + content().toString());

    }

    @Test
    public void thatCreateStackPassesLocationHeader() throws Exception {

        this.mockMvc.perform(
                post(testString.getStacks_path(userId.toString())).principal(new MyPrincipal("plop"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(standardStackJSON())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(header().string("Location",
                        Matchers.endsWith(testString.getStacks_path(userId.toString(), newStackId.toString()))));
    }

    @Test
    public void thatStackCreateEvent_contains_auth_user() throws Exception {
        ArgumentCaptor<CreateStackEvent> captor = ArgumentCaptor.forClass(CreateStackEvent.class);
        String auth_username = "plop";

        this.mockMvc.perform(
                post(testString.getStacks_path(userId.toString())).principal(new MyPrincipal(auth_username))
                        .content(standardStackJSON())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.description").value(description))
                .andExpect(jsonPath("$.type").value(StackType.TIME.getValue()));

        verify(service).createStack(captor.capture());

        assertEquals(auth_username, captor.getValue().getAuthenticated_user());

    }
}
