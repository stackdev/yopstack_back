package com.mystack.rest.fixture;

import static com.mystack.tool.Tool.getRandomUUIDList;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mystack.core.domain.User;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.users.result.AllUsersEvent;

public class RestUserDataFixtures {

    public static AllUsersEvent allUsers() {
        List<UserDetails> list = new ArrayList<UserDetails>();

        list.add(standardUserDetails());
        list.add(standardUserDetails());
        list.add(standardUserDetails());

        return new AllUsersEvent(list);
    }

    public static User standardUser() {
        return new User(UUID.randomUUID(), "StandardUser" + new Date().toString(), new ArrayList<UUID>(), new ArrayList<UUID>());
    }

    public static UserDetails customUserDetails(UUID id, String name, int nb_friend, int nb_stack, String password) {
        List<String> roles = new ArrayList<String>();
        roles.add("ROLE_USER");
        UserDetails details = new UserDetails(id, name, getRandomUUIDList(nb_friend), getRandomUUIDList(nb_stack), password, true, roles);
        details.setSetpw(password);
        return details;
    }

    public static UserDetails customUserDetails(UUID id, String name, int nb_friend, int nb_stack) {
        return customUserDetails(id, name, nb_friend, nb_stack, name + "_password");
    }

    public static UserDetails customIdUserDetails(UUID id, int nb_friend, int nb_stack) {
        return customUserDetails(id, "CustomIdUser" + UUID.randomUUID().toString(), nb_friend, nb_stack);
    }

    /**
     * Provide a list of USerDetails, with same number of friends and stacks
     * 
     * @param nb_user
     * @param nb_friend
     * @param nb_stack
     * @return
     */
    public static List<UserDetails> getListCustomIdUserDetails(int nb_user, int nb_friend, int nb_stack) {
        List<UserDetails> listResult = new ArrayList<>();
        for (int i = 0; i < nb_user; i++) {
            listResult.add(customIdUserDetails(UUID.randomUUID(), nb_friend, nb_stack));
        }
        return listResult;
    }

    public static UserDetails standardUserDetails() {
        return customIdUserDetails(UUID.randomUUID(), 2, 3);
    }

    public static String customizedUserJSON(UUID id, String name, int nb_friend, int nb_stack, String password) {
        return userJSONFromDetails(customUserDetails(id, name, nb_friend, nb_stack, password));
    }

    /** use userJSONFromDetails(UserDetails details) instead */
    @Deprecated
    public static String standardUserJSON() {
        UUID id = UUID.randomUUID();
        return customizedUserJSON(id, "Standard_User_" + id.toString(), 0, 0, "Standard_User_password_" + id.toString());
    }

    public static String userJSONFromDetails(UserDetails details) {

        Gson gson = new GsonBuilder().create();
        // We use User from REST DOMAIN !!!
        return gson.toJson(com.mystack.rest.domain.User.fromDetails(details));
    }

    public static UserDetails nowUserDetails() {
        Date now = new Date();
        String user = String.format("User %s", now.toString());
        ArrayList<UUID> friend_list = new ArrayList<>();
        friend_list.add(UUID.randomUUID());
        friend_list.add(UUID.randomUUID());
        friend_list.add(UUID.randomUUID());
        return new UserDetails(UUID.randomUUID(), user, friend_list, null);
    }

}
