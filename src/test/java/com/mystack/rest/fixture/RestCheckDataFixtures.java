package com.mystack.rest.fixture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import com.mystack.core.domain.CheckDetails;
import com.mystack.core.events.checks.result.AllChecksEvent;
import com.mystack.rest.domain.Check;

public class RestCheckDataFixtures {
    private static Logger LOG = LoggerFactory.getLogger(RestCheckDataFixtures.class);

    public static AllChecksEvent allChecks() {
        List<CheckDetails> list = new ArrayList<CheckDetails>();

        list.add(standardCheckDetails());
        list.add(standardCheckDetails());
        list.add(standardCheckDetails());

        return new AllChecksEvent(list);
    }

    public static Check standardCheck() {
        UUID stackid = UUID.randomUUID();
        return Check.fromDetails(customCheckDetails(UUID.randomUUID(), new Date(), stackid, stackid + "standard Check rest domain"));
    }

    public static CheckDetails customCheckDetails(UUID id, Date date, UUID stack, String comment) {
        CheckDetails details = new CheckDetails(id);
        details.setDate(date);
        details.setStack(stack);
        details.setComment(comment);
        return details;
    }

    public static CheckDetails customCheckDetails(Date date) {
        return customCheckDetails(UUID.randomUUID(), date, UUID.randomUUID(), "Now check: " + date);
    }

    public static CheckDetails standardCheckDetails() {
        Check result_check = standardCheck();
        LOG.info("Returning Check: " + result_check);
        CheckDetails result = result_check.toDetails();
        LOG.info("Returning CheckDetails: " + result);
        return result;
    }

    public static String customizedCheckJSON(UUID id, Date date, UUID stack, String comment) {
        return checkJSONFromDetails(customCheckDetails(id, date, stack, comment));
    }

    public static String checkJSONFromDetails(CheckDetails details) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, (JsonSerializer<Date>) (date, type, jsonSerializationContext) -> new JsonPrimitive(date.getTime()))
                .create();
        return gson.toJson(com.mystack.rest.domain.Check.fromDetails(details));
    }

    // TODO: to be enhanced to separate check with a specified offset of time
    public static List<CheckDetails> getListCheckDetails(int nb) {
        List<CheckDetails> result = new ArrayList<>();

        for (int i = 0; i < nb; i++) {
            result.add(customCheckDetails(new Date()));
        }
        return result;
    }
}
