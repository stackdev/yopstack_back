package com.mystack.rest.fixture;

import static com.mystack.rest.fixture.RestStackDataFixtures.customIdStackDetails;
import static com.mystack.rest.fixture.RestStackDataFixtures.standardStackDetails;

import java.util.UUID;

import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;

public class RestStackEventFixtures {

    public static StackDetailsEvent stackDetailsNotFound(UUID id) {
        return StackDetailsEvent.notFound(id);
    }

    public static StackDetailsEvent stackDetailsEvent(UUID id, String name, String description) {
        return new StackDetailsEvent(customIdStackDetails(id, name, description));
    }

    public static StackCreatedEvent stackCreated(UUID id, String name, String description) {
        return new StackCreatedEvent(id, customIdStackDetails(id, name, description));
    }

    public static StackDeletedEvent stackDeleted(UUID id) {
        return new StackDeletedEvent(standardStackDetails());
    }

    public static StackDeletedEvent stackDeletedFailed(UUID id) {
        return StackDeletedEvent.notAuthorized(standardStackDetails());
    }

    public static StackDeletedEvent stackDeletedNotFound(UUID id) {
        return StackDeletedEvent.notFound(standardStackDetails());
    }

}
