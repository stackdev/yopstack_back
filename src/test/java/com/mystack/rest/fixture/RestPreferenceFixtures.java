package com.mystack.rest.fixture;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.mystack.rest.domain.Preference;

public class RestPreferenceFixtures {
    private static Logger LOG = LoggerFactory.getLogger(RestPreferenceFixtures.class);

    // DATA

    //    public static Comment[] StandardPreference(int nb) {
    //        Comment[] output = new Comment[nb];
    //        for (int i = 0; i < nb; i++) {
    //            output[i] = Comment.FromDetails(new CommentDetails(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "lol", new Date(), null));
    //        }
    //        return output;
    //    }

    public static Preference randomPreference() {
        Preference pref = new Preference(UUID.randomUUID());
        pref.setId(UUID.randomUUID());
        return pref;
    }

    public static Preference userPreference(UUID user) {
        return new Preference(user);
    }

    public static String randomPreferenceJSON() {
        return preferenceJSON(randomPreference());
    }

    public static String preferenceJSON(Preference pref) {
        Gson gson = new Gson();
        return gson.toJson(pref);
    }

    // EVENTS

    //
    //    public static Comment standardComment() {
    //        CommentDetails comment_details = new CommentDetails(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "standard Comment rest domain",
    //                new Date(), null);
    //        return Comment.FromDetails(comment_details);
    //    }
    //
    //    public static CommentDetails customCommentDetails(UUID stack_userid, UUID stackid, UUID checkid, UUID comment_userid) {
    //        return customCommentDetails(stack_userid, "some text: ..." + UUID.randomUUID(), new Date(), stackid, checkid, comment_userid, null);
    //    }
    //
    //    public static CommentDetails customCommentDetails(UUID stack_userid, String text, Date date, UUID stackid, UUID checkid, UUID comment_userid, Date update_timestamp) {
    //        return new CommentDetails(UUID.randomUUID(), stack_userid, stackid, checkid, comment_userid, text, date, update_timestamp);
    //    }
    //
    //    public static CommentDetails standardCommentDetails() {
    //        Comment result_Comment = standardComment();
    //        LOG.info("Returning Comment: " + result_Comment);
    //        CommentDetails result = result_Comment.toDetails();
    //        LOG.info("Returning CommentDetails: " + result);
    //        return result;
    //    }
    //
    //    public static String customizedCommentJSON(UUID userid, String text, Date date, UUID stackid, UUID checkid, UUID comment_userid) {
    //        return CommentJSONFromDetails(new CommentDetails(UUID.randomUUID(), userid, stackid, checkid, comment_userid, text, date, null));
    //    }
    //
    //    public static String CommentJSONFromDetails(CommentDetails details) {
    //        Gson gson = new Gson();
    //        return gson.toJson(com.mystack.rest.domain.Comment.FromDetails(details));
    //    }
    //
    //    public static CommentDetails nowCommentDetails() {
    //        Date now = new Date();
    //        return getCommentDetails(now);
    //    }
    //
    //    public static CommentDetails getCommentDetails(Date date) {
    //        return new CommentDetails(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "Now Comment..." + date, date, null);
    //    }
    //
    //    // TODO: to be enhanced to separate Comment with a specified offset of time
    //    public static List<CommentDetails> getListCommentDetails(int nb) {
    //        List<CommentDetails> result = new ArrayList<>();
    //
    //        for (int i = 0; i < nb; i++) {
    //            result.add(getCommentDetails(new Date()));
    //        }
    //        return result;
    //    }

}
