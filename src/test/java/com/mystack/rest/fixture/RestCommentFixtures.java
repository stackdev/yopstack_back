package com.mystack.rest.fixture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.mystack.core.domain.CommentDetails;
import com.mystack.core.events.comments.result.AllCommentsEvent;
import com.mystack.rest.domain.Comment;

public class RestCommentFixtures {
    private static Logger LOG = LoggerFactory.getLogger(RestCommentFixtures.class);

    // DATA
    public static Comment[] StandardComment(int nb) {
        Comment[] output = new Comment[nb];
        for (int i = 0; i < nb; i++) {
            output[i] = Comment.FromDetails(new CommentDetails(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "lol", new Date(), null));
        }
        return output;
    }

    // EVENTS

    public static AllCommentsEvent allComments() {
        List<CommentDetails> list = new ArrayList<CommentDetails>();

        list.add(standardCommentDetails());
        list.add(standardCommentDetails());
        list.add(standardCommentDetails());

        return new AllCommentsEvent(list);
    }

    public static Comment standardComment() {
        CommentDetails comment_details = new CommentDetails(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "standard Comment rest domain",
                new Date(), null);
        return Comment.FromDetails(comment_details);
    }

    public static CommentDetails customCommentDetails(UUID stack_userid, UUID stackid, UUID checkid, UUID comment_userid) {
        return customCommentDetails(stack_userid, "some text: ..." + UUID.randomUUID(), new Date(), stackid, checkid, comment_userid, null);
    }

    public static CommentDetails customCommentDetails(UUID stack_userid, String text, Date date, UUID stackid, UUID checkid, UUID comment_userid, Date update_timestamp) {
        return new CommentDetails(UUID.randomUUID(), stack_userid, stackid, checkid, comment_userid, text, date, update_timestamp);
    }

    public static CommentDetails standardCommentDetails() {
        Comment result_Comment = standardComment();
        LOG.info("Returning Comment: " + result_Comment);
        CommentDetails result = result_Comment.toDetails();
        LOG.info("Returning CommentDetails: " + result);
        return result;
    }

    public static String customizedCommentJSON(UUID userid, String text, Date date, UUID stackid, UUID checkid, UUID comment_userid) {
        return CommentJSONFromDetails(new CommentDetails(UUID.randomUUID(), userid, stackid, checkid, comment_userid, text, date, null));
    }

    public static String CommentJSONFromDetails(CommentDetails details) {
        Gson gson = new Gson();
        return gson.toJson(com.mystack.rest.domain.Comment.FromDetails(details));
    }

    public static CommentDetails nowCommentDetails() {
        Date now = new Date();
        return getCommentDetails(now);
    }

    public static CommentDetails getCommentDetails(Date date) {
        return new CommentDetails(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), "Now Comment..." + date, date, null);
    }

    // TODO: to be enhanced to separate Comment with a specified offset of time
    public static List<CommentDetails> getListCommentDetails(int nb) {
        List<CommentDetails> result = new ArrayList<>();

        for (int i = 0; i < nb; i++) {
            result.add(getCommentDetails(new Date()));
        }
        return result;
    }

}
