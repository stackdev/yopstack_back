package com.mystack.rest.fixture;

import static com.mystack.rest.fixture.RestUserDataFixtures.customIdUserDetails;
import static com.mystack.rest.fixture.RestUserDataFixtures.standardUserDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mystack.core.domain.UserDetails;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;

public class RestUserEventFixtures {

    public static UserDetailsEvent UserDetailsNotFound(UUID id) {
        return UserDetailsEvent.notFound(id);
    }

    public static UserDetailsEvent customUserDetailsEvent(UUID id, int nb_friend, int nb_stack) {
        return new UserDetailsEvent(customIdUserDetails(id, nb_friend, nb_stack));
    }

    /*
     * Use customUserCreated with password instead
     */
    @Deprecated
    public static UserCreatedEvent customUserCreated(UUID id, String name, int nb_friend, int nb_stack) {
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");
        return customUserCreated(id, name, nb_friend, nb_stack, name, true, roles);

    }

    public static UserCreatedEvent customUserCreated(UUID id, String name, int nb_friend, int nb_stack, String password, boolean enabled, List<String> roles) {
        return new UserCreatedEvent(id, customIdUserDetails(id, nb_friend, nb_stack));
    }

    public static UserDeletedEvent customUserDeleted(UUID id) {
        return new UserDeletedEvent(standardUserDetails());
    }

    public static UserDeletedEvent customUserDeletedFailed(UUID id) {
        return UserDeletedEvent.notAuthorized(standardUserDetails());
    }

    public static UserDeletedEvent customUserDeletedNotFound(UUID id) {
        return UserDeletedEvent.notFound(standardUserDetails());
    }

    public static UserUpdatedEvent UserNameUpdated(String name, UserDetails details) {
        details.setName(name);
        return new UserUpdatedEvent(details);
    }

}
