package com.mystack.rest.fixture;

import static com.mystack.rest.fixture.RestCheckDataFixtures.customCheckDetails;
import static com.mystack.rest.fixture.RestCheckDataFixtures.standardCheckDetails;

import java.util.Date;
import java.util.UUID;

import com.mystack.core.events.checks.result.CheckCreatedEvent;
import com.mystack.core.events.checks.result.CheckDeletedEvent;
import com.mystack.core.events.checks.result.CheckDetailsEvent;

public class RestCheckEventFixtures {

    public static CheckDetailsEvent CheckDetailsNotFound(UUID id) {
        return CheckDetailsEvent.notFound(id);
    }

    public static CheckDetailsEvent customCheckDetailsEvent(UUID id, Date date, UUID stack, String comment) {
        return new CheckDetailsEvent(customCheckDetails(id, date, stack, comment));
    }

    public static CheckCreatedEvent customCheckCreated(UUID id, Date date, UUID stack, String comment) {
        return new CheckCreatedEvent(customCheckDetails(id, date, stack, comment));
    }

    public static CheckDeletedEvent customCheckDeleted(UUID id) {
        return new CheckDeletedEvent(standardCheckDetails());
    }

    public static CheckDeletedEvent customCheckDeletedFailed(UUID id) {
        return CheckDeletedEvent.notAuthorized(standardCheckDetails());
    }

    public static CheckDeletedEvent customCheckDeletedNotFound(UUID id) {
        return CheckDeletedEvent.notFound(standardCheckDetails());
    }

}
