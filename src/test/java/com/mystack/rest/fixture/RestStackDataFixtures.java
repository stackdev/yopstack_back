package com.mystack.rest.fixture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.rest.domain.Stack;

public class RestStackDataFixtures {

    private static Logger LOG = LoggerFactory.getLogger(RestStackDataFixtures.class);

    public static AllStacksEvent allStacks() {
        List<StackDetails> list = new ArrayList<StackDetails>();

        list.add(standardStackDetails());
        list.add(standardStackDetails());
        list.add(standardStackDetails());

        return new AllStacksEvent(list);
    }

    public static Stack standardStack() {
        return new Stack();
    }

    public static StackDetails customIdStackDetails(UUID id, String name, String description) {
        return new StackDetails(id, name, description);
    }

    public static StackDetails standardStackDetails() {
        return customIdStackDetails(UUID.randomUUID(), "some_name", "some description");
    }

    /**
     * Give a stack with an unique id, name and description containing date of creation
     * 
     * @return as a stackdetails
     */
    public static StackDetails nowStackStackDetails() {
        Date now = new Date();
        String stack_name = String.format("Stack %s", now.toString());
        String stack_desc = String.format("Test stack, created on: %s", now.toString());
        return new StackDetails(UUID.randomUUID(), stack_name, stack_desc);
    }

    public static String stackJSONFromDetails(StackDetails details) {

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
                .create();
        String json_string = gson.toJson(com.mystack.rest.domain.Stack.fromDetails(details));
        LOG.info("TMP: the json string: " + json_string);
        return json_string;
        //        return String.format("{ \"description\": \"%s\", \"name\": \"%s\", \"periodicity\": \"%s\","
        //                + " \"frequency\": \"%s\", \"type\": %s, \"isprivate\": %s, \"status\": %s }",
        //                details.getDescription(), details.getName(), details.getPeriodicity(),
        //                details.getFrequency(), details.getType().getValue(), details.getIsprivate(), details.getStatus().getValue());
    }

    public static String standardStackJSON() {
        String name = "standardStackName_" + UUID.randomUUID().toString();
        return customizedStackJSON(name, "customized description...");
    }

    public static String customizedStackJSON(String name, String description) {
        StackDetails details = new StackDetails(UUID.randomUUID(), name, description);
        details.setName(name);
        details.setDescription(description);
        return stackJSONFromDetails(details);
    }

}
