package com.mystack.core.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.core.domain.Preference;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.domain.fixtures.PreferencesFixtures;
import com.mystack.core.domain.fixtures.UsersFixtures;
import com.mystack.core.events.preferences.request.CreatePreferenceEvent;
import com.mystack.core.events.preferences.request.DeletePreferenceEvent;
import com.mystack.core.events.preferences.request.ReadPreferenceEvent;
import com.mystack.core.events.preferences.request.UpdatePreferenceEvent;
import com.mystack.core.events.preferences.result.PreferenceCreatedEvent;
import com.mystack.core.events.preferences.result.PreferenceDeletedEvent;
import com.mystack.core.events.preferences.result.PreferenceReadEvent;
import com.mystack.core.events.preferences.result.PreferenceUpdatedEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.persistence.services.PreferencePersistenceService;

public class PreferenceEventHandlerUnitTest {
    private static Logger                LOG = LoggerFactory.getLogger(PreferenceEventHandlerUnitTest.class);

    private PreferencePersistenceService persis_mocked;

    @Mock
    private UserService                  userService;

    @InjectMocks
    private PreferenceEventHandler       eventHandler;

    @Before
    public void init() {
        // TODO: check differences with annotations usage
        persis_mocked = mock(PreferencePersistenceService.class);
        //userService_mocked = mock(UserService.class);
        eventHandler = new PreferenceEventHandler(persis_mocked);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void thatCreatePreferenceEvent_are_processed() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();

        when(persis_mocked.createPreference(any(CreatePreferenceEvent.class))).thenReturn(new PreferenceCreatedEvent(preference.toDetails()));
        when(persis_mocked.readPreference(any(ReadPreferenceEvent.class))).thenReturn(PreferenceReadEvent.NotFound());
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceCreatedEvent result = eventHandler.createPreference(new CreatePreferenceEvent(preference.toDetails(), owner_name));

        ArgumentCaptor<CreatePreferenceEvent> argument = ArgumentCaptor.forClass(CreatePreferenceEvent.class);
        verify(persis_mocked).createPreference(argument.capture());
        assertEquals(argument.getValue().getDetails().getId(), preference.getId());
        assertTrue(result.isCreated());

    }

    @Test
    public void thatPreferenceCreation_failed_with_authuser_not_owner() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();
        when(persis_mocked.createPreference(any(CreatePreferenceEvent.class))).thenReturn(new PreferenceCreatedEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceCreatedEvent result = eventHandler.createPreference(new CreatePreferenceEvent(preference.toDetails(), "not_owner"));

        assertFalse(result.isCreated());
        verify(persis_mocked, times(0)).createPreference(any(CreatePreferenceEvent.class));
    }
    
    @Test
    public void thatPreferenceCreation_failed_if_already_exists() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();
        when(persis_mocked.createPreference(any(CreatePreferenceEvent.class))).thenReturn(new PreferenceCreatedEvent(preference.toDetails()));
        when(persis_mocked.readPreference(any(ReadPreferenceEvent.class))).thenReturn(new PreferenceReadEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceCreatedEvent result = eventHandler.createPreference(new CreatePreferenceEvent(preference.toDetails(), owner_name));

        assertFalse(result.isCreated());
        verify(persis_mocked, times(0)).createPreference(any(CreatePreferenceEvent.class));
    }

    @Test
    public void thatReadPreferenceEvent_are_processed() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();

        when(persis_mocked.readPreference(any(ReadPreferenceEvent.class))).thenReturn(new PreferenceReadEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceReadEvent result = eventHandler.readPreference(new ReadPreferenceEvent(preference.toDetails(), owner_name));

        ArgumentCaptor<ReadPreferenceEvent> argument = ArgumentCaptor.forClass(ReadPreferenceEvent.class);
        verify(persis_mocked).readPreference(argument.capture());
        assertEquals(argument.getValue().getDetails().getId(), preference.getId());
        assertTrue(result.isEntityFound());

    }

    @Test
    public void thatReadPreferenceEvent_failed_with_user_not_owner() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();

        when(persis_mocked.readPreference(any(ReadPreferenceEvent.class))).thenReturn(new PreferenceReadEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceReadEvent result = eventHandler.readPreference(new ReadPreferenceEvent(preference.toDetails(), "not_owner"));

        verify(persis_mocked, times(0)).readPreference(any(ReadPreferenceEvent.class));
        assertFalse(result.isEntityFound());

    }

    @Test
    public void thaUpdatePreferenceEvent_are_processed() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();

        when(persis_mocked.updatePreference(any(UpdatePreferenceEvent.class))).thenReturn(new PreferenceUpdatedEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceUpdatedEvent result = eventHandler.updatePreference(new UpdatePreferenceEvent(preference.toDetails(), owner_name));

        ArgumentCaptor<UpdatePreferenceEvent> argument = ArgumentCaptor.forClass(UpdatePreferenceEvent.class);
        verify(persis_mocked).updatePreference(argument.capture());
        assertEquals(argument.getValue().getDetails().getId(), preference.getId());
        assertTrue(result.isUpdated());

    }

    @Test
    public void thatUpdatePreferenceEvent_failed_with_user_not_owner() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();

        when(persis_mocked.updatePreference(any(UpdatePreferenceEvent.class))).thenReturn(new PreferenceUpdatedEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceUpdatedEvent result = eventHandler.updatePreference(new UpdatePreferenceEvent(preference.toDetails(), "not_owner"));

        verify(persis_mocked, times(0)).updatePreference(any(UpdatePreferenceEvent.class));
        assertFalse(result.isUpdated());

    }

    @Test
    public void thatDeletePreferenceEvent_are_processed() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();

        when(persis_mocked.deletePreference(any(DeletePreferenceEvent.class))).thenReturn(new PreferenceDeletedEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceDeletedEvent result = eventHandler.deletePreference(new DeletePreferenceEvent(preference.toDetails(), owner_name));

        ArgumentCaptor<DeletePreferenceEvent> argument = ArgumentCaptor.forClass(DeletePreferenceEvent.class);
        verify(persis_mocked).deletePreference(argument.capture());
        assertEquals(argument.getValue().getDetails().getId(), preference.getId());
        assertTrue(result.isDeletionCompleted());

    }

    @Test
    public void thatDeletePreferenceEvent_failed_with_user_not_owner() {
        String owner_name = "onwer";
        UserDetails user_owner = UsersFixtures.getStandardUserDetailsNoList(owner_name);

        Preference preference = PreferencesFixtures.getRandomPreference();

        when(persis_mocked.deletePreference(any(DeletePreferenceEvent.class))).thenReturn(new PreferenceDeletedEvent(preference.toDetails()));
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_owner));

        PreferenceDeletedEvent result = eventHandler.deletePreference(new DeletePreferenceEvent(preference.toDetails(), "not_owner"));

        verify(persis_mocked, times(0)).deletePreference(any(DeletePreferenceEvent.class));
        assertFalse(result.isDeletionCompleted());

    }

}
