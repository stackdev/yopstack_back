package com.mystack.core.services;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mystack.core.domain.Stack;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.domain.fixtures.StacksFixtures;
import com.mystack.core.domain.fixtures.UsersFixtures;
import com.mystack.core.events.stacks.request.CreateStackEvent;
import com.mystack.core.events.stacks.request.DeleteStackEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksByIdEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksEvent;
import com.mystack.core.events.stacks.request.RequestAllStacksFromUserEvent;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.AllStacksByIdEvent;
import com.mystack.core.events.stacks.result.AllStacksEvent;
import com.mystack.core.events.stacks.result.AllStacksFromUserEvent;
import com.mystack.core.events.stacks.result.StackCreatedEvent;
import com.mystack.core.events.stacks.result.StackDeletedEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.persistence.services.StackPersistenceService;

public class StackEventHandlerUnitTest {
    private static Logger           LOG = LoggerFactory.getLogger(StackEventHandlerUnitTest.class);

    private StackPersistenceService persis_mocked;

    @Mock
    private UserService             userService;

    @InjectMocks
    private StackEventHandler       eventHandler;

    @Before
    public void init() {
        // TODO: check differences with annotations usage
        persis_mocked = mock(StackPersistenceService.class);
        //userService_mocked = mock(UserService.class);
        eventHandler = new StackEventHandler(persis_mocked);
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Basic test for CreateStackEvent Processing
     */
    @Test
    public void thatStackCreateEventAreProcessed() {
        // We create a stack, UUID auto generated
        Stack stack = StacksFixtures.getStandardStack();
        UUID stackUser = UUID.randomUUID();
        // adding some unrelated data to the stack
        ArrayList<UUID> list_user = new ArrayList<>();
        list_user.add(stackUser);
        stack.setAdmins(list_user);
        stack.setCheckers(list_user);

        StackCreatedEvent event_mocked = new StackCreatedEvent(stack.getId(), stack.toDetails());
        String some_name = "some_name";

        // our stack is returned by the mock when findById is called
        when(persis_mocked.createStack(any(CreateStackEvent.class))).thenReturn(event_mocked);
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(stackUser, some_name, null, null));
        when(userService.updateUser(any(UpdateUserEvent.class))).thenReturn(new UserUpdatedEvent(new UserDetails(UUID.randomUUID())));

        // create event with a generated StackDetails i.e. with another UUID
        CreateStackEvent event = new CreateStackEvent(StacksFixtures.getStandardStackDetails(), stackUser, some_name);

        StackCreatedEvent resultEvent = eventHandler.createStack(event);

        // check that Persistence service use the same UUID
        ArgumentCaptor<CreateStackEvent> argument = ArgumentCaptor.forClass(CreateStackEvent.class);
        verify(persis_mocked).createStack(argument.capture());
        assertEquals(argument.getValue().getDetails().getId(), event.getDetails().getId());
        assertTrue(resultEvent.isCreated());

        // shared stack related test
        assertEquals(list_user, resultEvent.getDetails().getAdmins());
        assertEquals(list_user, resultEvent.getDetails().getCheckers());
    }

    /**
     * Shared stack specific
     */

    @Test
    public void thatSharedStackAreCreated() {
        // We create a shared stack 
        Stack stack = StacksFixtures.getStandardStack();
        stack.setIsShared(true);

        UUID stackUser = UUID.randomUUID();
        StackCreatedEvent event_mocked = new StackCreatedEvent(stack.getId(), stack.toDetails());
        String some_name = "some_name";

        // our stack is returned by the mock when findById is called
        when(persis_mocked.createStack(any(CreateStackEvent.class))).thenReturn(event_mocked);
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(stackUser, some_name, null, null));
        when(userService.updateUser(any(UpdateUserEvent.class))).thenReturn(new UserUpdatedEvent(new UserDetails(UUID.randomUUID())));

        // create event with a generated StackDetails i.e. with another UUID
        StackDetails details = StacksFixtures.getStandardStackDetails();
        details.setIsShared(true);
        CreateStackEvent event = new CreateStackEvent(details, stackUser, some_name);

        StackCreatedEvent resultEvent = eventHandler.createStack(event);

        // check that Persistence service use the same UUID
        ArgumentCaptor<CreateStackEvent> argument = ArgumentCaptor.forClass(CreateStackEvent.class);
        verify(persis_mocked).createStack(argument.capture());
        assertEquals(argument.getValue().getDetails().getId(), event.getDetails().getId());
        assertTrue(argument.getValue().getDetails().getAdmins().contains(stackUser));
        assertTrue(argument.getValue().getDetails().getCheckers().contains(stackUser));
        assertEquals(true, argument.getValue().getDetails().getIsShared());
        assertTrue(resultEvent.isCreated());
    }

    /**
     * Basic test for CreateStackEvent Processing
     */
    @Test
    public void thatStackCreation_failed_with_authuser_not_owner() {
        // We create a stack, UUID auto generated
        Stack stack = StacksFixtures.getStandardStack();
        UUID stackUser = UUID.randomUUID();
        String auth_user = "plop1";
        String owner_name = "plop2";
        StackCreatedEvent event_mocked = new StackCreatedEvent(stack.getId(), stack.toDetails());

        // our stack is returned by the mock when findById is called
        when(persis_mocked.createStack(any(CreateStackEvent.class))).thenReturn(event_mocked);
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(stackUser, owner_name, null, null));
        when(userService.updateUser(any(UpdateUserEvent.class))).thenReturn(new UserUpdatedEvent(new UserDetails(UUID.randomUUID())));

        // create event with a generated StackDetails i.e. with another UUID
        CreateStackEvent event = new CreateStackEvent(StacksFixtures.getStandardStackDetails(), stackUser, auth_user);

        eventHandler.createStack(event);

        verify(persis_mocked, times(0)).createStack(any(CreateStackEvent.class));

    }

    @Test
    public void thatStackUpdateEventAreProcessed() {
        // We create a stack, UUID auto generated
        Stack stack = StacksFixtures.getStandardStack();
        String user_name = "plopiii";
        UserDetails stack_owner = UsersFixtures.getStandardUserDetailsNoList(user_name);
        stack_owner.add_stack(stack.getId());
        StackUpdatedEvent event_mocked = new StackUpdatedEvent(stack.toDetails());

        // our stack is returned by the mock when findById is called
        when(persis_mocked.updateStack(any(UpdateStackEvent.class))).thenReturn(event_mocked);
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(stack_owner));
        when(userService.updateUser(any(UpdateUserEvent.class))).thenReturn(new UserUpdatedEvent(stack_owner));
        when(persis_mocked.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(new StackDetailsEvent(stack.toDetails()));

        // create event with a generated StackDetails i.e. with another UUID
        UpdateStackEvent event = new UpdateStackEvent(stack.toDetails(), stack_owner.getName());

        StackUpdatedEvent resultEvent = eventHandler.updateStack(event);

        // check that Persistence service use the same object
        verify(persis_mocked).updateStack(event);
        assertTrue(resultEvent.isUpdated());
    }

    // Three tests for DeleteStack event processing
    @Test
    public void thatStackDeleteEventAreProcessed() {
        // We create a stack, UUID auto generated
        Stack stack = StacksFixtures.getStandardStack();
        StackDeletedEvent mock_event = new StackDeletedEvent(stack.toDetails());

        // our stack is returned by the mock when findById is called
        when(persis_mocked.deleteStack(any(DeleteStackEvent.class))).thenReturn(mock_event);

        UserDetails user_stack_owner = UsersFixtures.getStandardUserDetailsNoList();
        user_stack_owner.add_stack(stack.getId());
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_stack_owner));
        when(userService.updateUser(any(UpdateUserEvent.class))).thenReturn(new UserUpdatedEvent(UsersFixtures.getStandardUserDetailsNoList()));

        // create event with a generated StackDetails i.e. with another UUID
        DeleteStackEvent event = new DeleteStackEvent(stack.getId(), UUID.randomUUID(), user_stack_owner.getName());

        StackDeletedEvent resultEvent = eventHandler.deleteStack(event);

        verify(userService, times(1)).getUserDetails(any(RequestUserDetailsEvent.class));
        verify(userService, times(1)).updateUser((any(UpdateUserEvent.class)));
        assertEquals(stack.getId(), resultEvent.getDetails().getId());
        assertTrue(resultEvent.isDeletionCompleted());
        assertTrue(resultEvent.isEntityFound());
    }

    // Three tests for DeleteStack event processing
    @Test
    public void thatStackDeleteEventAreProcessed_UserNotFound() {
        // We create a stack, UUID auto generated
        Stack stack = StacksFixtures.getStandardStack();
        StackDeletedEvent mock_event = new StackDeletedEvent(stack.toDetails());

        // our stack is returned by the mock when findById is called
        when(persis_mocked.deleteStack(any(DeleteStackEvent.class))).thenReturn(mock_event);
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(UserDetailsEvent.notFound(UsersFixtures.getStandardUserDetailsNoList().getId()));

        // create event with a generated StackDetails i.e. with another UUID
        DeleteStackEvent event = new DeleteStackEvent(stack.getId(), UUID.randomUUID());

        StackDeletedEvent resultEvent = eventHandler.deleteStack(event);

        verify(userService, times(1)).getUserDetails(any(RequestUserDetailsEvent.class));
        verify(userService, times(0)).updateUser((any(UpdateUserEvent.class)));
        assertEquals(stack.getId(), resultEvent.getDetails().getId());
        assertFalse(resultEvent.isDeletionCompleted());
        assertFalse(resultEvent.isEntityFound());
    }

    // Three tests for DeleteStack event processing
    @Test
    public void thatStackDeleteEventAreProcessed_UserFoundNotOwner() {
        // We create a stack, UUID auto generated
        Stack stack = StacksFixtures.getStandardStack();
        StackDeletedEvent mock_event = new StackDeletedEvent(stack.toDetails());

        // our stack is returned by the mock when findById is called
        when(persis_mocked.deleteStack(any(DeleteStackEvent.class))).thenReturn(mock_event);
        //user found but, own no stack
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(UsersFixtures.getStandardUserDetailsNoList()));

        // create event with a generated StackDetails i.e. with another UUID
        DeleteStackEvent event = new DeleteStackEvent(stack.getId(), UUID.randomUUID(), "some_user_not_use_because_of_the_mock");

        StackDeletedEvent resultEvent = eventHandler.deleteStack(event);

        verify(userService, times(1)).getUserDetails(any(RequestUserDetailsEvent.class));
        verify(userService, times(0)).updateUser((any(UpdateUserEvent.class)));
        assertEquals(stack.getId(), resultEvent.getDetails().getId());
        assertFalse(resultEvent.isDeletionCompleted());
    }

    @Test
    public void thatStackDeleteEventAreProcessed_StackNotFound() {
        // We create a stack, UUID auto generated
        Stack stack = StacksFixtures.getStandardStack();
        StackDeletedEvent event_mocked = StackDeletedEvent.notFound(stack.toDetails());
        // our stack is returned by the mock when findById is called
        when(persis_mocked.deleteStack(any(DeleteStackEvent.class))).thenReturn(event_mocked);
        //user found
        UserDetails user_for_mock = UsersFixtures.getStandardUserDetailsNoList();
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user_for_mock));

        // create event with a generated StackDetails i.e. with another UUID
        DeleteStackEvent event = new DeleteStackEvent(stack.getId(), user_for_mock.getId(), user_for_mock.getName());

        StackDeletedEvent resultEvent = eventHandler.deleteStack(event);

        assertFalse(resultEvent.isDeletionCompleted());
        assertFalse(resultEvent.isEntityFound());
    }

    // Test getAllStack operation
    @Test
    public void thatGetAllStackEventIsProcessed() {
        int nb = 4;
        List<StackDetails> list = StacksFixtures.getListStandardStack(nb);
        AllStacksEvent mocked_event = new AllStacksEvent(list);

        // our stack is returned by the mock when findById is called
        when(persis_mocked.getAllStacks(any(RequestAllStacksEvent.class))).thenReturn(mocked_event);

        // create event with a generated StackDetails i.e. with another UUID
        RequestAllStacksEvent event = new RequestAllStacksEvent();

        AllStacksEvent resultEvent = eventHandler.getAllStacks(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        assertEquals(resultEvent.getAllStackDetails().size(), nb);
    }

    /**
     * Test getAllStack operation
     * Check that:
     * - user service is called
     * - stack persistence service is called with the data provided by the user service layer
     */
    @Test
    public void thatGetAllStack_UsesListOfUserId_and_StackListFromUserService() {
        UserDetailsEvent userDetailsEvent = new UserDetailsEvent(UsersFixtures.getStandardUserDetails(1, 1));
        // return something if getUserDetails is called
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(userDetailsEvent);

        when(persis_mocked.getAllStacks(any(RequestAllStacksByIdEvent.class))).thenReturn(new AllStacksByIdEvent(new ArrayList<StackDetails>()));

        when(persis_mocked.getAllCheckableStacks(any(RequestAllStacksEvent.class))).thenReturn(new AllStacksEvent(new ArrayList<StackDetails>()));

        RequestAllStacksFromUserEvent stackServiceEvent = new RequestAllStacksFromUserEvent(UUID.randomUUID(), "plop");

        // ------- Call the tested method ------
        // verify that user service is used: getUserDetails
        // verify that stack persistence is called: getAllsStack( userid )
        AllStacksFromUserEvent resultEvent = eventHandler.getAllStacks(stackServiceEvent);
        // -------

        // Verify that user Service is called
        verify(userService, times(2)).getUserDetails(any(RequestUserDetailsEvent.class));

        // verify that the RequestAllStack event contains the list of stack return by the user service
        ArgumentCaptor<RequestAllStacksByIdEvent> argument = ArgumentCaptor.forClass(RequestAllStacksByIdEvent.class);
        verify(persis_mocked).getAllStacks(argument.capture());
        assertEquals(userDetailsEvent.getDetails().getList_stack(), argument.getValue().getStackid_list());
    }
    
    
    @Test
    public void thatGetAllStack_is_not_returning_duplicate_stacks_if_shared_stack_is_owned_bugfix() {
        UserDetailsEvent userDetailsEvent = new UserDetailsEvent(UsersFixtures.getStandardUserDetails(1, 1));

        // 2 stacks owned, one is shared.
        List<StackDetails> sharedStacks = StacksFixtures.getListStandardStack(1);
        List<StackDetails> ownedStacks = StacksFixtures.getListStandardStack(1);
        sharedStacks.add(ownedStacks.get(0));
        
        when(userService.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(userDetailsEvent);
        when(persis_mocked.getAllStacks(any(RequestAllStacksByIdEvent.class))).thenReturn(new AllStacksByIdEvent(ownedStacks));
        when(persis_mocked.getAllCheckableStacks(any(RequestAllStacksEvent.class))).thenReturn(new AllStacksEvent(sharedStacks));

        RequestAllStacksFromUserEvent stackServiceEvent = new RequestAllStacksFromUserEvent(UUID.randomUUID(), "plop");

        AllStacksFromUserEvent resultEvent = eventHandler.getAllStacks(stackServiceEvent);

        assertEquals(2, resultEvent.getStackDetailsList().size());
    }
    

    // Test getStackDetails operation
    @Test
    public void thatRequestStackDetailsEventIsProcessed() {
        Stack stack = StacksFixtures.getStandardStack();
        StackDetailsEvent event_mocked = new StackDetailsEvent(stack.toDetails());
        // our stack is returned by the mock when findById is called
        when(persis_mocked.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(event_mocked);

        // create event with a generated StackDetails i.e. with another UUID
        RequestStackDetailsEvent event = new RequestStackDetailsEvent(stack.getId());

        StackDetailsEvent resultEvent = eventHandler.getStackDetails(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        // TODO: not sure if comparison could work ??
        assertEquals(resultEvent.getDetails().getDescription(), stack.toDetails().getDescription());
        assertEquals(resultEvent.getDetails().getId(), stack.toDetails().getId());
        assertEquals(resultEvent.getDetails().getName(), stack.toDetails().getName());
        LOG.info("Result: " + resultEvent.getDetails().getDescription());

        // test on method called in repository
        //verify(repo_mocked).findById(stack.getId());
        //verifyNoMoreInteractions(repo_mocked);

    }

}
