package com.mystack.core.services;

import com.mystack.config.FunctionalConfiguration.ActionType;
import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.core.domain.Stack;
import com.mystack.core.domain.*;
import com.mystack.core.domain.fixtures.ChecksFixtures;
import com.mystack.core.domain.fixtures.StacksFixtures;
import com.mystack.core.domain.fixtures.UsersFixtures;
import com.mystack.core.events.checks.request.*;
import com.mystack.core.events.checks.result.*;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.request.UpdateStackEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.stacks.result.StackUpdatedEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.persistence.services.CheckPersistenceService;
import com.mystack.tool.MyPrincipal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


public class CheckEventHandlerTest {
	private static Logger LOG = LoggerFactory.getLogger(CheckEventHandlerTest.class);

	private CheckPersistenceService persMocked;
	private StackService stackServiceMocked;

	@Mock
	private UserService userServiceMocked;

	@InjectMocks
	private CheckEventHandler eventHandler;

	@BeforeEach
	public void init() {
		persMocked = mock(CheckPersistenceService.class);
		stackServiceMocked = mock(StackService.class);

		eventHandler = new CheckEventHandler(persMocked, stackServiceMocked);
		MockitoAnnotations.initMocks(this);
	}

	// Only function call are tested
	@Test
	public void that_CheckCreateEventAreProcessed() {
		//////////////// Duplicated with test below
		//////////////// to be put in a function
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date yesterday = cal.getTime();

		Stack stack_to_be_checked = StacksFixtures.getStandardStack(yesterday, 3, 2, yesterday);
		List<UUID> stack_list = new ArrayList<>();
		stack_list.add(stack_to_be_checked.getId());
		UserDetails stack_owner = new UserDetails(UUID.randomUUID(), "bob", new ArrayList<UUID>(), stack_list);
		///////////////////

		Check check = ChecksFixtures.getStandardCheck();
		check.setStack(stack_to_be_checked.getId());
		CheckCreatedEvent event_mocked = new CheckCreatedEvent(check.getId(), check.toDetails());

		// our check is returned by the mock when createUser is called
		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stack_to_be_checked.getId()));
		when(stackServiceMocked.updateStack(any(UpdateStackEvent.class)))
				.thenReturn(new StackUpdatedEvent(stack_to_be_checked.toDetails()));
		when(persMocked.createCheck(any(CreateCheckEvent.class))).thenReturn(event_mocked);
		when(persMocked.getLatestCheck(any(RequestLastChecksEvent.class)))
				.thenReturn(new CheckDetailsEvent(check.toDetails()));
		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(stack_owner));

		// create event with a generated StackDetails i.e. with another id
		Date check_initial_date = new Date(123456);
		CreateCheckEvent event = new CreateCheckEvent(ChecksFixtures.getStandardCheckDetails(
				stack_to_be_checked.getId(), "Check with a different date", check_initial_date), "none");

		CheckCreatedEvent resultEvent = eventHandler.createCheck(event);
		try {
			// verify that the check service is altering the date received in the check
			ArgumentCaptor<CreateCheckEvent> argument = ArgumentCaptor.forClass(CreateCheckEvent.class);
			verify(persMocked).createCheck(argument.capture());
			LOG.info("Date 1 " + check_initial_date + ", Date 2:" + argument.getValue().getDetails().getDate());
			assertEquals(check_initial_date, argument.getValue().getDetails().getDate());
			assertTrue(resultEvent.isCreated());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Only function call are tested
	// Two tests for check creation
	@Test
	public void that_StackIsVerified() {
		Check check = ChecksFixtures.getStandardCheck();
		UUID stack_id = check.getStack();

		CheckCreatedEvent event_mocked = new CheckCreatedEvent(check.getId(), check.toDetails());

		// our check is returned by the mock when createUser is called

		// when(pers_mocked.createCheck(any(CreateCheckEvent.class))).thenReturn(event_mocked);
		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(null);

		// create event with a generated StackDetails i.e. with another id
		CreateCheckEvent event = new CreateCheckEvent(ChecksFixtures.getStandardCheckDetails(), "none");

		CheckCreatedEvent resultEvent = eventHandler.createCheck(event);

		// check that Persistence service createUser method is called
		verify(stackServiceMocked).getStackDetails(any(RequestStackDetailsEvent.class));
		// check persistence service should not be called as the stack_id is unknown
		verifyZeroInteractions(persMocked);
		assertFalse(resultEvent.isCreated());
	}

	@Test
	public void that_check_creation_with_status_OK_cant_be_in_future() {
		final String username = "plop";
		final User userChecker = UsersFixtures.getStandardUserNoList(username);
		final StackDetails stackToCheck = StacksFixtures.getStandardStackLastCheckInThePast(2, 1).toDetails();
		stackToCheck.addChecker(userChecker.getId());
		final CheckDetails checkDetails = ChecksFixtures.getFuturePlannedCheckDetails();
		checkDetails.setStatus(CheckStatus.OK);

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stackToCheck));
		when(stackServiceMocked.updateStack(any(UpdateStackEvent.class)))
				.thenReturn(new StackUpdatedEvent(stackToCheck));

		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(userChecker.toDetails()));

		when(persMocked.createCheck(any(CreateCheckEvent.class)))
				.thenReturn(new CheckCreatedEvent(ChecksFixtures.getStandardCheck().toDetails()));

		// create event with a generated StackDetails i.e. with another id
		CreateCheckEvent event = new CreateCheckEvent(checkDetails, username);

		CheckCreatedEvent resultEvent = eventHandler.createCheck(event);

		// check that Persistence service createUser method is called
		verify(persMocked, times(0)).createCheck(any(CreateCheckEvent.class));
		verify(stackServiceMocked).getStackDetails(any(RequestStackDetailsEvent.class));
		verify(stackServiceMocked, times(0)).updateStack(any(UpdateStackEvent.class));

	}

	@Test
	public void that_check_creation_with_status_PLANNED_cant_be_created_in_the_past() {
		final String username = "plop";
		final User userChecker = UsersFixtures.getStandardUserNoList(username);
		final StackDetails stackToCheck = StacksFixtures.getStandardStackLastCheckInThePast(2, 1).toDetails();
		stackToCheck.addChecker(userChecker.getId());
		final CheckDetails checkDetails = ChecksFixtures.getPastOKCheckDetails();
		checkDetails.setStatus(CheckStatus.PLANNED);

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stackToCheck));
		when(stackServiceMocked.updateStack(any(UpdateStackEvent.class)))
				.thenReturn(new StackUpdatedEvent(stackToCheck));

		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(userChecker.toDetails()));

		when(persMocked.createCheck(any(CreateCheckEvent.class)))
				.thenReturn(new CheckCreatedEvent(ChecksFixtures.getStandardCheck().toDetails()));

		// create event with a generated StackDetails i.e. with another id
		CreateCheckEvent event = new CreateCheckEvent(checkDetails, username);

		eventHandler.createCheck(event);

		// check that Persistence service createUser method is called
		verify(persMocked, times(0)).createCheck(any(CreateCheckEvent.class));
		verify(stackServiceMocked).getStackDetails(any(RequestStackDetailsEvent.class));
		verify(stackServiceMocked, times(0)).updateStack(any(UpdateStackEvent.class));
	}

	@Test
	public void that_check_creation_with_status_PLANNED_dont_update_last_planned_if_not_last() {
		final String username = "plop";
		final User userChecker = UsersFixtures.getStandardUserNoList(username);
		final StackDetails stackToCheck = StacksFixtures.getStandardStackLastCheckInThePast(2, 1).toDetails();
		CheckDetails checkDetails = ChecksFixtures.getFuturePlannedCheckDetails();
		stackToCheck.addChecker(userChecker.getId());

		CheckDetails latestPlannedCheck = ChecksFixtures.getFuturePlannedCheckDetails();
		latestPlannedCheck.setDate(new Date(checkDetails.getDate().getTime() + 60000));
		stackToCheck.setLast_planned_check(latestPlannedCheck.getDate());

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stackToCheck));
		when(stackServiceMocked.updateStack(any(UpdateStackEvent.class)))
				.thenReturn(new StackUpdatedEvent(stackToCheck));

		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(userChecker.toDetails()));

		when(persMocked.createCheck(any(CreateCheckEvent.class))).thenReturn(new CheckCreatedEvent(checkDetails));
		when(persMocked.getLatestCheck(any(RequestLastChecksEvent.class)))
				.thenReturn(new CheckDetailsEvent(latestPlannedCheck));

		CreateCheckEvent event = new CreateCheckEvent(checkDetails, username);

		// the tested call
		eventHandler.createCheck(event);

		// check that Persistence service createUser method is called
		verify(persMocked).createCheck(any(CreateCheckEvent.class));
		// verify(persMocked).getLatestCheck(any(RequestLastChecksEvent.class));
		verify(stackServiceMocked).getStackDetails(any(RequestStackDetailsEvent.class));

		ArgumentCaptor<UpdateStackEvent> argument = ArgumentCaptor.forClass(UpdateStackEvent.class);
		verify(stackServiceMocked).updateStack(argument.capture());
		assertEquals(stackToCheck.getLast_planned_check(),
				argument.getValue().getStackDetails().getLast_planned_check());
		assertEquals(stackToCheck.getLast_check_count() + 1,
				argument.getValue().getStackDetails().getCheckCount().getPlanned());

		// check persistence service should not be called as the stack_id is unknown
		verifyZeroInteractions(persMocked);
	}

	@Test
	public void that_check_creation_with_status_SKIPPED_dont_update_last_check_date_planned_check() {
		final String username = "plop";
		final User userChecker = UsersFixtures.getStandardUserNoList(username);
		final StackDetails stackToCheck = StacksFixtures.getStandardStackLastCheckInThePast(2, 1).toDetails();
		stackToCheck.addChecker(userChecker.getId());
		CheckDetails checkDetails = ChecksFixtures.getFuturePlannedCheckDetails();
		checkDetails.setStatus(CheckStatus.SKIPPED);

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stackToCheck));
		when(stackServiceMocked.updateStack(any(UpdateStackEvent.class)))
				.thenReturn(new StackUpdatedEvent(stackToCheck));

		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(userChecker.toDetails()));

		when(persMocked.createCheck(any(CreateCheckEvent.class))).thenReturn(new CheckCreatedEvent(checkDetails));

		CreateCheckEvent event = new CreateCheckEvent(checkDetails, username);

		eventHandler.createCheck(event);

		// check that Persistence service createUser method is called
		verify(persMocked).createCheck(any(CreateCheckEvent.class));
		verify(stackServiceMocked).getStackDetails(any(RequestStackDetailsEvent.class));

		ArgumentCaptor<UpdateStackEvent> argument = ArgumentCaptor.forClass(UpdateStackEvent.class);
		verify(stackServiceMocked).updateStack(argument.capture());
		assertEquals(stackToCheck.getLast_planned_check(),
				argument.getValue().getStackDetails().getLast_planned_check());
		assertEquals(stackToCheck.getLast_check(), argument.getValue().getStackDetails().getLast_check());
		assertEquals(stackToCheck.getLast_check_count(), argument.getValue().getStackDetails().getCheckCount().getOk());
		assertEquals(stackToCheck.getCheckCount().getPlanned(),
				argument.getValue().getStackDetails().getCheckCount().getPlanned());
		assertEquals(1, argument.getValue().getStackDetails().getCheckCount().getSkipped());

		// check persistence service should not be called as the stack_id is unknown
		verifyZeroInteractions(persMocked);
	}

	static Stream<Arguments> lastCheckCountAndDateTestData() {
		return Stream.of(
				// count ok,pl,fa,sk,status, checkdelay,lok,lpl, expectcount ok,pl,lok,lpl
				Arguments.of(4, 2, 3, 0, CheckStatus.OK, 6, 5, 3, 5, 2, 6, 3),
				Arguments.of(4, 2, 3, 0, CheckStatus.OK, 4, 5, 3, 5, 2, 5, 3),
				Arguments.of(4, 2, 3, 0, CheckStatus.PLANNED, 49, 5, 3, 4, 3, 5, 49),
				Arguments.of(4, 2, 3, 0, CheckStatus.PLANNED, 49, 5, 60, 4, 3, 5, 60));
	}

	@ParameterizedTest
	@MethodSource("lastCheckCountAndDateTestData")
	public void that_check_creation_handle_counter_and_last_check_date(final int ok,
			final int planned, final int failed, final int skipped, CheckStatus status, final long checkDelay,
			long lastOkDelay, long lastPlannedDelay, final int expectedOkCount, final int expectedPlannedCount,
			final long expectedLastOkDelay, final long expectedLastPlannedDelay) {
		final String username = "plop";
		final long stackCreationHoursAgo = 48;

		final LocalDateTime stackCreationDate = LocalDateTime.now(ZoneId.systemDefault())
				.minusHours(stackCreationHoursAgo);
		final LocalDateTime checkDate = stackCreationDate.plusHours(checkDelay);

		final User userChecker = UsersFixtures.getStandardUserNoList(username);
		final CheckDetails check = ChecksFixtures.getStandardCheckDetails(status, checkDate);

		final LocalDateTime lastOkDateInitial = stackCreationDate.plusHours(lastOkDelay);
		final LocalDateTime lastPlannedDateInitial = stackCreationDate.plusHours(lastPlannedDelay);

		final LocalDateTime lastOkDateExpected = stackCreationDate.plusHours(expectedLastOkDelay);
		final LocalDateTime lastPlannedDateExpected = stackCreationDate.plusHours(expectedLastPlannedDelay);

		final CheckDetails lastOKCheckExpected = ChecksFixtures.getStandardCheckDetails(CheckStatus.OK,
				lastOkDateExpected);
		final CheckDetails lastPlannedCheckExpected = ChecksFixtures.getStandardCheckDetails(CheckStatus.PLANNED,
				lastPlannedDateExpected);

		final StackDetails stackToCheck = StacksFixtures
				.getStandardStack(stackCreationDate, lastOkDateInitial, lastPlannedDateInitial).toDetails();
		stackToCheck.addChecker(userChecker.getId());
		stackToCheck.setCheckCount(new CheckCount(ok, planned, failed, skipped));

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stackToCheck));
		when(stackServiceMocked.updateStack(any(UpdateStackEvent.class)))
				.thenReturn(new StackUpdatedEvent(stackToCheck));

		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(userChecker.toDetails()));

		when(persMocked.createCheck(any(CreateCheckEvent.class))).thenReturn(new CheckCreatedEvent(check));

		when(persMocked.getLatestCheck(Mockito.argThat(matchCheckStatus(CheckStatus.OK))))
				.thenReturn(new CheckDetailsEvent(lastOKCheckExpected));
		when(persMocked.getLatestCheck(Mockito.argThat(matchCheckStatus(CheckStatus.PLANNED))))
				.thenReturn(new CheckDetailsEvent(lastPlannedCheckExpected));

		// the tested call
		eventHandler.createCheck(new CreateCheckEvent(check, username));

		verify(persMocked).createCheck(any(CreateCheckEvent.class));
		verify(stackServiceMocked).getStackDetails(any(RequestStackDetailsEvent.class));
		ArgumentCaptor<UpdateStackEvent> argument = ArgumentCaptor.forClass(UpdateStackEvent.class);
		verify(stackServiceMocked).updateStack(argument.capture());

		assertEquals(expectedOkCount, argument.getValue().getStackDetails().getCheckCount().getOk());
		assertEquals(expectedPlannedCount, argument.getValue().getStackDetails().getCheckCount().getPlanned());

		assertEquals(lastOKCheckExpected.getDate(), argument.getValue().getStackDetails().getLast_check());
		assertEquals(lastPlannedCheckExpected.getDate(), argument.getValue().getStackDetails().getLast_planned_check());

		// check persistence service should not be called as the stack_id is unknown
		verifyZeroInteractions(persMocked);
	}

	@Test
	public void that_check_creation_for_sharedstack_is_possible_for_checkers_not_owner() {

		StackDetails stack_to_be_checked = StacksFixtures.getStandardStackDetails();

		// both user are not owner
		UserDetails checker = new UserDetails(UUID.randomUUID(), "bob", new ArrayList<UUID>(), new ArrayList<UUID>());
		UserDetails not_checker = new UserDetails(UUID.randomUUID(), "plop", new ArrayList<UUID>(),
				new ArrayList<UUID>());

		stack_to_be_checked.getIsShared();
		stack_to_be_checked.addChecker(checker.getId());

		CheckDetails some_check = ChecksFixtures.getStandardCheckDetails();
		some_check.setStack(stack_to_be_checked.getId());

		StackDetailsEvent stackDetailsEvent = new StackDetailsEvent(stack_to_be_checked);
		assertTrue(stackDetailsEvent.getDetails().getCreation_date() != null);

		// create event with a generated StackDetails i.e. with another id
		CheckDetails check_details_1 = ChecksFixtures.getStandardCheckDetails();
		CheckDetails check_details_2 = ChecksFixtures.getStandardCheckDetails();
		check_details_1.setStack(stack_to_be_checked.getId());
		check_details_2.setStack(stack_to_be_checked.getId());

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(stackDetailsEvent);
		when(stackServiceMocked.updateStack(any(UpdateStackEvent.class)))
				.thenReturn(new StackUpdatedEvent(stack_to_be_checked));

		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(checker), new UserDetailsEvent(not_checker));

		when(persMocked.createCheck(any(CreateCheckEvent.class))).thenReturn(new CheckCreatedEvent(check_details_1),
				new CheckCreatedEvent(check_details_2));
		when(persMocked.getAllChecks(any(RequestAllChecksEvent.class)))
				.thenReturn(new AllChecksEvent(new ArrayList<CheckDetails>()));
		when(persMocked.getLatestCheck(any(RequestLastChecksEvent.class)))
				.thenReturn(new CheckDetailsEvent(ChecksFixtures.getPastOKCheckDetails()));

		CheckCreatedEvent resultEvent1 = eventHandler.createCheck(new CreateCheckEvent(check_details_1, "check1"));
		CheckCreatedEvent resultEvent2 = eventHandler.createCheck(new CreateCheckEvent(check_details_2, "check2"));

		assertTrue(resultEvent1.isCreated());
		// TODO: verify if this test is well designed...
		assertFalse(resultEvent2.isCreated());

	}

	// Three tests for DeleteStack event processing
	@Test
	public void thatCheckDeleteEventAreProcessed() {
		// We create a stack, UUID auto generated
		Check check = ChecksFixtures.getStandardCheck();
		Stack stack = StacksFixtures.getStandardStack();
		User user = UsersFixtures.getStandardUserNoList();

		UserDetails user_details = user.toDetails();
		user_details.add_stack(stack.getId());
		user = User.fromDetails(user_details);
		check.setStack(stack.getId());

		CheckDeletedEvent mock_event = new CheckDeletedEvent(check.toDetails());

		// our stack is returned by the mock when findById is called
		when(persMocked.deleteCheck(any(DeleteCheckEvent.class))).thenReturn(mock_event);
		when(persMocked.getCheckDetails(any(RequestCheckDetailsEvent.class)))
				.thenReturn(new CheckDetailsEvent(check.toDetails()));
		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stack.toDetails()));
		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(user.toDetails()));

		// create event with a generated StackDetails i.e. with another UUID
		DeleteCheckEvent event = new DeleteCheckEvent(check.getId(), user.getName());

		CheckDeletedEvent resultEvent = eventHandler.deleteCheck(event);

		assertEquals(check.getId(), resultEvent.getDetails().getId());
		assertTrue(resultEvent.isDeletionCompleted());
		assertTrue(resultEvent.isEntityFound());
	}

	@Test
	public void thatCheckDeleteEventAreProcessed_UserNotFound() {
		// We create a user, UUID auto generated
		Check check = ChecksFixtures.getStandardCheck();
		CheckDeletedEvent event_mocked = CheckDeletedEvent.notFound(check.toDetails());
		// our stack is returned by the mock when findById is called
		when(persMocked.deleteCheck(any(DeleteCheckEvent.class))).thenReturn(event_mocked);

		// create event with a generated StackDetails i.e. with another UUID
		DeleteCheckEvent event = new DeleteCheckEvent(check.getId(), new MyPrincipal("plop").getName());

		CheckDeletedEvent resultEvent = eventHandler.deleteCheck(event);

		assertFalse(resultEvent.isDeletionCompleted());
		assertFalse(resultEvent.isEntityFound());
	}

	/*
	 * Not authorized not yet implemented
	 * 
	 * @Test public void thatStackDeleteEventAreProcessed_StackNotAuthorized() {
	 * Stack stack = StacksFixtures.getStandardStack();
	 * 
	 * // our stack is returned by the mock when findById is called
	 * when(repo_mocked.findById(any(UUID.class))).thenReturn(stack);
	 * 
	 * // create event with a generated StackDetails i.e. with another UUID
	 * DeleteStackEvent event = new DeleteStackEvent(stack.getId());
	 * 
	 * StackDeletedEvent resultEvent = eventHandler.deleteStack(event);
	 * 
	 * assertEquals(null, resultEvent.getDetails().getId());
	 * assertFalse(resultEvent.isDeletionCompleted());
	 * assertFalse(resultEvent.isEntityFound()); }
	 */

	// Test getAlUsers operation
	@Test
	public void thatGetAllCheckEventIsProcessed() {
		int nb_check = 4, nb_requested_check = 2;
		assertTrue(nb_check != nb_requested_check);

		List<CheckDetails> list = ChecksFixtures.getListStandardCheck(nb_check);
		AllChecksEvent mocked_event = new AllChecksEvent(list);

		// our stack is returned by the mock when findById is called
		when(persMocked.getAllChecks(any(RequestAllChecksEvent.class))).thenReturn(mocked_event);

		// create event with a generated StackDetails i.e. with another UUID
		RequestAllChecksEvent event = new RequestAllChecksEvent(UUID.randomUUID(), "", nb_requested_check);

		AllChecksEvent resultEvent = eventHandler.getAllChecks(event);

		// assert on event content
		assertTrue(resultEvent.isEntityFound());
	}

	// Test getUserDetails operation
	@Test
	public void thatRequestCheckDetailsEventIsProcessed() {
		Check check = ChecksFixtures.getStandardCheck();
		CheckDetailsEvent event_mocked = new CheckDetailsEvent(check.toDetails());
		// our stack is returned by the mock when findById is called
		when(persMocked.getCheckDetails(any(RequestCheckDetailsEvent.class))).thenReturn(event_mocked);

		// create event with a generated StackDetails i.e. with another UUID
		RequestCheckDetailsEvent event = new RequestCheckDetailsEvent(check.getId());

		CheckDetailsEvent resultEvent = eventHandler.getCheckDetails(event);

		// assert on event content
		assertTrue(resultEvent.isEntityFound());
		// TODO: not sure if comparison could work ??
		assertEquals(resultEvent.getDetails().getId(), check.toDetails().getId());
		assertEquals(resultEvent.getDetails().getComment(), check.toDetails().getComment());
		LOG.info("Result: " + resultEvent.getDetails().getComment());

		// test on method called in repository
		// verify(repo_mocked).findById(stack.getId());
		// verifyNoMoreInteractions(repo_mocked);

	}

	@Test
	public void that_UpdateCheckEvent_are_processed() {
		Check check = ChecksFixtures.getStandardCheck();
		Stack stack = StacksFixtures.getStandardStack(new Date(), 0, 0);
		User user = UsersFixtures.getStandardUserNoList();

		UserDetails user_details = user.toDetails();
		user_details.add_stack(stack.getId());
		user = User.fromDetails(user_details);
		check.setStack(stack.getId());

		LOG.info("User: " + user);
		LOG.info("Stack: " + stack);
		LOG.info("Check: " + check);

		CheckUpdatedEvent event_mocked = new CheckUpdatedEvent(check.toDetails());
		// our stack is returned by the mock when findById is called
		when(persMocked.updateCheck((any(UpdateCheckEvent.class)))).thenReturn(event_mocked);
		when(persMocked.getCheckDetails(any(RequestCheckDetailsEvent.class)))
				.thenReturn(new CheckDetailsEvent(check.toDetails()));

		when(persMocked.getLatestCheck(any(RequestLastChecksEvent.class)))
				.thenReturn(new CheckDetailsEvent(check.toDetails()));

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
				.thenReturn(new StackDetailsEvent(stack.toDetails()));
		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
				.thenReturn(new UserDetailsEvent(user.toDetails()));

		UpdateCheckEvent event = new UpdateCheckEvent(check.toDetails(), user.getName());

		CheckUpdatedEvent resultEvent = eventHandler.updateCheck(event);

		assertTrue(resultEvent.isUpdated());

	}

	@Test
	public void that_check_update_date_modification_triggers_stack_lastcheck_update() {
		Check check = ChecksFixtures.getStandardCheck();
		Stack stack = StacksFixtures.getStandardStack(new Date(), 1, 1);
		User user = UsersFixtures.getStandardUserNoList();

		UserDetails user_details = user.toDetails();
		user_details.add_stack(stack.getId());
		user = User.fromDetails(user_details);
		check.setStack(stack.getId());
		// setting check date after last_check property of the stack
		check.setDate(new Date(stack.getLast_check().getTime() + 6000));

		CheckUpdatedEvent updatedCheck = new CheckUpdatedEvent(check.toDetails(), ChecksFixtures.getStandardCheckDetails(CheckStatus.OK, LocalDateTime.now()));
		// our stack is returned by the mock when findById is called
		when(persMocked.updateCheck((any(UpdateCheckEvent.class))))
			.thenReturn(updatedCheck);
		when(persMocked.getCheckDetails(any(RequestCheckDetailsEvent.class)))
			.thenReturn(new CheckDetailsEvent(check.toDetails()));
		when(persMocked.getLatestCheck(any(RequestLastChecksEvent.class)))
			.thenReturn(new CheckDetailsEvent(check.toDetails()));

		when(userServiceMocked.getUserDetails(any(RequestUserDetailsEvent.class)))
			.thenReturn(new UserDetailsEvent(user.toDetails()));

		when(stackServiceMocked.getStackDetails(any(RequestStackDetailsEvent.class)))
			.thenReturn(new StackDetailsEvent(stack.toDetails()));

		ArgumentCaptor<UpdateStackEvent> argument = ArgumentCaptor.forClass(UpdateStackEvent.class);

		when(stackServiceMocked.updateStack(argument.capture()))
			.thenReturn(new StackUpdatedEvent(stack.toDetails()));

		CheckUpdatedEvent resultEvent = eventHandler.updateCheck(new UpdateCheckEvent(check.toDetails(), user.getName()));

		assertTrue(resultEvent.isUpdated());
		assertEquals(resultEvent.getDetails().getDate(), argument.getValue().getStackDetails().getLast_check());

	}

	static Stream<Arguments> creationAndDeletionTestData() {
		return Stream.of(Arguments.of(12, CheckStatus.OK, ActionType.CREATE, 13),
				Arguments.of(12, CheckStatus.FAILED, ActionType.CREATE, 13),
				Arguments.of(12, CheckStatus.SKIPPED, ActionType.CREATE, 13),
				Arguments.of(12, CheckStatus.PLANNED, ActionType.CREATE, 13),
				Arguments.of(12, CheckStatus.OK, ActionType.DELETE, 11),
				Arguments.of(12, CheckStatus.FAILED, ActionType.DELETE, 11),
				Arguments.of(12, CheckStatus.SKIPPED, ActionType.DELETE, 11),
				Arguments.of(12, CheckStatus.PLANNED, ActionType.DELETE, 11));
	}

	@ParameterizedTest
	@MethodSource("creationAndDeletionTestData")
	public void that_getStackCounterUpdateNeededAfterCreateOrDelete_unitTests(int count, CheckStatus status,
			ActionType action, int expected) {
		CheckCount initialCheckCount = new CheckCount(12, 6, 2, 4);
		initialCheckCount.setCount(status, count);

		StackDetails stack = StacksFixtures.getStandardStack(LocalDateTime.now(), LocalDateTime.now(),
				LocalDateTime.now(), 1, 2, initialCheckCount).toDetails();

		StackDetails result = eventHandler.getStackCounterUpdateNeededAfterCreateOrDelete(status, stack, action);

		assertEquals(expected, result.getCheckCount().getCount(status));
		verifyZeroInteractions(persMocked);
	}

	static Stream<Arguments> updateCounterTestData() {
		return Stream.of(
				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.OK,           CheckStatus.OK, new CheckCount(1, 2, 3, 4)),
				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.FAILED,   CheckStatus.FAILED, new CheckCount(1, 2, 3, 4)),
				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.PLANNED, CheckStatus.PLANNED, new CheckCount(1, 2, 3, 4)),
				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.SKIPPED, CheckStatus.SKIPPED, new CheckCount(1, 2, 3, 4)),

				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.OK,       CheckStatus.FAILED, new CheckCount(0, 2, 4, 4)),
				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.FAILED,       CheckStatus.OK, new CheckCount(2, 2, 2, 4)),
				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.PLANNED,  CheckStatus.FAILED, new CheckCount(1, 1, 4, 4)),
				Arguments.of(new CheckCount(1, 2, 3, 4), CheckStatus.PLANNED,      CheckStatus.OK, new CheckCount(2, 1, 3, 4)));
	}

	@ParameterizedTest
	@MethodSource("updateCounterTestData")
	public void that_getStackCounterUpdateNeededAfterUpdate_unitTests(CheckCount count, CheckStatus oldStatus, CheckStatus newStatus,
			CheckCount expectedCount) {

		StackDetails stack = StacksFixtures
				.getStandardStack(LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now(), 1, 2, count)
				.toDetails();

		StackDetails result = eventHandler.getStackCounterUpdateNeededAfterUpdate(newStatus, oldStatus, stack);

		assertEquals(expectedCount, result.getCheckCount());
		assertEquals(expectedCount.getTotal(), result.getCheckCount().getTotal());
		verifyZeroInteractions(persMocked);
	}

	static Stream<Arguments> lastCheckDateCreateDeleteTestData() {
		return Stream.of(
				// StackDetails stack, final CheckStatus status, final Date checkDate, final
				// ActionType action
				// ok, pl, ch, ... persi, ok, pl
				Arguments.of(10, 12, 13, ActionType.CREATE, CheckStatus.OK, false, 13, 12),
				Arguments.of(10, 12, 9, ActionType.CREATE, CheckStatus.OK, false, 10, 12),
				Arguments.of(10, 12, 13, ActionType.CREATE, CheckStatus.PLANNED, false, 10, 13),
				Arguments.of(10, 12, 9, ActionType.CREATE, CheckStatus.PLANNED, false, 10, 12),

				Arguments.of(10, 12, 8, ActionType.DELETE, CheckStatus.OK, false, 10, 12),
				Arguments.of(10, 12, 10, ActionType.DELETE, CheckStatus.OK, true, 0, 0),
				Arguments.of(10, 12, 12, ActionType.DELETE, CheckStatus.PLANNED, true, 0, 0));
	}

	@ParameterizedTest
	@MethodSource("lastCheckDateCreateDeleteTestData")
	public void that_stackUpdateRequiredForLastCheckDate(long lastOKCheckDelay, long lastPlannedCheckDelay,
			long checkDelay, ActionType action, CheckStatus status, boolean persistenceInteraction,
			long expectedOKDelay, long expectedPlannedDelay) {

		LocalDateTime stackCreationTime = LocalDateTime.now();
		LocalDateTime lastOK = stackCreationTime.plusHours(lastOKCheckDelay);
		LocalDateTime lastPlanned = stackCreationTime.plusHours(lastPlannedCheckDelay);
		Date expectedOK = Date.from(stackCreationTime.plusHours(expectedOKDelay).toInstant(ZoneOffset.UTC));
		Date expectedPlanned = Date.from(stackCreationTime.plusHours(expectedPlannedDelay).toInstant(ZoneOffset.UTC));
		LocalDateTime checkDate = stackCreationTime.plusHours(checkDelay);
		Date checkDateOld = Date.from(checkDate.toInstant(ZoneOffset.UTC));

		when(persMocked.getLatestCheck(Mockito.argThat(matchCheckStatus(CheckStatus.OK))))
				.thenReturn(new CheckDetailsEvent(ChecksFixtures.getOKCheckDetails(expectedOK)));
		when(persMocked.getLatestCheck(Mockito.argThat(matchCheckStatus(CheckStatus.PLANNED))))
				.thenReturn(new CheckDetailsEvent(ChecksFixtures.getPlannedCheckDetails(expectedPlanned)));

		StackDetails stack = StacksFixtures.getStandardStack(stackCreationTime, lastOK, lastPlanned).toDetails();

		StackDetails result = eventHandler.getStackUpdateRequiredForLastCheckDateAfterCreateOrDelete(stack, status,
				checkDateOld, action);

		if (persistenceInteraction) {
			ArgumentCaptor<RequestLastChecksEvent> argument = ArgumentCaptor.forClass(RequestLastChecksEvent.class);
			verify(persMocked).getLatestCheck(argument.capture());
			assertTrue(status.equals(argument.getValue().getCheckStatus()));
		} else {
			assertEquals(expectedOK, result.getLast_check());
			assertEquals(expectedPlanned, result.getLast_planned_check());
		}
		verifyZeroInteractions(persMocked);
	}
	
	
	static Stream<Arguments> lastCheckUpdateDateTestData() {
		return Stream.of(
				// StackDetails stack, final CheckStatus status, final Date checkDate, final
				// ActionType action
				// lok, lpl,    previous ch, status           new ch, status,       expected stack props: lok, lpl
				Arguments.of(11, 12,   10, CheckStatus.OK, 10, CheckStatus.PLANNED,                11, 12),
				Arguments.of(11, 12,   11, CheckStatus.OK, 10, CheckStatus.PLANNED,                 8, 12),
				Arguments.of(11, 12,   10, CheckStatus.OK, 13, CheckStatus.PLANNED,                11, 13));
	}

	@ParameterizedTest
	@MethodSource("lastCheckUpdateDateTestData")
	public void that_stackUpdateRequiredForLastCheckDate_Update(long lastOKCheckDelay, long lastPlannedCheckDelay,
			long oldCheckDelay, CheckStatus oldStatus, long newCheckDelay, CheckStatus newStatus,
			long expectedOKDelay, long expectedPlannedDelay) {

		LocalDateTime stackCreationTime = LocalDateTime.now().minusHours(48);

		LocalDateTime lastOK = stackCreationTime.plusHours(lastOKCheckDelay);
		LocalDateTime lastPlanned = stackCreationTime.plusHours(lastPlannedCheckDelay);

		Date expectedOK = Date.from(stackCreationTime.plusHours(expectedOKDelay).toInstant(ZoneOffset.UTC));
		Date expectedPlanned = Date.from(stackCreationTime.plusHours(expectedPlannedDelay).toInstant(ZoneOffset.UTC));

		LocalDateTime newCheckDate = stackCreationTime.plusHours(newCheckDelay);
		LocalDateTime oldCheckDate = stackCreationTime.plusHours(oldCheckDelay);

		when(persMocked.getLatestCheck(Mockito.argThat(matchCheckStatus(CheckStatus.OK))))
				.thenReturn(new CheckDetailsEvent(ChecksFixtures.getOKCheckDetails(expectedOK)));
		when(persMocked.getLatestCheck(Mockito.argThat(matchCheckStatus(CheckStatus.PLANNED))))
				.thenReturn(new CheckDetailsEvent(ChecksFixtures.getPlannedCheckDetails(expectedPlanned)));

		StackDetails stack = StacksFixtures.getStandardStack(stackCreationTime, lastOK, lastPlanned).toDetails();

		StackDetails result = eventHandler.getStackUpdateRequiredForLastCheckDateAfterUpdate(stack, oldStatus,
				newStatus, oldCheckDate, newCheckDate);

		assertEquals(expectedOK, result.getLast_check());
		assertEquals(expectedPlanned, result.getLast_planned_check());
		//verifyZeroInteractions(persMocked);
	}
	
	

	private ArgumentMatcher<RequestLastChecksEvent> matchCheckStatus(final CheckStatus status) {
		return event -> event != null && status.equals(event.getCheckStatus());
	}
}
