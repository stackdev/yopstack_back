package com.mystack.core.services;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.mystack.core.domain.User;
import com.mystack.core.domain.UserDetails;
import com.mystack.core.domain.fixtures.UsersFixtures;
import com.mystack.core.events.stacks.request.RequestStackDetailsEvent;
import com.mystack.core.events.stacks.result.StackDetailsEvent;
import com.mystack.core.events.users.request.CreateUserEvent;
import com.mystack.core.events.users.request.DeleteUserEvent;
import com.mystack.core.events.users.request.RequestAllUsersEvent;
import com.mystack.core.events.users.request.RequestUserDetailsEvent;
import com.mystack.core.events.users.request.UpdateUserEvent;
import com.mystack.core.events.users.result.AllUsersEvent;
import com.mystack.core.events.users.result.UserCreatedEvent;
import com.mystack.core.events.users.result.UserDeletedEvent;
import com.mystack.core.events.users.result.UserDetailsEvent;
import com.mystack.core.events.users.result.UserUpdatedEvent;
import com.mystack.persistence.services.UserPersistenceEventHandler;

public class UserEventHandlerUnitTest {
    private static Logger               LOG = LoggerFactory.getLogger(UserEventHandlerUnitTest.class);

    private UserPersistenceEventHandler user_pers_mocked;

    @Mock
    private StackService                stack_service_mocked;

    @Mock
    private PasswordEncoder             password_encoder;

    @InjectMocks
    private UserEventHandler            eventHandler;

    @Before
    public void init() {
        user_pers_mocked = mock(UserPersistenceEventHandler.class);
        eventHandler = new UserEventHandler(user_pers_mocked);
        MockitoAnnotations.initMocks(this);
        // return the provided string in input as encoding result
        when(password_encoder.encode(any(String.class))).
                thenAnswer(new Answer<String>() {
                    public String answer(InvocationOnMock invocation) {
                        Object arg0 = invocation.getArguments()[0];
                        if (null != arg0)
                            return arg0.toString();
                        return null;
                    }
                });
        when(password_encoder.matches(any(String.class), any(String.class))).
                thenAnswer(new Answer<Boolean>() {
                    public Boolean answer(InvocationOnMock invocation) {
                        Object arg0 = invocation.getArguments()[0];
                        Object arg1 = invocation.getArguments()[1];
                        if (null == arg0 || null == arg1)
                            return false;
                        if (arg0.equals(arg1))
                            return true;
                        return false;
                    }
                });

    }

    // Only function call are tested
    // Two tests for User creation
    /**
     * Basic test for CreateUserEvent Processing
     */
    @Test
    public void that_UserCreateEventAreProcessed() {
        // We create a user, UUID auto generated
        User user = UsersFixtures.getStandardUserNoList();
        // User user = PersistenceUserFixture.standardUser(2, 3);
        UserCreatedEvent event_mocked = new UserCreatedEvent(user.getId(), user.toDetails());

        // our user is returned by the mock when createUser is called

        when(user_pers_mocked.createUser(any(CreateUserEvent.class))).thenReturn(event_mocked);

        // create event with a generated StackDetails i.e. with another id
        CreateUserEvent event = new CreateUserEvent(UsersFixtures.getStandardUserDetailsNoList());

        UserCreatedEvent resultEvent = eventHandler.createUser(event);

        // check that Persistence service createUser method is called
        ArgumentCaptor<CreateUserEvent> argument = ArgumentCaptor.forClass(CreateUserEvent.class);
        verify(user_pers_mocked).createUser(argument.capture());
        assertTrue(event.getDetails().equals(argument.getValue().getDetails()));
        assertTrue(resultEvent.isCreated());
    }

    // Three tests for DeleteStack event processing
    @Test
    public void thatUserDeleteEventAreProcessed() {
        // We create a stack, UUID auto generated
        User user = UsersFixtures.getStandardUserNoList();
        UserDeletedEvent mock_event = new UserDeletedEvent(user.toDetails());

        // our stack is returned by the mock when findById is called
        when(user_pers_mocked.deleteUser(any(DeleteUserEvent.class))).thenReturn(mock_event);

        // create event with a generated StackDetails i.e. with another UUID
        DeleteUserEvent event = new DeleteUserEvent(user.getId());

        UserDeletedEvent resultEvent = eventHandler.deleteUser(event);

        assertEquals(user.getId(), resultEvent.getDetails().getId());
        assertTrue(resultEvent.isDeletionCompleted());
        assertTrue(resultEvent.isEntityFound());
    }

    @Test
    public void thatUserDeleteEventAreProcessed_UserNotFound() {
        // We create a user, UUID auto generated
        User user = UsersFixtures.getStandardUserNoList();
        UserDeletedEvent event_mocked = UserDeletedEvent.notFound(user.toDetails());
        // our stack is returned by the mock when findById is called
        when(user_pers_mocked.deleteUser(any(DeleteUserEvent.class))).thenReturn(event_mocked);

        // create event with a generated StackDetails i.e. with another UUID
        DeleteUserEvent event = new DeleteUserEvent(user.getId());

        UserDeletedEvent resultEvent = eventHandler.deleteUser(event);

        assertFalse(resultEvent.isDeletionCompleted());
        assertFalse(resultEvent.isEntityFound());
    }

    /**
     * Verify that the list of stack provided is verified and corrected StackPersistence layer methods must be called
     */
    @Test
    public void that_StackListIsVerified() {
        int nb_stack = 3;
        // We create a user, UUID auto generated
        User user = UsersFixtures.getStandardUserList(0, nb_stack);

        System.out.println("User: " + user);

        UserCreatedEvent event_mocked = new UserCreatedEvent(user.getId(), user.toDetails());

        // our user is returned by the mock when createUser is called

        when(stack_service_mocked.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(StackDetailsEvent.notFound(UUID.randomUUID()));

        CreateUserEvent event = new CreateUserEvent(user.toDetails());

        UserCreatedEvent resultEvent = eventHandler.createUser(event);

        // check that Persistence service createUser method is called
        verify(user_pers_mocked, times(1)).createUser(any(CreateUserEvent.class));
        verify(stack_service_mocked, times(nb_stack)).getStackDetails(any(RequestStackDetailsEvent.class));
        // We don't check result event as it is mocked
        // assertTrue(resultEvent.isCreated());
    }

    /**
     * Verify that the list of stack provided is verified and corrected StackPersistence layer methods must be called
     */
    @Test
    public void that_StackListIsVerified_EmptyList() {
        int nb_stack = 0;
        // We create a user, UUID auto generated
        User user = UsersFixtures.getStandardUserList(0, nb_stack);

        System.out.println("User: " + user);

        UserCreatedEvent event_mocked = new UserCreatedEvent(user.getId(), user.toDetails());

        // our user is returned by the mock when createUser is called

        // when(user_pers_mocked.createUser(any(CreateUserEvent.class))).thenReturn(event_mocked);
        when(stack_service_mocked.getStackDetails(any(RequestStackDetailsEvent.class))).thenReturn(StackDetailsEvent.notFound(UUID.randomUUID()));

        CreateUserEvent event = new CreateUserEvent(user.toDetails());

        UserCreatedEvent resultEvent = eventHandler.createUser(event);

        // check that Persistence service createUser method is called
        ArgumentCaptor<CreateUserEvent> argument = ArgumentCaptor.forClass(CreateUserEvent.class);
        verify(user_pers_mocked).createUser(argument.capture());
        assertTrue(event.getDetails().equals(argument.getValue().getDetails()));

        verify(stack_service_mocked, times(nb_stack)).getStackDetails(any(RequestStackDetailsEvent.class));
        // We don't check result event as it is mocked
        // assertTrue(resultEvent.isCreated());
    }

    /*
     * Not authorized not yet implemented
     * 
     * @Test public void thatStackDeleteEventAreProcessed_StackNotAuthorized() { Stack stack = StacksFixtures.getStandardStack();
     * 
     * // our stack is returned by the mock when findById is called when(repo_mocked.findById(any(UUID.class))).thenReturn(stack);
     * 
     * // create event with a generated StackDetails i.e. with another UUID DeleteStackEvent event = new DeleteStackEvent(stack.getId());
     * 
     * StackDeletedEvent resultEvent = eventHandler.deleteStack(event);
     * 
     * assertEquals(null, resultEvent.getDetails().getId()); assertFalse(resultEvent.isDeletionCompleted()); assertFalse(resultEvent.isEntityFound()); }
     */

    // Test getAlUsers operation
    @Test
    public void thatGetAllUserEventIsProcessed() {
        int nb = 4;
        List<UserDetails> list = UsersFixtures.getListStandardUser(nb);
        AllUsersEvent mocked_event = new AllUsersEvent(list);

        // our stack is returned by the mock when findById is called
        when(user_pers_mocked.getAllUsers(any(RequestAllUsersEvent.class))).thenReturn(mocked_event);

        // create event with a generated StackDetails i.e. with another UUID
        RequestAllUsersEvent event = new RequestAllUsersEvent();

        AllUsersEvent resultEvent = eventHandler.getAllUsers(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        assertEquals(resultEvent.getAllUserDetails().size(), nb);
    }

    // Test getUserDetails operation
    @Test
    public void thatRequestUserDetailsEventIsProcessed() {
        User user = UsersFixtures.getStandardUserNoList();
        UserDetailsEvent event_mocked = new UserDetailsEvent(user.toDetails());
        // our stack is returned by the mock when findById is called
        when(user_pers_mocked.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(event_mocked);

        // create event with a generated StackDetails i.e. with another UUID
        RequestUserDetailsEvent event = new RequestUserDetailsEvent(user.getId());

        UserDetailsEvent resultEvent = eventHandler.getUserDetails(event);

        // assert on event content
        assertTrue(resultEvent.isEntityFound());
        // TODO: not sure if comparison could work ??
        assertEquals(resultEvent.getDetails().getId(), user.toDetails().getId());
        assertEquals(resultEvent.getDetails().getName(), user.toDetails().getName());
        LOG.info("Result: " + resultEvent.getDetails().getName());
    }

    @Test
    public void that_UserUpdateEventAreProcessed() {
        // We create a user, UUID auto generated
        User user = UsersFixtures.getStandardUserNoList();
        // User user = PersistenceUserFixture.standardUser(2, 3);
        UserUpdatedEvent event_mocked = new UserUpdatedEvent(user.toDetails());
        UserDetailsEvent event_details_mocked = new UserDetailsEvent(user.toDetails());

        when(user_pers_mocked.updateUser(any(UpdateUserEvent.class))).thenReturn(event_mocked);
        when(user_pers_mocked.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(event_details_mocked);

        // update event forcing mock event name and new details to be identical, for user vs principal check
        UserDetails details = UsersFixtures.getStandardUserDetailsNoList();
        details.setName(event_details_mocked.getDetails().getName());
        UpdateUserEvent event = new UpdateUserEvent(details, details.getName());

        UserUpdatedEvent resultEvent = eventHandler.updateUser(event);

        // check that Persistence service createUser method is called
        verify(user_pers_mocked).updateUser(any(UpdateUserEvent.class));
        assertTrue(resultEvent.isUpdated());
    }

    @Test
    public void that_UserUpdate_preserve_password() {
        // We create a user, UUID auto generated
        String initial_password = UUID.randomUUID().toString();
        List<String> initial_roles = new ArrayList<>();
        initial_roles.add("plop");
        boolean initial_enabled = true;
        User initial_user = UsersFixtures.getStandardUserNoList();
        // copy user and remove password
        com.mystack.rest.domain.User user_for_update = com.mystack.rest.domain.User.fromDetails(initial_user.toDetails());

        initial_user.setPassword(initial_password);
        initial_user.setEnabled(initial_enabled);
        initial_user.setRoles(initial_roles);

        UserUpdatedEvent event_mocked = new UserUpdatedEvent(initial_user.toDetails());
        UserDetailsEvent event_details_mocked = new UserDetailsEvent(initial_user.toDetails());

        when(user_pers_mocked.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(event_details_mocked);
        when(user_pers_mocked.updateUser(any(UpdateUserEvent.class))).thenReturn(event_mocked);

        UserUpdatedEvent resultEvent = eventHandler.updateUser(new UpdateUserEvent(user_for_update.toDetails(), user_for_update.getName()));

        ArgumentCaptor<UpdateUserEvent> argument = ArgumentCaptor.forClass(UpdateUserEvent.class);

        // check that Persistence service createUser method is called
        verify(user_pers_mocked).updateUser(argument.capture());
        assertTrue(resultEvent.isUpdated());
        assertTrue(argument.getValue().getUserDetails().getPassword() == initial_password);
        assertTrue(argument.getValue().getUserDetails().getRoles() == initial_roles);
        assertTrue(argument.getValue().getUserDetails().getEnabled() == initial_enabled);

    }

    @Test
    public void that_UserCannotUpdateAnotherUser() {
        String username1 = "user1", username2 = "user2";
        String password1 = "pass1", password2 = "pass2";

        // We create 2 user, UUID auto generated
        User user1 = UsersFixtures.getStandardUserNoList(username1, password1);
        User user2 = UsersFixtures.getStandardUserNoList(username2, password2);

        // User user = PersistenceUserFixture.standardUser(2, 3);
        UserUpdatedEvent event_mocked = new UserUpdatedEvent(user1.toDetails());

        // persistence is called to retrieve user details first
        when(user_pers_mocked.getUserDetails(any(RequestUserDetailsEvent.class))).thenReturn(new UserDetailsEvent(user1.toDetails()));

        when(user_pers_mocked.updateUser(any(UpdateUserEvent.class))).thenReturn(event_mocked);

        // update event with a the details
        UserUpdatedEvent resultEvent = eventHandler.updateUser(new UpdateUserEvent(user1.toDetails(), user2.getName()));
        verify(user_pers_mocked, times(0)).updateUser(any(UpdateUserEvent.class));
        assertFalse(resultEvent.isUpdated());

        // 2nd update attempt with correct user but wrong old password
        UserDetails user_for_update = user1.toDetails();
        user_for_update.setSetpw("new password");
        user_for_update.setOldpw("wrong password");
        UpdateUserEvent event = new UpdateUserEvent(user_for_update, user1.getName());
        resultEvent = eventHandler.updateUser(event);
        verify(user_pers_mocked, times(0)).updateUser(event);
        assertFalse(resultEvent.isUpdated());

        // 3rd update attempt with correct user and correct old password
        user_for_update = user1.toDetails();
        user_for_update.setSetpw("new password");
        user_for_update.setOldpw(password1);
        event = new UpdateUserEvent(user_for_update, user1.getName());
        resultEvent = eventHandler.updateUser(event);
        verify(user_pers_mocked, times(1)).updateUser(any(UpdateUserEvent.class));
        assertTrue(resultEvent.isUpdated());

    }

    //TODO: test non update of user id and name
}