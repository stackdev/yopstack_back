package com.mystack.core.domain.fixtures;

import java.util.UUID;

import com.mystack.core.domain.Preference;

public class PreferencesFixtures {

    public static Preference getRandomPreference() {
        return new Preference(UUID.randomUUID(), UUID.randomUUID());
    }

}
