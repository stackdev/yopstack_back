package com.mystack.core.domain.fixtures;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mystack.core.domain.User;
import com.mystack.core.domain.UserDetails;
import com.mystack.tool.Tool;

public class UsersFixtures {

    public static final String name        = "user212";
    public static final String description = "test user !!!";

    public static User getStandardUserNoList(String username) {
        String password = username + "_password";
        return getStandardUserNoList(username, password);
    }

    public static User getStandardUserNoList(String username, String password) {
        if (null == username || "".equals(username)) {
            username = name + "_" + UUID.randomUUID();
        }
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");
        return new User(UUID.randomUUID(), username, new ArrayList<UUID>(), new ArrayList<UUID>(), password, true, roles);
    }

    public static User getStandardUserNoList() {
        return getStandardUserNoList(null);
    }

    public static UserDetails getStandardUserDetailsNoList() {
        return getStandardUserDetailsNoList(null);
    }

    public static UserDetails getStandardUserDetailsNoList(String user_name) {
        return getStandardUserNoList(user_name).toDetails();
    }

    public static UserDetails getStandardUserDetails(int friend_nb, int stack_nb) {
        return getStandardUserList(friend_nb, stack_nb).toDetails();
    }

    public static User getStandardUserList(int friend_nb, int stack_nb) {
        User user = new User(UUID.randomUUID(), name, null, null);
        user.setList_friend(Tool.getRandomUUIDList(friend_nb));
        user.setList_stack(Tool.getRandomUUIDList(stack_nb));
        return user;
    }

    public static List<UserDetails> getListStandardUser(int nb) {

        return getListStandardUser(nb, 0, 0);
    }

    public static List<UserDetails> getListStandardUser(int nb, int nb_stack, int nb_friend) {
        if (nb <= 0) {
            return null;
        }
        List<UserDetails> output = new ArrayList<UserDetails>();
        UserDetails tmp_details;
        for (int i = 0; i < nb; i++) {
            tmp_details = getStandardUserDetails(nb_friend, nb_stack);
            tmp_details.setName(String.format("list_user_%d", i));
            output.add(tmp_details);
        }
        return output;
    }
}
