package com.mystack.core.domain.fixtures;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;


import com.mystack.config.FunctionalConfiguration;
import com.mystack.core.domain.CheckCount;
import com.mystack.core.domain.Stack;
import com.mystack.core.domain.StackDetails;

public class StacksFixtures {

    private static String getName() {
        return UUID.randomUUID().toString();
    }

    private static String getDescription() {
        return "Some description: " + getName();
    }

    public static Stack getStandardStack() {
        return getStandardStack(new Date(), 0, 0);
    }

    public static Stack getStandardStack(Date last_check, int periodicity, int frequency) {
        return getStandardStack(last_check, periodicity, frequency, new Date());
    }
    
    public static Stack getStandardStackLastCheckInThePast(int periodicity, int frequency) {
    	Calendar fiveDaysAgo = Calendar.getInstance();
		fiveDaysAgo.add(Calendar.DATE, -5);
		
		Calendar tenDaysAgo = Calendar.getInstance();
		tenDaysAgo.add(Calendar.DATE, -10);
		
        return getStandardStack(fiveDaysAgo.getTime(), periodicity, frequency, tenDaysAgo.getTime());
    }
    
    
    public static Stack getStandardStack(LocalDateTime creationDate, LocalDateTime lastOkCheck, LocalDateTime lastPlannedCheck) {
    	return getStandardStack(creationDate, lastOkCheck, lastPlannedCheck, FunctionalConfiguration.DEFAULT_STACK_PERIODICITY, 
    							FunctionalConfiguration.DEFAULT_STACK_FREQUENCY,  new CheckCount(0,0,0,0));
    }
    
    /*
     * use getStandardStack(LocalDateTime creationDate, LocalDateTime lastOkCheck, LocalDateTime lastPlannedCheck) instead
     */
    @Deprecated
    public static Stack getStandardStack(Date lastCheck, int periodicity, int frequency, Date creationDate) {
    	
    	LocalDateTime creationDateNew = LocalDateTime.ofInstant(creationDate.toInstant(), ZoneId.systemDefault());
    	LocalDateTime lastOkCheckNew = LocalDateTime.ofInstant(lastCheck.toInstant(), ZoneId.systemDefault());
    	LocalDateTime lastPlannedCheckNew = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault());
      	
    	return getStandardStack(creationDateNew, lastOkCheckNew, lastPlannedCheckNew, periodicity, frequency,  new CheckCount(0,0,0,0));
    }
    
    public static Stack getStandardStack(LocalDateTime creationDate, LocalDateTime lastOkCheck, LocalDateTime lastPlannedCheck, int periodicity, int frequency, CheckCount checkCount) {
        Stack stack = Stack.fromDetails(new StackDetails(UUID.randomUUID(), getName(), getDescription()));
        if (null != lastOkCheck) {
            stack.setLast_check(Date.from(lastOkCheck.toInstant(ZoneOffset.UTC)));
        }
        if (null != lastPlannedCheck) {
            stack.setLast_planned_check(Date.from(lastPlannedCheck.toInstant(ZoneOffset.UTC)));
        }
        if (periodicity > 0) {
            stack.setPeriodicity(periodicity);
        }
        if (frequency > 0) {
            stack.setFrequency(frequency);
        }
        if (null == creationDate)
            creationDate = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault());

        stack.setCreation_date(Date.from(creationDate.toInstant(ZoneOffset.UTC)));
        stack.setCheckCount(checkCount);

        return stack;
    }

    public static StackDetails getStandardStackDetails() {
        return getStandardStack().toDetails();
    }

    public static List<StackDetails> getListStandardStack(int nb) {
        if (nb <= 0) {
            return null;
        }
        List<StackDetails> output = new ArrayList<StackDetails>();
        for (int i = 0; i < nb; i++) {
            output.add((new StackDetails(UUID.randomUUID(), nb + getName(), nb + getDescription())));
        }
        return output;
    }

}
