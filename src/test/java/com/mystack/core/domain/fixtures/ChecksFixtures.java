package com.mystack.core.domain.fixtures;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.net.ssl.SSLEngineResult.Status;

import com.mystack.config.FunctionalConfiguration.CheckStatus;
import com.mystack.core.domain.Check;
import com.mystack.core.domain.CheckDetails;

public class ChecksFixtures {

    public static final String comment = "my comment Check domain";

    public static Check getStandardCheck(UUID id, UUID stackId, String comment, Date date) {
        Check check = new Check();
        check.setId(id);
        check.setDate(date);
        check.setStack(stackId);
        check.setComment(comment);
        return check;
    }

    public static Check getStandardCheck() {
        return getStandardCheck(UUID.randomUUID(), UUID.randomUUID(), comment, new Date());
    }

    public static CheckDetails getStandardCheckDetails() {
        return getStandardCheckDetails("Some fixtured check", new Date());
    }

    public static CheckDetails getFuturePlannedCheckDetails() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, 1);
    	final Date futureDate = calendar.getTime();
        
    	CheckDetails details = getPlannedCheckDetails(futureDate);
        
        return details;
    }

    
    public static CheckDetails getPastOKCheckDetails() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DATE, -1);
    	final Date pastDate = calendar.getTime();
        
    	CheckDetails details = getOKCheckDetails(pastDate);
        
        return details;
    }
    
    public static CheckDetails getPlannedCheckDetails(final Date date) {
    	CheckDetails details = getStandardCheckDetails("A planned check", date);
    	details.setStatus(CheckStatus.PLANNED);
    	return details;
    }

	public static CheckDetails getOKCheckDetails(final Date date) {
		CheckDetails details = getStandardCheckDetails("An OK check", date);
        details.setStatus(CheckStatus.OK);
		return details;
	}
    
    public static CheckDetails getStandardCheckDetails(UUID stackId, String comment, Date date) {
        List<CheckDetails> list_details = getListStandardCheck(1, comment, date);

        CheckDetails details = list_details.get(0);
        if (null != stackId) {
            details.setStack(stackId);
        }
        return details;
    }
    
    public static CheckDetails getStandardCheckDetails(CheckStatus status, LocalDateTime date) {
    	//FIXME: all java.util.date have to be converted to LocalDateTime eventually
    	Date dateConverted = Date.from(date.toInstant(ZoneOffset.UTC));
        CheckDetails details = getStandardCheckDetails(null, "Some check comment", dateConverted);
        details.setStatus(status);
        return details;
    }

    public static CheckDetails getStandardCheckDetails(String comment, Date date) {
        return getStandardCheckDetails(null, comment, date);
    }

    public static List<CheckDetails> getListStandardCheck(int nb) {
        return getListStandardCheck(nb, null, null);
    }

    public static List<CheckDetails> getListStandardCheck(int nb, String comment, Date date) {
        if (nb <= 0) {
            return null;
        }
        if (null == comment || "".equals(comment)) {
            comment = "Check from test fixture class...";
        }
        if (null == date) {
            date = new Date();
        }
        List<CheckDetails> output = new ArrayList<CheckDetails>();
        CheckDetails tmp_details;
        for (int i = 0; i < nb; i++) {
            tmp_details = getStandardCheck().toDetails();
            tmp_details.setComment(String.format("%s %d", comment, i));
            tmp_details.setDate(date);

            output.add(tmp_details);
        }
        return output;
    }
}
