package com.mystack.tool;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

public class Tool {

    /**
     * Join all element of a collection with a given separator
     * 
     * @param s
     * @param delimiter
     * @return
     */
    public static String join(Collection<?> s, String delimiter) {
        if (s == null || s.size() == 0)
            return "";

        StringBuilder builder = new StringBuilder();
        Iterator<?> iter = s.iterator();
        while (iter.hasNext()) {
            builder.append(iter.next());
            if (!iter.hasNext()) {
                break;
            }
            builder.append(delimiter);
        }
        return builder.toString();
    }

    /**
     * 
     * @param length
     * @return
     */
    public static ArrayList<UUID> getRandomUUIDList(int length) {
        if (length <= 0)
            return new ArrayList<>();

        ArrayList<UUID> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add(UUID.randomUUID());
        }
        return list;
    }

}
