package com.mystack.tool;

import java.security.Principal;

public class MyPrincipal implements Principal {
    private String name;

    public MyPrincipal(String name) {
        setName(name);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
