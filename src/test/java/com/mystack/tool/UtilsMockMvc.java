package com.mystack.tool;

import static com.mystack.rest.fixture.RestStackDataFixtures.standardStackJSON;
import static com.mystack.rest.fixture.RestUserDataFixtures.standardUserJSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;
import java.util.UUID;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.google.gson.Gson;

public class UtilsMockMvc {

    /*
     * This method create a stack and return the id of this new stack
     */
    private static String postAStack(MockMvc mockMvc) {
        String uid = postAUSer(mockMvc);
        try {
            MvcResult result =
                    mockMvc.perform(
                            post("/aggregators/user/{uid}/stack", uid)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(standardStackJSON())
                                    .accept(MediaType.APPLICATION_JSON))
                            .andDo(print())
                            .andExpect(status().isCreated())
                            .andReturn();

            // status is 200 OK
            String body = result.getResponse().getContentAsString();
            //TODO: this to be done more cleanely (use Jackson here ?)
            Map json_body = new Gson().fromJson(body, Map.class);
            if (json_body.containsKey("id")) {
                return (String) json_body.get("id");
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    private static String postAUSer(MockMvc mockMvc) {
        String uid = UUID.randomUUID().toString();
        try {
            MvcResult result =
                    mockMvc.perform(
                            post("/aggregators/user/")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(standardUserJSON())
                                    .accept(MediaType.APPLICATION_JSON))
                            .andDo(print())
                            .andExpect(status().isCreated())
                            .andReturn();

            // status is 200 OK
            String body = result.getResponse().getContentAsString();
            //TODO: this to be done more cleanely (use Jackson here ?)
            Map json_body = new Gson().fromJson(body, Map.class);
            if (json_body.containsKey("id")) {
                return (String) json_body.get("id");
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }
}
