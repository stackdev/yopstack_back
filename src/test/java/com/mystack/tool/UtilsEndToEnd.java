package com.mystack.tool;

import static com.mystack.rest.fixture.RestCheckDataFixtures.checkJSONFromDetails;
import static com.mystack.rest.fixture.RestCheckDataFixtures.customCheckDetails;
import static com.mystack.rest.fixture.RestPreferenceFixtures.preferenceJSON;
import static com.mystack.rest.fixture.RestPreferenceFixtures.randomPreference;
import static com.mystack.rest.fixture.RestStackDataFixtures.standardStackDetails;
import static com.mystack.rest.fixture.RestUserDataFixtures.userJSONFromDetails;
import static junit.framework.TestCase.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import java.util.Base64;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.mystack.config.TestString;
import com.mystack.core.domain.CheckDetails;
import com.mystack.core.domain.CommentDetails;
import com.mystack.core.domain.PreferenceDetails;
import com.mystack.core.domain.StackDetails;
import com.mystack.core.domain.UserDetails;
import com.mystack.rest.domain.Check;
import com.mystack.rest.domain.Comment;
import com.mystack.rest.domain.Preference;
import com.mystack.rest.domain.Stack;
import com.mystack.rest.domain.User;
import com.mystack.rest.fixture.RestCheckDataFixtures;
import com.mystack.rest.fixture.RestCommentFixtures;
import com.mystack.rest.fixture.RestStackDataFixtures;

public class UtilsEndToEnd {
    private static Logger     LOG      = LoggerFactory.getLogger(UtilsEndToEnd.class);

    static private TestString testString;

    static String             username = "dzd";
    static String             password = "dzd";
    static String             auth     = username + ":" + password;

    public static void setTestString(TestString testString) {
        UtilsEndToEnd.testString = testString;
    }

    //----------------------------- AUTH 
    
	/**
	 * Run the authentication 
	 * @param username username
	 * @param password password
	 * @return the headers key,value map
	 */
    public static Map<String, String> authenticate(String username, String password) {
        try {
            String auth = username + ":" + password;
            LOG.debug("Authenticating.... first get");
            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    "{}",
                    getHeadersLogin(auth));
            RestTemplate template = new RestTemplate();
            ParameterizedTypeReference<List<User>> responseType = new ParameterizedTypeReference<List<User>>() {
            };
            LOG.debug("About to send: " + requestEntity);
            ResponseEntity<List<User>> entity = template.exchange(testString.getUsers_fullPath() + "?name=" + username, HttpMethod.GET, requestEntity, responseType);
            assertEquals(HttpStatus.OK, entity.getStatusCode());

            Map<String, String> new_cookies = getNewCookie(requestEntity.getHeaders(), entity.getHeaders());
            LOG.debug("New Cookies:" + new_cookies);

            LOG.debug("Authenticating.... second get");
            requestEntity = new HttpEntity<String>(
                    "{}",
                    getHeadersLoggedIn(auth, new_cookies));
            template = new RestTemplate();
            responseType = new ParameterizedTypeReference<List<User>>() {
            };
            LOG.debug("About to send: " + requestEntity);
            entity = template.exchange(testString.getUsers_fullPath() + "?name=" + username, HttpMethod.GET, requestEntity, responseType);
            assertEquals(HttpStatus.OK, entity.getStatusCode());

            LOG.debug("....." + requestEntity.getHeaders());
            Map<String, String> final_cookies = getNewCookie(requestEntity.getHeaders(), entity.getHeaders());
            LOG.debug("Final cookies: " + final_cookies);

            return final_cookies;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    
    /**
     * Get the csrf token by calling the info end point which does not require authentication
     * @return the csrf token and jsession value
     */
    public static Map<String, String> getCsrfTokenAndSessionFromSetCookies() {
        try {
            LOG.debug("Getting the csrf token via a call the the info endpoint");
            HttpEntity<String> requestEntity = new HttpEntity<String>("{}");
            RestTemplate template = new RestTemplate();
            ParameterizedTypeReference<Object> responseType = new ParameterizedTypeReference<Object>() {
            };
            ResponseEntity<Object> entity = template.exchange(testString.getInfo_fullPath(), HttpMethod.GET, requestEntity, responseType);
            assertEquals(HttpStatus.OK, entity.getStatusCode());
            LOG.debug(entity.toString());

            return getNewCookie(requestEntity.getHeaders(), entity.getHeaders());

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
     }
    

    private static Map<String, String> getNewCookie(HttpHeaders requestHeader, HttpHeaders lastResponseHeader) {
        Map<String, String> previous_cookies = getSessionAndTokenInCookie(requestHeader.get("Cookie"));
        Map<String, String> cookies_to_be_set = getSessionAndTokenInSetCookie(lastResponseHeader.get("Set-Cookie"));
        Map<String, String> new_cookies = new HashMap<>(previous_cookies);

        for (String key : cookies_to_be_set.keySet()) {
            new_cookies.put(key, cookies_to_be_set.get(key));
        }
        //        LOG.info("previous cookies  : " + previous_cookies);
        //        LOG.info("cookies to be set : " + cookies_to_be_set);
        //        LOG.info("New cookies       : " + new_cookies);
        return new_cookies;
    }

    private static Map<String, String> getSessionAndTokenInSetCookie(List<String> setCookie) {
        List<String> cookie = new ArrayList<>();
        for (String item : setCookie) {
            cookie.add(item.split(";")[0]);
        }
        return parseCookie(cookie);
    }

    private static Map<String, String> getSessionAndTokenInCookie(List<String> cookies) {
        List<String> cleaned_cookies = new ArrayList<>();
        if (null != cookies) {
            for (String cookie : cookies) {
                cookie = cookie.replaceAll("\\s+", "");
                for (String part : cookie.split(";")) {
                    cleaned_cookies.add(part);
                }
            }
        }
        return parseCookie(cleaned_cookies);
    }

    private static Map<String, String> parseCookie(List<String> setCookie) {
        Map<String, String> result = new HashMap<>();

        if (null == setCookie) {
            return result;
        }
        for (String cookie : setCookie) {
            String[] cookie_elements = cookie.split("=");
            LOG.debug(cookie_elements[0] + "..." + cookie_elements[1]);
            if ("XSRF-TOKEN".equals(cookie_elements[0])) {
                result.put("XSRF-TOKEN", cookie_elements[1]);
            }

            if ("JSESSIONID".equals(cookie_elements[0])) {
                result.put("JSESSIONID", cookie_elements[1]);
            }
        }
        return result;
    }

    //----------------------------- AUTH -- END

    //----------------------------- USER

    /**
     * Get a User and return the response entity
     * 
     * @return
     */
    public static ResponseEntity<List<User>> getAUser_Authenticate() {
        try {

            LOG.debug("Authenticating.... first get");

            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    "{}",
                    getHeadersLogin("dzd" + ":" + "dzd"));

            RestTemplate template = new RestTemplate();
            ParameterizedTypeReference<List<User>> responseType = new ParameterizedTypeReference<List<User>>() {
            };

            LOG.debug("About to send: " + requestEntity);
            ResponseEntity<List<User>> entity = template.exchange(testString.getUsers_fullPath() + "?name=dzd", HttpMethod.GET, requestEntity, responseType);

            assertEquals(HttpStatus.OK, entity.getStatusCode());

            return entity;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //TMP

    public static ResponseEntity<List<User>> getAUser_Authentified(UUID XSRF_TOKEN, List<String> cookies) {
        try {

            LOG.info("Authenticating....");

            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    "{}",
                    getHeaders("dzd" + ":" + "dzd", XSRF_TOKEN, cookies));

            RestTemplate template = new RestTemplate();
            ParameterizedTypeReference<List<User>> responseType = new ParameterizedTypeReference<List<User>>() {
            };

            ResponseEntity<List<User>> entity = template.exchange(testString.getUsers_fullPath() + "?name=dzd", HttpMethod.GET, requestEntity, responseType);

            assertEquals(HttpStatus.OK, entity.getStatusCode());

            return entity;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    // base get user function by id or name
    public static ResponseEntity<User> getAUserById(UUID id, String auth_name, String auth_password) {
        if (null == id) {
            return null;
        }
        try {
            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(auth_name, auth_password);
            String auth = auth_name + ":" + auth_password;
            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    getHeadersLoggedIn(auth, auth_cookie));
            RestTemplate template = new RestTemplate();

            ResponseEntity<User> entity;

            requestEntity = new HttpEntity<String>(UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));

            // getting user by id

            LOG.info("Getting user by id: " + id);
            String url = testString.getUsers_fullPath(id.toString());

            LOG.info("Getting user with URL: " + url);
            entity = template.exchange(url,
                    HttpMethod.GET, requestEntity, new ParameterizedTypeReference<User>() {
                    });
            assertEquals(HttpStatus.OK, entity.getStatusCode());
            return entity;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @deprecated use instead the version of this method passing auth_name and auth_password
     * @param id
     * @return
     */
    public static ResponseEntity<User> getAUserById(UUID id) {
        return getAUserById(id, username, password);
    }

    /**
     * * @deprecated use instead the version of this method passing auth_name and auth_password
     * 
     * @param name
     * @return
     */
    public static ResponseEntity<List<User>> getAUserByName(String name) {
        return getAUserByName(name, username, password);
    }

    public static ResponseEntity<List<User>> getAUserByName(String name, String auth_name, String auth_password) {
        if (null == name) {
            return null;
        }
        try {
            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(auth_name, auth_password);
            String auth = auth_name + ":" + auth_password;
            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    getHeadersLoggedIn(auth, auth_cookie));
            //LOG.info("JSONusER: " + jsonUser);

            RestTemplate template = new RestTemplate();

            ResponseEntity<List<User>> entity;

            LOG.info("Getting user by name: " + name);
            HashMap<String, String> params = new HashMap<>();
            params.put("name", name);

            requestEntity = new HttpEntity<String>(UtilsEndToEnd.getHeadersLoggedIn(auth, auth_cookie));
            String url = testString.getUsers_fullPath() + testString.getParam(params);
            LOG.info("Getting user with URL: " + url);
            entity = template.exchange(url,
                    HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<User>>() {
                    });

            assertEquals(HttpStatus.OK, entity.getStatusCode());
            User result_user = entity.getBody().get(0);
            UUID createdUserId = result_user.getId();
            LOG.info("Created user ID: " + createdUserId.toString());
            return entity;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * GEt a user by name
     * 
     * @param name
     * @return return the UUID of the user if it is found
     */
    public static UUID getAUserByName_id(String name) {
        LOG.info("Getting User with name:" + name);
        ResponseEntity<List<User>> user_entity = getAUserByName(name);
        if (null != user_entity && user_entity.getStatusCode() == HttpStatus.OK) {
            LOG.info("User Found !" + user_entity);
            return user_entity.getBody().get(0).getId();
        }
        LOG.info("User not found returning a null...");
        return null;
    }

    /**
     * Deprecated use: postAUser_(String name, String password) instead
     * 
     * @param name
     * @return
     */
    @Deprecated
    public static ResponseEntity<User> postAUser_(String name) {
        return postAUser_(name, name);
    }

    /**
     * create a User and return the response entity
     * 
     * @return
     */
    public static ResponseEntity<User> postAUser_(String name, String password) {
        //String uid = UUID.randomUUID().toString();
        if (null == name || "".equals(name)) {
            name = "RandomnUserName" + UUID.randomUUID();
            password = "";
        }
        try {
            List<String> roles = new ArrayList<>();
            roles.add("ROLE_USER");
            UserDetails details = new UserDetails(UUID.randomUUID(), name, null, null, null, true, roles);
            details.setSetpw(password);
            String jsonUser = userJSONFromDetails(details);
            LOG.info("Adding User with data (temporary Id): " + jsonUser);

            //Map<String, String> csrfAndSessionValues = UtilsEndToEnd.getCsrfTokenAndSessionFromSetCookies();
            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    jsonUser, UtilsEndToEnd.getStandardHeaders());
                    
                    //UtilsEndToEnd.getHeadersLoggedIn(null, csrfAndSessionValues));

            RestTemplate template = new RestTemplate();
            ResponseEntity<User> entity = template.postForEntity(
                    testString.getUsers_fullPath(),
                    requestEntity, User.class);
            //String path = entity.getHeaders().getLocation().getPath();

            assertEquals(HttpStatus.CREATED, entity.getStatusCode());
            User result_user = entity.getBody();
            UUID createdUserId = result_user.getId();
            LOG.info("Created user ID: " + createdUserId.toString());
            return entity;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Post a user without specifying a name
     * 
     * @return a user or null
     */
    public static User postAUser_User(String name, String password) {
        ResponseEntity<User> posted_user_response = postAUser(name, password);

        if (posted_user_response == null)
            return null;
        return posted_user_response.getBody();
    }

    /**
     * Post a user without specifying a name
     * 
     * @return
     */
    public static ResponseEntity<User> postAUser(String name, String password) {
        return postAUser_(name, password);
    }

    /**
     * Create a user with a given name and return only its id
     * 
     * @return
     */
    public static UUID postAUser_id(String name, String password) {
        ResponseEntity<User> entity = postAUser_(name, password);
        if (null != entity) {
            return entity.getBody().getId();
        }
        return null;
    }

    /**
     * Update a User and return the response entity
     * 
     * @param user_name
     *            might be different from detail name, in case of name update
     * @return
     */
    public static ResponseEntity<User> updateAUser(UserDetails details, String user_name, String user_password) {
        try {

            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(user_name, user_password);
            String jsonUser = userJSONFromDetails(details);
            LOG.info("Updating User with data (temporary Id): " + jsonUser);

            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    jsonUser,
                    getHeadersLoggedIn(user_name + ":" + user_password, auth_cookie));

            RestTemplate template = new RestTemplate();
            ResponseEntity<User> entity = template.exchange(
                    testString.getUsers_fullPath(details.getId().toString()), HttpMethod.PUT,
                    requestEntity, User.class);

            assertEquals(HttpStatus.OK, entity.getStatusCode());

            return entity;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //----------------------------- USER -- END

    //----------------------------- STACK

    public static ResponseEntity<Stack> getAStack(UUID stackid, UUID userid, String username, String password) {
        try {

            LOG.info("Username: " + username + ", password: " + password);
            if ("".equals(username)) {
                username = UtilsEndToEnd.username;
                password = UtilsEndToEnd.password;
            }
            String auth = username + ":" + password;

            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);

            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    getHeadersLoggedIn(auth, auth_cookie));

            String url = testString.getStacks_fullPath(userid.toString(), stackid.toString());
            HttpMethod method = HttpMethod.GET;

            RestTemplate template = new RestTemplate();
            ResponseEntity<Stack> entity = template.exchange(url, method, requestEntity, Stack.class);

            return entity;
        } catch (HttpClientErrorException e) {
            //e.printStackTrace();
            return new ResponseEntity<>(e.getStatusCode());
        }

    }

    public static ResponseEntity<List<Stack>> getAllStack(UUID userid, String username, String password) {

        try {

            LOG.info("Username: " + username + ", password: " + password);
            if ("".equals(username)) {
                username = UtilsEndToEnd.username;
                password = UtilsEndToEnd.password;
            }
            String auth = username + ":" + password;

            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);

            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    getHeadersLoggedIn(auth, auth_cookie));

            String url = testString.getStacks_fullPath(userid.toString());
            HttpMethod method = HttpMethod.GET;

            RestTemplate template = new RestTemplate();
            ResponseEntity<List<Stack>> entity = template.exchange(url, method, requestEntity, new ParameterizedTypeReference<List<Stack>>() {
            });

            return entity;
        } catch (HttpClientErrorException e) {
            //e.printStackTrace();
            return new ResponseEntity<List<Stack>>(e.getStatusCode());
        }

    }

    /**
     * create a stack and return the response entity
     * 
     * @return
     */
    public static ResponseEntity<Stack> postAStack() {
        return postAStack(null);
    }

    /**
     * Return only the id of the created stack
     * 
     * @deprecated use instead postAStack_id(UUID existing_user)
     * @return
     */
    public static UUID postAStack_id() {
        return postAStack_id(null);
    }

    /*
     * Return only the id of the created stack
     * 
     * @deprecated user postAStack_id(UUID existing_user, String username, String password) instead
     * 
     * @return
     */
    @Deprecated
    public static UUID postAStack_id(UUID existing_user) {
        ResponseEntity<Stack> entity = postAStack(existing_user);
        if (null != entity) {
            return entity.getBody().getId();
        }
        return null;
    }

    public static Stack postAStack_Stack(UUID existing_user_id) {
        ResponseEntity<Stack> responseEntity = postAStack(existing_user_id);

        if (responseEntity == null)
            return null;

        return responseEntity.getBody();
    }

    public static ResponseEntity<Stack> postAStack(UUID existing_user_id) {
        return postAStack(existing_user_id, "", "");
    }

    public static ResponseEntity<Stack> postAStack(UUID existing_user_id, String username, String password) {
        return postAStack(existing_user_id, null, username, password);
    }

    public static ResponseEntity<Stack> postAStack(UUID existing_user_id, StackDetails stack_details, String username, String password) {
        try {
            ResponseEntity<Stack> entity = post_put_stack_core(existing_user_id, stack_details, false, username, password);

            assertEquals(HttpStatus.CREATED, entity.getStatusCode());
            return entity;

        } catch (Exception e) {
            LOG.error("PostAStack in error: " + e.getStackTrace().toString());
            e.printStackTrace();
            return null;
        }
    }

    public static ResponseEntity<Stack> updateAStack(UUID existing_user_id, StackDetails stack_details) {
        return updateAStack(existing_user_id, stack_details, "", "");
    }

    public static ResponseEntity<Stack> updateAStack(UUID existing_user_id, StackDetails stack_details, String username, String password) {
        try {
            ResponseEntity<Stack> entity = post_put_stack_core(existing_user_id, stack_details, true, username, password);

            return entity;

        } catch (Exception e) {
            LOG.error("PostAStack in error: " + e.getStackTrace().toString());
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Base Method for stack creation and update, is not catching exceptions
     * 
     * @param existing_user_id
     * @param stack_details
     * @return
     */
    private static ResponseEntity<Stack> post_put_stack_core(UUID existing_user_id, StackDetails stack_details, boolean update, String username, String password) {
        LOG.info("Username: " + username + ", password: " + password);
        if ("".equals(username)) {
            username = UtilsEndToEnd.username;
            password = UtilsEndToEnd.password;
        }
        String auth = username + ":" + password;

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        UUID userid;
        if (null == existing_user_id) {
            userid = postAUser_id(username, password);
        } else {
            userid = existing_user_id;
        }

        StackDetails input_stack;
        if (null == stack_details) {
            input_stack = standardStackDetails();
        } else {
            input_stack = stack_details;
        }
        LOG.info("Input Stack Details (temporary id)" + input_stack);
        //String json_check = standardStackJSON();
        String json_check = RestStackDataFixtures.stackJSONFromDetails(input_stack);
        LOG.info("Adding Stack with data: " + json_check);

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                json_check,
                getHeadersLoggedIn(auth, auth_cookie));

        String url;
        HttpMethod method;
        if (update) {
            method = HttpMethod.PUT;
            url = testString.getStacks_fullPath(userid.toString(), input_stack.getId().toString());
        } else {
            method = HttpMethod.POST;
            url = testString.getStacks_fullPath(userid.toString());
        }

        RestTemplate template = new RestTemplate();
        ResponseEntity<Stack> entity = template.exchange(
                url, method,
                requestEntity, Stack.class);

        return entity;
    }

    //----------------------------- STACK -- END

    //----------------------------- CHECK 

    /**
     * Post a Check and return the response_entity
     * 
     * @param existing_stack_id
     * @return
     */
    public static ResponseEntity<Check> postACheck_(UUID existing_stack_id, String comment) {
        return postACheck_(existing_stack_id, comment, "", "");
    }

    public static ResponseEntity<Check> postACheck_(UUID existing_stack_id, String comment, String username_, String password_) {
        String _auth = auth;
        String _username = username;
        String _password = password;
        if (!"".equals(username_) && !"".equals(password_)) {
            _auth = username_ + ":" + password_;
        }
        if ("".equals(comment))
            comment = "Check posted from: " + UtilsEndToEnd.class.toString();

        String uid = UUID.randomUUID().toString();
        try {
            Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(_username, _password);

            String jsonCheck = checkJSONFromDetails(customCheckDetails(UUID.randomUUID(), new Date(), existing_stack_id, comment));
            LOG.info("Adding Check with data (temporary Id): " + jsonCheck);

            HttpEntity<String> requestEntity = new HttpEntity<String>(
                    jsonCheck,
                    UtilsEndToEnd.getHeadersLoggedIn(_auth, auth_cookie));

            RestTemplate template = new RestTemplate();
            ResponseEntity<Check> entity = template.postForEntity(
                    testString.getChecks_fullPath(),
                    requestEntity, Check.class);

            assertEquals(HttpStatus.CREATED, entity.getStatusCode());
            Check result_check = entity.getBody();
            UUID createdCheckId = result_check.getId();
            LOG.info("Created user ID: " + createdCheckId.toString());
            return entity;

        } catch (HttpClientErrorException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Post a check without specifying a comment
     * 
     * @param existing_stack_id
     * @return
     */
    public static ResponseEntity<Check> postACheck_(UUID existing_stack_id) {
        return postACheck_(existing_stack_id, "");
    }

    /**
     * Convenient method which returns the Check directly
     * 
     * @param existing_stack_id
     * @return
     */
    public static Check postACheck_Check(UUID existing_stack_id) {
        ResponseEntity<Check> response = postACheck_(existing_stack_id);
        return response.getBody();
    }

    /**
     * Convenient method which returns the Check UUID directly
     * 
     * @param existing_stack_id
     * @return
     */
    public static UUID postACheck_UUID(UUID existing_stack_id) {
        ResponseEntity<Check> response = postACheck_(existing_stack_id);
        return response.getBody().getId();
    }

    /**
     * Update a Check and return the response entity
     * 
     * @return
     */
    public static ResponseEntity<Check> updateACheck(CheckDetails details) {

        return updateACheck(details, username, password);

    }

    public static ResponseEntity<Check> updateACheck(CheckDetails details, String username_, String password_) {

        Map<String, String> auth_cookie = authenticate(username_, password_);
        String auth = username_ + ":" + password_;
        String url = "";
        if (null != details.getId()) {
            url = details.getId().toString();
        }

        String jsonCheck = RestCheckDataFixtures.checkJSONFromDetails(details);
        LOG.info("Updating Check with data: " + jsonCheck);

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                jsonCheck,
                getHeadersLoggedIn(auth, auth_cookie));

        RestTemplate template = new RestTemplate();
        ResponseEntity<Check> entity = template.exchange(testString.getChecks_fullPath(url), HttpMethod.PUT, requestEntity, Check.class);

        return entity;

    }

    public static ResponseEntity<Check> getACheck(UUID checkid) {

        Map<String, String> auth_cookie = authenticate(username, password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeadersLoggedIn(auth, auth_cookie));
        RestTemplate template = new RestTemplate();
        ResponseEntity<Check> entity = template.exchange(
                testString.getChecks_fullPath(checkid.toString()),
                HttpMethod.GET, requestEntity, Check.class);
        Check result_check2 = entity.getBody();
        LOG.info("Result of the GET: " + result_check2.toString());
        return entity;
    }

    public static ResponseEntity<List<Check>> getAllCheck(UUID stack_id, Date before_date, int count) {
        return getAllCheck(stack_id, before_date, null, count);
    }

    public static ResponseEntity<List<Check>> getAllCheck(UUID stack_id, Date before_date, Date after_date, int count) {
        HashMap<String, String> params = new HashMap<>();

        if (null != before_date) {
            String before_date_str = String.valueOf(before_date.getTime());
            params.put("before_date", before_date_str);
        }

        if (null != after_date) {
            String after_date_str = String.valueOf(after_date.getTime());
            params.put("after_date", after_date_str);
        }

        if (count > 0) {
            params.put("count", String.valueOf(count));
        }
        params.put("stack", stack_id.toString());

        Map<String, String> auth_cookie = authenticate(username, password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeadersLoggedIn(auth, auth_cookie));
        RestTemplate template = new RestTemplate();

        String url = testString.getChecks_fullPath() + testString.getParam(params);
        LOG.info("GetAllChecks URL: " + url);
        ResponseEntity<List<Check>> entity = template.exchange(
                url,
                HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Check>>() {
                });
        List<Check> result_check2 = entity.getBody();
        LOG.info("Result of the GET: " + result_check2.toString());
        return entity;
    }

    //----------------------------- CHECK -- END 

    //----------------------------- COMMENTS 

    public static ResponseEntity<Comment> postAComment(CommentDetails comment_details) {

        return post_put_comment_core(comment_details, false, username, password);
    }

    /**
     * Base Method for stack creation and update, is not catching exceptions
     * 
     * @param existing_user_id
     * @param stack_details
     * @return
     */
    private static ResponseEntity<Comment> post_put_comment_core(CommentDetails comment_details, boolean update, String username, String password) {
        UUID existing_user_id = comment_details.getStack_userid();
        UUID existing_stack_id = comment_details.getStackid();
        UUID existing_check_id = comment_details.getCheckid();

        LOG.info("Username: " + username + ", password: " + password);
        if ("".equals(username)) {
            username = UtilsEndToEnd.username;
            password = UtilsEndToEnd.password;
        }
        String auth = username + ":" + password;

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        UUID userid, stackid, checkid;
        if (null == existing_user_id) {
            userid = postAUser_id(username, password);
        } else {
            userid = existing_user_id;
        }

        if (null == existing_stack_id) {
            stackid = postAStack_id(userid);
        } else {
            stackid = existing_stack_id;
        }

        if (null == existing_check_id) {
            checkid = postACheck_UUID(stackid);
        } else {
            checkid = existing_check_id;
        }

        CommentDetails input_comment = comment_details;

        LOG.info("Input Comment Details (temporary id)" + input_comment);
        String json_comment = RestCommentFixtures.CommentJSONFromDetails(comment_details);
        LOG.info("Adding Comment with data: " + json_comment);

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                json_comment,
                getHeadersLoggedIn(auth, auth_cookie));

        String url;
        HttpMethod method;
        if (update) {
            method = HttpMethod.PUT;
            url = testString.getComments_fullPath(userid.toString(), input_comment.getStackid().toString(), input_comment.getCheckid().toString(), input_comment.getId().toString());
        } else {
            method = HttpMethod.POST;
            url = testString.getComments_fullPath(userid.toString(), input_comment.getStackid().toString(), input_comment.getCheckid().toString());
        }

        RestTemplate template = new RestTemplate();
        ResponseEntity<Comment> entity = template.exchange(
                url, method,
                requestEntity, Comment.class);

        return entity;
    }

    public static ResponseEntity<Comment> getAComment(UUID userid, UUID stackid, UUID checkid, UUID commentid) {

        Map<String, String> auth_cookie = authenticate(username, password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeadersLoggedIn(auth, auth_cookie));
        RestTemplate template = new RestTemplate();
        ResponseEntity<Comment> entity = template.exchange(
                testString.getComments_fullPath(userid.toString(), stackid.toString(), checkid.toString(), commentid.toString()),
                HttpMethod.GET, requestEntity, Comment.class);
        Comment result_comment2 = entity.getBody();
        LOG.info("Result of the GET: " + result_comment2.toString());
        return entity;
    }

    public static ResponseEntity<List<Comment>> getAllComment(UUID userid, UUID stackid, UUID checkid, Date before_date, int count) {
        return getAllComment(userid, stackid, checkid, before_date, null, count);
    }

    public static ResponseEntity<List<Comment>> getAllComment(UUID userid, UUID stackid, UUID checkid, Date before_date, Date after_date, int count) {
        HashMap<String, String> params = new HashMap<>();

        if (null != before_date) {
            String before_date_str = String.valueOf(before_date.getTime());
            params.put("before_date", before_date_str);
        }

        if (null != after_date) {
            String after_date_str = String.valueOf(after_date.getTime());
            params.put("after_date", after_date_str);
        }

        if (count > 0) {
            params.put("count", String.valueOf(count));
        }

        Map<String, String> auth_cookie = authenticate(username, password);
        HttpEntity<String> requestEntity = new HttpEntity<String>(
                getHeadersLoggedIn(auth, auth_cookie));
        RestTemplate template = new RestTemplate();

        String url = testString.getComments_fullPath(userid.toString(), stackid.toString(), checkid.toString()) + testString.getParam(params);
        LOG.info("GetAllComments URL: " + url);
        ResponseEntity<List<Comment>> entity = template.exchange(
                url,
                HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Comment>>() {
                });
        List<Comment> result_comment2 = entity.getBody();
        LOG.info("Result of the GET: " + result_comment2.toString());
        return entity;
    }

    public static ResponseEntity<Comment> updateAComment(CommentDetails details) {
        return updateAComment(details, username, password);
    }

    public static ResponseEntity<Comment> updateAComment(CommentDetails details, String username_, String password_) {

        //        Map<String, String> auth_cookie = authenticate(username_, password_);
        //        String auth = username_ + ":" + password_;
        //
        //        String jsonCheck = RestCommentFixtures.CommentJSONFromDetails(details);
        //        LOG.info("Updating Comment with data: " + jsonCheck);
        //
        //        HttpEntity<String> requestEntity = new HttpEntity<String>(
        //                jsonCheck,
        //                getHeadersLoggedIn(auth, auth_cookie));
        //
        //        RestTemplate template = new RestTemplate();
        //        ResponseEntity<Comment> entity = template.exchange(
        //                testString.getComments_fullPath(details.getStack_userid().toString(), details.getStackid().toString(), details.getCheckid().toString()),
        //                HttpMethod.PUT, requestEntity, Comment.class);
        //        
        return post_put_comment_core(details, true, username_, password_);

    }

    //----------------------------- COMMENTS -- END

    //----------------------------- PREFERENCES --

    public static Preference postAPreference(UUID existing_user_id, String auth_user, String auth_password) {
        return post_put_preference_core(existing_user_id, randomPreference().toDetails(), false, auth_user, auth_password).getBody();
    }

    public static Preference updateAPreference(UUID existing_user_id, PreferenceDetails details, String auth_user, String auth_password) {
        return post_put_preference_core(existing_user_id, details, true, auth_user, auth_password).getBody();
    }

    /**
     * Base Method for stack creation and update, is not catching exceptions
     * 
     * @param existing_user_id
     * @param stack_details
     * @return
     */
    private static ResponseEntity<Preference> post_put_preference_core(UUID existing_user_id, PreferenceDetails input_details, boolean update, String username, String password) {
        LOG.info("Username: " + username + ", password: " + password);
        if ("".equals(username)) {
            username = UtilsEndToEnd.username;
            password = UtilsEndToEnd.password;
        }
        String auth = username + ":" + password;

        Map<String, String> auth_cookie = UtilsEndToEnd.authenticate(username, password);
        UUID userid;
        if (null == existing_user_id) {
            userid = postAUser_id(username, password);
        } else {
            userid = existing_user_id;
        }

        PreferenceDetails details;
        if (null == input_details) {
            details = randomPreference().toDetails();
        } else {
            details = input_details;
        }

        details.setUser(existing_user_id);
        LOG.info("Input Preference Details (temporary id)" + details);
        //String json_check = standardStackJSON();
        String json = preferenceJSON(com.mystack.rest.domain.Preference.fromDetails(details));
        LOG.info("Adding Preference with data: " + json);

        HttpEntity<String> requestEntity = new HttpEntity<String>(
                json,
                getHeadersLoggedIn(auth, auth_cookie));

        String url;
        HttpMethod method;
        if (update) {
            method = HttpMethod.PUT;
            url = testString.getPreferences_fullPath(userid.toString(), details.getId().toString());
        } else {
            method = HttpMethod.POST;
            url = testString.getPreferences_fullPath(userid.toString());
        }

        RestTemplate template = new RestTemplate();
        ResponseEntity<Preference> entity = template.exchange(
                url, method,
                requestEntity, Preference.class);

        return entity;
    }

    //----------------------------- PREFERENCES -- END

    /**
     * Get header list when
     * 
     * @param auth
     * @return
     */
    @Deprecated
    public static HttpHeaders getHeaders(String auth) {
        return getHeaders(auth, null, null);
    }

    public static HttpHeaders getHeadersLogin(String auth) {
        // Random value provided for the X-XSRF-TOKEN, the server will return a new value
        return getHeaders(auth, null, null);
    }

    public static HttpHeaders getHeadersLoggedIn(String auth, Map<String, String> auth_cookie) {

        UUID X_XSRF_TOKEN = UUID.fromString(auth_cookie.get("XSRF-TOKEN"));
        List<String> cookies_list = new ArrayList<>();
        for (String key : auth_cookie.keySet()) {
            cookies_list.add(key + "=" + auth_cookie.get(key));
        }
        return getHeaders(auth, X_XSRF_TOKEN, cookies_list);
    }

    public static HttpHeaders getHeaders(String auth, UUID XSRF_Header, List<String> cookies) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        if (null != auth) {
            byte[] encodedAuthorisation = Base64.getEncoder().encode(auth.getBytes());
            headers.add("Authorization", "Basic " + new String(encodedAuthorisation));
        }
        if (null != XSRF_Header) {
            headers.add("X-XSRF-TOKEN", XSRF_Header.toString());
        }
        if (null != cookies) {
            Pattern pattern = Pattern.compile("([^\"]+=[^\"]+);.*");
            String clean_cookies = "";
            for (String cookie : cookies) {

                String[] test = cookie.split(";");
                clean_cookies += (" " + test[0] + ";");

                Matcher matcher = pattern.matcher(cookie);
                if (matcher.matches()) {
                    LOG.info("-----------------" + matcher.group(1));
                    LOG.info("................." + test[0]);

                }
            }

            headers.set("Cookie", clean_cookies);
        }
        return headers;
    }

    static public HttpHeaders getStandardHeaders() {
        return getStandardHeaders(null);
    }

    static public HttpHeaders getStandardHeaders(String auth) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        if (null != auth) {
            byte[] encodedAuthorisation = Base64.getEncoder().encode(auth.getBytes());
            headers.add("Authorization", "Basic " + new String(encodedAuthorisation));
        }
        return headers;
    }

}
