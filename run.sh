docker rm yopstack_back_v1 
docker run -it --name yopstack_back_v1\
    -e OPENSHIFT_MONGODB_DB_URL=mongodb://localhost:1234 \
    -e MONGODB_HOST=plop \
    -e MONGODB_PORT=12345 \
    -e OPENSHIFT_APP_NAME=yopstack_back \
    -e OPENSHIFT_MONGODB_DB_USERNAME=admin \
    -e OPENSHIFT_MONGODB_DB_PASSWORD=admin \
    yopstack_back/0.0.1
