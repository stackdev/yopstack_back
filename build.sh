if [ -z $1 ];
then
  echo "missing version, exiting"
  exit 1
fi

VERSION="$1"
echo "Building yopstack_back docker image version $VERSION"

./gradlew bootJar -Penv=prod
docker build --build-arg JAR_FILE=./build/libs/yopstack_back.jar --tag yopstack_back:${VERSION} .
docker tag yopstack_back:${VERSION} dzd2009/yopstack_back:${VERSION}
docker push dzd2009/yopstack_back:${VERSION} 
