#!/usr/bin/env python3

from os import path, remove
from sys import exit, stdout
from datetime import datetime
import json
from ruamel.yaml import YAML, RoundTripLoader
from subprocess import check_output, call, run, PIPE
from re import match
from copy import copy
from argparse import ArgumentParser
from enum import Enum

from pprint import pprint

YAML_PLACEHOLDER = "#__NEW_VERSION__"
yaml_source = "changelog.yaml"
json_changelog = "changelog.json"
g_script_dir = path.dirname(path.realpath(__file__)) + "/"
g_script_name = path.basename(path.realpath(__file__)).strip(".py")

g_action_list = ["major", "minor", "patch"]
g_tag_pattern = "^v(\d+)\.(\d+)\.(\d+)$"
g_application_properties_path = "src/main/environment/common/application.properties"


class ProjectType(Enum):
    NODE = 1
    SPRING = 2


# Not used ?
def _get_versions(project_type="js"):
    if project_type == "js":
        return _get_versions_js()
    else:
        return _get_version_springboot()


def _get_versions_js():
    """
    get current version from package.json
    """
    versions = {"current": "", "previous": ""}
    yaml = YAML()
    try:
        with open("package.json", "r") as package:
            data = json.load(package)
            # print(data)
            if "version" in data.keys():
                versions["current"] = data["version"]

        with open(yaml_source, "r") as source:
            data = yaml.load(source)
            if "latest_version" in data.keys():
                versions["previous"] = data["latest_version"]

        print(
            "Version retrieved:\ncurrent: {}\nprevious: {}".format(
                versions["current"], versions["previous"]
            )
        )
    except (FileNotFoundError) as e:
        print("Pb while getting version from package.json or yaml...")
    return versions


def _check_repo_clean():
    """ check the state of the git repo """
    # TODO: check that we are on master (or release) branch
    print("script dir is: {}".format(g_script_dir))
    unclean_list = check_output(
        "git status --porcelain".split(" "), cwd=g_script_dir, encoding="utf-8"
    )
    if len(unclean_list):
        print(f"The repo under {g_script_dir} is not clean:\n{unclean_list}")
        return False
    return True


def _get_latest_tag_and_commit():
    """retrieve latest tag string and corresponding commit hash"""

    cmd = "git tag --sort=v:refname".split()
    res = run(cmd, encoding="utf-8", cwd=g_script_dir, stdout=PIPE)

    all_tags = [i for i in res.stdout.split() if match(g_tag_pattern, i)]
    latest_tag = all_tags[-1]

    # get the commit hash from the latest tag
    cmd = f"git rev-parse {latest_tag}".split()
    latest_tag_commit = run(cmd, encoding="utf-8", cwd=g_script_dir, stdout=PIPE).stdout.strip()

    print(f"Latest tag: {latest_tag}, ({latest_tag_commit})")

    return {"tag": latest_tag, "commit": latest_tag_commit}


def _get_current_commit():
    """get current local commit"""
    # TODO: enforce branch check somewhere (only master?)
    cmd = "git rev-parse HEAD".split()
    current_commit = run(cmd, encoding="utf-8", cwd=g_script_dir, stdout=PIPE).stdout.strip()

    print(f"Current commit is: {current_commit}")
    return current_commit


def get_new_version(action, tag):
    if action not in g_action_list:
        print(f"Unexpected value provided for action: {action}, expected value are: {g_action_list}")
        return False

    version = {}

    m = match(g_tag_pattern, tag)
    if not m or not m.groups():
        print(f"Provided tag: {tag} doesn't match pattern: {g_tag_pattern}")
        return False

    print(f"match: {m.groups()}")
    for i, level in enumerate(g_action_list, 1):
        version[level] = int(m.group(i))
    print(f"Identified version : {version['major']}.{version['minor']}.{version['patch']}")
    # based on the action increment the version

    new_version = {action: 0 for action in g_action_list}
    for a in g_action_list[: g_action_list.index(action)]:
        new_version[a] = version[a]
    new_version[action] = version[action] + 1
    print(f"New version after {action}: {new_version}")
    return f"v{new_version['major']}.{new_version['minor']}.{new_version['patch']}"


def update_git_version(version):
    """ run a git tag with the provided version """
    cmd = f"git tag -m Automated_release_tag. {version}".split()
    res = run(cmd, encoding="utf-8", cwd=g_script_dir, stdout=PIPE)
    if res.returncode != 0:
        print(f"error while executing cmd: {cmd}. Stdout: {res.stdout}")
        return
    print(f"Git tag set to: {version}")


def update_version(action):
    latest_version = _get_latest_tag_and_commit()
    current_commit = _get_current_commit()

    if latest_version["commit"] == current_commit:
        print(f"Latest commit is already tagged with {latest_version['tag']}. No update needed.")
        return
    new_version = get_new_version(action, latest_version["tag"])
    if not new_version:
        return
    update_git_version(new_version)

    return {"previous": latest_version["tag"], "current": new_version}


# TODO: to be refactored... it is doing a lot of different stuff
def update_application_properties(new_version, log="no comment provided..."):
    """update the application version and date """
    base_prefix = "info.application"
    version_pattern_prefix = f"{base_prefix}.version="
    # date_pattern_prefix = f"{base_prefix}.date="
    version_pattern = f"{version_pattern_prefix}.*$"
    changelog_prefix = f"{base_prefix}.changelog"
    changelog_pattern = "##__changelog__##"
    now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    new_version = new_version.replace(".", "_")
    log = "".join(log).replace("\n", "|")

    file_buffer = []
    with open(g_script_dir + g_application_properties_path, "r") as f:
        # parse the version property
        for line in f.readlines():
            m = match(version_pattern, line)
            # m_date = match(f"{date_pattern_prefix}(.*)", line)
            m_changelog = match(f"{changelog_pattern}", line)
            if m:
                print(f"match: {line}")
                file_buffer.append(f"{version_pattern_prefix}{new_version}\n")
                print(f"New version: {new_version}")
            # elif m_date and m_date.groups():
            #     file_buffer.append(f"{date_pattern_prefix}{now}")
            elif m_changelog:
                file_buffer.append(f"{changelog_pattern}\n")
                file_buffer.append(f"{changelog_prefix}.{new_version}.date={now}\n")
                file_buffer.append(f"{changelog_prefix}.{new_version}.log={log}\n\n")
            else:
                file_buffer.append(line)

    with open(g_script_dir + g_application_properties_path, "w") as f:
        f.writelines("".join(file_buffer))
        return True
    return False


def _get_user_input(earliest, latest, release_date):
    """
    prompt the user for edition of the commit log history
    returns a string with the changelog info 
    """

    timestamp = release_date.strftime(format="%Y%m%d-%H:%M:%S")
    tmp_file = "/tmp/{}_{}_{}.log".format(timestamp, latest, g_script_name)

    cmd = 'git log {earliest}..{latest} --pretty=format:"%s"'.format(
        earliest=earliest, latest=latest
    )
    raw_gitlog = check_output(cmd.split(" "), cwd=g_script_dir, encoding="utf-8")
    # print("change logs:\n{}".format(raw_gitlog))

    edition_text = """\
# Modify the lines below for the changelog version
# lines starting with a '#' will be ignored""".split(
        "\n"
    )

    # for all line from the gitlog except first and last
    # return all char execpt first and last
    edition_text.extend([comment[1:-1] for comment in raw_gitlog.split("\n")[1:-1]])

    with open(tmp_file, "w") as t:
        for line in edition_text:
            if line[0] != "#":
                line = "- {}".format(line)
            t.write("{}\n".format(line))

    try:
        print("Opening changelog edition with vim, press enter to confirm")
        editor_input = input()
        cmd = "vim {}".format(tmp_file)
        res = call(cmd.split(" "))

        changelog_edited = []
        with open(tmp_file, "r") as t:
            for line in t.readlines():
                if line[0] != "#":
                    changelog_edited.append(line)
        print("edition result: {}".format(changelog_edited))
    except Exception as e:
        print(
            "Problem occured will processing file: {}, exception: {}, removing file".format(
                tmp_file, e
            )
        )
        remove(tmp_file)
        return False
    return changelog_edited


def _get_yaml_section(version, date, log):
    """ produce the yaml section for the new version """

    changelog_date = date.strftime(format="%d/%m/%Y %H:%M:%S")
    if not isinstance(log, list):
        log = [log]
    section_pattern = """\
{placeholder}
- version: {version}
  date: {date} 
  log: |
{log}"""
    indented_logs = "".join(["    {}".format(line) for line in log])
    res = section_pattern.format(
        placeholder=YAML_PLACEHOLDER,
        version=version,
        date=changelog_date,
        log=indented_logs,
    )
    print("returning: {}".format(res))
    return res


def _update_changelog_yaml(version, date, log):
    """
    updating the changelog.yaml
    """
    yaml = YAML()

    try:
        yaml_read = open(yaml_source, "r")
        yaml_str = "".join(yaml_read.readlines())
        yaml_read.close()

        # had to do a replace because ruamel has not implemented
        # orderdict in py3...
        yaml_str = yaml_str.replace(
            YAML_PLACEHOLDER, _get_yaml_section(version=version, date=date, log=log)
        )
        data = yaml.load(yaml_str)

        if not "latest_version" in data.keys():
            print("Missing latest_version field in {}".format(yaml_source))
            return False

        data["latest_version"] = version

        # writing the new changelog
        yaml_write = open(yaml_source, "w")
        yaml.dump(data, yaml_write)
        yaml_write.close()

    except Exception as e:
        print("issue while editing the {}, exceptio: {}".format(yaml_source, e))
        return


def _generate_new_changelog(earliest, latest, project_type=ProjectType.SPRING):
    """ 
    main method for change  log generation 
    """
    release_date = datetime.now()

    changelog_edited = _get_user_input(
        earliest=earliest, latest=latest, release_date=release_date
    )
    if project_type == ProjectType.NODE:
        _update_changelog_yaml(version=latest, date=release_date, log=changelog_edited)
    else:
        update_application_properties(new_version=latest, log=changelog_edited)


def _generate_json():
    with open(yaml_source, "r") as source:
        yaml = YAML()
        data = yaml.load(source)
        with open(json_changelog, "w") as dest:
            json.dump(data, dest)


def _prompt_diff():
    gitdiff = check_output("git diff".split(" "), cwd=g_script_dir, encoding="utf-8")
    print("About to commit following diffs:")
    pprint("".join(gitdiff))
    print("Press 'enter' to confirm or any key to quit")

    a = input()
    if a != "":
        return False
    return True


def commit_and_push(modified_files_list, tag=""):
    try:
        print("Commit amending")
        check_output(
            f"git add {' '.join(modified_files_list)}".split(" "), cwd=g_script_dir
        )
        check_output("git commit --amend --no-edit".split(" "), cwd=g_script_dir)

        print("Pushing")
        check_output(f"git push origin {tag}".split(" "), cwd=g_script_dir)
    except Exception as e:
        print(
            f"An issue occured while commiting/push {modified_files_list}: {e}".format(
                e
            )
        )
        return
    print("done")


def publish_new_docker_image(version):
    print("Running the docker stage")
    cmd = f"bash build.sh {version}".split()
    res = run(cmd, encoding="utf-8", cwd=g_script_dir, stdout=PIPE)
    if res.returncode != 0:
        print(f"Docker image publishing failed with error:\n{res.stdout}")
        return
    print(f"Docker image publishing successful tag: {version}")


def get_args():
    parser = ArgumentParser(description="release script for yopstack front and backend")
    parser.add_argument(
        "action",
        choices=g_action_list,
        help="Specify if the new release is a patch, a minor or major version",
    )
    args = parser.parse_args()
    if not args:
        parser.print_help()
        exit(0)
    return args


if __name__ == "__main__":

    args = get_args()
    # check repo clean
    if not _check_repo_clean():
        print("exiting")
        exit(1)

    versions = update_version(args.action)
    if not versions:
        exit(1)

    # # propose changelog text based on commit log since previous tag
    # # update change_log.yaml
    _generate_new_changelog(
        earliest=versions["previous"],
        latest=versions["current"],
        project_type=ProjectType.SPRING,
    )

    # # generate change_log.json
    # _generate_json()

    # if not _prompt_diff():
    #     print("exiting")
    #     exit(1)

    # amend and push (if not already pushed)
    commit_and_push([g_application_properties_path], versions["current"])

    publish_new_docker_image(versions["current"])
