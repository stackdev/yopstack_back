
# README #

### What is this repository for? ###

* prototype for a REST API using Spring and MongoDb 


### pre-requisite ###
* use yopstack_config and start the mongo service from the docker compose

### how to ###

* compile:
> ./gradlew build
 
* Run unit test
> ./gradlew test

* Run e2e tests
> start the application in intellij: alt+8, ctrl + shift+f10  
> ./gradlew e2etest 

* Dependencies
> spring 3.? ... 

* Database configuration
> mongod needs to be running..

* Deployment instructions
> ./gradlew -Penv=prod build //will build a war with production properties, with no option dev properties will be used


### Branchs usage ###
*master:  validated dev
*jenkins_build: used to deploy to jenkins

